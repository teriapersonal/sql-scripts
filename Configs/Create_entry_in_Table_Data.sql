USE [IEMQS]
/*
SELECT TOP (1000) [Id]
      ,[Name]
      ,[Description]
      ,[Responsible]
      ,[HaveScheduler]
      ,[LNSource]
      ,[Interval]
      ,[IsBackEndMaster]
      ,[UsedAsForeignKeyTable]
  FROM [IEMQS].[dbo].[Table_Data] where name like '%PCR%' 
*/
  IF NOT EXISTS(SELECT 0 FROM [dbo].[Table_Data] AS P WHERE P.[Name] = 'PCR506')
   BEGIN
	  INSERT INTO [IEMQS].[dbo].[Table_Data]
	  (
		[Name], [Description], [Responsible], [HaveScheduler], [LNSource], [Interval], [IsBackEndMaster], [UsedAsForeignKeyTable]
	  )
   
   VALUES('PCR506', 'PCR Lines', 'Functional', 0, NULL, NULL, 0, 0)
  END

  IF NOT EXISTS(SELECT 0 FROM [dbo].[Table_Data] AS P WHERE P.[Name] = 'PCR501')
   BEGIN

	  INSERT INTO [IEMQS].[dbo].[Table_Data]
	  (
		[Name], [Description], [Responsible], [HaveScheduler], [LNSource], [Interval], [IsBackEndMaster], [UsedAsForeignKeyTable]
	  )
	  VALUES('PCR501', 'Plate Cutting Request Header', 'Functional', 0, NULL, NULL, 0, 0)
  
  END

  IF NOT EXISTS(SELECT 0 FROM [dbo].[Table_Data] AS P WHERE P.[Name] = 'PCR503')
   BEGIN
	  INSERT INTO [IEMQS].[dbo].[Table_Data]
	  (
		[Name], [Description], [Responsible], [HaveScheduler], [LNSource], [Interval], [IsBackEndMaster], [UsedAsForeignKeyTable]
	  )
	  VALUES('PCR503', 'Plate Cutting Request Lines', 'Functional', 0, NULL, NULL, 0, 0)
   END

  IF NOT EXISTS(SELECT 0 FROM [dbo].[Table_Data] AS P WHERE P.[Name] = 'PCR504')
   BEGIN
	  INSERT INTO [IEMQS].[dbo].[Table_Data]
	  (
		[Name], [Description], [Responsible], [HaveScheduler], [LNSource], [Interval], [IsBackEndMaster], [UsedAsForeignKeyTable]
	  )
	  VALUES('PCR504', 'PCL Header', 'Functional', 0, NULL, NULL, 0, 0)
   END

   IF NOT EXISTS(SELECT 0 FROM [dbo].[Table_Data] AS P WHERE P.[Name] = 'PCR505')
    BEGIN
	  INSERT INTO [IEMQS].[dbo].[Table_Data]
	  (
		[Name], [Description], [Responsible], [HaveScheduler], [LNSource], [Interval], [IsBackEndMaster], [UsedAsForeignKeyTable]
	  )
	  VALUES('PCR505', 'Dimenson for Return Balance - PCR ', 'Functional', 0, NULL, NULL, 0, 0)

    END
	
	IF NOT EXISTS(SELECT 0 FROM [dbo].[Table_Data] AS P WHERE P.[Name] = 'PCR500')
    BEGIN
	  INSERT INTO [IEMQS].[dbo].[Table_Data]
	  (
		[Name], [Description], [Responsible], [HaveScheduler], [LNSource], [Interval], [IsBackEndMaster], [UsedAsForeignKeyTable]
	  )
	  VALUES('PCR500', 'Stock Master', 'Functional', 0, NULL, NULL, 0, 0)

    END

   IF NOT EXISTS(SELECT 0 FROM [dbo].[Table_Data] AS P WHERE P.[Name] = 'PCR000')
    BEGIN
	  INSERT INTO [IEMQS].[dbo].[Table_Data]
	  (
		[Name], [Description], [Responsible], [HaveScheduler], [LNSource], [Interval], [IsBackEndMaster], [UsedAsForeignKeyTable]
	  )
	  VALUES('PCR000', 'PCR Related Enum Description', 'Functional', 0, NULL, NULL, 0, 0)

    END
