USE [IEMQS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Generate_New_Stock_Revision]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Generate_New_Stock_Revision]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
--exec [dbo].[SP_PCR_Generate_New_Stock_Revision] 1060 , '122963'
--select Id, t_stat, t_orno, t_pcln, t_revn , t_rbal from pcr504
ALTER PROCEDURE [dbo].[SP_PCR_Generate_New_Stock_Revision]
    @Id INT,
	@PSNo NVARCHAR(20)
AS
 BEGIN
 BEGIN TRAN
  BEGIN TRY
   SET NOCOUNT ON
   DECLARE @A AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(1))
   DECLARE @T AS TABLE(RowIndex INT IDENTITY(1,1), [Length] FLOAT, [Width] FLOAT, Area FLOAT, [OddShape] INT)
   DECLARE @RowIndex INT
   DECLARE @PId INT
   DECLARE @Total INT
   DECLARE @Length FLOAT
   DECLARE @Width FLOAT
   DECLARE @Area500 FLOAT
   DECLARE @Area505 FLOAT
   DECLARE @Thickness FLOAT
   DECLARE @CNVF FLOAT
   DECLARE @CNVB FLOAT
   DECLARE @CDT1 FLOAT
   DECLARE @CCD1 FLOAT
   DECLARE @CDT2 FLOAT
   DECLARE @CCD2 FLOAT
   DECLARE @PCLN NVARCHAR(100)
   DECLARE @Revision INT
   DECLARE @CurrentStockNumber NVARCHAR(100)
   DECLARE @CurrentStockRevision INT
   DECLARE @PrevStockNumber NVARCHAR(100)
   DECLARE @PrevStockRevision INT
   DECLARE @StockNumber NVARCHAR(100)
   DECLARE @StockRevision INT
   DECLARE @Status INT
   DECLARE @Quantity FLOAT
   DECLARE @OddShape INT
   DECLARE @TWGT FLOAT
   DECLARE @Item NVARCHAR(100)
   DECLARE @RBAL INT
   DECLARE @SR INT
   DECLARE @Alpha NVARCHAR(1)
   INSERT INTO @A([Value])
   VALUES
	('A'), ('B'), ('C'), ('D'), ('E'), ('F'), ('G'), ('H'), ('I'), ('J'), ('K'), ('L'), ('M'), 
	('N'), ('O'), ('P'), ('Q'), ('R'), ('S'), ('T'), ('U'), ('V'), ('W'), ('X'), ('Y'), ('Z')
	SELECT
	 @PCLN = P.[t_pcln],
	 @Revision = P.[t_revn],
	 @RBAL = P.[t_rbal]
	FROM 
	 [dbo].[PCR504] AS P
	WHERE 
	 P.[Id] = @Id

	SELECT
	 TOP 1
	  @CurrentStockNumber = P0.[t_stkn] ,
	  @CurrentStockRevision = P0.[t_stkr],
	  @Item = P0.[t_item],
	  @Area500 = P0.[t_area],
	  @CNVF = P0.[t_cnvf],
	  @Thickness = P0.[t_thic],
	  @CDT1= P0.[t_cdt1],
	  @CDT2 = P0.[t_cdt2],
	  @CCD1 = P0.[t_ccd1],
	  @CCD2 = P0.[t_ccd2],
	  @CNVB = P0.[t_cnvb],
	  @PId = P0.[Id]
	FROM
	 [dbo].[PCR500] AS P0
	WHERE 
	 P0.[t_pcln] = @PCLN
	 
	SELECT 
	   TOP 1
	   @StockRevision = P0.[t_stkr]
	FROM 
	  [dbo].[PCR500] AS P0
	 WHERE 
	  LTRIM(RTRIM(P0.[t_item])) = LTRIM(RTRIM(@Item))
	    AND 
	  P0.[t_stkn] = @CurrentStockNumber
	 ORDER BY P0.[t_stkn] DESC

	SELECT @StockRevision += 1
	SET @SR = @StockRevision
	
	INSERT INTO
	 @T
	  (
	    [Length],
		[Width],
		[Area],
		[OddShape]
      )
	 SELECT
	   P.[t_leng],
	   P.[t_widt],
	   P.[t_area],
	   P.[t_odds]
	 FROM
	   [dbo].[PCR505] AS P
	 WHERE
	    P.[t_pcln] = @PCLN 
		 AND
        P.[t_revn] = @Revision
    
   SELECT @RowIndex = 1
   SELECT @Total = COUNT(0) FROM @T
	
   IF(@RBAL = 1)
	 BEGIN
	   SET @PrevStockNumber = @CurrentStockNumber
	   SET @PrevStockRevision = @CurrentStockRevision
	 END
	
	SELECT
	    	TOP 1 @Status = [Value] 
	 FROM 
			[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('Stock Status','ltsfc.stk.stat') 
	 WHERE 
	        UPPER([LNConstant]) = 'CREATED'
	 
	WHILE(@RowIndex <= @Total)
	 BEGIN
	  SELECT 
	    @Area505 = T.[Area] ,
	    @Length = T.[Length] ,
	    @Width = T.[Width],
		@OddShape = T.[OddShape]
	  FROM
	    @T AS T
	  WHERE 
		T.[RowIndex] = @RowIndex
	   IF(@RBAL = 1)
	    BEGIN
		  SET @StockNumber = @CurrentStockNumber
		  SET @StockRevision = @SR
		  SET @SR += 1
	    END
	   ELSE
		BEGIN
		  SELECT TOP  1
			@Alpha = A.[Value] 
		  FROM 
			@A AS A 
		  WHERE 
			A.[RowIndex] = @RowIndex
		  SET @StockRevision = @CurrentStockRevision
		  SET @StockNumber = CONCAT(@CurrentStockNumber, @Alpha)
		END
		
		SET @Quantity = (@Area505/1000) * ((@Thickness * @CNVF) + ((@CDT1 * @CCD1) + (@CDT2 * @CCD2)))		
		SET @TWGT = ((@Area505) * ((@CNVB * @Thickness) + (@CCD1 * @CDT1) + (@CCD2 * @CDT2)))/1000
		INSERT INTO 
			[dbo].[PCR500]
			(
				[t_pstk], [t_prev], [t_stkn], [t_stkr],
				[t_leng], [t_widt],	[t_area], [t_arcs],
				[t_pcln], [t_qall],	[t_qfre], [t_qnty],
				[t_stat], [t_twgt],	[t_odds],
				[t_item], [t_loca], [t_arms], [t_mtrl],
				[t_qres], [t_qcut], [t_orno], [t_pono],
				[t_rcno], [t_rcln], [t_shtp], [t_shfl],
				[t_size], [t_cwar], [t_wloc], [t_cnvf],
				[t_htno], [t_tcno], [t_gorn], [t_mchn], 
				[t_armv], [t_qapr], [t_cono], [t_cdt1], 
				[t_cdt2], [t_cnvb], [t_ccd1], [t_ccd2], 
				[t_psta], [t_clot], [t_idsf], [t_cprj],
				[t_cspa], [t_rmrk], [t_cwoc], [t_amod],
				[t_rorn], [t_rrst], [t_srmk], [t_sono],
				[t_runn], [t_cons], [t_sprj], [t_okey],
				[t_sspa], [t_sact], [t_scrn], [t_shpm],
				[t_rqno], [t_ncra], [t_ncrn], [t_ncrr], 
				[t_rsfp], [t_rsby], [t_rsdt], [t_ityp], 
				[t_remk], [t_Refcntd], [t_Refcntu]
			 )		
			
			 SELECT 
			    @PrevStockNumber, @PrevStockRevision, @StockNumber, @StockRevision,
			    @Length, @Width, @Area505, 0, 
				NULL, 0, @Area500, @Quantity, 
				@Status, @TWGT, @OddShape,
				P0.[t_item], P0.[t_loca], P0.[t_arms], P0.[t_mtrl],
				P0.[t_qres], P0.[t_qcut], P0.[t_orno], P0.[t_pono],
				P0.[t_rcno], P0.[t_rcln], P0.[t_shtp], P0.[t_shfl],
				P0.[t_size], P0.[t_cwar], P0.[t_wloc], P0.[t_cnvf],
				P0.[t_htno], P0.[t_tcno], P0.[t_gorn], P0.[t_mchn], 
				P0.[t_armv], P0.[t_qapr], P0.[t_cono], P0.[t_cdt1], 
				P0.[t_cdt2], P0.[t_cnvb], P0.[t_ccd1], P0.[t_ccd2], 
				P0.[t_psta], P0.[t_clot], P0.[t_idsf], P0.[t_cprj],
				P0.[t_cspa], P0.[t_rmrk], P0.[t_cwoc], P0.[t_amod],
				P0.[t_rorn], P0.[t_rrst], P0.[t_srmk], P0.[t_sono],
				P0.[t_runn], P0.[t_cons], P0.[t_sprj], P0.[t_okey],
				P0.[t_sspa], P0.[t_sact], P0.[t_scrn], P0.[t_shpm],
				P0.[t_rqno], P0.[t_ncra], P0.[t_ncrn], P0.[t_ncrr], 
				P0.[t_rsfp], P0.[t_rsby], P0.[t_rsdt], P0.[t_ityp], 
				P0.[t_remk], P0.[t_Refcntd], [t_Refcntu]				
			  FROM 
				 [dbo].[PCR500] AS P0
               WHERE 
				  P0.[Id] = @PId
	    SET @RowIndex += 1	  
	 END
	 COMMIT TRAN							 
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(), 
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END