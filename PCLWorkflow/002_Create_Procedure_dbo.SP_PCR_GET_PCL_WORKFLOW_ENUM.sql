USE [IEMQS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_ENUM_LIST]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_ENUM_LIST]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
--exec [dbo].[SP_PCR_GET_ENUM_LIST] '122963'
ALTER PROCEDURE [dbo].[SP_PCR_GET_ENUM_LIST]
    @PSNo NVARCHAR(MAX)
AS
BEGIN
	SELECT 
	  DISTINCT 
		ProcessesTable.[Process] AS Code, 
		ProcessesTable.[Description] AS [Description], 
		ProcessesTable.[Process] AS CodeDescription, 
		ProcessesTable.[OrderNo]
	FROM [dbo].[ATH001] AS U
		INNER JOIN [dbo].[ATH004] AS RolesTable ON RolesTable.[Id] = U.[Role]
		INNER JOIN [dbo].[ATH003] AS RoleProcessesTable ON RolesTable.[Id] = RoleProcessesTable.[RoleId]	
		INNER JOIN [dbo].[ATH002] AS ProcessesTable ON RoleProcessesTable.[ProcessId] = ProcessesTable.[Id]
	WHERE 
	    U.[Employee] = @PSNo 
		  AND 
	    ProcessesTable.[Area] = 'PCR' 
		  AND 
	    ProcessesTable.[IsActive] = 1 
		  AND 
	    ProcessesTable.[Controller] = 'PCLWorkflow'
	      AND
	    RoleProcessesTable.[IsActive] = 1
	ORDER BY ProcessesTable.[OrderNo]
END

