USE [IEMQS]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Authenticate_Workflow_Status]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Authenticate_Workflow_Status]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Authenticate_Workflow_Status]
    @PSNo NVARCHAR(100),
	@ScreenName NVARCHAR(100)
AS
BEGIN
    
	DECLARE @Exists BIT 
	DECLARE @Count INT
    SELECT @Count = COUNT(0) 
	FROM [dbo].[ATH001] AS U
	INNER JOIN [dbo].[ATH004] AS RolesTable ON RolesTable.[Id] = U.[Role]
	INNER JOIN [dbo].[ATH003] AS RoleProcessesTable ON RolesTable.[Id] = RoleProcessesTable.[RoleId]	
	INNER JOIN [dbo].[ATH002] AS ProcessesTable ON RoleProcessesTable.[ProcessId] = ProcessesTable.[Id]
	WHERE U.[Employee] = @PSNo AND ProcessesTable.[Area] = 'PCR' AND ProcessesTable.[IsActive] = 1 AND 
	ProcessesTable.[Process]  = @ScreenName AND RoleProcessesTable.[IsActive] = 1
    RETURN (CASE WHEN @Count > 0 THEN 1 ELSE 0 END)
	
END

