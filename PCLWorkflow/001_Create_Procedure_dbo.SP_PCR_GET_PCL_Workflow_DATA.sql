USE [IEMQS]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_PCL_Workflow_DATA]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_PCL_Workflow_DATA]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
--exec [dbo].[SP_PCR_GET_PCL_Workflow_DATA] '122963', 'First Stage Clearance'
ALTER PROCEDURE [dbo].[SP_PCR_GET_PCL_Workflow_DATA]
    @PSNo NVARCHAR(100),
	@ScreenName NVARCHAR(100)
AS
BEGIN 
	DECLARE @IsAuthorized BIT
    EXECUTE @IsAuthorized =  [dbo].[SP_PCR_Authenticate_Workflow_Status] @PSNo, @ScreenName
    IF(@IsAuthorized = 0)
	 BEGIN
	   RAISERROR('You are not authorized to see data for this screen', 11, 1)
	 END
	
    DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');
	DECLARE @Collation NVARCHAR(MAX) = ' COLLATE Latin1_General_100_CS_AS_KS_WS';
	DECLARE @SqlQuery NVARCHAR(MAX);

	DECLARE @StatusPerScreen AS TABLE
	(
	  [t_cnst] NVARCHAR(100)	      
	)
	DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	IF(@ScreenName = 'First Stage Clearance') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('conf.pmg')
	 END
	 IF(@ScreenName = 'Confirm Plate Cutting Request') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('appr.qa')
	 END
	 IF(@ScreenName = 'Receive Plate') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('plt.issued'),
	  ('plt.recv'),
	  ('rej.qc')
	 END
	 IF(@ScreenName = 'Process Plate Cutting Request') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('apprv.qc'),
	  ('plt.cut')
	 END
	 IF(@ScreenName = 'Second Stage Inspection') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('plt.mark'),
	  ('plt.cut')
	 END
	 
	
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')

	DECLARE @tblWCLocation TABLE
	(
		[t_cadr] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_cwoc] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[WCDeliverName] NVARCHAR(MAX)
	)
	INSERT INTO @tblWCLocation([t_cadr], [t_cwoc], [t_dsca], [WCDeliverName])
	EXEC [dbo].[SP_PCR_Get_WorkCenter_With_Location] 1 

	DECLARE @tblLocationAddress AS TABLE 
	(
		[t_pcrn] NVARCHAR(100), 
		[t_loca] NVARCHAR(100),
		[t_ladr] NVARCHAR(100)
	)
	SET @SqlQuery = 
	' SELECT  
		A.[t_pcrn] , 
		A.[t_loca],
		C.[t_ladr] 
	    FROM ' + @lnlinkedserver + '.dbo.tltlnt000'+ @lncompanyid  + ' AS C 
	    INNER JOIN PCR501 AS A 
	      ON A.[t_loca] = C.[t_dimx]'

    INSERT INTO @tblLocationAddress
	(
		[t_pcrn],
		[t_loca],
		[t_ladr]
	)
	EXEC(@SqlQuery)
	
    SELECT
	    DISTINCT
		P.[Id] AS Id,
		P.[t_pcln] AS PCLNumber,
		P.[t_revn] AS Revision,
		P.[t_mvtp] AS MovementType,
		PS.[t_txt] AS PCLStatus,
		P3.[t_prdt] AS PCRRequirementDate,
		P3.[t_pcrn] AS PCRNumber,
		WC.WCDeliverName AS WCDeliverName,
		NULL AS StockPhysical,
		P0.[t_stkn] AS StockNumber,
		P0.[t_area] AS TotalArea,
		P.[t_rbal] AS ReturnBalance,
		P.[t_rtar] AS ReturnArea,
		NULL AS Project
		FROM [dbo].[PCR504] AS P
		INNER JOIN [dbo].[PCR500] AS P0 
		  ON P.[t_pcln] = P0.[t_pcln]
		INNER JOIN [dbo].[PCR503] AS P3 
		  ON P.[t_pcln] = P3.[t_pcln]
		INNER JOIN [dbo].[PCR501] AS P1
		  ON P3.[t_pcrn] = P1.[t_pcrn]
		LEFT OUTER JOIN @tblPCLStatus AS PS
		  ON P.[t_stat] = PS.[t_val]
		LEFT OUTER JOIN @tblLocationAddress AS LA
		  ON P3.[t_pcrn] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_pcrn] AND P1.[t_loca] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_loca]
		LEFT OUTER JOIN @tblWCLocation AS WC
		  ON P.[t_cwoc]  COLLATE Latin1_General_100_CS_AS_KS_WS = WC.[t_cwoc]
		AND WC.[t_cadr] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_ladr]
		WHERE
		 UPPER(PS.[t_cnst]) IN (SELECT UPPER(SS.[t_cnst]) FROM @StatusPerScreen AS SS)

END

