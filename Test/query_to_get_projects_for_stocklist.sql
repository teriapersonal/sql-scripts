
    DECLARE @StockStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
	DECLARE @StockPhysicalStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)			
	INSERT INTO @StockPhysicalStatusTable([LNConstant], [Text], [Value])
	SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums 
	WHERE UPPER(Enums.[EnumName]) = UPPER('Stock Physical Status')
		AND	UPPER(Enums.[LNConstant]) IN (UPPER('virtual'), UPPER('at.store'), UPPER('at.pps'), UPPER('ret.pps'))

INSERT INTO @StockStatusTable([LNConstant], [Text], [Value])
SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums WHERE 
UPPER(Enums.[EnumName]) = UPPER('Stock Status') AND UPPER(Enums.[LNConstant]) <> UPPER('delete')  
DECLARE @I INT = 1 
    SELECT		        
		TOP 5000
		P.[Id] AS Id, 
		P.[t_cono] AS [Contract],
		A.[t_cprj] AS [Project],
		LTRIM(RTRIM(P.[t_item])) AS ChildItem,
		LTRIM(RTRIM(P.[t_stkn])) AS StockNumber, 
		P.[t_stkr] AS StockRevision, 
		P.[t_area] AS TotalArea, 
		P.[t_qall] AS AreaAllocated, 
		P.[t_qfre] As FreeArea,
		1 AS UseAlternateStock
	FROM 
		[dbo].[PCR500] AS P 
	INNER JOIN [HZPDSQL2K12\IN02].[lnappqa3db].dbo.ttpctm110175 AS A 
	ON A.[t_cono] = P.[t_cono]
		--INNER JOIN
		--[dbo].[PCR503] AS P3 ON P3.[t_cprj] = A.[t_cprj] 
	WHERE 
		P.[t_loca] = 'PEW' 			   
		--AND 
		--P3.[Id] = @I
		AND
		P.[t_stat] IN 
		(
		SELECT 
			SS.[Value] 
		FROM @StockStatusTable AS SS
		)
		AND
		P.[t_psta] IN 
		(
		SELECT 
			SPS.[Value] 
		FROM @StockPhysicalStatusTable AS SPS WHERE UPPER(SPS.LNConstant) <> UPPER('VIRTUAL')
		) 

		 Order by [Contract], [Project], [ChildItem]