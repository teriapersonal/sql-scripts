     IF NOT EXISTS(SELECT 0 FROM PCR000 WHERE EnumName='Work Authorization Status')
		BEGIN
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Work Authorization Status', 'tpptc100','tppdm.wast','free','Free',1)


			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Work Authorization Status', 'tpptc100','tppdm.wast','onhold','On Hold',2)


			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Work Authorization Status', 'tpptc100','tppdm.wast','released','Released',3)


			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Work Authorization Status', 'tpptc100','tppdm.wast','finished','Finished',4)

			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Work Authorization Status', 'tpptc100','tppdm.wast','closed','Closed',5)

		END

		IF NOT EXISTS(SELECT 0 FROM PCR000 WHERE EnumName='Status Technical Calculation')
		 BEGIN		    

			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Status Technical Calculation', 'tpptc100','tpptc.stat','free','Free',1)
		
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Status Technical Calculation', 'tpptc100','tpptc.stat','actual','Actual',2)
		
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Status Technical Calculation', 'tpptc100','tpptc.stat','definite','Final',3)
         END
	    IF NOT EXISTS(SELECT 0 FROM PCR000 WHERE EnumName='Yes No Choice')
		 BEGIN	
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Yes No Choice', '','tppdm.yeno','yes','YES',1)
		
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Yes No Choice', '','tppdm.yeno','no','NO',2)
		 END

		 IF NOT EXISTS(SELECT 0 FROM PCR000 WHERE EnumName='Project Status')
		   BEGIN
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Project Status', '','tppdm.psts','free','Free',1)
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Project Status', '','tppdm.psts','construction','Active',2)
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Project Status', '','tppdm.psts','finished','Finished',3)
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Project Status', '','tppdm.psts','closed','Closed',4)
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Project Status', '','tppdm.psts','archived','Archived',5)
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Project Status', '','tppdm.psts','deleted','Deleted',6)

			END

			
     IF NOT EXISTS(SELECT 0 FROM PCR000 WHERE EnumName='Project Type')
		   BEGIN
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Project Type', '','tppdm.kopr','main','Main Project',1)
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Project Type', '','tppdm.kopr','sub','Subproject',2)
			insert into PCR000(EnumName,LNTableName,LNDomainName,LNConstant,[Text],[Value])
			values('Project Type', '','tppdm.kopr','single','Single project',3)
			
			END
