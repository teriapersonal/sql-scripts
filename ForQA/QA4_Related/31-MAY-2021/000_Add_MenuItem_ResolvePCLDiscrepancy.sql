USE [IEMQS_QA]
--USE [IEMQS]
GO

BEGIN
DECLARE @Area NVARCHAR(3) = 'PCR'
DECLARE @UserID NVARCHAR(9) = '122963'
DECLARE @Process NVARCHAR(MAX) = 'Resolve PCL Discrepancy'
DECLARE @ControllerName NVARCHAR(100) = 'PCLWorkflow'
DECLARE @ActionName NVARCHAR(100) = 'ResolvePCLDiscrepancy'
DECLARE @ParentID INT
DECLARE @RoleID INT
DECLARE @ProcessID INT
DECLARE @HighestOrder INT = 0
SELECT TOP 1 @ParentID = Id FROM [dbo].[ATH002] WHERE [Area] = @Area and [ParentId] IS NULL
SELECT @HighestOrder = MAX([OrderNo])  FROM [dbo].[ATH002] WHERE [Area] = @Area
IF NOT EXISTS(SELECT 0 FROM [dbo].[ATH002] WHERE [Area] = @Area AND [ParentId] = @ParentID AND [Process] = @Process)
 BEGIN
  INSERT INTO [dbo].[ATH002] ([Process], [Description], [Area], [Controller], [Action], [ParentId], [MainMenuOrderNo], 
  [OrderNo], [IsDisplayInMenu], [IconClass], [IsActive], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [Help], [FAQ], [IsMobileMenu])
   VALUES (@Process, @Process, @Area, @ControllerName, @ActionName, @ParentID, NULL, @HighestOrder + 1, 1, NULL, 
   1, @UserID, GETUTCDATE(), NULL, NULL, NULL, NULL, NULL)
 END
 ELSE
  BEGIN
    IF NOT EXISTS(SELECT 0 FROM [dbo].[ATH002] WHERE [Area] = @Area 
	AND [ParentId] = @ParentID AND [Process] = @Process AND [Controller] = @ControllerName AND [Action] = @ActionName)
	 BEGIN
	   UPDATE [dbo].[ATH002]
	    SET 
		[Controller] = @ControllerName, 
		[Action] = @ActionName,
		[OrderNo] = @HighestOrder + 1,
		[IsActive] = 1
		WHERE 
		[Area] = @Area
		AND 
		[ParentId] = @ParentID 
		AND 
		[Process] = @Process
	 END
  END
 SELECT @ProcessID = Id FROM [dbo].[ATH002] WHERE [Area] = @Area AND  [Process] = @Process
 DECLARE @T AS TABLE(RoleId INT, RowNumber INT IDENTITY(1,1) PRIMARY KEY)
 DECLARE @Counter INT
 DECLARE @Total INT
 INSERT INTO @T(RoleId)
 SELECT A.[Id] FROM [dbo].[ATH004] AS A WHERE A.[Role] IN ('MATL3')--'IT1', 'IT2', 'IT3', 'MATL3', 'ITA1')
 SELECT @Total = COUNT(0) FROM @T
 SELECT @Counter = 1
 WHILE(@Counter <= @Total)
  BEGIN
    SELECT @RoleID = T.[RoleId] FROM @T AS T WHERE T.[RowNumber] = @Counter
	IF NOT EXISTS (SELECT 0 FROM  [dbo].[ATH003] AS A WHERE A.[RoleId] = @RoleID AND A.[ProcessId] = @ProcessID)
	 BEGIN
	   INSERT INTO [dbo].[ATH003]([ProcessId], [RoleId], [IsActive], [IsMobileMenu], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn])
	   VALUES(@ProcessID, @RoleID, 1, 0, @UserID, GETUTCDATE(), NULL, NULL)
	   --PRINT @Counter
	 END
	SET @Counter += 1
  END
END

