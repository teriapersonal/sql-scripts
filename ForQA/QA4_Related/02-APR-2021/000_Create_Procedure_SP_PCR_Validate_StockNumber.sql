--USE [IEMQS]
USE [IEMQS_QA]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_StockNumber]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_StockNumber]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_StockNumber]
   @PSNo NVARCHAR(100),
   @Id INT,   
   @StockNumber NVARCHAR(100),
   @StockRevision INT,
   @UseAlternateStock INT
AS
BEGIN 
	SET NOCOUNT ON
	 BEGIN TRY
	    DECLARE @Location NVARCHAR(100)
		DECLARE @Contract NVARCHAR(100)
	    DECLARE @Project NVARCHAR(100) 
		DECLARE @ChildItem NVARCHAR(100)
		DECLARE @PartNumber INT
		DECLARE @Element NVARCHAR(100)
		DECLARE @PCRN NVARCHAR(100)
		DECLARE @PONO INT, @Revision INT
		DECLARE @PCRCreatedDate DATETIME
		DECLARE @PrevPCRN NVARCHAR(100)
		DECLARE @PrevPONO INT
		DECLARE @PrevRevision INT
		DECLARE @Message NVARCHAR(MAX)
		DECLARE @YesValue INT
		DECLARE @NoValue INT
		DECLARE @InvalidMessage NVARCHAR(MAX) = 'Specified Stock number is invalid. Enter another Stock number'
		DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	    DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');
		DECLARE @Sql NVARCHAR(MAX)		 
		 
	 
		SELECT
			TOP 1 @YesValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		WHERE UPPER([LNConstant]) = 'YES'

		SELECT
			TOP 1 @NoValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		WHERE UPPER([LNConstant]) = 'NO'
		
		
		
		SELECT
			 @Project = P.[t_cprj],
			 @Element = P.[t_cspa],
			 @ChildItem = LTRIM(RTRIM(P.[t_sitm])),
			 @PartNumber = P.[t_prtn],			 
			 @PCRN = P.[t_pcrn],
			 @PONO = P.[t_pono],
			 @Revision = P.[t_revn],
			 @PCRCreatedDate = P.[t_pcr_crdt]
		 FROM 
			[dbo].[PCR503] AS P
		 WHERE 
			P.[Id] = @Id

		SELECT 
			TOP 1 @Location = P.[t_loca]
		FROM 
			[dbo].[PCR501] AS P
		WHERE 
			P.[t_pcrn] = @PCRN

		IF(@StockNumber IS NULL OR LEN(@StockNumber) = 0)
		  BEGIN
		   RAISERROR('Please select stock number', 12, 1);
		  END
		  IF EXISTS(
			SELECT 
				0 
			FROM 
				[dbo].[PCR503] AS P
			WHERE 
				P.[t_stkn] = @StockNumber 
					AND 
				P.[t_stkr] = @StockRevision 
					AND 
				P.[t_lsta] <> 10
				    AND
				P.[Id] <> @Id
				)
				BEGIN
					RAISERROR('The selected stock number and revision are used by some other PCL', 12, 1)
				END
		 IF EXISTS
		 (
			SELECT 
				0
			FROM 
				[dbo].[PCR500] AS P 
			WHERE 
				P.[t_item] = @ChildItem 
					AND 
				P.[t_stkn] = @StockNumber 
					AND 
				P.[t_stkr] = @StockRevision
					AND 
				P.[t_cwar] LIKE '%SC%'
		  )
		   BEGIN
		    RAISERROR('Selected Stock Number has subcontracting warehouse. Enter another stock Number', 12,1)
		   END
		 DECLARE @StatusTable AS TABLE([LNConstant] NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
		
		 INSERT INTO 
				@StatusTable
				(
					[LNConstant], 
					[Text], 
					[Value]
				)
			SELECT 
				Enums.[LNConstant], 
				Enums.[Text], 
				Enums.[Value] 
			FROM 
				[dbo].[PCR000] AS Enums 
			WHERE 
				UPPER(Enums.[EnumName]) = UPPER('PCR Line Status')
				AND
				UPPER(Enums.[LNConstant]) IN 
				(
					UPPER('created'), 
					UPPER('rel.mpln'), 
					UPPER('rel.nest')					
				)

		 SELECT 
		   @PrevPCRN = P.[t_pcrn], 
		   @PrevPONO = P.[t_pono] ,
		   @PrevRevision = P.[t_revn]
		 FROM 
		 [dbo].[PCR503] AS P
		 
		 WHERE 
		   P.[t_cprj] = @Project 
			AND 
		   P.[t_cspa] = @Element
			AND 
		   P.[t_prtn] = @PartNumber 
		    AND 
		   LTRIM(RTRIM(P.[t_sitm])) = @ChildItem
			AND  
		   P.[t_lsta] IN (SELECT S.[Value] FROM @StatusTable AS S)
			AND 
			(P.[t_pcrn] <> @PCRN OR P.[t_pono] <> @PONO OR P.[t_revn] <> @Revision)
			AND 
		  (P.[t_stkn] IS NULL OR LEN(P.[t_stkn]) = 0)
			AND 
		   P.[t_pcr_crdt] < @PCRCreatedDate
		    
		 IF (@PrevPCRN IS NOT NULL AND @PrevPONO IS NOT NULL AND @PrevRevision IS  NOT NULL)
  		  BEGIN
		    IF(@PrevPCRN <> @PCRN OR @PrevPONO <> @PONO OR @PrevRevision <> @Revision)
		      BEGIN
		        SET @Message = CONCAT('Please Proceed previous PCR Number: ' , @PCRN , 
			    ', PCR Line: ', @PrevPONO, ' and Revision: ', @PrevRevision)
		        RAISERROR(@Message, 12, 1)
			  END
		  END
		  IF(@UseAlternateStock = @NoValue) -- No
		   BEGIN
		     IF NOT EXISTS(
			 SELECT 0 
			 FROM [dbo].[PCR500] AS P0 
			 WHERE
				P0.[t_loca] = @Location 
					AND 
				LTRIM(RTRIM(P0.[t_item])) = @ChildItem 
					AND 
				P0.[t_stkn] = @StockNumber 
					AND 
				P0.[t_stkr] = @StockRevision)
			  BEGIN
				 --RAISERROR(@InvalidMessage, 12, 1)
				 PRINT 1
			  END
			 ELSE
			    BEGIN
			     EXEC [dbo].[SP_PCR_ValidateInner_StockNumber]
				   @PSNo = @PSNo,
				   @Id = @Id,
				   @StockNumber = @StockNumber,
				   @StockRevision = @StockRevision,
				   @UseAlternateStock = @UseAlternateStock,
				   @Location = @Location,
				   @Project = @Project, 
				   @ChildItem = @ChildItem,
				   @PartNumber = @PartNumber,
				   @Element = @Element,
				   @PCRN = @PCRN,
				   @PONO = @PONO, 
				   @Revision = @Revision     
			   END
		    END
		  ELSE
		    BEGIN
		      IF NOT EXISTS(
				SELECT 0 
				FROM [dbo].[PCR500] AS P0 
				WHERE
			     P0.[t_stkn] = @StockNumber 
					AND
				 P0.[t_stkr] = @StockRevision 
					AND
				 P0.[t_loca] = @Location
				 )
	   		     BEGIN
	 			   --RAISERROR(@InvalidMessage, 12, 1)
				   PRINT 1
			     END
              ELSE
			   BEGIN
			     EXEC [dbo].[SP_PCR_ValidateInner_StockNumber]
				   @PSNo = @PSNo,
				   @Id = @Id,
				   @StockNumber = @StockNumber,
				   @StockRevision = @StockRevision,
				   @UseAlternateStock = @UseAlternateStock,
				   @Location = @Location,
				   @Project = @Project, 
				   @ChildItem = @ChildItem,
				   @PartNumber = @PartNumber,
				   @Element = @Element,
				   @PCRN = @PCRN,
				   @PONO = @PONO, 
				   @Revision = @Revision
				 
				 DECLARE @OtherItem NVARCHAR(100)
				 DECLARE @CItem NVARCHAR(100)
				 DECLARE @OtherItemMatGrade NVARCHAR(100)
				 DECLARE @CItemMatGrade NVARCHAR(100)				   
				 DECLARE @T AS TABLE([Value] NVARCHAR(1000))
				 SET @OtherItem = @ChildItem
				 SELECT 
					TOP 1 @CItem = LTRIM(RTRIM(P.[t_item]))
					FROM [dbo].[PCR500] AS P 
					WHERE 
						P.[t_stkn] = @StockNumber 
							AND 
						P.[t_stkr] = @StockRevision
				 SET @Sql = ' SELECT A.[t_dscb] FROM ' + @LNLinkedServer + 
				 '.dbo.ttcibd001' + @LNCompanyId + ' AS A WHERE LTRIM(RTRIM(A.[t_item]))= ''' + @CItem + '''' 
				 
				 INSERT INTO @T([Value])
				 EXEC(@Sql)
				 
				 SELECT 
					TOP 1 @CItemMatGrade = T.[Value] 
				 FROM @T AS T
				 
				 DELETE FROM @T
				 
				 SET @Sql = ' SELECT A.[t_dscb] FROM ' + @LNLinkedServer + 
				 '.dbo.ttcibd001' + @LNCompanyId + ' AS A WHERE LTRIM(RTRIM(A.[t_item]))= ''' + @OtherItem + '''' 
				 
				 INSERT INTO @T([Value])
				 EXEC(@Sql)
				 
				 SELECT 
					TOP 1 @OtherItemMatGrade = T.[Value] 
				  FROM @T AS T
				 
				 IF(@OtherItemMatGrade <> @CItemMatGrade)
				   BEGIN
					 RAISERROR('The material grade is different. Do you want to continue ?', 11, 1)
				   END				
			     
			   END
			END

			DECLARE @RSBY NVARCHAR(100)
		    DECLARE @RSFP INT				   
		    SELECT 
				@RSBY = P0.[t_rsby], 
				@RSFP = P0.[t_rsfp] 
			FROM 
				[dbo].[PCR500] AS P0 
		    WHERE  
				LTRIM(RTRIM(P0.[t_item])) = @ChildItem
				    AND
				P0.[t_stkn] = @StockNumber 
					AND
				P0.[t_stkr] = @StockRevision 
				
			IF(@RSFP = @YesValue) --Yes
			  BEGIN
				SET @Message = CONCAT('Stock number is reserved by user ', @RSBY)
				RAISERROR(@Message, 12, 1)
			  END
		SELECT 1 AS [Status], NULL AS [ErrorMessage]
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		IF(@ErrorSeverity = 11) --> Warning for showing dialog box
		  BEGIN
		   SELECT 0 AS [Status], @ErrorMessage AS [MESSAGE]
		  END
		 ELSE --> 11 means error
		  BEGIN		    
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		  END 
	 END CATCH
END