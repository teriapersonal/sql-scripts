USE [IEMQS_QA]
--USE [IEMQS]
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Return_PCR]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Return_PCR]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Return_PCR]
   @Id INT,
   @PSNo NVARCHAR(100),
   @Remarks NVARCHAR(MAX)
AS
BEGIN 
    DECLARE @LineCount INT = 0 
    DECLARE @CurrentDate DATETIME
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)	
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @Pono INT
	DECLARE @Revn INT
	DECLARE @Init NVARCHAR(100)
	DECLARE @Sql NVARCHAR(MAX)
	DECLARE @FrequencyTable AS TABLE([Value] INT) 
	DECLARE @Frequency INT
	DECLARE @TotalConsumed INT	
	DECLARE @SerialNumber INT
	DECLARE @SequenceNumber INT
	DECLARE @Diff INT
	DECLARE @Id2 INT
	DECLARE @Tbl911 AS TABLE([Id] INT, [t_wid] FLOAT, [t_leng] FLOAT, [t_noun] FLOAT)
	DECLARE @PCLN NVARCHAR(100)
	DECLARE @CWOC NVARCHAR(100)
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)

	BEGIN TRAN
	 BEGIN TRY
	  SET @CurrentDate = GETUTCDATE()  
	  IF(@Remarks IS NULL)
	   BEGIN
	     RAISERROR('Return Remark is blank',11,1)
	   END
        SELECT 
		TOP 1 
		 @Init = P0.[t_init], 
		 @PCRN = P0.[t_pcrn] ,
		 @Pono = P.[t_pono],
		 @Revn = P.[t_revn]
		FROM 
		 [dbo].[PCR501] AS P0 
		   INNER JOIN 
	     [dbo].[PCR503] AS P ON P.[t_pcrn] = P0.[t_pcrn]  
		WHERE P.[Id] = @Id

		SELECT
		 TOP 1
		   @Project = P.[t_cprj],
		   @Element = P.[t_cspa],
		   @SerialNumber = P.[t_sern],
		   @SequenceNumber = P.[t_seqn],
		   @Id2 = P.[Id]
		 FROM 
			[dbo].[PCR506] AS P 
		 WHERE 
			P.[t_pcrn] = @PCRN AND P.[t_pono] = @PONO
		 SELECT @LineCount = COUNT(0) FROM [dbo].[PCR503] WHERE [t_pcrn] = @PCRN
		 SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer')
	     SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId')
		  SET @Sql =
						'DECLARE @Proj NVARCHAR(100) = ' + '''' + @Project + '''
						 DECLARE @Elem NVARCHAR(100) = ' + '''' + @Element + '''
						 EXEC [dbo].[SP_PCR_GET_FREQUENCY]  @Proj, @Elem, ''' + @LNLinkedServer + '''' + ',' + '''' + @LNCompanyId + ''''
					DELETE FROM @FrequencyTable
					INSERT INTO @FrequencyTable EXEC(@Sql)
					SELECT @Frequency = [Value] FROM @FrequencyTable	
					SELECT @TotalConsumed = SUM(P.[t_npcs_m])
					FROM [dbo].[PCR506] AS P WHERE 
					 P.[t_cprj] = @Project AND P.[t_cspa] = @Element AND P.[t_sern] = @SerialNumber
					  AND
					 P.[t_pcrn] IS NOT NULL AND P.[t_seqn] <>  @SequenceNumber
					
					  SET @Sql = ' SELECT TOP 1 ' 
					  SET @Sql = CONCAT(@Sql, @Id2)
					  SET @Sql = CONCAT(@Sql, ', tbl911.[t_leng], tbl911.[t_widt], tbl911.[t_noun]')
					  SET @Sql = CONCAT(@Sql, ' FROM ' + @LNLinkedServer + '.dbo.tltibd911' + @LNCompanyId + ' AS tbl911 WITH (NOLOCK) ')
					  SET @SQl = CONCAT(@Sql, 
					  ' WHERE tbl911.[t_cprj] = ''' + @Project + ''' AND tbl911.[t_cspa] = ''' +  @Element + ''' AND ')
					  SET @Sql = CONCAT(@Sql, 'CONVERT(NVARCHAR(100), tbl911.[t_sern]) = '''+ CONVERT(NVARCHAR(100), @SerialNumber) + '''')
					  INSERT INTO @Tbl911
					  EXEC(@Sql)

					  UPDATE P
					    SET 
						P.[t_leng] = T.[t_leng],
						P.[t_widt] = T.[t_wid],
						P.[t_npcs] = T.[t_noun] * @Frequency,
						P.[t_leng_m] = T.[t_leng],
						P.[t_widt_m] = T.[t_leng],
						P.[t_npcs_m] = CASE WHEN P.[t_npcs] > @TotalConsumed THEN   P.[t_npcs] - @TotalConsumed  ELSE 0 END,
						P.[t_pcrn] = NULL,
						P.[t_pono] = 0,
						P.[t_rtby] = @PSNo,
						P.[t_rtdt] = GETUTCDATE(),
						P.[t_rrmk] = @Remarks   
					  FROM [dbo].[PCR506] AS P
					  JOIN @Tbl911 AS T ON T.[Id] = P.[Id]
					  WHERE P.[t_pcrn] = @PCRN AND P.[t_pono] = @PONO

					  DELETE FROM [dbo].[PCR506] 
					  WHERE [t_cprj] = @Project 
					  AND [t_cspa] = @Element 
					  AND [t_sern] = @SerialNumber 
					  AND [t_seqn] <> @SequenceNumber 
					  AND [t_pcrn] IS NULL
					  
					  DELETE FROM [dbo].[PCR503] 
					  WHERE [t_pcrn] = @PCRN
					  AND [t_pono]= @PONO
					  AND [t_revn] = @Revn
					  
					  DELETE FROM [dbo].[PCR501] WHERE [t_pcrn] = @PCRN AND @LineCount <= 1

		 COMMIT TRAN						
	  SELECT 1
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END
