USE [IEMQS_QA]
--USE [IEMQS]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_Workflow_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_Workflow_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Update_Workflow_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT  = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @Id INT
	 DECLARE @StatusValue INT 
	 DECLARE @ApprovedStatusValue INT
	 DECLARE @TempTableName NVARCHAR(100) 
	 DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [ReleaseTo] NVARCHAR(100)		 
	 )
	    
	INSERT INTO @TBL
	(
	[Id], 
	[ReleaseTo]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('ReleaseTo').value('.', 'NVARCHAR(100)') AS [ReleaseTo]
	FROM 
		@Data.nodes('/ArrayOfPCL_WORKFLOW_Result/PCL_WORKFLOW_Result') AS Array(Entity)
   

    IF(@Mode = 1) --UPDATE
	 BEGIN
	  SELECT 
		TOP 1 @StatusValue = P.[t_stat] 
	  FROM 
		[dbo].[PCR504] AS P INNER JOIN @TBL AS T ON T.Id = P.[Id]
	 

	 SELECT 
		TOP 1 @ApprovedStatusValue = [Value] 
	 FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCL Status', 'ltpcl.stat')
	 WHERE 
		UPPER([LNConstant]) = UPPER('appr.qa')

	IF(@StatusValue <> @ApprovedStatusValue)
	 BEGIN
		 RAISERROR('Release To value cannot be applied to this screen', 12, 1)
	 END
	UPDATE tbl504
		SET 
		  tbl504.[t_rlto] = B.ReleaseTo
		FROM 
		[dbo].[PCR504] AS tbl504
		INNER JOIN @TBL AS B ON B.Id = tbl504.Id
		
	  COMMIT
	 END	
	  
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE() ,
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END 
