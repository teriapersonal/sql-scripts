
--USE [IEMQS_QA]
USE [IEMQS]
GO
--0
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]')) 
  BEGIN 
	PRINT 1
		EXEC ('
			CREATE FUNCTION [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]
				(
				  @pKey NVARCHAR(100) 
				)
				  RETURNS NVARCHAR(100)
					AS  
					BEGIN  
					 RETURN NULL
					END
			')
	END
GO
ALTER FUNCTION [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]  
(  
 @pKey NVARCHAR(100)  
)  
RETURNS NVARCHAR(max)  
AS  
BEGIN  
   
 DECLARE @ReturnValue NVARCHAR(max)  
 IF(@pKey = 'LNLinkedServer')
  BEGIN
    SELECT @ReturnValue = '[PhzpdSqldb2k12].[Lnappqa4db]'
  END
 ELSE
 BEGIN
  SELECT @ReturnValue = C.[Value] FROM [dbo].[Config] AS C WHERE C.[Key] = @pKey  
 END
  
 RETURN @ReturnValue;  
  
END
GO
--1
------------------------
 IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_WorkCenter]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_WorkCenter]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_WorkCenter]
   @PCRN NVARCHAR(100),
   @WCDeliver NVARCHAR(100)
AS
BEGIN 
   BEGIN TRY
	SET NOCOUNT ON
	DECLARE @Message NVARCHAR(MAX)
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @LNLinkedServer NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @Result AS TABLE([Count] INT)
	DECLARE @Count INT
	SET @SqlQuery = '	SELECT  
	                        COUNT(0) AS CNT
					   FROM [dbo].[COM002] AS C
					   INNER JOIN '+ @LNLinkedServer +'.[dbo].[tltlnt000'+ @LNCompanyId + '] AS A 
					ON A.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					INNER JOIN '+ @LNLinkedServer + '.dbo.ttcmcs065'+ @LNCompanyId + ' AS B 
					   ON B.[t_cadr] = A.[t_ladr] 
					   INNER JOIN [dbo].[PCR501] AS P ON P.[t_loca] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					   WHERE P.[t_pcrn] = ''' + @PCRN + ''' AND B.[t_cwoc] COLLATE Latin1_General_100_CS_AS_KS_WS = ''' + @WCDeliver + ''''
	 INSERT INTO @Result([Count])
	 EXEC(@SqlQuery)
	 SELECT @Count = R.[Count] FROM @Result AS R
	 IF(@Count = 0)
	  BEGIN
	    SET @Message = 'Selected Work center ' + @WCDeliver
		SET @Message += ' does not belong to location for which Plate cutting request ' + @PCRN + ' was generated for'
	    RAISERROR(@Message, 12, 1)
	  END
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END

----------------------------------
--2
GO


IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_ProjectList]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_ProjectList]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

ALTER Procedure [dbo].[SP_PCR_Get_ProjectList]
(
	 @PSNo NVARCHAR(100)
)
AS

BEGIN
	DECLARE @Sql NVARCHAR(MAX); 
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)	
	SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer')
	SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId')
	DECLARE @ProjectStatusValue INT
	DECLARE @ProjectTypeValue INT
	DECLARE @V INT
	SELECT	
		TOP 1 @ProjectStatusValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Project Status','tppdm.psts') WHERE UPPER([LNConstant]) = 'CONSTRUCTION'
	SELECT
		TOP 1 @ProjectTypeValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Project Type','tppdm.kopr') WHERE UPPER([LNConstant]) = 'SUB'
	SELECT
		 @V = 1
	SET @SQL = 
	'SELECT PP.[t_cprj], PP.[Description], PP.[CodeDescription] FROM
	(
	Select [t_cprj], [t_dsca] AS [Description], [t_cprj] + '' - '' + [t_dsca] AS CodeDescription  FROM ' +
	@LNLinkedServer + '.dbo.ttppdm600' + @LNCompanyId + ' AS P WITH (NOLOCK) 
	WHERE P.[t_psts] = '
	SET @Sql = CONCAT(@Sql, @ProjectStatusValue)
	SET @Sql = CONCAT(@Sql, 
	' AND P.[t_kopr] = ')
	SET @Sql = CONCAT(@Sql, @ProjectTypeValue)
	SET @Sql = CONCAT(@Sql, 
	' ) AS PP
	WHERE PP.[t_cprj] 
	NOT IN 
	 (SELECT  P2.[t_sprj] FROM ' + @LNLinkedServer + '.dbo.tltctm108' + @LNCompanyId + ' AS P2 WHERE P2.[t_serl] = ')
	 SET @Sql = CONCAT(@Sql, @V)
	 SET @Sql = CONCAT(@Sql, 
	 ')')


	--PRINT(@SQL)
	EXEC(@SQl)
	
END
----------------------------------------
--3
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_FREQUENCY]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_FREQUENCY]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
 
Alter Procedure [dbo].[SP_PCR_GET_FREQUENCY]
(
	 @Project NVARCHAR(20),
	 @Element NVARCHAR(100),
	 @LNLinkedServer NVARCHAR(100),
	 @LnCompanyId NVARCHAR(10)	
)
AS

BEGIN
	DECLARE @Sql NVARCHAR(MAX)
	SET 
	@Sql =
		'BEGIN
 			SELECT	 
				tbl.t_qant AS [Value] 
			FROM	' + @LNLinkedServer + '.dbo.ttpptc101' + @LNCompanyId + '
			AS tbl 
			WHERE 	
				tbl.t_cprj = ''' + @Project	+ '''
		          AND 
				tbl.t_cspc = ''' + @Element + '''		
		END '
  EXEC (@Sql)

END
-----------------------------------------------
--4
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_IS_PLATE]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_IS_PLATE]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

ALTER Procedure [dbo].[SP_PCR_IS_PLATE]
(
	 @Item NVARCHAR(50),
	 @Pcrn NVARCHAR(9),
	 @Location NVARCHAR(9),
	 @LNLinkedServer NVARCHAR(100),
	 @LnCompanyId NVARCHAR(10)
)
AS

BEGIN
	DECLARE @Sql NVARCHAR(MAX); 
	
	SET @Sql =
	'BEGIN
	  DECLARE @PType NVARCHAR(100)  
	  DECLARE @CType NVARCHAR(100)
	  DECLARE @Loc NVARCHAR(9)
	  DECLARE @Returned BIT
	  SET @Returned = 0
	  SET @Loc = ' + '''' + @Location + '''
	  IF(@Loc IS NULL OR LTRIM(RTRIM(@Loc)) = '''') 
	    BEGIN
	     SELECT TOP 1 @Loc = tbl.[t_loca] FROM ' + @LNLinkedServer + '.[dbo].[tltsfc501' + @LNCompanyId + '] AS tbl WHERE tbl.[t_pcrn] = ''' + @Pcrn + '''
	    END 
	  SELECT TOP 1 @PType = tbl.[t_ptyp] FROM ' + @LNLinkedServer + '.[dbo].[tltlnt000' + @LNCompanyId + '] AS tbl WHERE tbl.[t_dimx] = @Loc 
	  IF(@PType IS NOT NULL AND LTRIM(RTRIM(@PType)) <> '''')
	    BEGIN
		  SELECT TOP 1 @CType = tbl.[t_ctyp] FROM ' + @LNLinkedServer + '.[dbo].[ttcibd001' + @LNCompanyId + '] AS tbl WHERE tbl.[t_item] = ''' + @Item + '''
		  SELECT @Returned = CASE WHEN @PType = @CType THEN 1 ELSE 0 END
	     END 	  	
	  SELECT @Returned AS [Value]
	 END ' 
	
  EXEC(@Sql)

END
----------------------------------
--5
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GENERATE_PCR_ACTIONS_FETCH_DATA]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GENERATE_PCR_ACTIONS_FETCH_DATA]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


Alter Procedure [dbo].[SP_PCR_GENERATE_PCR_ACTIONS_FETCH_DATA]
(
   @Project NVARCHAR(100),
   @PSNo NVARCHAR(100)  
)
AS

BEGIN
	SET NOCOUNT ON
	DECLARE @ElementsTable AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(100),[Description] NVARCHAR(500))
	DECLARE @ValidElementsTable AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(100))
    DECLARE @TempElementTable AS TABLE(Element NVARCHAR(100))
	DECLARE @Element NVARCHAR(100)
	DECLARE @Index INT = 1
	DECLARE @TotalElements INT = 10
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)
	DECLARE @Frequency INT
	DECLARE @FoundRowCount INT
	DECLARE @T_Dimx NVARCHAR(100) = NULL 
	DECLARE @RowIndexInSource INT, @TotalRowsInSource INT, @IsValid BIT 
	DECLARE @PCRTypeValue INT 
	DECLARE @Sum_Value FLOAT, @T_Seqn_Value INT 
	DECLARE @RowIndexInTempTable INT, @TotalRowsInTempTable INT 
	DECLARE @T_Cprj NVARCHAR(9), @T_Cspa NVARCHAR(8) , @T_Sern INT, @T_Seqn INT  	
	DECLARE @T_Citm NVARCHAR(47), @T_Leng FLOAT, @T_Widt FLOAT, @T_Npcs INT 
	DECLARE @TotalConsumed INT 
	DECLARE @Sql NVARCHAR(MAX)	
	DECLARE @InsertRecordCount INT = 0
	DECLARE @UpdateRecordCount INT = 0
	DECLARE @Loc NVARCHAR(100)
	DECLARE @ReleasedWorkAuthStatusValue INT
	DECLARE @ActualStatusValue INT
	DECLARE @V INT
	DECLARE @ItemTypePlateValue INT
	
 BEGIN TRAN
   BEGIN TRY 
	SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer')
	SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId')
	SELECT @Loc = [t_loca] FROM COM003 WITH (NOLOCK) WHERE  [t_psno] = @PSNo
	SELECT
		TOP 1 @ReleasedWorkAuthStatusValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Work Authorization Status','tppdm.wast') WHERE UPPER([LNConstant]) = 'RELEASED'
	SELECT
		TOP 1 @ActualStatusValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Status Technical Calculation','tpptc.stat') WHERE UPPER([LNConstant]) = 'ACTUAL'
	SELECT
		TOP 1 @V = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') WHERE UPPER([LNCONSTANT]) = 'NO'
    SELECT 
	    TOP 1 @ItemTypePlateValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PLATE'
	SELECT 
		@PCRTypeValue = tblEnum.[Value]  
	FROM 
		[dbo].[PCR000] AS tblEnum  
	WHERE  
		UPPER(tblEnum.LNTableName)  = 'LTSFC506' AND UPPER(tblEnum.LNDomainName) = 'LTSFC.PCR.TYPE'  
		AND UPPER(tblEnum.LNConstant) ='DRAW' 

    SET @Sql = 'SELECT DISTINCT A.[t_cspa], A.[t_desc] FROM ' + @LNLinkedServer + '.dbo.ttpptc100' + @LNCompanyId + ' AS A '
	  + ' JOIN ' + @LNLinkedServer + '.dbo.ttpptc120' + @LNCompanyId + ' AS B ON B.[t_cprj] = A.[t_cprj]   WHERE  ' +
	    ' A.[t_wast] = ' 
		SET @Sql = CONCAT(@Sql, @ReleasedWorkAuthStatusValue)
		SET @Sql = CONCAT (@Sql, '  AND A.[t_stat] = ')
		SET @Sql = CONCAT(@Sql, @ActualStatusValue)
		SET @Sql = CONCAT(@Sql, ' AND A.[t_cctr] <> ')
		SET @Sql = CONCAT(@Sql, @V)
		SET @Sql = CONCAT(@Sql, +' AND A.[t_cprj] = ''' + @Project + '''')
	 
	 INSERT INTO @ElementsTable([Value], [Description])
	 EXEC(@Sql)
	 --PRINT(@Sql)
     
	 SELECT @TotalElements = COUNT(0) FROM @ElementsTable
	
	 WHILE(@Index <= @TotalElements)
	  BEGIN
	    SELECT @Element = [Value] FROM @ElementsTable WHERE RowIndex = @Index
	    SET @Sql = 
		' SELECT [t_cspa] FROM ' + @LNLinkedServer + '.dbo.ttppdm600' + @LNCompanyId + ' WHERE [t_cprj] = ''' + @Project + ''' AND [t_cspa] = ''' + @Element + '''' 
	    
		DELETE FROM @TempElementTable
		INSERT INTO @TempElementTable
		EXEC(@Sql)
		SELECT @FoundRowCount = COUNT(0) FROM @TempElementTable
		IF (@FoundRowCount = 0)
		 BEGIN
		  INSERT INTO @ValidElementsTable([Value])
		  SELECT @Element
		 END
	   SET @Index += 1
	 END
     
	 SELECT @TotalElements = COUNT(0) FROM @ValidElementsTable
	 SET @Index = 1
	 --SELECT * FROM @ValidElementsTable 
	 WHILE(@Index <= @TotalElements)
	  BEGIN	   
	   DECLARE @FrequencyTable AS TABLE([Value] INT) 
	   DECLARE @DimxTable AS TABLE([Value] NVARCHAR(100)) 
	   DECLARE @SourceTable TABLE( 
		 [index] INT PRIMARY KEY IDENTITY(1,1) NOT NULL, [t_cprj] NVARCHAR(9) NOT NULL, [t_cspa] NVARCHAR(8) NOT NULL, 
		 [t_sern] INT NOT NULL, [t_citm] NVARCHAR(47) NOT NULL, [t_crev] NVARCHAR(6) NOT NULL, [t_pitm] NVARCHAR(47) NOT NULL, 
		 [t_leng] FLOAT NOT NULL, [t_widt] FLOAT NOT NULL, [t_noun] NVARCHAR(100) NULL 
	   )
	   DECLARE @IsValidTable AS TABLE([Value] INT)
	   DECLARE @TempTable TABLE( 
		 [index] INT IDENTITY(1,1) PRIMARY KEY, 
		 [t_npcs_m] FLOAT, [t_sern] NVARCHAR(100), 
		 [t_seqn] NVARCHAR(100), [sum] FLOAT 
	   )
	   SELECT @Element = [Value] FROM @ValidElementsTable WHERE RowIndex = @Index
	  
	   SET @Sql =
		'DECLARE @Proj NVARCHAR(100) = ' + '''' + @Project + '''
		 DECLARE @Elem NVARCHAR(100) = ' + '''' + @Element + '''
		 EXEC [dbo].[SP_PCR_GET_FREQUENCY]  @Proj, @Elem, ''' + @LNLinkedServer + '''' + ',' + '''' + @LNCompanyId + ''''
       DELETE FROM @FrequencyTable
	   INSERT INTO @FrequencyTable EXEC(@Sql)
       SELECT @Frequency = [Value] FROM @FrequencyTable	
	   SET @Sql =   
		'SELECT Top 1 tbl000.[t_dimx] FROM ' + @LnLinkedServer + '.[dbo].[tltlnt000' + @LNCompanyId + '] AS tbl000 WITH (NOLOCK) WHERE 
		tbl000.[t_ptyp] <> '''''
       DELETE FROM @DimxTable
	   INSERT INTO @DimxTable EXEC(@Sql)
       SELECT @T_Dimx = [Value] FROM @DimxTable	
	   --SELECT @T_Dimx AS DIMX	   
	   	  
	   IF(@T_Dimx IS NULL) 
		 BEGIN 
			RAISERROR ('Parameter not maintain for ''Product Type ''', 11, 1) 
		 END
		 SET @Sql =
			   'DECLARE @Proj NVARCHAR(100) = ' + '''' + @Project + '''
				DECLARE @Elem NVARCHAR(100) = ' + '''' + @Element + '''			
				SELECT 
					tbl911.[t_cprj], tbl911.[t_cspa], tbl911.[t_sern], 
			  		tbl911.[t_citm], tbl911.[t_crev], tbl911.[t_pitm], 
					tbl911.[t_leng], tbl911.[t_widt], tbl911.[t_noun] 
			FROM ' + 
		 		@LNLinkedServer + '.[dbo].[tltibd911' + @LnCompanyId + '] AS tbl911 WITH (NOLOCK) 
			WHERE  
				tbl911.[t_cprj] = @Proj AND tbl911.[t_cspa] = @Elem '
		  DELETE FROM @SourceTable
		  INSERT INTO @SourceTable( 
				 [t_cprj], [t_cspa], [t_sern],[t_citm],[t_crev],[t_pitm], [t_leng], [t_widt],[t_noun] 
		   )
		  EXEC(@Sql)
			   
		  SELECT @TotalRowsInSource = COUNT(0) FROM @SourceTable 
		  SELECT @RowIndexInSource = 1 
		  WHILE @RowIndexInSource <= @TotalRowsInSource  
			    BEGIN
				 SELECT 
					@T_Citm = tblSource.[t_citm] 
				 FROM 
					@SourceTable AS tblSource WHERE tblSource.[index] = @RowIndexInSource 
				--SELECT @T_Citm AS T_CITM
				 DELETE FROM @IsValidTable
				 INSERT INTO 
					@IsValidTable 
				 EXEC [dbo].[SP_PCR_IS_PLATE] @T_Citm, '', @T_Dimx, @LnLinkedServer, @LNCompanyId  
				
				 SELECT @IsValid = [Value] FROM @IsValidTable 
				 --SELECT @IsValid AS IsValid
				 IF(@IsValid = 1) 
					BEGIN 
						SELECT  
							@T_Cprj = tblSource.[t_cprj], 
							@T_Cspa = tblSource.[t_cspa], 
							@T_Sern = tblSource.[t_sern] 
						FROM   
							@SourceTable AS tblSource  
						WHERE  
							tblSource.[index] = @RowIndexInSource  
						IF NOT EXISTS(  
							SELECT 0 
							FROM 
								[dbo].[PCR506] AS tbl506  
							WHERE  
								tbl506.[t_cprj] = @Project AND tbl506.[t_cspa] = @Element AND tbl506.[t_sern] = @T_Sern   
								AND tbl506.[t_pcrn] IS NULL 
						) --Insert 
						 
							BEGIN	 	
								INSERT INTO [dbo].[PCR506] 
									( 
										[t_cprj], [t_cspa], [t_sern], [t_seqn], [t_sitm], [t_revi],  
										[t_mitm], [t_leng], [t_widt], [t_npcs], [t_leng_m], [t_widt_m],  
										[t_npcs_m], [t_loca], [t_init], [t_pcrn], [t_pono] , [t_ityp], CreatedBy, CreatedOn
									) 
								SELECT 
										tblSource.[t_cprj], tblSource.[t_cspa], tblSource.[t_sern], 1, tblSource.[t_citm], tblSource.[t_crev],  
										tblSource.[t_pitm], tblSource.[t_leng], tblSource.[t_widt], tblSource.[t_noun] * @Frequency, tblSource.[t_leng], tblSource.[t_widt],  
										tblSource.[t_noun] * @Frequency, @Loc, @PSNo, NULL, 0, @ItemTypePlateValue, @PSNo, GETUTCDATE() 
								FROM
										@SourceTable AS tblSource  
								WHERE  
										tblSource.[t_cprj] = @T_Cprj AND  tblSource.[t_cspa] = @T_Cspa 
										AND tblSource.[t_sern] = @T_Sern	
										SET @InsertRecordCount += 1 										
							END	
						ELSE --Update 
							BEGIN 
								SELECT 
	  								@T_Leng = tblSource.[t_leng], @T_Widt = tblSource.[t_widt], @T_Npcs = tblSource.[t_noun] * @Frequency 
								FROM  
									@SourceTable AS tblSource  
								WHERE  
									tblSource.[index] = @RowIndexInSource 
			                    DELETE FROM @TempTable
								INSERT INTO  
									@TempTable([t_npcs_m], [t_sern], [t_seqn], [sum]) 
								SELECT  
									tbl506.[t_npcs_m], tbl506.[t_sern], tbl506.[t_seqn], 0  
								FROM  
									[dbo].[PCR506] AS tbl506  
								WHERE  
									tbl506.[t_cprj] = @Project AND tbl506.[t_cspa] = @Element 
									AND tbl506.[t_sern] = @T_Sern  AND tbl506.[t_pcrn] IS NULL 
								
								SELECT @TotalRowsInTempTable = COUNT(0) FROM @TempTable 
								SET @RowIndexInTempTable = 1 
								WHILE(@RowIndexInTempTable <= @TotalRowsInTempTable)  
									BEGIN 
										SELECT 
											@T_Seqn_Value = T.[t_seqn] 
										FROM 
											@TempTable AS T 
										WHERE 
											T.[index] = @RowIndexInTempTable 
										SELECT 
											@Sum_Value =  SUM(T.[t_npcs_m]) 
										FROM 
											@TempTable AS T 
										WHERE 
											T.[t_seqn] <> @T_Seqn_Value 
										
										UPDATE @TempTable 
											SET [sum] = ISNULL(@Sum_Value, 0) 
										WHERE 
											[index] = @RowIndexInTempTable 
										
										SET @RowIndexInTempTable += 1 
									END
								UPDATE tbl506   
									SET 
									tbl506.[t_leng] = @T_Leng, tbl506.[t_widt] = @T_Widt, tbl506.[t_npcs] = @T_Npcs, 
									tbl506.[t_leng_m] = 
										CASE WHEN tbl506.[t_pcrt] = @PCRTypeValue 
											THEN 
												@T_Leng 
											ELSE 
												tbl506.[t_leng_m] 
											END, 
									tbl506.[t_widt_m] = 
										CASE WHEN tbl506.[t_pcrt] = @PCRTypeValue 
											THEN 
												@T_Widt 
											ELSE 
												tbl506.[t_widt_m] 
											END, 
									tbl506.[t_npcs_m] = 
										CASE WHEN T.[sum] < tbl506.[t_npcs] 
											THEN 
												tbl506.[t_npcs] - T.[sum] 
											ELSE tbl506.[t_npcs_m] 
										END 
									FROM 
										[dbo].[PCR506] AS tbl506			
									JOIN (SELECT * FROM @TempTable) AS T 
										ON T.[t_seqn] = tbl506.[t_seqn] 
									WHERE 
										tbl506.[t_cprj] = @Project AND tbl506.[t_cspa] = @Element 
										AND tbl506.[t_sern] = @T_Sern 
								SET @UpdateRecordCount += 1 										 
							END	-- Update Ends 	'
					END	--IsValid Ends 
					 	SET @RowIndexInSource += 1 
				END --While Ends			     
		  
	     SET @Index += 1
	  END
	  COMMIT TRAN						
	  SELECT (@InsertRecordCount + @UpdateRecordCount) AS EffectedRowsForElement
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END 

------------------------------
--6

GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_PCR_DATA]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_PCR_DATA]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE  [dbo].[SP_PCR_GET_PCR_DATA]
    @StartIndex INT = 1,
    @EndIndex INT = 10,
    @SortingFields VARCHAR(50) = '', 
	@Project NVARCHAR(100),
	@PCR504Id INT = 0	
AS
BEGIN
	DECLARE @FilterQuery NVARCHAR(MAX)  = ' P.[t_cprj] = ''' + @Project + ''
	DECLARE @DeclareQuery NVARCHAR(MAX) = ''	
	
	IF LEN(ISNULL(@SortingFields, '')) > 0
	BEGIN
		SET @SortingFields = SUBSTRING(LTRIM(RTRIM(@SortingFields)), 9, LEN(LTRIM(RTRIM(@SortingFields)))-8)
	END
	SET @SortingFields = ' ORDER BY P.[t_cprj], P.[t_cspa], P.[t_sern] '
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @SQlQuery1 NVARCHAR(MAX) = ''
	DECLARE @SQlQuery2 NVARCHAR(MAX) = ''
	DECLARE @SQlQuery3 NVARCHAR(MAX) = ''
	DECLARE @Spacer NVARCHAR(100) 
    DECLARE @SegmentLength INT = 9
    DECLARE @i INT =0
    WHILE(@i < @SegmentLength)
    BEGIN
      SELECT @Spacer = CASE WHEN @i = 0 THEN ' ' ELSE   @Spacer + ' ' END 
      SET @i += 1
    END

	DECLARE @ConditionalJoin NVARCHAR(MAX) = ''
	 IF (@PCR504Id <> 0)
		BEGIN
		  SET @ConditionalJoin = @ConditionalJoin + ' INNER JOIN [dbo].[PCR503] AS P3 ON P.[t_pcrn] = P3.[t_pcrn] AND  P.[t_pono] = P3.[t_pono]'
		  SET @ConditionalJoin = @ConditionalJoin + ' INNER JOIN [dbo].[PCR504] AS P4 ON P4.[t_pcln] = P3.[t_pcln] AND P4.[t_revn] = P3.[t_revn] '
		  
		END
		ELSE
		BEGIN
		  SET @ConditionalJoin= ''
	END;

	DECLARE @ConditionalWhere NVARCHAR(MAX) = ''
	IF (@PCR504Id <> 0)
		BEGIN
		  SET @ConditionalWhere = @ConditionalWhere + ' AND P4.[Id] = '
		  SET @ConditionalWhere = CONCAT(@ConditionalWhere, @PCR504Id)
		END
		ELSE
		BEGIN
		  SET @ConditionalWhere= ''
	END;

	SET @DeclareQuery = '
	DECLARE @tblLocationAddress AS TABLE 
	(
		PCRN NVARCHAR(100), 
		LocationAddress NVARCHAR(100)
	)
	DECLARE @tblManufacturingItemAndFindNo TABLE
	(
		[t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_sern] INT,
		[t_pitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_dsca] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_fnno] INT
	)	
	DECLARE @tblChildItemAndGradeAndThickness TABLE
	(
		[t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_sern] INT, 
		[t_item] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_dsca] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dscb] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_desc] NVARCHAR(1000) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_thic] INT,
		[t_mtrl] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS
	)	
	DECLARE @tblWCLocation TABLE
	(
		[t_cadr] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_cwoc] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[WCDeliverName] NVARCHAR(MAX)
	)
	DECLARE @Spacer NVARCHAR(100) = ''' + @Spacer + '''
    DECLARE @SegmentLength INT = ' 
	SET @DeclareQuery = CONCAT(@DeclareQuery, @SegmentLength)
	SET @DeclareQuery = @DeclareQuery + ' ' +
	'INSERT INTO @tblManufacturingItemAndFindNo
	(
		[t_cspa], 
		[t_sern], 
		[t_pitm], 
		[t_dsca], 
		[t_fnno]
	)
	SELECT 
		B.[t_cspa],
		B.[t_sern], 
		A.[t_item], 
		A.[t_dsca], 
		tbl911.[t_fnno]
	FROM ' + @LNLinkedServer  + '.dbo.tltibd911' + @LNCompanyId + ' AS tbl911 
		INNER JOIN ' + @LNLinkedServer  + '.dbo.ttpptc120' + @LNCompanyId + ' AS B WITH (NOLOCK) 
			ON 	B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern]
		INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
	        ON A.[t_item] = tbl911.[t_pitm] 
	     WHERE B.[t_cprj] = ''' + @Project + '''
	
	INSERT INTO @tblChildItemAndGradeAndThickness
	(
		[t_cspa], 
		[t_sern], 
		[t_item], 
		[t_dsca], 
		[t_dscb], 
		[t_desc], 
		[t_thic],
		[t_mtrl]
	)
	SELECT 
	    DISTINCT
		B.[t_cspa],
		B.[t_sern], 
		A.[t_item], 
		A.[t_dsca], 
		A.[t_dscb], 
		'''', 
		tbl903.[t_thic],
		T.[t_mtrl]
	FROM ' + @LNLinkedServer   + '.dbo.tltibd911' + @LnCompanyId + ' AS tbl911 
	    INNER JOIN ' +	@LNLinkedServer + '.dbo.ttpptc120' + @LNCompanyId  + ' AS B WITH (NOLOCK) 
		  ON B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern] 
		INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
	       ON A.[t_item]= tbl911.[t_citm] 
	    INNER JOIN (SELECT distinct [t_mtrl] FROM   ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ') AS T ON T.[t_mtrl] = A.[t_dscb]
        INNER JOIN ' + @LNLinkedServer + '.dbo.tltibd903' + @LNCompanyId + ' AS tbl903 WITH (NOLOCK) 
		   ON tbl903.[t_item] = tbl911.[t_citm] 
	    WHERE B.[t_cprj] = ''' + @Project + '''
	
	UPDATE T
	SET T.[t_desc] = A.[t_desc]
	FROM @tblChildItemAndGradeAndThickness AS T
	JOIN ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ' AS A
	ON A.[t_mtrl] COLLATE  Latin1_General_100_CS_AS_KS_WS = T.[t_mtrl]
	
	INSERT INTO @tblWCLocation([t_cadr], [t_cwoc], [t_dsca], [WCDeliverName])
		EXEC [dbo].[SP_PCR_Get_WorkCenter_With_Location] 1 '

	
	SET @SqlQuery1 = '
		
				  SELECT 
								P.[Id],
								P.[t_cprj] AS Project,
								P.[t_cspa] AS Element,
								P.[t_mitm] AS ManufacturingItem,
								ManufacturingItemTable.[t_dsca] AS ManufacturingItemDesc,
								P.[t_sern] AS PartNumber,	
								P.[t_sern] AS BudgetLine,								
								P.[t_seqn] AS [Sequence],
								ManufacturingItemTable.[t_fnno] AS FindNo,
								@Spacer AS [ChildItemSegment],
								CASE WHEN SUBSTRING(P.[t_sitm], 1, @SegmentLength ) = @Spacer THEN SUBSTRING(P.[t_sitm], @SegmentLength + 1, LEN(P.[t_sitm]) - @SegmentLength) ELSE P.[t_sitm] END AS [ChildItem], 
								ChildItemTable.[t_dsca] AS ChildItemDesc,
								P.[t_leng] AS [Length],
								P.[t_widt] AS Width,
								P.[t_leng_m] AS LengthModified,
								P.[t_widt_m] AS WidthModified,
								P.[t_npcs] AS NoOfPieces,
								P.[t_npcs_m] AS RequiredQuantity,
								ChildItemTable.[t_dscb] AS ItemMaterialGrade,
								ISNULL(P.[t_pcrn], '''') PCRNumber,								
								P.[t_pono] AS PCRLineNo,
								P.[t_pcrt] AS PCRType,
								P.[t_prdt] AS PCRRequirementDate,
								P.[t_cwoc] AS WCDeliver,
								P.[t_prmk] AS PlannerRemark, ' ;
	SET @SqlQuery2 = 
								'
								P.[t_prio] AS [Priority],
								P.[t_loca] AS [Location],
								P.[t_cloc] AS CuttingLocation,
								P.[t_sloc] AS SubLocation,
								P.[t_revi] AS ItemRevision,								
								ChildItemTable.[t_thic] AS Thickness,								
								PCRT.[PCRTypeName],																								
								ChildItemTable.[t_desc] AS ItemMaterialGradeName,								
								C2.[LocationName],
								WCLOCA.[WCDeliverName]								
								FROM [dbo].[PCR506] AS P
								LEFT JOIN @tblManufacturingItemAndFindNo AS ManufacturingItemTable ON ManufacturingItemTable.[t_cspa] = P.[t_cspa] ';
	SET @SQLQuery3 = 
								' AND ManufacturingItemTable.[t_sern] = P.[t_sern] AND ManufacturingItemTable.[t_pitm] = P.[t_mitm]
								LEFT JOIN @tblChildItemAndGradeAndThickness AS ChildItemTable ON ChildItemTable.[t_cspa] = P.[t_cspa] 
								AND ChildItemTable.[t_sern] = P.[t_sern] AND ChildItemTable.[t_item] = P.[t_sitm]
								LEFT JOIN (SELECT [t_dimx], [t_dimx] + ''-'' + [t_desc] AS LocationName FROM [dbo].[COM002] WITH (NOLOCK) WHERE [t_dtyp] = 1) 
								AS C2 ON P.[t_loca] = C2.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS
								LEFT JOIN @tblWCLocation AS WCLOCA ON P.[t_cwoc] = WCLOCA.[t_cwoc]
								LEFT JOIN (Select [Value], [Text] AS PCRTypeName FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS(''PCR Type'',''ltsfc.pcr.type'')) AS PCRT 
								ON PCRT.[Value] = P.[t_pcrt] ' + @ConditionalJoin + ' 
  								WHERE ' + @FilterQuery  + '''' + @ConditionalWhere 
                        
						
	--SELECT CAST('<root><![CDATA[' + @DeclareQuery + @SQlQuery1 + @SQlQuery2  + @SQlQuery3 + @SortingFields + ']]></root>' AS XML)
    EXEC(@DeclareQuery+ @SqlQuery1 + @SqlQuery2 + @SqlQuery3)
END
-----------------------
--7
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_Generate_PCR]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_Generate_PCR]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


Alter Procedure [dbo].[SP_PCR_Validate_Generate_PCR]
(
   @ProjectIds NVARCHAR(MAX),
   @ElementIds NVARCHAR(MAX),   
   @BudgetLineIds NVARCHAR(MAX),
   @ChildItemIds NVARCHAR(MAX),
   @Seperator NVARCHAR(10)  
)
AS

BEGIN
	SET NOCOUNT ON
	DECLARE @Query NVARCHAR(MAX)
	DECLARE @IsValid BIT = 0
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)
	DECLARE @ProjectsTable AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(100)) 
	DECLARE @ElementsTable AS TABLE(RowIndex INT IDENTITY(1,1),[Value] NVARCHAR(100)) 
	DECLARE @BudgetLinesTable AS TABLE(RowIndex INT IDENTITY(1,1),[Value] NVARCHAR(100)) 
	DECLARE @ChildItemsTable AS TABLE(RowIndex INT IDENTITY(1,1),[Value] NVARCHAR(100))
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)
	DECLARE @BudgetLine NVARCHAR(MAX)
	DECLARE @ChildItem NVARCHAR(MAX)
	DECLARE @Item NVARCHAR(MAX)
	DECLARE @RowIndex INT
	DECLARE @Total INT
	DECLARE @IsValidTable Table ([Value] BIT)
	DECLARE @Message NVARCHAR(MAX)
	SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer')
	SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId')
	INSERT INTO @ProjectsTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@ProjectIds, @Seperator) 
	INSERT INTO @ElementsTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@ElementIds, @Seperator) 
	INSERT INTO @BudgetLinesTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@BudgetLineIds, @Seperator) 
	INSERT INTO @ChildItemsTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@ChildItemIds, @Seperator) 
   
	
	BEGIN TRY
	     
		 SET @RowIndex = 1
		 SELECT @Total = COUNT(0) FROM @ProjectsTable
		 WHILE(@RowIndex <= @Total)
			 BEGIN
				 SELECT @Project = [VALUE] FROM @ProjectsTable WHERE RowIndex = @RowIndex
				 SELECT @Element = [VALUE] FROM @ElementsTable WHERE RowIndex = @RowIndex
				 SELECT @BudgetLine = [VALUE] FROM @BudgetLinesTable WHERE RowIndex = @RowIndex
				 SELECT @ChildItem = [VALUE] FROM @ChildItemsTable WHERE RowIndex = @RowIndex
				 PRINT 'C' 
				 PRINT @ChildItem
				 SET @Query = 'DECLARE @Item NVARCHAR(MAX)'
				 SET @Query +=  ' SELECT Top 1 @Item = tbl911.[t_citm] FROM ' + @LNLinkedServer + '.dbo.' + 'tltibd911' + @LNCompanyId + ' AS tbl911 WITH (NOLOCK) 
			                	WHERE tbl911.[t_cprj] = ''' + @Project + ''' AND tbl911.[t_cspa] = ''' + @Element + ''' AND CONVERT(NVARCHAR(100), tbl911.[t_sern]) = ''' + @BudgetLine + ''''
				 SET @Query += ' PRINT @Item IF(LTRIM(RTRIM(@Item)) <> ''' + LTRIM(RTRIM(@ChildItem)) + ''') BEGIN SELECT 0 END ELSE BEGIN SELECT 1 END '
				 DELETE FROM @IsValidTable
		         INSERT INTO @IsValidTable
		         EXEC (@Query)
		         SELECT @IsValid = [Value] FROM @IsValidTable

				 IF(@IsValid = 0)
				  BEGIN
				    SET @Message = CONCAT('Cant Generate PCR.\nReason : Mismatch in Budget Line Item ' , @BudgetLine)
					SET @Message = CONCAT(@Message,' and PCR Item')
		   			RAISERROR(@Message, 11, 1) 
				  END
				SET @RowIndex += 1
			END
			SELECT 1
	END TRY 
	BEGIN CATCH 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
					@ErrorMessage = ERROR_MESSAGE() ,  
					@ErrorSeverity = ERROR_SEVERITY(),  
					@ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	END CATCH 
	
END 
-----------------------------
--8
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Generate_Plate_Cutting_Request]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Generate_Plate_Cutting_Request]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

Alter Procedure [dbo].[SP_PCR_Generate_Plate_Cutting_Request]
(
   @Ids NVARCHAR(MAX) ,
   @Seperator NVARCHAR(10),
   @PSNo NVARCHAR(100)
)
AS

BEGIN
	SET NOCOUNT ON
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)
	DECLARE @IdsTable AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(1000)) 
	DECLARE @ValueTable Table ([Value] NVARCHAR(100))
	DECLARE @LocTable Table ([Value] NVARCHAR(100))
	DECLARE @FrequencyTable AS TABLE([Value] INT) 
	DECLARE @ThicknessTable Table ([Value] INT)
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)
	DECLARE @SerialNumber INT
    DECLARE @Sql NVARCHAR(MAX)
	DECLARE @Value NVARCHAR(1000)
	DECLARE @Loc NVARCHAR(100)
	DECLARE @NumberOfPieces INT
	DECLARE @TotalConsumed INT	
    DECLARE @Frequency INT
	DECLARE @PCRN NVARCHAR(100)	
	DECLARE @PonoIncrement INT = 10
	DECLARE @Pono INT = 0
	DECLARE @ChildItem NVARCHAR(MAX)	
	DECLARE @Id INT
	DECLARE @NextSeq INT
	DECLARE @Diff INT
	DECLARE @CharValue NVARCHAR(9) 
	DECLARE @CharLength INT 
	DECLARE @StartLength INT = 0
	DECLARE @RowIndex INT
	DECLARE @Total INT
	DECLARE @Thickness INT
	DECLARE @EarlierValue NVARCHAR(9)
    DECLARE @MaxValue INT
	DECLARE @Message NVARCHAR(MAX)
	DECLARE @ReleasedValue INT
	DECLARE @PCRTDrawValue INT
	DECLARE @ItemTypePlateValue INT
	SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer')
	SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId')
	INSERT INTO @IdsTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@Ids, @Seperator) 
	
	BEGIN TRAN
	   BEGIN TRY	     
		 
	      SELECT
	    	TOP 1 @ReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'
	
	      SELECT
	    	TOP 1 @PCRTDrawValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Type','ltsfc.pcr.type') WHERE UPPER([LNConstant]) = 'DRAW'
	      
		  SELECT 
		    TOP 1 @Loc = [t_loca] FROM [dbo].[PCR506] WITH (NOLOCK) WHERE Id IN (SELECT CONVERT(INT, [Value]) FROM @IdsTable) 
		  
		  SELECT 
	        TOP 1 @ItemTypePlateValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PLATE'
		   --SELECT @Loc
		   SET @Sql = ' SELECT [t_pser] FROM ' + @LNLinkedServer + '.dbo.' + 'tltlnt000' + @LNCompanyId + ' AS A WITH (NOLOCK) WHERE [t_dimx] = ''' + @Loc + ''''
		   --PRINT(@Sql)
		   
		   INSERT INTO @ValueTable
		   EXEC (@Sql)
		   SELECT TOP 1 @Value = [Value] FROM @ValueTable
		   --TODO PCRN Computation
		   DELETE FROM @ValueTable
		   SET @Value = LEFT(@Value, 3)
		   --PRINT(@Value)
		   IF EXISTS (SELECT [t_pcrn] FROM [dbo].[PCR501] WHERE [t_pcrn] LIKE @Value + '%')
		   BEGIN
		     SELECT Top 1 @EarlierValue = [t_pcrn] FROM [dbo].[PCR501] WHERE [t_pcrn] LIKE @Value + '%' ORDER BY [t_pcrn] DESC
			 --PRINT(@EarlierValue)
			 SET @EarlierValue = RIGHT(@EarlierValue, 6)
			 --PRINT(@EarlierValue)
			 SELECT @MaxValue = CONVERT(INT, @EarlierValue)
			 PRINT(@MaxValue)
			 SET @MaxValue = @MaxValue + 1
			 SET @CharValue = CONVERT(NVARCHAR, @MaxValue)
			 SET @CharLength = LEN(@CharValue)
			 SET @PCRN = @Value
			 while(@StartLength < 6 - @CharLength)
			 BEGIN
			   SET @PCRN = CONCAT(@PCRN, '0')
			   SET @StartLength += 1
			 END
			 SET @PCRN = CONCAT(@PCRN, @MaxValue)
		   END
		   ELSE
		     BEGIN
			   SET @PCRN = CONCAT(@Value, '000001')
			 END
		   INSERT INTO [dbo].[PCR501]([t_pcrn], [t_cdat], [t_init], [t_loca])
		   SELECT @PCRN, GETUTCDATE(), @PSNo, @Loc		   
		   
		   SET @RowIndex = 1
		   SELECT @Total = COUNT(0) FROM @IdsTable
		   WHILE(@RowIndex <= @Total)
			 BEGIN
			     SET @Pono = @Pono + @PonoIncrement
				 SELECT @Id = CONVERT(INT, [Value]) FROM @IdsTable WHERE RowIndex = @RowIndex
				 SELECT @ChildItem = [t_sitm], @NumberOfPieces = [t_npcs] FROM [dbo].[PCR506] WITH (NOLOCK) WHERE [Id] = @Id
				 SELECT @TotalConsumed = SUM([t_npcs_m]) FROM [dbo].[PCR506] WITH (NOLOCK) WHERE [Id] = @Id
				 DELETE FROM @ThicknessTable				 				 
				 SET @Sql = ' SELECT [t_thic] FROM ' + @LNLinkedServer + '.dbo.' + 'tltibd903' + @LNCompanyId + ' WITH (NOLOCK) WHERE [t_item] = ''' + @ChildItem + ''''
				 --PRINT(@Sql)
				 INSERT INTO @ThicknessTable				 
				 EXEC(@Sql)
				 SELECT TOP 1 @Thickness = [Value] FROM @ThicknessTable 
				 INSERT INTO [dbo].[PCR503](
					t_pcrn, t_pono, t_revn, t_leng, t_widt, t_npcs, t_pcrt, 
					t_prtn, t_mitm, t_sitm, t_revi, t_cprj, t_cspa,
					t_thic, t_pcr_crdt, t_prdt, t_cwoc, t_prmk, t_pcr_rldt, t_lsta, t_area, t_ityp, t_init
				 )
				 SELECT 
				 @PCRN, @Pono, 0, P.[t_leng_m], P.[t_widt_m], P.[t_npcs_m], P.[t_pcrt], P.[t_sern], 
				 P.[t_mitm], P.[t_sitm], P.[t_revi], P.[t_cprj], P.[t_cspa], 
				 @Thickness, GETUTCDATE(), P.[t_prdt], P.[t_cwoc], P.[t_prmk], 
				 GETUTCDATE(), @ReleasedValue, (P.[t_leng_m] * P.[t_widt_m])/1000000, P.[t_ityp], @PSNo
				 FROM [dbo].[PCR506] AS P WITH (NOLOCK) 
				 WHERE P.Id = @Id
        		 
				 UPDATE [dbo].[PCR501] SET [t_rdat] = GETUTCDATE() WHERE [t_pcrn] = @PCRN
				 UPDATE [dbo].[PCR506] SET [t_pcrn] = @PCRN, [t_pono] = @Pono WHERE [Id] = @Id
				 SET @Diff = @NumberOfPieces - @TotalConsumed
				 IF(@Diff > 0)
				  BEGIN				   				   
				   SELECT 
				   @Project = P.[t_cprj], 
				   @Element = [t_cspa], 
				   @SerialNumber = [t_sern] 
				   FROM [dbo].[PCR506] AS P WITH (NOLOCK) 
				   WHERE P.[Id] = @Id
				   
				   SELECT TOP 1 @NextSeq = P.[t_seqn] 
				   FROM [dbo].[PCR506] AS P WITH (NOLOCK) 
				   WHERE 
				   P.[t_cprj] = @Project 
				   AND 
				   P.[t_cspa] = @Element 
				   AND 
				   P.[t_sern] = @SerialNumber
				   ORDER BY P.[t_seqn] DESC
				   
				   IF(@NextSeq IS NULL)
				     BEGIN
					  SET @NextSeq = 1
					 END
					ELSE
					 BEGIN
					  SET @NextSeq = @NextSeq + 1
					 END					
				    SET @Sql =
						'DECLARE @Proj NVARCHAR(100) = ' + '''' + @Project + '''
						 DECLARE @Elem NVARCHAR(100) = ' + '''' + @Element + '''
						 EXEC [dbo].[SP_PCR_GET_FREQUENCY]  @Proj, @Elem, ''' + @LNLinkedServer + '''' + ',' + '''' + @LNCompanyId + ''''
					DELETE FROM @FrequencyTable
					INSERT INTO @FrequencyTable EXEC(@Sql)
					SELECT @Frequency = [Value] FROM @FrequencyTable	
					--PRINT(@Frequency)
					SET @Sql = 
					  'SELECT TOP 1 ''' + @Project + ''', ''' + @Element + ''''
					  SET @Sql = CONCAT(@Sql,', ' )
					  SET @Sql = CONCAT(@Sql, @SerialNumber)
					  SET @Sql = CONCAT(@Sql, ', ')
					  SET @Sql = CONCAT(@Sql, @NextSeq)
					  SET @Sql = CONCAT(@Sql, ', ')
					  SET @Sql = CONCAT(@Sql, ' GETUTCDATE(), ''' + @PSNo + ''', tbl911.[t_citm], tbl911.[t_crev], tbl911.[t_pitm], 
					  tbl911.[t_leng], tbl911.[t_widt], ' + 'CONVERT(NVARCHAR(100), tbl911.[t_noun] * ')
					  SET @Sql = CONCAT(@Sql, @Frequency)
					  SET @Sql = CONCAT(@Sql, '), tbl911.[t_leng], tbl911.[t_widt], '); 
					  SET @Sql = CONCAT(@Sql, @Diff)					  
					  SET @Sql = CONCAT(@Sql,  ', NULL, 0,')
					  SET @Sql = CONCAT(@Sql, @PCRTDrawValue)
					  SET @Sql = CONCAT(@Sql, ', NULL, NULL, NULL, ' + 
					  '''' + @Loc + '''' + ', ' + 
					  '''' + @PSNo + '''' + ', ')
					  SET @Sql = CONCAT(@Sql, @ItemTypePlateValue)
					  SET @Sql = CONCAT(@Sql, ' FROM ' + @LNLinkedServer + '.dbo.tltibd911' + @LNCompanyId + ' AS tbl911 WITH (NOLOCK) ')
					  SET @Sql = CONCAT(@Sql, 
					  ' WHERE tbl911.[t_cprj] = ''' + @Project + ''' AND tbl911.[t_cspa] = ''' +  @Element + ''' AND ')
					  SET @Sql = CONCAT(@Sql, 'CONVERT(NVARCHAR(100), tbl911.[t_sern]) = '''+ CONVERT(NVARCHAR(100), @SerialNumber) + '''')
					  PRINT(@Sql)
					  INSERT INTO [dbo].[PCR506](
					    [t_cprj], [t_cspa], [t_sern], [t_seqn], CreatedOn, CreatedBy, [t_sitm],
						[t_revi], [t_mitm], [t_leng], [t_widt], [t_npcs], [t_leng_m], [t_widt_m], [t_npcs_m], 
					    [t_pcrn], [t_pono],  [t_pcrt], [t_prdt], [t_cwoc], [t_prmk], [t_loca], [t_init], [t_ityp])
					  EXEC(@Sql)
				 END                               
           		 SET @RowIndex += 1
			END
		COMMIT TRAN
		SELECT 1
	END TRY 
	BEGIN CATCH 
	   IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRAN 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
					@ErrorMessage = ERROR_MESSAGE() ,  
					@ErrorSeverity = ERROR_SEVERITY(),  
					@ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	END CATCH 	
END 

--------------------
--9
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Retract_Plate_Cutting_Request]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Retract_Plate_Cutting_Request]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


Alter Procedure [dbo].[SP_PCR_Retract_Plate_Cutting_Request]
(
   @Id INT ,
   @Project NVARCHAR(100),
   @Element NVARCHAR(100),
   @PSNo NVARCHAR(100)
)
AS
 BEGIN
   DECLARE @TempTable As table(RowIndex INT IDENTITY(1,1), [t_pcrn] NVARCHAR(100), [t_pono] INT,[t_lsta] INT, [t_sern] INT, [t_seqn] INT)
   DECLARE @QuantityTable AS TABLE([Value] FLOAT)
   DECLARE @tbl911 AS TABLE([t_sern] INT, [t_leng] INT, [t_widt] INT, [t_noun] FLOAT)
   DECLARE @LNLinkedServer NVARCHAR(100)
   DECLARE @LNCompanyId NVARCHAR(10)
   DECLARE @Loca NVARCHAR(100)
   DECLARE @Sql NVARCHAR(MAX) 
   DECLARE @t_pcrn NVARCHAR(100)
   DECLARE @t_pono INT
   DECLARE @t_sern INT
   DECLARE @t_seq INT
   DECLARE @t_lsta INT
   DECLARE @t_mrmk NVARCHAR(MAX)
   DECLARE @Quantity FLOAT
   DECLARE @t_noun Float
   DECLARE @Diff Float
   DECLARE @TotalConsumed FLOAT
   DECLARE @PCRCreatedValue INT
   DECLARE @PCRReleasedToNestingValue INT
   DECLARE @PCLGeneratedValue INT
   DECLARE @PCLReleasedValue INT
   DECLARE @PCRDeletedValue INT
   DECLARE @LineCount INT
   BEGIN TRAN
     BEGIN TRY
	   SELECT @Loca = [t_loca] FROM [dbo].[COM003] WITH (NOLOCK) WHERE  [t_psno] = @PSNo
	   SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer')
       SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId')
	   INSERT INTO @TempTable([t_pcrn], [t_pono], [t_sern] , [t_seqn])
	   SELECT P.[t_pcrn], P.[t_pono], P.[t_sern], P.[t_seqn] FROM [dbo].[PCR506] AS P WHERE P.[Id] = @Id
	   SELECT @t_pcrn = T.[t_pcrn], @t_pono = T.[t_pono], @t_sern = T.[t_sern] , @t_seq = T.[t_seqn] FROM @TempTable AS T
	   SELECT @t_lsta = P.[t_lsta], @t_mrmk = P.[t_mrmk] FROM [dbo].[PCR503] AS P WHERE P.[t_pcrn] = @t_pcrn AND P.[t_pono] = @t_pono
	   SELECT
	    	TOP 1 @PCRCreatedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'CREATED'
	   SELECT
	    	TOP 1 @PCRReleasedToNestingValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.NEST'
	   SELECT
	    	TOP 1 @PCLGeneratedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'PCL.GEN'
	   SELECT
	    	TOP 1 @PCLReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'PCL.REL'
	   SELECT
	    	TOP 1 @PCRDeletedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'DELETE'

	   IF(@t_lsta IS NULL)
	    BEGIN
			RAISERROR('PCR cannot be retracted as it is not created yet.', 11, 1)
	    END
	   IF(@t_lsta <= @PCRCreatedValue)
	    BEGIN
			RAISERROR('PCR cannot be retracted as it is not released to material planner yet.', 11, 1)
	    END
		IF(@t_lsta = @PCRReleasedToNestingValue)
	    BEGIN
			RAISERROR('PCR cannot be retracted as it is released to nesting.', 11, 1)
	    END
	   IF(@t_lsta = @PCLGeneratedValue OR @t_lsta = @PCLReleasedValue)
	    BEGIN
			RAISERROR('PCL is Generated/Released,cannot retract PCR.', 11, 1)
	    END
		IF(@t_lsta = @PCRDeletedValue)
	    BEGIN
			RAISERROR('PCR cannot be retracted as it is deleted.', 11, 1)
	    END
       SELECT @LineCount = COUNT(0) FROM [dbo].[PCR503] WHERE [t_pcrn] = @t_pcrn
	   DELETE FROM [dbo].[PCR503] WHERE [t_pcrn] = @t_pcrn AND [t_pono] = @t_pono
	   SET @Sql = ' SELECT [t_qant] FROM ' + @LNLinkedServer + '.dbo.ttpptc101' + @LNCompanyId + ' WHERE [t_cprj] = ''' + @Project + ''' AND [t_cspc] = ''' + @Element + ''''
	   INSERT INTO @QuantityTable([Value])
	   EXEC(@Sql)
	   SELECT @Quantity = [Value] FROM @QuantityTable
	   SELECT @TotalConsumed = SUM(P.[t_npcs_m]) FROM [dbo].[PCR506] AS P WHERE P.[t_cprj] = @Project AND P.[t_cspa] = @Element 
	   AND P.[t_sern] = @t_sern AND P.[t_seqn] <> @t_seq AND P.[t_pcrn] IS NOT NULL AND LEN(P.[t_pcrn]) > 0
	   SET @Sql = 'SELECT P.[t_sern], P.[t_leng], P.[t_widt], P.[t_noun]  FROM ' + @LNLinkedServer + '.dbo.tltibd911' 
	   + @LNCompanyId + ' AS P WHERE P.[t_cprj] = ''' + @Project + ''' AND P.[t_cspa] = ''' + @Element + ''' AND P.[t_sern] = ''' 
	   SET @Sql = CONCAT(@Sql, @t_sern)
	   Set @Sql = CONCAT(@Sql, '''')
	   INSERT INTO @tbl911([t_sern], [t_leng], [t_widt], [t_noun])
	   EXEC(@Sql)

	   UPDATE tbl506
	     SET 
		 tbl506.[t_leng] = T.[t_leng],
		 tbl506.[t_widt] = T.[t_widt],
		 tbl506.[t_npcs] = T.[t_noun] * @Quantity,
		 tbl506.[t_leng_m] = T.[t_leng],
		 tbl506.[t_widt_m] = T.[t_widt],
         tbl506.[t_loca] = @Loca,
		 tbl506.[t_npcs_m] = CASE WHEN tbl506.[t_npcs] > @TotalConsumed THEN   tbl506.[t_npcs] - @TotalConsumed  ELSE 0 END,
		 tbl506.[t_pcrn] = NULL,
		 tbl506.[t_pono] = 0,
		 tbl506.[t_rtby] = @PSNo,
		 tbl506.[t_rtdt] = GETUTCDATE(),
		 tbl506.[t_rrmk] = @t_mrmk                                 
		FROM 
		 [dbo].[PCR506] AS tbl506			
	    JOIN @tbl911 AS T 
		 ON tbl506.[t_cprj] = @Project AND tbl506.[t_cspa] = @Element AND tbl506.[t_sern] = T.[t_sern]

		 DELETE FROM [dbo].[PCR506] WHERE [t_cprj] = @Project AND [t_cspa]= @Element AND [t_sern] = @t_sern AND [t_seqn] <> @t_seq AND [t_pcrn] IS NULL
		 DELETE FROM [dbo].[PCR501] WHERE [t_pcrn] = @t_pcrn AND @LineCount <= 1
	   COMMIT
	   SELECT 1
    END TRY
	BEGIN CATCH
	  IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	END CATCH
END 

-------------------------------
--10
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Insert_Update_PCR_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Insert_Update_PCR_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Insert_Update_PCR_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT  = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @Id INT
	 DECLARE @TempTableName NVARCHAR(100) 
	 DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [LengthModified] FLOAT, 
		 [WidthModified] FLOAT, 
		 [RequiredQuantity] FLOAT, 
		 [Location] NVARCHAR(100),
		 [CuttingLocation] NVARCHAR(100),
		 [SubLocation] NVARCHAR(100),
		 [WCDeliver] NVARCHAR(100), 
		 [PCRRequirementDate] DATETIME,
		 [PCRType] NVARCHAR(100),
		 [PlannerRemark] NVARCHAR(100),
		 [Priority] INT
	 )
	    
   IF(@IsDuplicate = 0)
     BEGIN
	   INSERT INTO @TBL
	   (
		[Id], 
		[LengthModified], 
		[WidthModified], 
		[RequiredQuantity], 
		[Location], 
		[CuttingLocation], 
		[SubLocation], 
		[WCDeliver], 
		[PCRRequirementDate],
		[PCRType],
		[PlannerRemark],
		[Priority]
	   )
	   SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('LengthModified').value('.', 'FLOAT') AS [LengthModified], 
		Array.Entity.query('WidthModified').value('.', 'FLOAT') AS [WidthModified],  
		Array.Entity.query('RequiredQuantity').value('.', 'FLOAT') AS [RequiredQuantity],	
		Array.Entity.query('Location').value('.', 'NVARCHAR(100)') AS [Location],	
		Array.Entity.query('CuttingLocation').value('.', 'NVARCHAR(100)') AS [CuttingLocation],
		Array.Entity.query('SubLocation').value('.', 'NVARCHAR(100)') AS [SubLocation],
		Array.Entity.query('WCDeliver').value('.', 'NVARCHAR(100)') AS [WCDeliver],
		Array.Entity.query('PCRRequirementDate').value('.', 'DATETIME') AS [PCRRequirementDate],
		Array.Entity.query('PCRType').value('.', 'NVARCHAR(100)') AS [PCRType],
		Array.Entity.query('PlannerRemark').value('.', 'NVARCHAR(100)') AS [PlannerRemark],
		Array.Entity.query('Priority').value('.','INT') AS [Priority]
	  FROM @Data.nodes('/ArrayOfSP_PCR_GET_PCR_DATA_Result/SP_PCR_GET_PCR_DATA_Result') AS Array(Entity)
   END
   ELSE
     BEGIN
	  DECLARE @PartNumber INT, @Project NVARCHAR(100), @Element NVARCHAR(100)
	  DECLARE @MaxSeq INT
	   INSERT INTO @TBL
	   (
		[Id]
	   )
	   SELECT 
		  Array.Entity.query('Id').value('.','INT') AS [Id] 
	   FROM @Data.nodes('/ArrayOfSP_PCR_GET_PCR_DATA_Result/SP_PCR_GET_PCR_DATA_Result') AS Array(Entity)

		SELECT TOP 1 @Id = [Id] FROM @TBL
		SELECT TOP 1 @Project = P.[t_cprj] , @Element = P.[t_cspa] , @PartNumber = P.[t_sern] FROM [dbo].[PCR506] AS P WHERE P.[Id] = @Id
		SELECT TOP 1 @MaxSeq = P.[t_seqn]  FROM [dbo].[PCR506] AS P WHERE P.[t_cprj] = @Project AND P.[t_cspa] = @Element AND P.[t_sern] = @PartNumber
		ORDER BY P.[t_seqn] DESC
		SET @TempTableName = CONCAT('#TempPCR506_', @Id)
		DECLARE @Sql NVARCHAR(MAX) = '
			IF OBJECT_ID(N''tempdb..' + @TempTableName + ''') IS NOT NULL
				BEGIN
					DROP TABLE ' + @TempTableName +
			 '  END'
		
			--PRINT(@sql1)
			EXEC(@sql)
			SET @Sql =  CONCAT(' SELECT * INTO ' + @TempTableName + ' FROM [dbo].[PCR506] AS P WHERE P.[Id] = ',  @Id)
			SET @Sql += 
			' UPDATE ' + @TempTableName + ' 
			  SET 
			   [t_pcrn] = NULL, 
			   [t_seqn] = '
			   SET @Sql = CONCAT(@Sql, @MaxSeq + 1)
			   SET @Sql = CONCAT(@Sql, ',
			     [CreatedOn] = GETUTCDATE(), 
			     [t_rtby] = ''' + @UserId + ''', 
			     [t_rtdt] = NULL ' )
               SET @Sql += 
			    ' ALTER TABLE ' + @TempTableName + ' DROP COLUMN [Id];
		          INSERT INTO [dbo].[PCR506] 
			      SELECT * FROM ' + @TempTableName
	 END	

    IF(@Mode = 1 AND @IsDuplicate = 0) --UPDATE
	 BEGIN
	  UPDATE tbl506
		SET 
		  tbl506.[t_leng_m] = B.LengthModified,
		  tbl506.[t_widt_m] = B.WidthModified,
		  tbl506.[t_npcs_m] = B.RequiredQuantity,
		  tbl506.[t_loca] = B.[Location],
		  tbl506.[t_cloc] = B.CuttingLocation,
		  tbl506.[t_sloc] = B.SubLocation,
		  tbl506.[t_cwoc] = B.WCDeliver,
		  tbl506.[t_prio] = B.[Priority],
		  tbl506.[t_prdt] = B.PCRRequirementDate,
		  tbl506.[t_rtby] = @UserId,
		  tbl506.[t_rtdt] = GETUTCDATE(),
		  tbl506.[t_pcrt] = B.PCRType,
		  tbl506.[t_prmk] = B.PlannerRemark
		FROM [dbo].[PCR506] AS tbl506
		INNER JOIN @TBL AS B ON B.Id = tbl506.Id
		
	 END	
	  ELSE IF(@IsDuplicate = 1)
	   BEGIN
	     EXEC(@Sql)
	   END 
	   ELSE IF(@Mode = 0)
	    BEGIN
		  PRINT 1 --TODO
	    END

	  COMMIT
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE() ,
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END 
-------------------------
--11

GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_StockNumberList]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_StockNumberList]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_StockNumberList] 
 @PSNo NVARCHAR(100),
 @Id INT,
 @PageNumber INT = 1,
 @PageSize INT = 1000
AS
BEGIN 
	DECLARE @Location NVARCHAR(100)
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @LNLinkedServer NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @StockPhysicalStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
	DECLARE @StockStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)	
	DECLARE @ResultTable AS TABLE(
		[Id] INT, 
		[Contract] NVARCHAR(100), 
		[Project] NVARCHAR(100),
		[ChildItem] NVARCHAR(100), 
		[StockNumber] NVARCHAR(100), 
		[StockRevision] INT, 						
		[UseAlternateStock] BIT,		
		[FreeArea] FLOAT,		
		[TotalArea] FLOAT,
		[AreaAllocated] FLOAT
	)
	
	IF (@Id > 0)
	BEGIN
		SELECT 
			TOP 1 @Location = P.[t_loca]
		FROM 
			[dbo].[PCR501] AS P INNER JOIN [dbo].[PCR503] AS P3 ON P3.[t_pcrn] = P.[t_pcrn]
		WHERE 
			P3.[Id] = @Id
	END

    --SELECT @Location = 'PEW'
	INSERT INTO 
		@StockPhysicalStatusTable
		(
			[LNConstant], 
			[Text], 
			[Value]
		)
    SELECT 
		Enums.[LNConstant], 
		Enums.[Text], 
		Enums.[Value] 
	FROM 
		[dbo].[PCR000] AS Enums 
	WHERE 
		UPPER(Enums.[EnumName]) = UPPER('Stock Physical Status')
		AND
		(
			(
				@Id > 0 
				AND 
				UPPER(Enums.[LNConstant]) IN 
				(
					UPPER('virtual'), 
					UPPER('at.store'), 
					UPPER('at.pps'),
					UPPER('ret.pps')
				)
			)
				
			OR 
			(
				@Id = 0
			)
		
		)

		INSERT INTO 
			@StockStatusTable
			(
				[LNConstant], 
				[Text], 
				[Value]
			)
		SELECT 
			Enums.[LNConstant], 
			Enums.[Text], 
			Enums.[Value] 
		FROM 
			[dbo].[PCR000] AS Enums 
		WHERE 
			UPPER(Enums.[EnumName]) = UPPER('Stock Status') 
			AND
			UPPER(Enums.[LNConstant]) <> UPPER('delete')
	
	--SET @Id = 1219
    IF(@Id > 0)
	 BEGIN
	 
	   SET @SqlQuery =
		   'DECLARE @StockStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
		    DECLARE @StockPhysicalStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)			
			INSERT INTO @StockPhysicalStatusTable([LNConstant], [Text], [Value])
			SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums 
			WHERE UPPER(Enums.[EnumName]) = UPPER(''Stock Physical Status'')
				AND	UPPER(Enums.[LNConstant]) IN (UPPER(''virtual''), UPPER(''at.store''), UPPER(''at.pps''), UPPER(''ret.pps''))

			INSERT INTO @StockStatusTable([LNConstant], [Text], [Value])
			SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums WHERE 
			UPPER(Enums.[EnumName]) = UPPER(''Stock Status'') AND UPPER(Enums.[LNConstant]) <> UPPER(''delete'') '
	   SET @SqlQuery += 
		   CONCAT(' DECLARE @I INT = ', @Id)
       SET @SqlQuery += 
			  ' SELECT
				P.[Id] AS Id, 
				P.[t_cono] AS [Contract],
				A.[t_cprj] AS [Project],
				LTRIM(RTRIM(P.[t_item])) AS ChildItem,
				LTRIM(RTRIM(P.[t_stkn])) AS StockNumber, 
				P.[t_stkr] AS StockRevision, 
				P.[t_area] AS TotalArea, 
				P.[t_qall] AS AreaAllocated, 
				P.[t_qfre] As FreeArea,
				1 AS UseAlternateStock
			FROM 
				[dbo].[PCR500] AS P 
			INNER JOIN ' +
			  @LNLinkedServer + '.dbo.ttpctm110' + @LNCompanyId + ' AS A 
			ON A.[t_cono] = P.[t_cono]
			 INNER JOIN
			    [dbo].[PCR503] AS P3 ON P3.[t_cprj] = A.[t_cprj] 
			WHERE 
			  P.[t_loca] = ''' + @Location + '''' + ' ' + '			   
			   AND 
			  P3.[Id] = @I
			   AND
			  P.[t_stat] IN 
			  (
				SELECT 
					SS.[Value] 
				FROM @StockStatusTable AS SS
			  )
			  AND
			  P.[t_psta] IN 
			  (
				SELECT 
					SPS.[Value] 
				FROM @StockPhysicalStatusTable AS SPS WHERE UPPER(SPS.LNConstant) <> UPPER(''VIRTUAL'')
			  ) ORDER BY P.[t_item], P.[t_stkn], P.[t_stkr]'
			
    
		 INSERT INTO 
		  @ResultTable
		  (
			[Id], 
			[Contract], 
			[Project], 
			[ChildItem], 
			[StockNumber], 
			[StockRevision], 
			[TotalArea], 
			[AreaAllocated], 
			[FreeArea],
			[UseAlternateStock]
		  )
		 EXEC(@SqlQuery)
	

		 INSERT INTO 
		  @ResultTable
		  (
			[Id], 
			[Contract], 
			[Project], 
			[ChildItem], 
			[StockNumber], 
			[StockRevision], 
			[TotalArea], 
			[AreaAllocated], 
			[FreeArea],
			[UseAlternateStock]
		  )
		  SELECT
				DISTINCT
				P.[Id] AS Id, 
				P.[t_cono] AS [Contract],
				NULL AS [Project],
				LTRIM(RTRIM(P.[t_item])) AS [ChildItem],
				LTRIM(RTRIM(P.[t_stkn])) AS [StockNumber],  
				P.[t_stkr] AS [StockRevision], 
				P.[t_area] AS [TotalArea], 
				P.[t_qall] AS [AreaAllocated], 
				P.[t_qfre] As [FreeArea],
				0 AS [UseAlternateStock]
		  FROM 
				[dbo].[PCR500] AS P
		  INNER JOIN 
				[dbo].[PCR503] AS P3 ON LTRIM(RTRIM(P.[t_item])) = LTRIM(RTRIM(P3.[t_sitm]))  
		  WHERE 
			P.[t_loca] = @Location 
			 AND
			P3.[Id] = @Id
			 AND
			P.[t_stat] IN 
			(
			  SELECT 
				SS.[Value] 
			  FROM @StockStatusTable AS SS
			)
			 AND
			P.[t_psta] IN 
			(
			  SELECT 
				SPS.[Value] 
			  FROM @StockPhysicalStatusTable AS SPS 
			)	
			ORDER BY [ChildItem], [StockNumber], [StockRevision]

			SELECT * from @ResultTable ORDER BY [ChildItem], [StockNumber], [StockRevision]
	END
	 ELSE
	  BEGIN	     
		  SET @SqlQuery = ' DECLARE @StockStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
		    DECLARE @StockPhysicalStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)			
			INSERT INTO @StockPhysicalStatusTable([LNConstant], [Text], [Value])
			SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums 
			WHERE UPPER(Enums.[EnumName]) = UPPER(''Stock Physical Status'')
				AND	UPPER(Enums.[LNConstant]) IN (UPPER(''virtual''), UPPER(''at.store''), UPPER(''at.pps''), UPPER(''ret.pps''))

			INSERT INTO @StockStatusTable([LNConstant], [Text], [Value])
			SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums WHERE 
			UPPER(Enums.[EnumName]) = UPPER(''Stock Status'') AND UPPER(Enums.[LNConstant]) <> UPPER(''delete'') '
		 SET @SqlQuery += 'SELECT
				
				[Id], [t_cono] AS [Contract], [t_cprj] AS [Project], [t_item] AS [ChildItem], [t_stkn] [StockNumber], [t_stkr] AS [StockRevision],
				[t_area] AS [TotalArea], [t_arcs] AS [AreaAllocated], [t_loca] AS [Location], [t_leng] AS [Length], [t_widt] AS [Width], [t_qfre] AS [FreeArea], [t_qnty] AS [Quantity],
				[t_mtrl] AS [Materials], [t_thic] AS [Thickness], [t_orno] AS [PurchaseOrder], [t_pono] AS [POPosition], [t_rcno] AS [ReceiptNumber], [t_rcln] AS [ReceiptLine],
				[t_shtp] AS [ShapeType], [t_shfl] AS [ShapeFile], [t_size] AS [SizeCode], [t_cwar] AS [Warehouse], [t_wloc] AS [WarehouseLocation], [t_cnvf] AS [DerivedConversion],
				[t_stat] AS [StockStatus], [t_htno] AS [HeatNo], [t_tcno] AS [TCNo], [t_gorn] AS [GraiOrientation], [t_mchn] AS [CuttingMachine], [t_pcln] AS [PCLNumber],
				[t_armv] AS [ARMVersion], [t_cdt1] AS [CladThickness1], [t_cdt2] AS [CladThickness2], [t_ccd1] AS [TheoreticalCon1], [t_ccd2] AS [TheoreticalCon2], [t_twgt] AS [TheoreticalWeight],
				[t_psta] AS [StockPhysicalStatus], [t_clot] AS [ActualLot], [t_pstk] AS [ParentStockNumber], [t_prev] AS [ParentStockRevision], [t_odds] AS [OddShape], [t_idsf] AS [IssueDirectly],
				[t_cspa] AS [Element], [t_rmrk] AS [Remark], [t_srmk] AS [StockRelatedRevision], [t_sprj] AS [StockProject], [t_ncra] AS [NCRApplicable], [t_ncrr] AS [NCRRemark],
				[t_ncrn] AS [NCRNumber], [t_rsfp] AS [ReservedForProject], [t_rsby] AS [ReservedByWho], [t_rsdt] AS [ReservationDate], [t_arms] AS [ARMSet], [t_cnvb] AS [TheoreticalCon]
		  FROM 
				[dbo].[PCR500] AS P		   
		  WHERE 
			P.[t_stkn] IS NOT NULL AND LEN(P.[t_stkn]) > 0
			 AND
			P.[t_stat] IN 
			(
			  SELECT 
				SS.[Value] 
			  FROM @StockStatusTable AS SS
			)
			 AND
			P.[t_psta] IN 
			(
			  SELECT 
				SPS.[Value] 
			  FROM @StockPhysicalStatusTable AS SPS 
			)
			 ORDER BY P.[t_item], P.[t_stkn], P.[t_stkr]  OFFSET '
		SET @SqlQuery = CONCAT(@SqlQuery, (@PageNumber - 1) * @PageSize, ' ROWS FETCH NEXT ', @PageSize, ' ROWS ONLY')
		EXEC (@SqlQuery)
	  END
	
	
END

-----------------------------
--12
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_Location_With_Address]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_Location_With_Address] 
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_Location_With_Address]   
AS
BEGIN 
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @LNLinkedServer NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	

	SET @SqlQuery = 'SELECT  
							A.[t_ladr] AS ParentID,
							C.[t_dimx] AS Code, 
						    C.[t_desc] AS Description, 
						    CONCAT(C.[t_dimx], ' + '''-'' + C.[t_desc]) AS CodeDescription							
					FROM  [dbo].[COM002] AS C INNER JOIN '+ @LNLinkedServer +'.[dbo].[tltlnt000'+ @LNCompanyId + '] AS A 
					ON A.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx] WHERE C.[t_dimx] IS NOT NULL AND C.[t_dtyp] = 1 
					ORDER BY A.[t_ladr], C.[t_dimx], C.[t_desc]'

	EXEC (@SqlQuery)
END
-------------------------
--13

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_MATERIAL_PLANNER_DATA]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_MATERIAL_PLANNER_DATA]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_GET_MATERIAL_PLANNER_DATA]
    @StartIndex INT = 1,
    @EndIndex INT = 10,
    @SortingFields VARCHAR(50) = '',
	@PCR504Id INT = 0
AS
BEGIN 
	DECLARE @RowFilterQuery NVARCHAR(MAX) = '';
	DECLARE @DeclareQuery NVARCHAR(MAX) = ''
	DECLARE @Spacer NVARCHAR(100) 
    DECLARE @SegmentLength INT = 9
    DECLARE @i INT =0
    WHILE(@i < @SegmentLength)
    BEGIN
      SELECT @Spacer = CASE WHEN @i = 0 THEN ' ' ELSE   @Spacer + ' ' END 
      SET @i += 1
    END
	
	IF LEN(ISNULL(@SortingFields, '')) > 0
	BEGIN
		SET @SortingFields = SUBSTRING(LTRIM(RTRIM(@SortingFields)), 9, LEN(LTRIM(RTRIM(@SortingFields)))-8)
	END

	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @SQlQuery NVARCHAR(MAX) 
	DECLARE @SQlQuery1 NVARCHAR(MAX) 
	DECLARE @SQlQuery2 NVARCHAR(MAX) 
	DECLARE @SQlQuery3 NVARCHAR(MAX) 
	DECLARE @SQlQuery4 NVARCHAR(MAX) 
	DECLARE @SQlQuery5 NVARCHAR(MAX)
	DECLARE @SQlQuery6 NVARCHAR(MAX)
	--SET @SqlQuery = 
	
    
	--EXEC(@SqlQuery)
	DECLARE @Collation NVARCHAR(100) = ''
	SET @Collation = ' COLLATE ' + 'Latin1_General_100_CS_AS_KS_WS';
	IF LEN(ISNULL(@SortingFields, '')) > 0
	 BEGIN
	 	SET @SortingFields = SUBSTRING(LTRIM(RTRIM(@SortingFields)), 9, LEN(LTRIM(RTRIM(@SortingFields)))-8)
	 END

	 DECLARE @ConditionalJoin NVARCHAR(MAX) = ''
	 IF (@PCR504Id <> 0)
		BEGIN
		  SET @ConditionalJoin = @ConditionalJoin + ' INNER JOIN [dbo].[PCR504] AS P4 ON P4.[t_pcln] = P.[t_pcln] '
		  SET @ConditionalJoin = @ConditionalJoin + ' AND P4.[t_revn] = P.[t_revn] '
		END
		ELSE
		BEGIN
		  SET @ConditionalJoin= ''
	END;

	DECLARE @ConditionalWhere NVARCHAR(MAX) = ''
	IF (@PCR504Id <> 0)
		BEGIN
		  SET @ConditionalWhere = @ConditionalWhere + ' AND P4.[Id] = ' 
		  SET @ConditionalWhere = CONCAT(@ConditionalWhere, @PCR504Id)
		END
		ELSE
		BEGIN
		  SET @ConditionalWhere= ''
	END;

	SET @SQlQuery1 = 
	'
	 DECLARE @tblName AS TABLE
	 (
	   [t_emno] NVARCHAR(100),
	   [t_nama] NVARCHAR(200)
	 ) 
	 DECLARE @tblAllProjects TABLE
	 (
		[Id] NVARCHAR(100) , 
		[Description] NVARCHAR(MAX) , 
		[CodeDescription] NVARCHAR(MAX)
	 )
	 
	  INSERT INTO @tblAllProjects
	  (
		[Id], 
		[Description], 
		[CodeDescription]
	  )
	   EXEC [dbo].[SP_PCR_Get_ProjectList] NULL	 
	 DECLARE @tblProjects TABLE
	 (
		[Id] NVARCHAR(100) , 
		[Description] NVARCHAR(MAX) , 
		[CodeDescription] NVARCHAR(MAX)
	 )
	 DECLARE @tblElements TABLE
	 (
		Id NVARCHAR(100) , 
		[Description] NVARCHAR(MAX)
	 )
	 DECLARE @tblParameter AS TABLE 
	(
		PCRN NVARCHAR(100), 
		Parameter NVARCHAR(100)
	)
	DECLARE @tblLocationAddress AS TABLE 
	(
		PCRN NVARCHAR(100), 
		LocationAddress NVARCHAR(100)
	)
	INSERT INTO @tblProjects
	(
		[Id], 
		[Description], 
		[CodeDescription]
	)
	SELECT * 
	FROM @tblAllProjects 
	WHERE [Id] COLLATE Latin1_General_100_CS_AS_KS_WS IN 
	(
			SELECT DISTINCT Proj.[t_cprj] FROM [dbo].[PCR503] AS Proj WITH (NOLOCK)
	)
	INSERT INTO @tblElements
	(
			[Id], 
			[Description]
	)
	SELECT 
	  [t_cspa], 
	  [t_dsca] 
	FROM ' + @LNLinkedServer  + '.dbo.ttppdm600' + @LNCompanyId +  ' AS tbl 
	WHERE tbl.[t_cprj] COLLATE Latin1_General_100_CS_AS_KS_WS IN 
	(
			SELECT [Id] FROM @tblProjects
	)
	INSERT INTO @tblParameter
	(
		PCRN, 
		[Parameter]
	)
	SELECT  
		A.[t_pcrn], 
		C.[t_pcr_cuni]  
	FROM ' + @lnlinkedserver + '.dbo.tltlnt000'+ @lncompanyid  + ' AS C 
	 INNER JOIN PCR501 AS A 
	   ON  A.[t_loca] = C.[t_dimx] 
    INSERT INTO @tblLocationAddress
	(
		PCRN, 
		LocationAddress
	)
	SELECT  
		A.[t_pcrn] , 
		C.[t_ladr] 
	FROM ' + @lnlinkedserver + '.dbo.tltlnt000'+ @lncompanyid  + ' AS C 
	 INNER JOIN PCR501 AS A 
	   ON  A.[t_loca] = C.[t_dimx]
	INSERT INTO @tblName([t_emno], [t_nama])
	 SELECT  
	    DISTINCT
		A.[t_emno] , 
		A.[t_nama]
	  FROM ' + @LNLinkedServer + '.dbo.ttccom001'+ @LNCompanyId  + ' AS A
	   INNER JOIN [dbo].[PCR503] AS C 
	     ON C.[t_init] COLLATE Latin1_General_100_CS_AS_KS_WS = A.[t_emno] 
	DECLARE @tblManufacturingItem TABLE
	(
		[t_cprj] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_pitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_mitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS
	)	
	DECLARE @tblChildItem TABLE
	(
		[t_cprj] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_sern] INT, 
		[t_item] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_desc] NVARCHAR(1000) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dscb] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dscd] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_mtrl] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS
	)	
	DECLARE @tblStatuses TABLE
	(
	 [Name] NVARCHAR(100),
	 [Value] INT,
	 [Text] NVARCHAR(100)
	)
	DECLARE @tblWCLocation TABLE
	(
		[t_cadr] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_cwoc] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[WCDeliverName] NVARCHAR(MAX)
	)
	DECLARE @Spacer NVARCHAR(100) = ''' + @Spacer + '''
    DECLARE @SegmentLength INT = ' 
	SET @SQlQuery1 = CONCAT(@SqlQuery1, @SegmentLength)
	SET @SQlQuery1 = @SQlQuery1 + ' ' +
	'INSERT INTO @tblManufacturingItem
	(
		[t_cprj], 
		[t_cspa], 
		[t_pitm], 
		[t_dsca]
	)
	SELECT 
		DISTINCT 
		B.[t_cprj], 
		B.[t_cspa], 
		A.[t_item], 
		A.[t_dsca]
	FROM ' + @LNLinkedServer  + '.dbo.tltibd911' + @LNCompanyId + ' AS tbl911 
		INNER JOIN ' + @LNLinkedServer  + '.dbo.ttpptc120' + @LNCompanyId + ' AS B WITH (NOLOCK) 
			ON B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern]
		INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
	        ON A.[t_item]= tbl911.[t_pitm] 
	     WHERE B.[t_cprj]' + @Collation +' IN 
		  (
			SELECT Id FROM @tblProjects
		  ) 
	
	INSERT INTO @tblStatuses
	(
	  [Name],
	  [Value],
	  [Text]
	)
	SELECT P.[EnumName], P.[Value], P.[Text] FROM [dbo].[PCR000] AS P
	WHERE P.[EnumName] IN (''Item Type'', ''PCR Type'', ''PCR Line Status'', ''Yes No Choice'')
	INSERT INTO @tblChildItem
	(
		[t_cprj], 
		[t_cspa], 
		[t_sern], 
		[t_item], 
		[t_dsca], 
		[t_desc], 
		[t_dscd], 
		[t_dscb],
		[t_mtrl]
	)
	SELECT 
	  DISTINCT 
		B.[t_cprj], 
		B.[t_cspa],
		B.[t_sern], 
		A.[t_item], 
		A.[t_dsca], 
		'''', 
		A.[t_dscd], 
		A.[t_dscb],
		T.[t_mtrl]
		FROM ' + @LNLinkedServer   + '.dbo.tltibd911' + @LnCompanyId + ' AS tbl911 
		INNER JOIN ' +@LNLinkedServer + '.dbo.ttpptc120' + @LNCompanyId  + ' AS B WITH (NOLOCK) 
			ON B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern] 
		INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
	        ON A.[t_item]= tbl911.[t_citm] 
		
		INNER JOIN (SELECT distinct [t_mtrl] FROM   ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ') AS T ON T.[t_mtrl] = A.[t_dscb]
		INNER JOIN ' + @LNLinkedServer + '.dbo.tltibd903' + @LNCompanyId + ' AS tbl903 WITH (NOLOCK) 
		   ON tbl903.[t_item] = tbl911.[t_citm]
		WHERE B.[t_cprj]' + @Collation + ' IN 
			(
				SELECT Id FROM @tblProjects
			)
		UPDATE T
			SET T.[t_desc] = A.[t_desc]
	    FROM @tblChildItem AS T
		JOIN ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ' AS A
		ON A.[t_mtrl] = T.[t_mtrl]
	    
		INSERT INTO @tblWCLocation([t_cadr], [t_cwoc], [t_dsca], [WCDeliverName])
		EXEC [dbo].[SP_PCR_Get_WorkCenter_With_Location] 1 '
		
		
	    SET @SqlQuery2 = ' 
							SELECT 
							   DISTINCT
								P.[Id],
								P.[t_pcrn] AS PCRNumber,
								P.[t_pono] AS PCRLineNo,
								P.[t_revn] AS PCRLineRevision,
								P.[t_pcrt] AS PCRType,
								(SELECT TOP 1 [Text] FROM @tblStatuses WHERE [Value] = P.[t_pcrt] AND [Name] = ''PCR Type'') AS PCRTypeName,
								P.[t_prdt] AS PCRRequirementDate,
								P.[t_lsta] AS PCRLineStatus,
								(SELECT TOP 1 [Text] FROM @tblStatuses WHERE [Value] = P.[t_lsta] AND [Name] = ''PCR Line Status'') AS PCRLineStatusName,
								P.[t_prtn] AS PartNumber,
								P.[t_mitm] AS ManufacturingItem,
                                ManufacturingItemTable.[t_dsca] AS ManufacturingItemDesc,
								@Spacer AS [ChildItemSegment],
								CASE WHEN SUBSTRING(P.[t_sitm], 1, @SegmentLength ) = @Spacer THEN SUBSTRING(P.[t_sitm], @SegmentLength + 1, LEN(P.[t_sitm]) - @SegmentLength) ELSE P.[t_sitm] END AS [ChildItem], 
								ChildItemTable.[t_dsca] AS ChildItemDesc,
								P.[t_sqty] AS ScrapQuantity,
								P.[t_nqty] AS NestedQuantity,								
								ROUND(P.[t_arcs], 2) AS AreaConsumed,
                                P2.[t_leng_m] AS [Length],
								P2.[t_widt_m] AS [Width],
								P.[t_npcs] AS NoOfPieces,
								P.[t_stkn] AS StockNumber,
								P.[t_pcln] AS PCLNumber,
								P.[t_ityp] AS ItemType,
								(SELECT TOP 1 [Text] FROM @tblStatuses WHERE [Value] = P.[t_ityp] AND [Name] = ''Item Type'') AS ItemTypeName,
								P.[t_mvtp] AS MovementType,
								P.[t_prmk] AS PCRPlannerRemark,
								P.[t_stkr] AS StockRevision,
								P.[t_mrmk] AS MaterialPlanner,
								P.[t_pcr_crdt] AS PCRCreationDate,
								P.[t_pcr_rldt] AS PCRReleaseDate,
								P.[t_pcl_crdt] AS PCLCreationDate,
								P.[t_pcl_rldt] AS PCLReleaseDate,
								P.[t_cprj] AS Project,
								P.[t_cspa] AS Element,
								P.[t_mpos] AS ManufacturingItemPosition,
                                Projects.Description AS ProjectDesc,
                                Elements.Description AS ElementDesc,								
								LA.LocationAddress AS LocationAddress,
								P.[t_gorn] AS GrainOrientation,
								P.[t_shtp] AS ShapeType,
								P.[t_shfl] AS ShapeFile,
								P.[t_thic] AS Thickness,
								P.[t_cwoc] AS WCDeliver,'
       SET @SQLQuery3 =
							  ' WCLOCA.[WCDeliverName] AS WCDeliverName,
                                ChildItemTable.[t_dscb] AS MaterialSpecification,
								P.[t_hcno] AS PCRHardCopyNo,
                                P2.[t_cloc] AS CuttingLocation,
                                P.[t_alts] AS UseAlternateStock,
								(SELECT TOP 1 [Text] FROM @tblStatuses WHERE [Value] = P.[t_alts] AND [Name] = ''Yes No Choice'') AS UseAlternateStockName,
                                P.[t_armk] AS AlternateStockRemark,
								P.[t_leng] AS [LengthModified],
								P.[t_widt] AS [WidthModified],
								P.[t_cloc] AS CuttingLocationModified,
                                P.[t_oitm] AS OriginalItem,
								P.[t_rqty],
								P.[t_qapr],
								P.[t_pr01],
								P.[t_pr02],
								P.[t_pr03],
								P.[t_pr04],
								P.[t_pr05],
								P.[t_pr06],
								P.[t_pr07],
								P.[t_pr08],
								P.[t_pr09],
								P.[t_pr10],'

	  SET @SQlQuery4 =    			                    
							  ' NULL AS EmployeeName,
                                CONCAT(N.[t_emno], ''-'' + N.[t_nama]) AS PCRInitiator,							
                                P.[t_revi] AS ItemRevision,
                                P.[t_lscr] AS LinkToSCR,
                                P.[t_scrn] AS SCRNumber,
								P.[t_iqty] AS ApprovedQuantity,
								P.[t_rqty] AS RejectedQuantity,
                                ChildItemTable.[t_dscd] AS ARMCode
			                    FROM [dbo].[PCR503] AS P 
								INNER JOIN @tblProjects AS Projects ON Projects.[Id] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_cprj]								
								LEFT JOIN [dbo].[PCR506] AS P2 ON P2.[t_cprj] = P.[t_cprj] AND 
								P2.[t_cspa] = P.[t_cspa] AND P2.[t_pcrn] = P.[t_pcrn] AND P2.[t_pono] = P.[t_pono]
								INNER JOIN @tblParameter AS PR ON PR.[PCRN] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcrn] 
								INNER JOIN @tblLocationAddress AS LA ON LA.[PCRN] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcrn] 
								LEFT JOIN @tblManufacturingItem AS ManufacturingItemTable ON ManufacturingItemTable.[t_cprj] = P.[t_cprj] 
								AND ManufacturingItemTable.[t_cspa] = P.[t_cspa]';
						
	  SET @SQlQuery5 =       '  AND ManufacturingItemTable.[t_pitm] = P.[t_mitm]
								LEFT JOIN @tblChildItem AS ChildItemTable ON ChildItemTable.[t_cspa] = P.[t_cspa] AND ChildItemTable.[t_cprj] = P.[t_cprj]
								AND ChildItemTable.[t_item] = P.[t_sitm] 
								LEFT JOIN @tblWCLocation AS WCLOCA ON P.[t_cwoc] = WCLOCA.[t_cwoc] 								
								LEFT JOIN @tblElements AS Elements ON Elements.[Id] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_cspa]
								LEFT OUTER JOIN @tblName AS N ON P.[t_init] COLLATE Latin1_General_100_CS_AS_KS_WS = N.[t_emno]' + @ConditionalJoin
								
	
	  SET @SqlQuery6 =       ' WHERE P.[t_lsta] NOT IN 
								(
								   SELECT
								    [Value] 
								  FROM 
									[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS](''PCR Line Status'',''ltpcr.lsta'') 
								  WHERE 
									UPPER([LNConstant]) = ''DELETE''
                                 ) '  + @ConditionalWhere
	--SELECT CAST('<![CDATA[' + @SQlQuery1 + @SQlQuery2  + @SQlQuery3 + @SQlQuery4 + @SQlQuery5 + @SqlQuery6 + ']]>' AS XML)
	EXEC(@SQlQuery1 + @SQlQuery2 + @SQlQuery3 + @SQlQuery4 + @SQlQuery5 + @SqlQuery6)
END

---------------------
--14
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_WorkCenter_With_Location]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_WorkCenter_With_Location]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_WorkCenter_With_Location]
 @ShowAll BIT   = 1
AS
BEGIN 
	
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @LNLinkedServer NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	

	/*SET @SqlQuery = '	Select  
	                        [t_cadr] AS ParentID ,
							[t_cwoc] AS Code, 
						    [t_dsca] AS Description, 
						    CONCAT([t_cwoc], ' + '''-'' + [t_dsca]) AS CodeDescription							
					   FROM '+ @LNLinkedServer + '.dbo.ttcmcs065'+ @LNCompanyId + ' ORDER BY [t_cadr], [t_cwoc], [t_dsca]'*/
     IF(@ShowAll = 1)
	  BEGIN 
	   SET @SqlQuery = '	SELECT  
	                        DISTINCT
	                        B.[t_cadr] AS ParentID ,
							B.[t_cwoc] AS Code, 
						    B.[t_dsca] AS Description, 
						    CONCAT(B.[t_cwoc], ' + '''-'' + B.[t_dsca]) AS CodeDescription							
					   FROM [dbo].[COM002] AS C
					   INNER JOIN '+ @LNLinkedServer +'.[dbo].[tltlnt000'+ @LNCompanyId + '] AS A 
					ON A.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					INNER JOIN '+ @LNLinkedServer + '.dbo.ttcmcs065'+ @LNCompanyId + ' AS B 
					   ON B.[t_cadr] = A.[t_ladr] WHERE C.[t_dimx] IS NOT NULL AND C.[t_dtyp] = 1 ORDER BY B.[t_cadr], B.[t_cwoc], B.[t_dsca]'
     END
	 ELSE
	  BEGIN
	    SET @SqlQuery = '	SELECT  
	                        DISTINCT
	                        B.[t_cadr] AS ParentID ,
							B.[t_cwoc] AS Code, 
						    B.[t_dsca] AS Description, 
						    CONCAT(B.[t_cwoc], ' + '''-'' + B.[t_dsca]) AS CodeDescription							
					   FROM [dbo].[COM002] AS C
					   INNER JOIN '+ @LNLinkedServer +'.[dbo].[tltlnt000'+ @LNCompanyId + '] AS A 
					ON A.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					INNER JOIN [dbo].[PCR501] AS P ON P.[t_loca] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					INNER JOIN '+ @LNLinkedServer + '.dbo.ttcmcs065'+ @LNCompanyId + ' AS B 
					   ON B.[t_cadr] = A.[t_ladr] WHERE C.[t_dimx] IS NOT NULL AND C.[t_dtyp] = 1 ORDER BY B.[t_cadr], B.[t_cwoc], B.[t_dsca]'
	  END
	EXEC (@SqlQuery)
END
-------------------
--15
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_CHECK_STOCK_NUMBER_MATERIAL_PLANNER]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_CHECK_STOCK_NUMBER_MATERIAL_PLANNER]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER Procedure [dbo].[SP_PCR_CHECK_STOCK_NUMBER_MATERIAL_PLANNER]
AS
BEGIN 
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @SQlQuery NVARCHAR(MAX) 
	DECLARE @SQlQuery1 NVARCHAR(MAX) 
	DECLARE @SQlQuery2 NVARCHAR(MAX) 

	DECLARE @Collation NVARCHAR(100) = ''
	SET @Collation = ' COLLATE ' + 'Latin1_General_100_CS_AS_KS_WS';

	SET @SQlQuery = 
		'SELECT CASE WHEN EXISTS (
			SELECT *
			FROM  ' + @LNLinkedServer  + '.dbo.tltfsc500' + @LNCompanyId + ' AS A
			LEFT JOIN [dbo].[PCR503] AS P ON A.[t_sitm]=P.[t_sitm] AND A.[t_stkn]=P.[t_stkn]
			AND A.[t_stkr]=P.[t_stkr]
			WHERE A.[t_cwar] = "SC"
		)
		THEN CAST(0 AS BIT)
		ELSE CAST(1 AS BIT)'
END
---------------------------
--16
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Check_WarehouseLocation]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Check_WarehouseLocation]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE  [dbo].[SP_PCR_Check_WarehouseLocation]
   @Id INT
AS
BEGIN 
	SET NOCOUNT ON
	DECLARE @IsAvailableInINS BIT
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @t_cwar NVARCHAR(100)
	DECLARE @t_sitm NVARCHAR(100)
	DECLARE @t_item NVARCHAR(100)
	DECLARE @t_stkn NVARCHAR(100)
	DECLARE @Total INT
	DECLARE @tbl AS TABLE(RowIndex INT IDENTITY(1,1), t_cwar NVARCHAR(100), t_item NVARCHAR(100), t_stkn NVARCHAR(100))
	DECLARE @R AS TABLE([Value] NVARCHAR(100))
	DECLARE @V NVARCHAR(100)
	DECLARE @Sql NVARCHAR(MAX)
	INSERT INTO @tbl(t_cwar,t_item,t_stkn)
	SELECT
	TOP 1 P0.[t_cwar], P0.[t_item], P0.[t_stkn]
	  FROM
	  [dbo].[PCR500] AS P0 WITH (NOLOCK) INNER JOIN [dbo].[PCR503] AS P ON LTRIM(RTRIM(P0.[t_item])) = LTRIM(RTRIM(P.[t_sitm])) AND P0.[t_stkn] = P.[t_stkn] WHERE P.[Id] = @Id
	  ORDER BY P0.[t_stkr] DESC

    SELECT @Total = COUNT(0) FROM @tbl
	IF(@Total = 0)
	 BEGIN
	   SET @IsAvailableInINS = 0
	 END
	 ELSE
	  BEGIN
	    SELECT 
		       @t_cwar = T.[t_cwar],
		       @t_item = T.[t_item],
			   @t_stkn = T.[t_stkn]			   
	    FROM @tbl AS T WHERE T.RowIndex = 1
		SET @Sql = 
		' SELECT C.[t_loca] FROM ' 
		+ @LNLinkedServer + '.dbo.twhinr140'+ @LNCompanyId + 
		' AS C WHERE C.[t_cwar] = ''' + @t_cwar + ''' AND LTRIM(RTRIM(C.[t_item])) = ''' + 
		LTRIM(RTRIM(@t_item)) + ''' AND C.[t_clot] = ''' + @t_stkn + 
		''' AND C.[t_qhnd] > 0 AND UPPER(LTRIM(RTRIM(C.[t_loca]))) = ''INS'' '
	    
		PRINT(@Sql)
	    INSERT INTO @R
		EXEC(@Sql)
		SELECT @Total = COUNT(0) FROM @R
		IF(@Total > 0)
		BEGIN
		 SET @IsAvailableInINS = 1
		END
		ELSE
		BEGIN
		   SET @IsAvailableInINS = 0
		END
	  END
	  RETURN @IsAvailableInINS
	 
END
-------------------------
--17
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_WorkCenter]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_WorkCenter]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_WorkCenter]
   @WorkCenter NVARCHAR(100),
   @LocationAddress NVARCHAR(100)
AS
BEGIN 
	SET NOCOUNT ON
	
	BEGIN TRY
	     DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	     DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	 
		 DECLARE @Sql NVARCHAR(MAX)
		 DECLARE @ResultTable AS TABLE(WorkCenter NVARCHAR(100))
		 DECLARE @Count INT
		 SET @Sql = ' SELECT [t_cwoc] FROM ' + @LNLinkedServer + '.dbo.ttcmcs065' + @LNCompanyId + ' WHERE [t_cwoc] = ''' + @WorkCenter + ''' AND [t_cadr] = ''' + @LocationAddress + ''''
	     INSERT INTO @ResultTable(WorkCenter)
		  EXEC(@Sql)
		 SELECT @Count = COUNT(0) FROM @ResultTable
		 IF(@Count = 0)
		  BEGIN
		    PRINT 1
		    --RAISERROR('Specified Work Center does not belong to PCR Location. Enter another Work center',12, 1);
		  END
		  SELECT 1
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END
-----------------------------
--18
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_Return_PCR]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_Return_PCR]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_Return_PCR]
   @Id INT
AS
BEGIN 
	DECLARE @Allow BIT = 1
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @PONO INT
	DECLARE @Revn INT
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)
	DECLARE @SerialNumber INT
	DECLARE @Sequence INT
	DECLARE @tbl AS TABLE(RowINDEX INT IDENTITY(1,1), Project NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, Element NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, SerialNumber INT, Sequence INT)
	DECLARE @PCRLineStatusDeletedValue INT
	BEGIN TRY
	   SELECT 
		TOP 1  @PCRLineStatusDeletedValue =
		[Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'DELETE'
	  
	   SELECT 	   
	     @PCRN = P.[t_pcrn],
	     @PONO = P.[t_pono],
	     @Revn = P.[t_revn]
	   FROM 
	    [dbo].[PCR503] AS P 
	   WHERE 
	    P.Id = @Id

		IF EXISTS (SELECT 0 FROM [dbo].[PCR503] AS P WHERE P.[t_pono] = @PONO AND P.[t_pcrn] = @PCRN AND P.[t_revn] <> @Revn AND P.[t_lsta] <> @PCRLineStatusDeletedValue)
		 BEGIN
		   SET @Allow = 0
		 END
		 ELSE
		  BEGIN
		    INSERT INTO @tbl(Project, Element, SerialNumber, [Sequence])
			SELECT 
			P0.[t_cprj], P0.[t_cspa], P0.[t_sern], P0.[t_seqn]
			FROM [dbo].[PCR506] AS P0 WHERE P0.[t_pcrn] = @PCRN AND P0.[t_pono] = @PONO  
			IF EXISTS (
			 SELECT 0 FROM [dbo].[PCR506] AS P0 INNER JOIN @tbl AS T ON T.Project = P0.[t_cprj] COLLATE Latin1_General_100_CS_AS_KS_WS AND T.Element = P0.[t_cspa]  COLLATE Latin1_General_100_CS_AS_KS_WS
			  AND T.[SerialNumber] = P0.[t_sern] AND T.[Sequence] > P0.[t_seqn])
			   BEGIN
			     SET @Allow = 0
			   END
			ELSE
			   BEGIN
			    SET @Allow = 1
			  END
		  END		
		  SET @Allow = 1 --TODO remove after confirmation
		IF(@Allow = 0)
		 BEGIN
		   RAISERROR('Return PCR not allowed', 11, 1)
		 END
		SELECT 
		TOP 1 
		@StockNumber = P.[t_stkn] 
		FROM [dbo].[PCR503] AS P 
		WHERE P.[t_pcrn] = @PCRN AND P.[t_pono] = @PONO AND P.[t_revn] = @Revn
		IF(@StockNumber IS NOT NULL AND LEN(@StockNumber) > 0)
		BEGIN
		 RAISERROR('Please unlink stock first.', 11, 1)
		END
	   SELECT 1
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END
----------------------------------
--19
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_ValidateInner_StockNumber]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_ValidateInner_StockNumber]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_ValidateInner_StockNumber]
   @PSNo NVARCHAR(100),
   @Id INT,   
   @StockNumber NVARCHAR(100),
   @StockRevision INT,
   @UseAlternateStock INT,
   @Location NVARCHAR(100),   
   @Project NVARCHAR(100), 
   @ChildItem NVARCHAR(100),
   @PartNumber INT,
   @Element NVARCHAR(100),
   @PCRN NVARCHAR(100),
   @PONO INT, 
   @Revision INT
		
AS
BEGIN 
	SET NOCOUNT ON
	BEGIN TRY
	     DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	     DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');
		 DECLARE @Sql NVARCHAR(MAX)		 
		 DECLARE @Status INT 
		 DECLARE @RCNO NVARCHAR(100)
		 DECLARE @RCLN INT 
		 DECLARE @Quantity FLOAT
		 DECLARE @PCLN NVARCHAR(100)
		 DECLARE @PSTA INT
		 DECLARE @Item NVARCHAR(100)
		 DECLARE @CLOT NVARCHAR(100)
		 DECLARE @CWAR NVARCHAR(100)
	     DECLARE @ContractTable AS TABLE([Value] NVARCHAR(100))		
		 DECLARE @Contract NVARCHAR(100)
		 DECLARE @OContract NVARCHAR(100)
		 DECLARE @R AS TABLE([CNT] INT)
		 DECLARE @AA AS TABLE([V] INT)
		 DECLARE @VV INT			   
		 DECLARE @CNT INT
		 DECLARE @YesValue INT
		 DECLARE @NoValue INT 
		 DECLARE @S INT
		 DECLARE @Message NVARCHAR(MAX)
		 DECLARE @StatusReleasedValue INT
		 DECLARE @StockPhysicalStatusConsumed INT
		 
		 SELECT
			TOP 1 @YesValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		 WHERE UPPER([LNConstant]) = 'YES'

		SELECT
			TOP 1 @NoValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		WHERE UPPER([LNConstant]) = 'NO'

		 IF(@UseAlternateStock = @NoValue) -- No
		   BEGIN		       
			   SELECT    
			     TOP 1
				  @Status = P0.[t_stat],
				  @RCNO = P0.[t_rcno],
				  @RCLN = P0.[t_rcln],
				  @PCLN = P0.[t_pcln],
				  @Item = LTRIM(RTRIM(P0.[t_item])),
				  @Quantity = P0.[t_qnty],
				  @PSTA = P0.[t_psta],
				  @CLOT = P0.[t_clot],
				  @CWAR = P0.[t_cwar],
				  @Contract = P0.[t_cono]			   
			 FROM 
				[dbo].[PCR500] AS P0 
			 WHERE 
			   P0.[t_loca] = @Location 
			  AND 
			   LTRIM(RTRIM(P0.[t_item])) = @ChildItem 
			  AND 
			   P0.[t_stkn] = @StockNumber 
			  AND 
			   P0.[t_stkr] = @StockRevision

			END
		 ELSE
			BEGIN
			  SELECT    
			     TOP 1
				  @Status = P0.[t_stat],
				  @RCNO = P0.[t_rcno],
				  @RCLN = P0.[t_rcln],
				  @PCLN = P0.[t_pcln],
				  @Item = LTRIM(RTRIM(P0.[t_item])),
				  @Quantity = P0.[t_qnty],
				  @PSTA = P0.[t_psta],
				  @CLOT = P0.[t_clot],
				  @CWAR = P0.[t_cwar],
				  @Contract = P0.[t_cono]
			   /*ltsfc500.stkn, ltsfc500.stat, ltsfc500.rcno, ltsfc500.rcln, ltsfc500.qnty, 
               ltsfc500.cono, ltsfc500.pcln, ltsfc500.psta, ltsfc500.item, ltsfc500.clot, ltsfc500.cwar*/
			 FROM 
				[dbo].[PCR500] AS P0 
			 WHERE
			   P0.[t_stkn] = @StockNumber 
			    AND
			   P0.[t_stkr] = @StockRevision
			    AND
			   P0.[t_loca] = @Location 			  
			   
		 END

		SET @Sql = 
		 ' SELECT 
		   TOP 1 A.[t_cono] 
			FROM ' + @LNLinkedServer + '.dbo.ttpctm110' + @LNCompanyId + ' AS A 
			WHERE A.[t_cprj] = ''' + @Project + ''''
		 
		INSERT INTO @ContractTable([Value])
		EXEC(@Sql)
		 
		SELECT 
			TOP 1 
			@OContract = C.[Value] 
		FROM 
			@ContractTable AS C
	    IF(@Contract <> @OContract)
		  BEGIN
		   -- RAISERROR('PCR Line does not belong to contract mentioned in stock', 12, 1)
		   PRINT 1
		  END

		 
		 SET @Sql = 'SELECT COUNT(0) AS CNT FROM ' + @LNLinkedServer + '.dbo.tltmig601' + @LNCompanyId + ' AS A 
		 WHERE LTRIM(RTRIM(A.[t_eitm])) = ''' + @Item + ''' AND A.[t_elot] = ''' + @CLOT + ''''
		 INSERT INTO @R([CNT])
		 EXEC(@Sql)
		 SELECT @CNT = R.[CNT] FROM @R AS R
		 IF(@CNT = 0)
		   BEGIN
		     IF(@RCNO IS NULL OR LEN(@RCNO) = 0)
			   BEGIN
			     --RAISERROR('Receipt not generated for specified stock', 12, 1)
				 PRINT 2
			   END
			 SET @Sql = 
			   CONCAT(' SELECT 
					TOP 1 A.[t_conf] 
				FROM ' + @LNLinkedServer + '.dbo.twhinh312' + @LNCompanyId + ' AS A 
			    WHERE A.[t_rcln] = ', @RCLN , ' AND A.[t_rcno] = ''' + @RCNO + '''')
			 
			 --DELETE FROM @AA 
			 --INSERT INTO @AA([V])
			 --EXEC(@Sql)
			   
			 --SELECT 
				--TOP 1 @VV = A.[V] 
			 --FROM @AA AS A
			 
			 IF(@VV <> @YesValue) -- Not Yes
			    BEGIN
				 -- RAISERROR('Receipt not Confirmed for specified stock', 12, 1)
				 PRINT 3
			    END
				
			 IF(@Quantity = 0)
				 BEGIN
				   --SET @Message = CONCAT('Receipt', @RCNO, ' for stock number ', @StockNumber , ' is yet to be inspected')
				   --RAISERROR(@Message, 12, 1)
				   PRINT 4
				 END
           END
	  IF(@PCLN IS NOT NULL AND LEN(@PCLN) > 0)
		 BEGIN
			SELECT 
				TOP 1 @S = P.[t_stat]  
			FROM [dbo].[PCR504] AS P 
			WHERE P.[t_pcln] = @PCLN AND P.[t_revn] = @Revision
					
			SELECT
				TOP 1 @StatusReleasedValue = [Value] 
			FROM 
				[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
			WHERE 
				UPPER([LNConstant]) = 'PCL.REL'
					
			IF(@S = @StatusReleasedValue) 
				BEGIN
					SET @Message = CONCAT(
					'PCL ', 
					@PCLN, 
					' already released for stock number ', 
					@StockNumber, 
					' and revision', 
					@Revision)
					RAISERROR(@Message, 12, 1)
				END
		 END
				  
      SELECT
		TOP 1 @StockPhysicalStatusConsumed = [Value] 
	  FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('Stock Physical Status','ltsfc.p.stat') 
	  WHERE 
		UPPER([LNConstant]) = 'CONSUMED'
	  IF(@PSTA = @StockPhysicalStatusConsumed)
		BEGIN
			SET @Message = CONCAT('Stock number ', @StockNumber, ' and revision', @Revision, ' is already consumed')
			RAISERROR(@Message, 12, 1)
		END
		 
	  DELETE FROM @R
	  SET @Sql = 
				' SELECT 
					COUNT(0) AS CNT 
					FROM ' + @LNLinkedServer + '.dbo.twhinr140' + @LNCompanyId +
					' AS A 
					WHERE 
						A.[t_cwar] = ''' + @CWAR + ''' AND 
						LTRIM(RTRIM( A.[t_item])) = ''' + @Item + ''' AND
					   ( A.[t_clot] = ''' + @StockNumber + ''' OR A.[t_clot] = ''' + @StockNumber + ''')'
	  INSERT INTO @R([CNT])
	  EXEC(@Sql)
		 
	  SELECT @CNT = R.[CNT] FROM @R AS R 
		 
	  IF(@CNT = 0)
		 BEGIN
			--RAISERROR('Mismatch in stock inventory and actual stock point inventory', 12, 1)
			PRINT 5
		 END
		  
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END
----------------------
--20

GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_StockNumber]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_StockNumber]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_StockNumber]
   @PSNo NVARCHAR(100),
   @Id INT,   
   @StockNumber NVARCHAR(100),
   @StockRevision INT,
   @UseAlternateStock INT
AS
BEGIN 
	SET NOCOUNT ON
	 BEGIN TRY
	    DECLARE @Location NVARCHAR(100)
		DECLARE @Contract NVARCHAR(100)
	    DECLARE @Project NVARCHAR(100) 
		DECLARE @ChildItem NVARCHAR(100)
		DECLARE @PartNumber INT
		DECLARE @Element NVARCHAR(100)
		DECLARE @PCRN NVARCHAR(100)
		DECLARE @PONO INT, @Revision INT
		DECLARE @PCRCreatedDate DATETIME
		DECLARE @PrevPCRN NVARCHAR(100)
		DECLARE @PrevPONO INT
		DECLARE @PrevRevision INT
		DECLARE @Message NVARCHAR(MAX)
		DECLARE @YesValue INT
		DECLARE @NoValue INT
		DECLARE @InvalidMessage NVARCHAR(MAX) = 'Specified Stock number is invalid. Enter another Stock number'
		DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	    DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');
		DECLARE @Sql NVARCHAR(MAX)		 
		 
	 
		SELECT
			TOP 1 @YesValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		WHERE UPPER([LNConstant]) = 'YES'

		SELECT
			TOP 1 @NoValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		WHERE UPPER([LNConstant]) = 'NO'
		
		
		
		SELECT
			 @Project = P.[t_cprj],
			 @Element = P.[t_cspa],
			 @ChildItem = LTRIM(RTRIM(P.[t_sitm])),
			 @PartNumber = P.[t_prtn],			 
			 @PCRN = P.[t_pcrn],
			 @PONO = P.[t_pono],
			 @Revision = P.[t_revn],
			 @PCRCreatedDate = P.[t_pcr_crdt]
		 FROM 
			[dbo].[PCR503] AS P
		 WHERE 
			P.[Id] = @Id

		SELECT 
			TOP 1 @Location = P.[t_loca]
		FROM 
			[dbo].[PCR501] AS P
		WHERE 
			P.[t_pcrn] = @PCRN

		IF(@StockNumber IS NULL OR LEN(@StockNumber) = 0)
		  BEGIN
		   RAISERROR('Please select stock number', 12, 1);
		  END
		  IF EXISTS(
			SELECT 
				0 
			FROM 
				[dbo].[PCR503] AS P
			WHERE 
				P.[t_stkn] = @StockNumber 
					AND 
				P.[t_stkr] = @StockRevision 
					AND 
				P.[t_lsta] <> 10
				    AND
				P.[Id] <> @Id
				)
				BEGIN
					RAISERROR('The selected stock number and revision are used by some other PCL', 12, 1)
				END
		 IF EXISTS
		 (
			SELECT 
				0
			FROM 
				[dbo].[PCR500] AS P 
			WHERE 
				P.[t_item] = @ChildItem 
					AND 
				P.[t_stkn] = @StockNumber 
					AND 
				P.[t_stkr] = @StockRevision
					AND 
				P.[t_cwar] LIKE '%SC%'
		  )
		   BEGIN
		    RAISERROR('Selected Stock Number has subcontracting warehouse. Enter another stock Number', 12,1)
		   END
		 DECLARE @StatusTable AS TABLE([LNConstant] NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
		
		 INSERT INTO 
				@StatusTable
				(
					[LNConstant], 
					[Text], 
					[Value]
				)
			SELECT 
				Enums.[LNConstant], 
				Enums.[Text], 
				Enums.[Value] 
			FROM 
				[dbo].[PCR000] AS Enums 
			WHERE 
				UPPER(Enums.[EnumName]) = UPPER('PCR Line Status')
				AND
				UPPER(Enums.[LNConstant]) IN 
				(
					UPPER('created'), 
					UPPER('rel.mpln'), 
					UPPER('rel.nest')					
				)

		 SELECT 
		   @PrevPCRN = P.[t_pcrn], 
		   @PrevPONO = P.[t_pono] ,
		   @PrevRevision = P.[t_revn]
		 FROM 
		 [dbo].[PCR503] AS P
		 
		 WHERE 
		   P.[t_cprj] = @Project 
			AND 
		   P.[t_cspa] = @Element
			AND 
		   P.[t_prtn] = @PartNumber 
		    AND 
		   LTRIM(RTRIM(P.[t_sitm])) = @ChildItem
			AND  
		   P.[t_lsta] IN (SELECT S.[Value] FROM @StatusTable AS S)
			AND 
			(P.[t_pcrn] <> @PCRN OR P.[t_pono] <> @PONO OR P.[t_revn] <> @Revision)
			AND 
		  (P.[t_stkn] IS NULL OR LEN(P.[t_stkn]) = 0)
			AND 
		   P.[t_pcr_crdt] < @PCRCreatedDate
		    
		 IF (@PrevPCRN IS NOT NULL AND @PrevPONO IS NOT NULL AND @PrevRevision IS  NOT NULL)
  		  BEGIN
		    IF(@PrevPCRN <> @PCRN OR @PrevPONO <> @PONO OR @PrevRevision <> @Revision)
		      BEGIN
		        SET @Message = CONCAT('Please Proceed previous PCR Number: ' , @PCRN , 
			    ', PCR Line: ', @PrevPONO, ' and Revision: ', @PrevRevision)
		        RAISERROR(@Message, 12, 1)
			  END
		  END
		  IF(@UseAlternateStock = @NoValue) -- No
		   BEGIN
		     IF NOT EXISTS(
			 SELECT 0 
			 FROM [dbo].[PCR500] AS P0 
			 WHERE
				P0.[t_loca] = @Location 
					AND 
				LTRIM(RTRIM(P0.[t_item])) = @ChildItem 
					AND 
				P0.[t_stkn] = @StockNumber 
					AND 
				P0.[t_stkr] = @StockRevision)
			  BEGIN
				 --RAISERROR(@InvalidMessage, 12, 1)
				 PRINT 1
			  END
			 ELSE
			    BEGIN
			     EXEC [dbo].[SP_PCR_ValidateInner_StockNumber]
				   @PSNo = @PSNo,
				   @Id = @Id,
				   @StockNumber = @StockNumber,
				   @StockRevision = @StockRevision,
				   @UseAlternateStock = @UseAlternateStock,
				   @Location = @Location,
				   @Project = @Project, 
				   @ChildItem = @ChildItem,
				   @PartNumber = @PartNumber,
				   @Element = @Element,
				   @PCRN = @PCRN,
				   @PONO = @PONO, 
				   @Revision = @Revision     
			   END
		    END
		  ELSE
		    BEGIN
		      IF NOT EXISTS(
				SELECT 0 
				FROM [dbo].[PCR500] AS P0 
				WHERE
			     P0.[t_stkn] = @StockNumber 
					AND
				 P0.[t_stkr] = @StockRevision 
					AND
				 P0.[t_loca] = @Location
				 )
	   		     BEGIN
	 			   --RAISERROR(@InvalidMessage, 12, 1)
				   PRINT 1
			     END
              ELSE
			   BEGIN
			     EXEC [dbo].[SP_PCR_ValidateInner_StockNumber]
				   @PSNo = @PSNo,
				   @Id = @Id,
				   @StockNumber = @StockNumber,
				   @StockRevision = @StockRevision,
				   @UseAlternateStock = @UseAlternateStock,
				   @Location = @Location,
				   @Project = @Project, 
				   @ChildItem = @ChildItem,
				   @PartNumber = @PartNumber,
				   @Element = @Element,
				   @PCRN = @PCRN,
				   @PONO = @PONO, 
				   @Revision = @Revision
				 
				 DECLARE @OtherItem NVARCHAR(100)
				 DECLARE @CItem NVARCHAR(100)
				 DECLARE @OtherItemMatGrade NVARCHAR(100)
				 DECLARE @CItemMatGrade NVARCHAR(100)				   
				 DECLARE @T AS TABLE([Value] NVARCHAR(1000))
				 SET @OtherItem = @ChildItem
				 SELECT 
					TOP 1 @CItem = LTRIM(RTRIM(P.[t_item]))
					FROM [dbo].[PCR500] AS P 
					WHERE 
						P.[t_stkn] = @StockNumber 
							AND 
						P.[t_stkr] = @StockRevision
				 SET @Sql = ' SELECT A.[t_dscb] FROM ' + @LNLinkedServer + 
				 '.dbo.ttcibd001' + @LNCompanyId + ' AS A WHERE LTRIM(RTRIM(A.[t_item]))= ''' + @CItem + '''' 
				 
				 INSERT INTO @T([Value])
				 EXEC(@Sql)
				 
				 SELECT 
					TOP 1 @CItemMatGrade = T.[Value] 
				 FROM @T AS T
				 
				 DELETE FROM @T
				 
				 SET @Sql = ' SELECT A.[t_dscb] FROM ' + @LNLinkedServer + 
				 '.dbo.ttcibd001' + @LNCompanyId + ' AS A WHERE LTRIM(RTRIM(A.[t_item]))= ''' + @OtherItem + '''' 
				 
				 INSERT INTO @T([Value])
				 EXEC(@Sql)
				 
				 SELECT 
					TOP 1 @OtherItemMatGrade = T.[Value] 
				  FROM @T AS T
				 
				 IF(@OtherItemMatGrade <> @CItemMatGrade)
				   BEGIN
					 RAISERROR('The material grade is different. Do you want to continue ?', 11, 1)
				   END				
			     
			   END
			END

			DECLARE @RSBY NVARCHAR(100)
		    DECLARE @RSFP INT				   
		    SELECT 
				@RSBY = P0.[t_rsby], 
				@RSFP = P0.[t_rsfp] 
			FROM 
				[dbo].[PCR500] AS P0 
		    WHERE  
				LTRIM(RTRIM(P0.[t_item])) = @ChildItem
				    AND
				P0.[t_stkn] = @StockNumber 
					AND
				P0.[t_stkr] = @StockRevision 
				
			IF(@RSFP = @YesValue) --Yes
			  BEGIN
				SET @Message = CONCAT('Stock number is reserved by user ', @RSBY)
				RAISERROR(@Message, 12, 1)
			  END
		SELECT 1 AS [Status], NULL AS [ErrorMessage]
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		IF(@ErrorSeverity = 11) --> Warning for showing dialog box
		  BEGIN
		   SELECT 0 AS [Status], @ErrorMessage AS [MESSAGE]
		  END
		 ELSE --> 11 means error
		  BEGIN		    
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		  END 
	 END CATCH
END
-----------------------------------
--21
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Unlink_Stock]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Unlink_Stock]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Unlink_Stock]
   @Id INT,
   @PSNo NVARCHAR(100)
AS
BEGIN 
    DECLARE @PCRN NVARCHAR(100)
	DECLARE @PCLN NVARCHAR(100)
	DECLARE @ARCS FLOAT
	DECLARE @ChildItem NVARCHAR(100)
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @STKR INT
	DECLARE @Pono INT
	DECLARE @Revn INT
	DECLARE @RecordCount INT
	DECLARE @Area FLOAT
	DECLARE @StockArea FLOAT
	DECLARE @IsDeleted BIT = 0
	DECLARE @ReleasedValue INT
	DECLARE @Allow BIT = 1
	DECLARE @ErrMessage NVARCHAR(100) = ''
	DECLARE @CompleteErrorMessage NVARCHAR(100)
	BEGIN TRAN
	 BEGIN TRY
	  
	   SELECT
	      @PCLN = P.[t_pcln],
		  @ARCS = P.[t_arcs],
		  @ChildItem = P.[t_sitm],
		  @STKR = P.[t_stkr],
		  @StockNumber = P.[t_stkn],
		  @PCRN = P.[t_pcrn],
		  @Pono = P.[t_pono] ,
		  @Revn = P.[t_revn]
	     FROM [dbo].[PCR503] AS P WHERE P.[Id] = @Id
	  
	  IF(@StockNumber IS NULL OR LEN(@StockNumber) = 0)
	   BEGIN
	    SET @Allow = 0
		SET @ErrMessage = 'Stock number does not exist'
	   END
	  ELSE -- Stock number exists
	   BEGIN
	     IF(@PCLN IS NULL OR LEN(@PCLN) = 0)
		  BEGIN
		   SET @Allow = 1
		  END
		  ELSE -- PCLN exists
		   BEGIN
		    SELECT @RecordCount = COUNT(0) FROM [dbo].[PCR504] AS PP WHERE PP.[t_pcln] = @PCLN
		    IF @RecordCount = 0
			  BEGIN
			   SET @Allow = 1
			  END
			ELSE
			  BEGIN
			   SET @Allow = 0
			   SET @ErrMessage = 'PCL is either generated or released'
			  END
		   END
	   END
	  --SET @Allow = 1 --TODO remove after validating
	  IF(@Allow = 0)
	   BEGIN
	      SET @CompleteErrorMessage = 'Unlink stock is not allowed'
		  SET @CompleteErrorMessage = CASE WHEN LEN(@ErrMessage) > 0 THEN @CompleteErrorMessage + ' because ' + @ErrMessage ELSE @CompleteErrorMessage END
	      RAISERROR(@CompleteErrorMessage , 11, 1)
	   END
	  
	  SELECT
	    TOP 1 @ReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'

	   
	   SELECT TOP 1 @Area = P.[t_area]
	   FROM [dbo].[PCR504] AS P WHERE P.[t_pcln] = @PCLN
	   IF(@PCLN IS NOT NULL AND LEN(@PCLN) > 0)
	    BEGIN
		  DELETE FROM [dbo].[PCR505] WHERE [t_pcln] = @PCLN
	      IF(@ARCS = @Area) 
		   BEGIN
		      DELETE FROM [dbo].[PCR504] WHERE [t_pcln] = @PCLN
			  SET @IsDeleted = 1
		   END
		  ELSE
			 BEGIN
			    SET @StockArea = 0
				SELECT TOP 1 @StockArea = P.[t_area]
				 FROM [dbo].[PCR500] AS P WHERE P.[t_pcln] = @PCLN

				 UPDATE
				 [dbo].[PCR504] 
				  SET 
				   [t_area] = @Area - @ARCS,
				   [t_rtar] = @StockArea - @Area,
				   [t_cwoc] = NULL,
				   [t_sqty] = 0,
				   [t_emno] = NULL,
				   [t_rbal] = 0,
				   [t_tclt] = 0,
				   [t_mvtp] = NULL
				 WHERE [t_pcln] = @PCLN
			 END
	     END
		 UPDATE [dbo].[PCR500]
		  SET 
		   [t_qall] = [t_qall] - @ARCS,
		   [t_qfre] = [t_qfre] + @ARCS,
		   [t_stat] = CASE WHEN @IsDeleted = 1 THEN 1 ELSE [t_stat] END,
		   [t_pcln] = CASE WHEN @IsDeleted = 1 THEN NULL ELSE [t_pcln] END
         WHERE [t_item] = @ChildItem AND [t_stkn] = @StockNumber AND [t_stkr] = @STKR

		 UPDATE [dbo].[PCR503]
		  SET 
		   [t_stkn] = NULL,
		   [t_nqty] = 0,
		   [t_arcs] = 0,
		   [t_lsta] = @ReleasedValue,
		   [t_pcln] = NULL,
		   [t_pcl_crdt] = 0,
		   [t_pcl_rldt] = 0

		  WHERE [t_pcrn]= @PCRN AND [t_pono] = @PONO AND [t_revn] = @Revn

		 COMMIT TRAN						
	  SELECT 1
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END
-------------------------------
--22
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Return_PCR]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Return_PCR]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Return_PCR]
   @Id INT,
   @PSNo NVARCHAR(100),
   @Remarks NVARCHAR(MAX)
AS
BEGIN 
    DECLARE @LineCount INT = 0 
    DECLARE @CurrentDate DATETIME
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)	
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @Pono INT
	DECLARE @Revn INT
	DECLARE @Init NVARCHAR(100)
	DECLARE @Sql NVARCHAR(MAX)
	DECLARE @FrequencyTable AS TABLE([Value] INT) 
	DECLARE @Frequency INT
	DECLARE @TotalConsumed INT	
	DECLARE @SerialNumber INT
	DECLARE @SequenceNumber INT
	DECLARE @Diff INT
	DECLARE @Id2 INT
	DECLARE @Tbl911 AS TABLE([Id] INT, [t_wid] FLOAT, [t_leng] FLOAT, [t_noun] FLOAT)
	DECLARE @PCLN NVARCHAR(100)
	DECLARE @CWOC NVARCHAR(100)
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)

	BEGIN TRAN
	 BEGIN TRY
	  SET @CurrentDate = GETUTCDATE()  
	  IF(@Remarks IS NULL)
	   BEGIN
	     RAISERROR('Return Remark is blank',11,1)
	   END
        SELECT 
		TOP 1 
		 @Init = P0.[t_init], 
		 @PCRN = P0.[t_pcrn] ,
		 @Pono = P.[t_pono],
		 @Revn = P.[t_revn]
		FROM 
		 [dbo].[PCR501] AS P0 
		   INNER JOIN 
	     [dbo].[PCR503] AS P ON P.[t_pcrn] = P0.[t_pcrn]  
		WHERE P.[Id] = @Id

		SELECT
		 TOP 1
		   @Project = P.[t_cprj],
		   @Element = P.[t_cspa],
		   @SerialNumber = P.[t_sern],
		   @SequenceNumber = P.[t_seqn],
		   @Id2 = P.[Id]
		 FROM 
			[dbo].[PCR506] AS P 
		 WHERE 
			P.[t_pcrn] = @PCRN AND P.[t_pono] = @PONO
		 SELECT @LineCount = COUNT(0) FROM [dbo].[PCR503] WHERE [t_pcrn] = @PCRN
		 SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer')
	     SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId')
		  SET @Sql =
						'DECLARE @Proj NVARCHAR(100) = ' + '''' + @Project + '''
						 DECLARE @Elem NVARCHAR(100) = ' + '''' + @Element + '''
						 EXEC [dbo].[SP_PCR_GET_FREQUENCY]  @Proj, @Elem, ''' + @LNLinkedServer + '''' + ',' + '''' + @LNCompanyId + ''''
					DELETE FROM @FrequencyTable
					INSERT INTO @FrequencyTable EXEC(@Sql)
					SELECT @Frequency = [Value] FROM @FrequencyTable	
					SELECT @TotalConsumed = SUM(P.[t_npcs_m])
					FROM [dbo].[PCR506] AS P WHERE 
					 P.[t_cprj] = @Project AND P.[t_cspa] = @Element AND P.[t_sern] = @SerialNumber
					  AND
					 P.[t_pcrn] IS NOT NULL AND P.[t_seqn] <>  @SequenceNumber
					
					  SET @Sql = ' SELECT TOP 1 ' 
					  SET @Sql = CONCAT(@Sql, @Id2)
					  SET @Sql = CONCAT(@Sql, ', tbl911.[t_leng], tbl911.[t_widt], tbl911.[t_noun]')
					  SET @Sql = CONCAT(@Sql, ' FROM ' + @LNLinkedServer + '.dbo.tltibd911' + @LNCompanyId + ' AS tbl911 WITH (NOLOCK) ')
					  SET @SQl = CONCAT(@Sql, 
					  ' WHERE tbl911.[t_cprj] = ''' + @Project + ''' AND tbl911.[t_cspa] = ''' +  @Element + ''' AND ')
					  SET @Sql = CONCAT(@Sql, 'CONVERT(NVARCHAR(100), tbl911.[t_sern]) = '''+ CONVERT(NVARCHAR(100), @SerialNumber) + '''')
					  INSERT INTO @Tbl911
					  EXEC(@Sql)

					  UPDATE P
					    SET 
						P.[t_leng] = T.[t_leng],
						P.[t_widt] = T.[t_wid],
						P.[t_npcs] = T.[t_noun] * @Frequency,
						P.[t_leng_m] = T.[t_leng],
						P.[t_widt_m] = T.[t_leng],
						P.[t_npcs_m] = CASE WHEN P.[t_npcs] > @TotalConsumed THEN   P.[t_npcs] - @TotalConsumed  ELSE 0 END,
						P.[t_pcrn] = NULL,
						P.[t_pono] = 0,
						P.[t_rtby] = @PSNo,
						P.[t_rtdt] = GETUTCDATE(),
						P.[t_rrmk] = @Remarks   
					  FROM [dbo].[PCR506] AS P
					  JOIN @Tbl911 AS T ON T.[Id] = P.[Id]
					  WHERE P.[t_pcrn] = @PCRN AND P.[t_pono] = @PONO

					  DELETE FROM [dbo].[PCR506] 
					  WHERE [t_cprj] = @Project 
					  AND [t_cspa] = @Element 
					  AND [t_sern] = @SerialNumber 
					  AND [t_seqn] <> @SequenceNumber 
					  AND [t_pcrn] IS NULL
					  
					  DELETE FROM [dbo].[PCR503] 
					  WHERE [t_pcrn] = @PCRN
					  AND [t_pono]= @PONO
					  AND [t_revn] = @Revn
					  
					  DELETE FROM [dbo].[PCR501] WHERE [t_pcrn] = @PCRN AND @LineCount <= 1

		 COMMIT TRAN						
	  SELECT 1
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END
------------------------------
--23
GO
 IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_Generate_PCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_Generate_PCL]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_Generate_PCL]
   @Id INT
AS
BEGIN 
	SET NOCOUNT ON
	DECLARE @IsWarehouseInINS BIT = 0
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @LastPCRDate DATETIME
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)
	DECLARE @ChildItem NVARCHAR(100)
	DECLARE @PartNumber NVARCHAR(100)
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @PCRDATE DATETIME
	DECLARE @PONO INT
	DECLARE @Revn INT
	DECLARE @NQty INT
	DECLARE @ARCS FLOAT
	DECLARE @PrevPCRN NVARCHAR(100)
	DECLARE @PrevPONO INT
	DECLARE @PrevREVN INT
	DECLARE @MSG NVARCHAR(1000)
	DECLARE @Index INT
	DECLARE @Total INT
	DECLARE @PCRReleasedValue INT
	DECLARE @PCRDeletedValue INT
	DECLARE @Tbl AS TABLE(RowIndex INT IDENTITY(1,1), [t_arcs] FLOAT, [t_nqty] INT, [t_pcrn] NVARCHAR(100), [t_pono] INT)	
	BEGIN TRY
	 
	 EXECUTE @IsWarehouseInINS = [dbo].[SP_PCR_Check_WarehouseLocation] @Id
	 
	 IF(@IsWarehouseInINS = 1)
	  BEGIN
	    RAISERROR('Inventory is available in Location : INS, So you cant generate PCL', 11, 1)
	  END

	 SELECT @StockNumber = P.[t_stkn] FROM [dbo].[PCR503] AS P WHERE P.[Id] = @Id
	 IF(@StockNumber IS NULL OR LEN(@StockNumber) = 0)
	  BEGIN
	   RAISERROR('Stock Number should not be blank for selected line', 11, 1)
	  END
	  SELECT
	    	TOP 1 @PCRReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'
      SELECT
	    	TOP 1 @PCRDeletedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'DELETE'

	  INSERT INTO @Tbl([t_arcs], [t_nqty], [t_pcrn], [t_pono])
	  SELECT
	   P.[t_arcs], P.[t_nqty], P.[t_pcrn], [t_pono] 
	   FROM [dbo].[PCR503] AS P
	   WHERE P.[t_lsta] = @PCRReleasedValue
	    AND P.[t_stkn] = @StockNumber
      SET @Index = 1
	  SELECT @Total = COUNT(0) FROM @Tbl
	  WHILE(@Index <= @Total)
	   BEGIN
	     SELECT 
		  @NQty = ISNULL(T.[t_nqty], 0),
		  @PCRN = T.[t_pcrn],
		  @ARCS = ISNULL(T.[t_arcs], 0),
		  @PONO = T.[t_pono]
		  FROM @Tbl AS T WHERE T.[RowIndex] = @Index
		 IF(@NQty = 0)
		  BEGIN
		   SET @MSG = 'Nested Quantity for PCR Number '
		   SET @MSG = CONCAT(@MSG, @PCRN)
		   SET @MSG  = CONCAT(@MSG, ' and PCR Line ')
		   SET @MSG  = CONCAT(@MSG, @PONO)
		   SET @MSG  = CONCAT(@MSG, ' is zero ')
		   BREAK;
		  END
		 IF(@ARCS = 0)
		   BEGIN
		      SET @MSG = 'Area Consumed for PCR Number '
			  SET @MSG = CONCAT(@MSG, @PCRN)
			  SET @MSG  = CONCAT(@MSG, ' and PCR Line ')
			  SET @MSG  = CONCAT(@MSG, @PONO)
			  SET @MSG  = CONCAT(@MSG, ' is zero ')
		      BREAK;
		    END
		    SET @Index += 1
	    END		
	 
        IF(@Total = 0)
	      BEGIN
	        SET @MSG = 'No Records found to generate PCL'
	      END
	     IF(@MSG IS NOT NULL)
	      BEGIN
		    RAISERROR(@MSG, 11, 1)
		  END

		  SELECT
		 
		    @Project = P.[t_cprj], 
		    @Element = P.[t_cspa],
		    @ChildItem = P.[t_sitm],
		    @PCRN = P.[t_pcrn], 
		    @PONO = P.[t_pono], 
		    @Revn = P.[t_revn],
		    @PCRDATE = P.[t_pcr_crdt],
		    @PartNumber = P.[t_prtn]

		     FROM [dbo].[PCR503] AS P
		     WHERE P.[Id] = @Id

		     IF EXISTS(
					SELECT 
						P.[t_pcr_crdt] 
					FROM 
						[dbo].[PCR503] AS P 
					WHERE P.[t_cprj] = @Project AND P.[t_cspa] = @Element AND P.[t_prtn] = @PartNumber AND P.[t_stkn] IS NOT NULL
		      )
		        BEGIN
		           SELECT TOP 1 @LastPCRDate = P.[t_pcr_crdt] 
		           FROM [dbo].[PCR503] AS P WHERE P.[t_cprj] = @Project AND P.[t_cspa] = @Element AND P.[t_prtn] = @PartNumber AND P.[t_stkn] IS NOT NULL
			       ORDER BY P.[t_pcr_crdt] DESC
		        END
		     ELSE
		       BEGIN
		        SELECT @LastPCRDate = @PCRDATE
		     END
		 
		
		    SELECT       
			   TOP 1 
				@PrevPCRN = P.[t_pcrn],
				@PrevPONO = P.[t_pono],
				@PrevREVN = P.[t_revn]
			FROM        
			  [dbo].[PCR503] AS P
			WHERE
				P.[t_cprj] =  @Project
				 AND        
				P.[t_sitm] =  @ChildItem
				 AND        
				P.[t_prtn] =  @PartNumber
				 AND
				P.[t_cspa] = @Element
				 AND
				 (
				   P.[t_pcrn] <> @PCRN
						OR
					P.[t_pono] <> @PONO
						OR
					P.[t_revn] <> @Revn
				 )
				 AND
    			CAST(P.[t_pcr_crdt] AS DATE) < CAST(@LastPCRDate AS DATE)
		  		 AND
	    		P.[t_stkn] IS NULL
				 AND
			    P.[t_lsta] <> @PCRDeletedValue 
 
          IF(@PrevPCRN IS NOT NULL AND @PrevPONO IS NOT NULL AND @PrevPONO IS NOT NULL)
	       BEGIN
	          SET @MSG = 'Please proceed previous PCR No.: '
		      SET @MSG = CONCAT(@MSG, @PrevPCRN)
		      SET @MSG = CONCAT(@MSG, ', PCR Line: ')
		      SET @MSG = CONCAT(@MSG, @PrevPONO)
		      SET @MSG = CONCAT(@MSG, 'and Revision: ')
		      SET @MSG = CONCAT(@MSG, @PrevREVN)
              RAISERROR(@MSG, 11, 1)
	       END

	      SELECT 1
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END
------------------------------
--24
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Generate_PCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Generate_PCL] 
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE  [dbo].[SP_PCR_Generate_PCL]
   @Id INT,
   @PSNo NVARCHAR(100) 
AS
BEGIN 
    SET NOCOUNT ON
    DECLARE @Sql NVARCHAR(MAX)
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @CurrentDate DATETIME
	Declare @H as Table(RowIndex INT IDENTITY(1,1),[t_pcln] NVARCHAR(100),[t_sitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,[t_stkn] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,[t_stkr] INT,[t_ityp] INT)
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @PCLN NVARCHAR(100)
	DECLARE @CWOC NVARCHAR(100)
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)
	DECLARE @ValueTable Table ([Value] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS)
	DECLARE @IPCL NVARCHAR(100)
	DECLARE @LOC NVARCHAR(100)
	DECLARE @PCRReleasedValue INT
	DECLARE @PCLGeneratedValue INT
	DECLARE @PipeTypeValue INT
	DECLARE @MovementTypeValue INT
	DECLARE @StockStatusAllocatedValue INT
	BEGIN TRAN
	 BEGIN TRY
	  SET @CurrentDate = GETUTCDATE() 
	  
	   SELECT
	    	TOP 1 @PCRReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'
	   SELECT
	    	TOP 1 @PCLGeneratedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'PCL.GEN'
	 
	    SELECT
	    	TOP 1 @PipeTypeValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PIPE'
	 
	   SELECT
	    	TOP 1 @MovementTypeValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Movement Type','ltmov.type') WHERE UPPER([LNConstant]) = 'PROFILE.CUT'

	   SELECT
	    	TOP 1 @StockStatusAllocatedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Stock Status','ltsfc.stk.stat') WHERE UPPER([LNConstant]) = 'ALLOCATED'
	 
	  SELECT 
	    @StockNumber = P.[t_stkn], 
	    @PCRN = P.[t_pcrn] 
	   FROM [dbo].[PCR503] AS P WHERE P.Id = @Id
	   SELECT
	    @LOC = P.[t_loca]
		FROM [dbo].[PCR501] AS P WHERE P.[t_pcrn] = @PCRN
	  
	  SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer')
	  SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId')
	  SET @Sql = 'SELECT TOP 1 [t_ipcl] FROM ' + @LNLinkedServer + '.dbo.tltlnt000' + @LNCompanyId + ' WHERE [t_dimx] = ''' + @LOC + ''''
	  INSERT INTO @ValueTable
	  EXEC(@Sql)
	  SELECT TOP 1 @IPCL = [Value] FROM @ValueTable
	  --print @ipcl
	  --print @stocknumber
	  UPDATE [dbo].[PCR503] SET
	  [t_lsta] = @PCLGeneratedValue,
	  [t_pcln] = CONCAT(CONCAT(@IPCL,LTRIM(RTRIM(@StockNumber))),LTRIM(RTRIM(ISNULL([t_stkr], 0)))) ,
	  [t_pcl_crdt] = @CurrentDate
	  WHERE [t_stkn] = @StockNumber AND [t_lsta] = @PCRReleasedValue 

      INSERT 
	   INTO 
	    @H([t_pcln], [t_sitm], [t_stkn], [t_stkr], [t_ityp])
	  SELECT P.[t_pcln], P.[t_sitm], P.[t_stkn], ISNULL(P.[t_stkr], 0), P.[t_ityp]
        FROM     [dbo].[PCR503] AS P
       WHERE   P.[t_lsta] = @PCLGeneratedValue
        AND     CAST(P.[t_pcl_crdt] AS DATE) = CAST(@CurrentDate AS DATE)
		AND P.[t_pcln] = CONCAT(CONCAT(@IPCL,LTRIM(RTRIM(@StockNumber))),LTRIM(RTRIM(ISNULL([t_stkr], 0)))) 
        GROUP BY P.[t_pcln], P.[t_sitm], P.[t_stkn], P.[t_stkr], P.[t_ityp]
        
		INSERT INTO [dbo].[PCR504](
		[t_pcln], [t_revn], [t_emno], [t_area], [t_rtar], [t_stat], [t_orno], [t_sqty], [t_rbal], 
		[t_pcl_rldt], [t_pmg_cdat], [t_pmg_rdat], [t_qa_adat], [t_qa_rdat], [t_sfc_cdat], [t_plt_idat],  [t_plt_rdat],  [t_plt_mdat], [t_qc_adat], [t_qc_rdat], [t_plt_cdat],
		[t_plt_rtdt], [t_pcl_cndt], [t_pcl_cldt], 
		[t_ityp], [t_mvtp], [t_odds])
		SELECT 
		
		
		  P.[t_pcln], 0, @PSNo, P0.[t_qall], P0.[t_area] - P0.[t_qall], NULL, NULL, 0, 0, 
		  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
		  P.[t_ityp], CASE WHEN  P.[t_ityp] = @PipeTypeValue THEN @MovementTypeValue ELSE NULL END, 0 --TODO check 
		FROM @H AS P
		INNER JOIN [dbo].[PCR500] AS P0 ON 
		LTRIM(RTRIM(P0.[t_item])) = LTRIM(RTRIM(P.[t_sitm])) 
		AND P0.[t_stkn] = P.[t_stkn] 
		AND ISNULL(P0.[t_stkr], 0) = ISNULL(P.[t_stkr], 0)
		
       DELETE FROM @H
	    
	    INSERT INTO @H([t_pcln],[t_sitm],[t_stkn], [t_stkr])
	    SELECT P.[t_pcln], P.[t_sitm], P.[t_stkn], P.[t_stkr]
		 FROM  [dbo].[PCR503] AS P
			WHERE   P.[t_lsta] = @PCLGeneratedValue
		  AND  CAST(P.[t_pcl_crdt] AS DATE) = CAST(@CurrentDate AS DATE)
		  AND P.[t_pcln] = CONCAT(CONCAT(@IPCL, LTRIM(RTRIM(@StockNumber))),LTRIM(RTRIM(ISNULL(P.[t_stkr], 0))))
        GROUP BY P.[t_pcln], P.[t_sitm], P.[t_stkn], P.[t_stkr]

		UPDATE P
		 SET 
		   P.[t_stat] = @StockStatusAllocatedValue , 
		   P.[t_pcln] = H.[t_pcln]
		 FROM
		 [dbo].[PCR500] AS P
		 JOIN @H AS H ON 
		  LTRIM(RTRIM(H.[t_sitm]))  = LTRIM(RTRIM(P.[t_item])) 
		   AND 
		  H.[t_stkn]  = P.[t_stkn] 
		   AND
		  ISNULL(H.[t_stkr], 0) = ISNULL(P.[t_stkr], 0)

		  SELECT 
		    TOP 1
			 @PCLN = P.[t_pcln],
		     @CWOC = P.[t_cwoc]
		  
		   FROM [dbo].[PCR503] AS P 
		   WHERE 
		   CAST(P.[t_pcl_crdt] AS DATE) = CAST(@CurrentDate AS DATE) 
		   AND
		   P.[t_pcln] = CONCAT(CONCAT(@IPCL, LTRIM(RTRIM(@StockNumber))),LTRIM(RTRIM(ISNULL(P.[t_stkr], 0))))
	
	       UPDATE [dbo].[PCR504]
		   SET [t_cwoc] = @CWOC
		   WHERE [t_cwoc] IS NULL AND [t_pcln] = @PCLN

	      COMMIT TRAN						
	  SELECT 1
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(), 
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END
------------------------------
--25

GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_Update_Material_Planner_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_Update_Material_Planner_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

ALTER PROCEDURE [dbo].[SP_PCR_Validate_Update_Material_Planner_Data]
	@UserId NVARCHAR(20),
	@Data XML
AS
BEGIN
 BEGIN TRY
    DECLARE @Id INT
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @StockRevision INT
	DECLARE @ToleranceLimit FLOAT = 0.1
	DECLARE @UseAlternateStock INT
	DECLARE @AreaConsumed FLOAT
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @Revision INT
	DECLARE @Length FLOAT
	DECLARE @Width FLOAT
	DECLARE @PONO INT
	DECLARE @OldAreaConsumed FLOAT
	DECLARE @DArea FLOAT
	DECLARE @DAreaU FLOAT
	DECLARE @DAreaL FLOAT
	DECLARE @ChildItem NVARCHAR(100)
	DECLARE @Denom DECIMAL = 1000000
	DECLARE @OtherItem NVARCHAR(100)
	DECLARE @CItem NVARCHAR(100)
	DECLARE @StockQall FLOAT
	DECLARE @StockArea FLOAT
	DECLARE @ItemTypeValuePipe INT
	DECLARE @ItemType INT
	DECLARE @WCDeliver NVARCHAR(100)
	DECLARE @Message NVARCHAR(1000)
	DECLARE @YesValue INT	 
	DECLARE @NestedQuantity INT
	DECLARE @ApprovedQuantity INT
	DECLARE @RejectedQuantity INT
	DECLARE @IsMain BIT = 1
	SELECT
		TOP 1 @YesValue = [Value] 
	FROM 
	    [dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('Yes No Choice','tppdm.yeno') 
	WHERE 
	   UPPER([LNConstant]) = 'YES'
	DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [PCRRequirementDate] DATETIME, 
		 [ManufacturingItem] NVARCHAR(100), 
		 [ChildItem] NVARCHAR(100), 
		 [NestedQuantity] INT,
		 [AreaConsumed] FLOAT,
		 [LengthModified] FLOAT,
		 [WidthModified] FLOAT, 
		 [StockNumber] NVARCHAR(100),
		 [PCLNumber] NVARCHAR(100),
		 [MovementType] INT,
		 [PCRPlannerRemark] NVARCHAR(100),
		 [StockRevision] INT,
		 [MaterialPlanner] NVARCHAR(100),
		 [GrainOrientation] NVARCHAR(100),
		 [ShapeType] NVARCHAR(100),
		 [ShapeFile] NVARCHAR(100),
		 [WCDeliver] NVARCHAR(100),
		 [UseAlternateStock] INT,
		 [AlternateStockRemark] NVARCHAR(MAX),
		 [CuttingLocationModified] NVARCHAR(100),
		 [ApprovedQuantity] INT,
		 [RejectedQuantity] INT,
		 [IsMain] BIT
	 )
	    
	INSERT INTO @TBL
	(
		 [Id],
		 [PCRRequirementDate], 
		 [ManufacturingItem], 
		 [ChildItem], 
		 [NestedQuantity],
		 [AreaConsumed],
		 [LengthModified],
		 [WidthModified], 
		 [StockNumber],
		 [PCLNumber],
		 [MovementType],
		 [PCRPlannerRemark],
		 [StockRevision],
		 [MaterialPlanner],
		 [GrainOrientation],
		 [ShapeType],
		 [ShapeFile],
		 [WCDeliver],
		 [UseAlternateStock],
		 [AlternateStockRemark],
		 [CuttingLocationModified],
		 [ApprovedQuantity],
		 [RejectedQuantity],
		 [IsMain]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('PCRRequirementDate').value('.', 'DATETIME') AS [PCRRequirementDate], 
		Array.Entity.query('ManufacturingItem').value('.', 'NVARCHAR(100)') AS [ManufacturingItem],  
		Array.Entity.query('ChildItem').value('.', 'NVARCHAR(100)') AS [ChildItem],	
		Array.Entity.query('NestedQuantity').value('.', 'INT') AS [NestedQuantity],	
		Array.Entity.query('AreaConsumed').value('.', 'FLOAT') AS [AreaConsumed],
		Array.Entity.query('LengthModified').value('.', 'FLOAT') AS [LengthModified],
		Array.Entity.query('WidthModified').value('.', 'FLOAT') AS [WidthModified],
		Array.Entity.query('StockNumber').value('.', 'NVARCHAR(100)') AS [StockNumber],	
		Array.Entity.query('PCLNumber').value('.', 'NVARCHAR(100)') AS [PCLNumber],	
		Array.Entity.query('MovementType').value('.', 'INT') AS [MovementType],	
		Array.Entity.query('PCRPlannerRemark').value('.', 'NVARCHAR(100)') AS [PCRPlannerRemark],	
		Array.Entity.query('StockRevision').value('.', 'INT') AS [StockRevision],	
		Array.Entity.query('MaterialPlanner').value('.', 'NVARCHAR(100)') AS [MaterialPlanner],	
		Array.Entity.query('GrainOrientation').value('.', 'NVARCHAR(100)') AS [GrainOrientation],	
		Array.Entity.query('ShapeType').value('.', 'NVARCHAR(100)') AS [ShapeType],	
		Array.Entity.query('ShapeFile').value('.', 'NVARCHAR(100)') AS [ShapeFile],	
		Array.Entity.query('WCDeliver').value('.', 'NVARCHAR(100)') AS [WCDeliver],	
		Array.Entity.query('UseAlternateStock').value('.', 'INT') AS [UseAlternateStock],	
		Array.Entity.query('AlternateStockRemark').value('.', 'NVARCHAR(MAX)') AS [AlternateStock],	
		Array.Entity.query('CuttingLocationModified').value('.', 'NVARCHAR(100)') AS [CuttingLocationModified],
		Array.Entity.query('ApprovedQuantity').value('.', 'INT') AS [ApprovedQuantity],	
		Array.Entity.query('RejectedQuantity').value('.', 'INT') AS [RejectedQuantity],
		Array.Entity.query('IsMain').value('.', 'BIT') AS [IsMain]
	FROM @Data.nodes('/ArrayOfSP_PCR_GET_MATERIAL_PLANNER_DATA_Result/SP_PCR_GET_MATERIAL_PLANNER_DATA_Result') AS Array(Entity)
	
	SELECT 
		TOP 1 @ItemTypeValuePipe = [Value] 
	FROM 
		dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PIPE'
	SELECT 
	 TOP 1 
	     @Id = T.[Id],
		 @UseAlternateStock = T.[UseAlternateStock],  
		 @StockNumber = T.[StockNumber],
		 @StockRevision = ISNULL(T.[StockRevision], 0),		 
		 @AreaConsumed = ROUND(ISNULL(T.[AreaConsumed], 0), 3),
		 @OldAreaConsumed = ROUND(ISNULL(P.[t_arcs], 0), 3),
		 @PCRN = P.[t_pcrn],
		 @PONO = P.[t_pono],
		 @Revision = P.[t_revn],
		 @ItemType = P.[t_ityp],
		 @CItem = P.[t_sitm],
		 @Length = P.[t_leng],
		 @Width = P.[t_widt],
		 @WCDeliver = T.[WCDeliver],
		 @NestedQuantity = CASE WHEN T.[IsMain] = 1 THEN T.[NestedQuantity] ELSE P.[t_nqty] END,
		 @ApprovedQuantity = T.[ApprovedQuantity],
		 @RejectedQuantity = T.[RejectedQuantity],
		 @IsMain = T.[IsMain]
	 FROM @TBL AS T 
	   INNER JOIN [dbo].[PCR503] AS P ON T.[Id] = P.[Id]
	   
	IF(@IsMain = 0)
	  BEGIN	    
	    IF(@NestedQuantity <> @ApprovedQuantity + @RejectedQuantity)
		   BEGIN
		    SET @Message= CONCAT('Total of approved quantity: ', 
					@ApprovedQuantity, 
					' and rejected quantity: ', 
					@RejectedQuantity, 
					' should be equal to nested quantity: ', 
					@NestedQuantity)
		    RAISERROR(@Message, 12, 1)
		   END
	  END
	IF(@IsMain = 1)
	 BEGIN
	   EXEC [dbo].[SP_PCR_Validate_StockNumber]
	     @PSNo = @UserId,
	     @Id = @Id,   
	     @StockNumber = @StockNumber,
	     @StockRevision = @StockRevision,
	     @UseAlternateStock = @UseAlternateStock
	
	  IF(@UseAlternateStock = @YesValue)
	    BEGIN
	     SET @OtherItem =  @CItem  --, @CItem = P.[t_item]  FROM [dbo].PCR500 AS P WHERE P.[t_stkn] = @StockNumber AND P.[t_stkr] = @StockRevision	  
	    END
	
	  EXEC [dbo].[SP_PCR_Validate_WorkCenter] @PCRN, @WCDeliver
     
	  IF(@AreaConsumed = 0)
	    BEGIN
		  RAISERROR('Please enter value for area consumed', 12, 1)
	    END
     
	  IF(@StockArea <= (@AreaConsumed + @StockQall - @OldAreaConsumed))
		BEGIN
		 SET @Message =  'Area Consumed : '
		 SET @Message = CONCAT(@Message, ROUND((@AreaConsumed + @StockQall - @OldAreaConsumed), 3))
		 SET @Message = CONCAT(@Message, ' for stock ')
		 SET @Message = CONCAT(@Message, @StockNumber)
		 SET @Message = CONCAT(@Message, ' in PCR Lines exceed total available area : ')
		 SET @Message = CONCAT(@Message, ROUND(@StockArea, 3))		 
		 RAISERROR(@Message, 12, 1)
		END
      
      IF(@ItemType <> @ItemTypeValuePipe)
	   BEGIN
		 SET @DArea = (@Width * @Length)/@Denom
		 SET @DAreaU = ROUND(@DArea + (@DArea * @ToleranceLimit), 3)
		 SET @DAreaL = ROUND(@DArea - (@DArea * @ToleranceLimit), 3)
		 --select @Width, @Length, @DareaL, @DAreaU
		 IF(@AreaConsumed < @DAreaL OR @AreaConsumed > @DAreaU)
		   BEGIN
			SET @Message = CONCAT('Area maintained : ', @AreaConsumed)
			SET @Message = CONCAT(@Message, ' is out side ')
			SET @Message = CONCAT(@Message, @ToleranceLimit * 100)
			SET @Message = CONCAT(@Message, ' percent tolerance limit of ')
			SET @Message = CONCAT(@Message, ROUND(@DArea, 3))
			SET @Message = CONCAT(@Message, ' (ie from ') 
			SET @Message = CONCAT(@Message, @DAreaL)
			SET @Message = CONCAT(@Message, ' to ')
			SET @Message = CONCAT(@Message, @DAreaU)
			SET @Message = CONCAT(@Message, '). Would you like to proceed?')
		    RAISERROR(@Message, 11, 1)
		   END
		 END
      END
	  SELECT 1 AS [Status], NULL AS [MESSAGE]
  END TRY
  BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(), 
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		IF(@ErrorSeverity = 11) --> Warning for showing dialog box
		  BEGIN
		   SELECT 0 AS [Status], @ErrorMessage AS [MESSAGE]
		  END
		 ELSE --> 11 means error
		  BEGIN
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		  END
   END CATCH 	
END 
------------------------
--26

GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Insert_Update_Material_Planner_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Insert_Update_Material_Planner_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Insert_Update_Material_Planner_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @ChildItem NVARCHAR(100)
	 DECLARE @OtherItem NVARCHAR(100)
	 DECLARE @AreaConsumed FLOAT
	 DECLARE @UseAlternativeStock INT
	 DECLARE @StockNumber NVARCHAR(100)
	 DECLARE @StockRevision INT
	 DECLARE @OldStockNumber NVARCHAR(100)
	 DECLARE @OldStockRevision INT
	 DECLARE @OldAreaConsumed FLOAT
	 DECLARE @Id INT
	 DECLARE @SQall FLOAT
	 DECLARE @SArea FLOAT
	 DECLARE @SQCut FLOAT
	 DECLARE @YesValue INT 
	 DECLARE @AA FLOAT
	 DECLARE @StockArea FLOAT
	 DECLARE @PCLN NVARCHAR(100)
	 DECLARE @ApprovedQuantity INT
	 DECLARE @RejectedQuantity INT
	 DECLARE @IsMain BIT
	 SELECT
		TOP 1 @YesValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		WHERE UPPER([LNConstant]) = 'YES'
	  
     DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [PCRRequirementDate] DATETIME, 
		 [ManufacturingItem] NVARCHAR(100), 
		 [ChildItem] NVARCHAR(100), 
		 [NestedQuantity] INT,
		 [AreaConsumed] FLOAT,
		 --[LengthModified] FLOAT,
		 --[WidthModified] FLOAT, 
		 [StockNumber] NVARCHAR(100),
		 [PCLNumber] NVARCHAR(100),
		 --[MovementType] INT,
		 [PCRPlannerRemark] NVARCHAR(100),
		 [StockRevision] INT,
		 [MaterialPlanner] NVARCHAR(100),
		 --[GrainOrientation] NVARCHAR(100),
		 --[ShapeType] NVARCHAR(100),
		 [ShapeFile] NVARCHAR(100),
		 [WCDeliver] NVARCHAR(100),
		 [UseAlternateStock] INT,
		 [AlternateStockRemark] NVARCHAR(MAX),
		 [CuttingLocationModified] NVARCHAR(100),
		 [ApprovedQuantity] INT,
		 [RejectedQuantity] INT,
		 [IsMain] BIT
	 )
	    
	INSERT INTO @TBL
	(
		 [Id],
		 [PCRRequirementDate], 
		 [ManufacturingItem], 
		 [ChildItem], 
		 [NestedQuantity],
		 [AreaConsumed],
		 --[LengthModified],
		 --[WidthModified], 
		 [StockNumber],
		 [PCLNumber],
		 --[MovementType],
		 [PCRPlannerRemark],
		 [StockRevision],
		 [MaterialPlanner],
		 --[GrainOrientation],
		 --[ShapeType],
		 [ShapeFile],
		 [WCDeliver],
		 [UseAlternateStock],
		 [AlternateStockRemark],
		 [CuttingLocationModified],
		 [ApprovedQuantity],
		 [RejectedQuantity],
		 [IsMain]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('PCRRequirementDate').value('.', 'DATETIME') AS [PCRRequirementDate], 
		Array.Entity.query('ManufacturingItem').value('.', 'NVARCHAR(100)') AS [ManufacturingItem],  
		Array.Entity.query('ChildItem').value('.', 'NVARCHAR(100)') AS [ChildItem],	
		Array.Entity.query('NestedQuantity').value('.', 'INT') AS [NestedQuantity],	
		Array.Entity.query('AreaConsumed').value('.', 'FLOAT') AS [AreaConsumed],
		--Array.Entity.query('LengthModified').value('.', 'FLOAT') AS [LengthModified],
		--Array.Entity.query('WidthModified').value('.', 'FLOAT') AS [WidthModified],
		Array.Entity.query('StockNumber').value('.', 'NVARCHAR(100)') AS [StockNumber],	
		Array.Entity.query('PCLNumber').value('.', 'NVARCHAR(100)') AS [PCLNumber],	
		--Array.Entity.query('MovementType').value('.', 'INT') AS [MovementType],	
		Array.Entity.query('PCRPlannerRemark').value('.', 'NVARCHAR(100)') AS [PCRPlannerRemark],	
		Array.Entity.query('StockRevision').value('.', 'INT') AS [StockRevision],	
		Array.Entity.query('MaterialPlanner').value('.', 'NVARCHAR(100)') AS [MaterialPlanner],	
		--Array.Entity.query('GrainOrientation').value('.', 'NVARCHAR(100)') AS [GrainOrientation],	
		--Array.Entity.query('ShapeType').value('.', 'NVARCHAR(100)') AS [ShapeType],	
		Array.Entity.query('ShapeFile').value('.', 'NVARCHAR(100)') AS [ShapeFile],	
		Array.Entity.query('WCDeliver').value('.', 'NVARCHAR(100)') AS [WCDeliver],	
		Array.Entity.query('UseAlternateStock').value('.', 'INT') AS [UseAlternateStock],	
		Array.Entity.query('AlternateStockRemark').value('.', 'NVARCHAR(MAX)') AS [AlternateStock],	
		Array.Entity.query('CuttingLocationModified').value('.', 'NVARCHAR(100)') AS [CuttingLocationModified],
		Array.Entity.query('ApprovedQuantity').value('.', 'INT') AS [ApprovedQuantity],	
		Array.Entity.query('RejectedQuantity').value('.', 'INT') AS [RejectedQuantity],
		Array.Entity.query('IsMain').value('.', 'BIT') AS [IsMain]
			
	FROM @Data.nodes('/ArrayOfSP_PCR_GET_MATERIAL_PLANNER_DATA_Result/SP_PCR_GET_MATERIAL_PLANNER_DATA_Result') AS Array(Entity)
	
    IF(@Mode = 1) --UPDATE
	  SELECT 
		TOP 1 @IsMain = T.[IsMain] 
	  FROM @TBL AS T 
	 BEGIN
	  IF(@IsMain = 0)
	   BEGIN
	    UPDATE tbl503
		SET 
		  tbl503.[t_iqty] = B.ApprovedQuantity,
		  tbl503.[t_rqty] = B.RejectedQuantity
		FROM [dbo].[PCR503] AS tbl503
		INNER JOIN @TBL AS B ON B.[Id] = tbl503.[Id]
	  END
	  ELSE
	   BEGIN
	    SELECT
	    TOP 1 
		  @UseAlternativeStock = T.[UseAlternateStock],
		  @StockNumber = T.[StockNumber] ,
		  @StockRevision = T.[StockRevision],
		  @AreaConsumed = T.[AreaConsumed],
		  @Id = T.[Id]
	  FROM @TBL AS T
	  
	    SELECT 
	    @OldStockNumber = P.[t_stkn],
		@OldStockRevision = P.[t_stkr],
		@OldAreaConsumed = ISNULL(P.[t_arcs], 0),
		@ChildItem = P.[t_sitm]
	  FROM [dbo].[PCR503] AS P WHERE P.[Id] = @Id

	    IF(@UseAlternativeStock = @YesValue) 
	   BEGIN
	     SELECT 
	     @OtherItem = @ChildItem,
	     @ChildItem = P.[t_item]
	     FROM [dbo].PCR500 AS P WHERE P.[t_stkn] = @StockNumber AND P.[t_stkr] = @StockRevision
	   END
	     UPDATE tbl503
		  SET 
		    tbl503.[t_prdt] = B.PCRRequirementDate,
		   --tbl503.[t_mitm] = B.ManufacturingItem,
		   tbl503.[t_sitm] = CASE WHEN @UseAlternativeStock <> @YesValue THEN tbl503.[t_sitm] ELSE @ChildItem END,
		   tbl503.[t_oitm] = CASE WHEN @UseAlternativeStock <> @YesValue THEN tbl503.[t_oitm] ELSE @OtherItem END,
		   tbl503.[t_nqty] = B.NestedQuantity,
		   tbl503.[t_arcs] = B.AreaConsumed,
		   --tbl503.[t_leng] = B.LengthModified,
		   --tbl503.[t_widt] = B.WidthModified,
		   tbl503.[t_stkn] = B.StockNumber,
		   tbl503.[t_pcln] = B.PCLNumber,
		   --tbl503.[t_mvtp] = B.MovementType,
		   tbl503.[t_prmk] = B.PCRPlannerRemark,
		   tbl503.[t_stkr] = B.StockRevision,
		   tbl503.[t_mrmk] = B.MaterialPlanner,
		   --tbl503.[t_gorn] = B.GrainOrientation,
		   --tbl503.[t_shtp] = B.ShapeType,
		   tbl503.[t_shfl] = B.ShapeFile,
		   tbl503.[t_cwoc] = B.WCDeliver,
		   tbl503.[t_alts] = B.UseAlternateStock,
		   tbl503.[t_armk] = B.AlternateStockRemark--,
		   --tbl503.[t_cloc] = B.CuttingLocationModified
		 FROM [dbo].[PCR503] AS tbl503
		  INNER JOIN @TBL AS B ON B.[Id] = tbl503.[Id]
		
		 IF(@StockNumber <> @OldStockNumber OR @OldStockRevision <> @StockRevision)
		 BEGIN
		   UPDATE [dbo].[PCR500] 
		   SET 
		    [t_qall] = [t_qall] - @OldAreaConsumed,
		    [t_qfre] = [t_qfre] + @OldAreaConsumed
		   WHERE 
		    LTRIM(RTRIM([t_item])) = LTRIM(RTRIM(@ChildItem)) AND [t_stkn] = @OldStockNumber AND [t_stkr] = @OldStockRevision
		 END
		  SELECT 
		 TOP 1
			 @SQall = P.[t_qall] ,
			 @SArea = P.[t_area],
			 @SQCut = P.[t_qcut]
		 FROM [dbo].[PCR500] AS P WHERE LTRIM(RTRIM([t_item])) = LTRIM(RTRIM(@ChildItem)) AND P.[t_stkn] = @StockNumber AND P.[t_stkr] = @StockRevision
		 
		  UPDATE [dbo].[PCR500] 
		   SET 
		    [t_qall] = @SQall + @AreaConsumed - @OldAreaConsumed,
		    [t_qfre] = @SArea - @SQall - @SQCut			  
		   WHERE 
		    LTRIM(RTRIM([t_item])) = LTRIM(RTRIM(@ChildItem)) AND [t_stkn] = @StockNumber AND [t_stkr] = @StockRevision

			SELECT 
				TOP 1 @PCLN = T.PCLNumber 
			FROM @TBL AS T

			SELECT 
			   TOP 1 @StockArea = P.[t_area]
			FROM 
				[dbo].[PCR500] AS P 
			WHERE 
				P.[t_pcln] = @PCLN

			DELETE FROM [dbo].[PCR505] WHERE [t_pcln] = @PCLN
			
			SELECT 
				TOP 1 @AA = P.[t_area] 
			FROM 
				[dbo].[PCR504] AS P 
			WHERE 
				P.[t_pcln] = @PCLN
			
			SET @AA = @AA + @AreaConsumed - @OldAreaConsumed
			
			UPDATE 
				[dbo].[PCR504] 
			SET 
				[t_area] = @AA,
				[t_rtar] = @StockArea - @AA,
				[t_sqty] = 0
			WHERE 
				[t_pcln] = @PCLN
       END
	  COMMIT
	END	 
	
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE() ,
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END
---------------------
--27
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_PlannerNames]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_PlannerNames]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_PlannerNames]
    @PSNo NVARCHAR(100)
AS
BEGIN 
	DECLARE @TableName NVARCHAR(MAX)='ttccom001'
	DECLARE @CodeColName NVARCHAR(MAX)='t_emno'
	DECLARE @DescColName NVARCHAR(MAX)='t_nama'

	DECLARE @SqlQuery NVARCHAR(MAX)=Null
	DECLARE @LNLinkedServer NVARCHAR(max) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(max) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	

	SET @SqlQuery='	Select TOP 1000 '+@CodeColName+' as Code, 
						   '+@DescColName+' as Description, 
						   '+@CodeColName+'+''-''+'+@DescColName+' as CodeDescription 
					FROM '+@LNLinkedServer+'.dbo.'+@TableName+@LNCompanyId

	
	PRINT (@SqlQuery)
	EXEC (@SqlQuery)
	
    
END
-----------------------------
--28
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_PCL_Number_List]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_PCL_Number_List]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_PCL_Number_List]
AS
BEGIN 
	
	DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')

	

	SELECT  
			ROW_NUMBER() OVER (
			ORDER BY P.[t_pcln]
			) Code,
	        P.[t_pcln] AS CodeDescription							
	FROM [dbo].[PCR504] AS P 
	LEFT OUTER JOIN @tblPCLStatus AS PS ON PS.[t_val] = P.[t_stat]
	WHERE UPPER(PS.[t_cnst]) IN ('CONF.PMG','PCL.REL','APPR.QA','CONF.SFC','REJ.QC') OR P.[t_stat] IS NULL
	
END
----------------------------
--29

GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_ReleasePCL_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_ReleasePCL_Data]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Get_ReleasePCL_Data]
    @PSNo NVARCHAR(100)
AS
BEGIN 
	
    DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @SqlQuery NVARCHAR(MAX)
	--DECLARE @YesValue INT
	DECLARE @NoValue INT  
	DECLARE @Collation NVARCHAR(100) = ''
	SET @Collation = ' COLLATE Latin1_General_100_CS_AS_KS_WS';
	
	DECLARE @tblSTGI AS TABLE
	(
	  [t_loca] NVARCHAR(100),
	  [t_pcln] NVARCHAR(100),
	  [t_stgi] INT
	)
	DECLARE @tblName AS TABLE
	(
	 [t_emno] NVARCHAR(100),
	 [t_nama] NVARCHAR(200)
	)

	
	DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')

	DECLARE @tblYesNoStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblYesNoStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], [LNConstant] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno')

	DECLARE @tblItemTypeStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblItemTypeStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp')

	DECLARE @tblMovementTypeStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblMovementTypeStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Movement Type','ltmov.type')
	
    
	SELECT 
	  @NoValue = YNS.[t_val] FROM  @tblYesNoStatus AS YNS WHERE UPPER(YNS.[t_cnst]) = 'NO'
	

	
	SET @SqlQuery =
	' SELECT 
	  P0.[t_loca],
	  P.[t_pcln],
	  A.[t_stgi]
	  FROM ' + @LNLinkedServer + '.dbo.tltcom000'+ @LNCompanyId + ' AS A
	  INNER JOIN [dbo].[PCR500] AS P0 ON P0.[t_loca] = A.[t_dimx]
	  INNER JOIN [dbo].[PCR504] AS P ON P.[t_pcln] = P0.[t_pcln]
	  '
    INSERT INTO @tblSTGI([t_loca], [t_pcln], [t_stgi])
	EXEC(@SqlQuery)

	SET @SqlQuery = 
	' SELECT  
		A.[t_emno] , 
		A.[t_nama]
		FROM ' + @LNLinkedServer + '.dbo.ttccom001'+ @LNCompanyId  + ' AS A
	    INNER JOIN [dbo].[PCR504] AS C 
	      ON C.[t_emno] = A.[t_emno] '
    INSERT INTO @tblName([t_emno], [t_nama])
	EXEC(@SqlQuery)

	DECLARE @tblLocationAddress AS TABLE 
	(
		[t_pcrn] NVARCHAR(100), 
		[t_loca] NVARCHAR(100),
		[t_ladr] NVARCHAR(100)
	)
	SET @SqlQuery = 
	' SELECT  
		A.[t_pcrn] , 
		A.[t_loca],
		C.[t_ladr] 
	    FROM ' + @lnlinkedserver + '.dbo.tltlnt000'+ @lncompanyid  + ' AS C 
	    INNER JOIN PCR501 AS A 
	      ON A.[t_loca] = C.[t_dimx]'

    INSERT INTO @tblLocationAddress
	(
		[t_pcrn],
		[t_loca],
		[t_ladr]
	)
	EXEC(@SqlQuery)
	
		
	DECLARE @tblWCLocation TABLE
	(
		[t_cadr] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_cwoc] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[WCDeliverName] NVARCHAR(MAX)
	)
	INSERT INTO @tblWCLocation([t_cadr], [t_cwoc], [t_dsca], [WCDeliverName])
	EXEC [dbo].[SP_PCR_Get_WorkCenter_With_Location] 0
	
       SELECT
	     DISTINCT
			P.[Id] AS Id,
			P.[t_rbal] AS ReturnBalance,
			P.[t_pcln] AS PCLNumber,
			P.[t_revn] AS Revision,
			P.[t_area] AS TotalConsumedArea,
			P.[t_sqty] AS ScrapQuantity,
			P.[t_rtar] AS ReturnArea,
			PS.[t_val] AS PCLStatus,  
			PS.[t_txt] AS PCLStatusName,
			P.[t_rrmk] AS ReturnRemark,
			P.[t_tclt] AS TotalCuttingLength,
			P0.[t_stkn] AS StockNumber,
			P0.[t_area] AS TotalArea,
			WC.[t_cwoc] AS WCDeliver,
			WC.WCDeliverName AS WCDeliverName,
			CONCAT(N.[t_emno], '-' + N.[t_nama]) AS Planner,							
			--N.[t_nama] AS Planner,
			P.[t_mrmk] AS Remark,
			P.[t_mvtp] AS MovementType,
			MTS.[t_txt] AS MovementTypeName,
			P.[t_ityp] AS ItemType,
			ITS.[t_txt] AS ItemTypeName,
			P.[t_pcl_rldt] AS PCLReleaseDate,
			P.[t_stgi] AS AllowedStage1,
			YNS.[t_txt] AS AllowedStage1Name,
			CASE WHEN S.[t_stgi] = @NoValue THEN 0 ELSE 1 END AS DisableSTGI,		
			CASE WHEN P5.[t_pcln] IS NOT NULL AND LEN(P5.[t_pcln]) > 0 AND P5.[t_revn] IS NOT NULL THEN 1 ELSE 0 END AS DisableReturnBalance
		FROM 
		  [dbo].[PCR504] AS P
		   INNER JOIN [dbo].[PCR500] AS P0 
		     ON P.[t_pcln] = P0.[t_pcln]
		   INNER JOIN [dbo].[PCR503] AS P3 
		     ON P.[t_pcln] = P3.[t_pcln]
		   INNER JOIN [dbo].[PCR501] AS P1
		     ON P3.[t_pcrn] = P1.[t_pcrn] 
		   LEFT OUTER JOIN [dbo].[PCR505] AS P5
		     ON P.[t_pcln] = P5.[t_pcln] AND P.[t_revn] = P5.[t_revn]
		   LEFT OUTER JOIN @tblName AS N
		     ON P.[t_emno] COLLATE Latin1_General_100_CS_AS_KS_WS = N.[t_emno]
		   LEFT OUTER JOIN @tblSTGI AS S
		     ON S.[t_pcln] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcln]
		   LEFT OUTER JOIN @tblPCLStatus AS PS 
		     ON P.[t_stat] = PS.[t_val]
		   LEFT OUTER JOIN @tblItemTypeStatus AS ITS
		     ON P.[t_ityp] = ITS.[t_val]
		   LEFT OUTER JOIN @tblMovementTypeStatus AS MTS
		     ON P.[t_mvtp] = MTS.[t_val]		
		   LEFT OUTER JOIN @tblYesNoStatus AS YNS
		     ON P.[t_stgi] = YNS.[t_val]
		   LEFT OUTER JOIN @tblLocationAddress AS LA
		    ON P3.[t_pcrn] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_pcrn] AND P1.[t_loca] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_loca]
		   LEFT OUTER JOIN @tblWCLocation AS WC
		    ON P.[t_cwoc]  COLLATE Latin1_General_100_CS_AS_KS_WS = WC.[t_cwoc]
		   AND WC.[t_cadr] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_ladr]
		  --WHERE 1 = 1
		  --UPPER(PS.[t_cnst]) IN ('CONF.PMG','PCL.REL','APPR.QA','CONF.SFC','REJ.QC') OR P.[t_stat] IS NULL

		 
	
END

----------------------------
--30
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_OriginalReturnDistribution_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_OriginalReturnDistribution_Data]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_OriginalReturnDistribution_Data]
    @PSNo NVARCHAR(100),
	@Id INT
AS
BEGIN 
    --SET @PCLNumber = 'PCLCA13-8440'
	DECLARE @PCLNumber NVARCHAR(100)
	DECLARE @Revision INT
	DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	SELECT
	TOP 1
	  @PCLNumber = P.[t_pcln] ,
	  @Revision = P.[t_revn]
	FROM [dbo].[PCR504] AS P WHERE P.[Id] = @Id

	DECLARE @TotalArea FLOAT
	SELECT @TotalArea = SUM(P.[t_area]) FROM [dbo].[PCR505] AS P WHERE P.[t_pcln] = @PCLNumber AND P.[t_revn] = @Revision
	SET @TotalArea = CASE WHEN @TotalArea IS NULL THEN 0 ELSE ROUND(@TotalArea, 2) END
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')
	SELECT 
	DISTINCT
	 P0.[t_pcln] AS PCLNumber,	
	 PS.[t_txt] AS PCLStatus,  
	 P4.[t_ityp] AS ItemType,
	 ROUND(P0.[t_area] - P4.[t_area], 2) AS MaximumReturnArea,	
	 @TotalArea AS TotalArea,
	 P0.[t_leng] AS LengthOriginal,
	 P0.[t_widt] AS WidthOriginal,
	 P0.[t_loca] AS LocationOriginal,
	 P0.[t_area] AS AreaOriginal,
	 P0.[t_odds] AS OddShapeOriginal,
	 P0.[t_cnvf] AS FactorOriginal,
	 P0.[t_thic] AS ThicknessOriginal,
	 P0.[t_ccd1] AS CCD1Original,
	 P0.[t_cdt1] AS CDT1Original,
	 P0.[t_ccd2] AS CCD2Original,
	 P0.[t_cdt2] AS CDT2Original,
	 P4.[t_revn] AS Revision,
	 P4.[t_rbal] AS ReturnBalance
	FROM [dbo].[PCR500] AS P0
	LEFT OUTER JOIN [dbo].[PCR504] AS P4
	ON P0.[t_pcln] = P4.[t_pcln] 
	LEFT OUTER JOIN @tblPCLStatus AS PS 
    ON P4.[t_stat] = PS.[t_val]
	LEFT OUTER JOIN [dbo].[PCR505] AS P
	ON P.[t_pcln] = P0.[t_pcln] AND P.[t_revn] = P4.[t_revn]
	WHERE P4.[t_pcln] = @PCLNumber 	
	AND P4.[t_revn] = @Revision
   
END
---------------------
--31
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_ReturnDistribution_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_ReturnDistribution_Data]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_ReturnDistribution_Data]
    @PSNo NVARCHAR(100),
	@Id INT
AS
BEGIN 
    --SET @PCLNumber = 'PCLCA13-8440'
	DECLARE @PCLNumber NVARCHAR(100)
	DECLARE @Revision INT
	SELECT
	TOP 1
	  @PCLNumber = P.[t_pcln] ,
	  @Revision = P.[t_revn]
	FROM [dbo].[PCR504] AS P WHERE P.[Id] = @Id

	DECLARE @TotalArea FLOAT
	SELECT @TotalArea = SUM(P.[t_area]) FROM [dbo].[PCR505] AS P WHERE P.[t_pcln] = @PCLNumber AND P.[t_revn] = @Revision
	SET @TotalArea = CASE WHEN @TotalArea IS NULL THEN 0 ELSE ROUND(@TotalArea, 2) END
	SELECT 
	DISTINCT
	  P.[Id] AS Id,
	  P0.[t_pcln] AS PCLNumber,	
	 P.[t_widt] AS Width,	
	 P.[t_leng] AS [Length],	
	 P.[t_area] AS Area,	
	 P4.[t_ityp] AS ItemType,	
	 P.[t_odds] AS OddShape,	
	 P.[t_rmrk] AS Remark,	
	 P.[t_seqn]  AS [Sequence],	
	 ROUND(P0.[t_area] - P4.[t_area], 2) AS MaximumReturnArea,	
	 P.[t_stkr] AS StockRevision,	
	 P4.[t_revn] AS Revision,	
	 P4.[t_rbal] AS ReturnBalance,	
	 @TotalArea AS TotalArea,
	 P0.[t_leng] AS LengthOriginal,
	 P0.[t_widt] AS WidthOriginal,
	 P0.[t_loca] AS LocationOriginal,
	 P0.[t_area] AS AreaOriginal,
	 P0.[t_odds] AS OddShapeOriginal,
	 P0.[t_cnvf] AS FactorOriginal,
	 P0.[t_thic] AS ThicknessOriginal,
	 P0.[t_ccd1] AS CCD1Original,
	 P0.[t_cdt1] AS CDT1Original,
	 P0.[t_ccd2] AS CCD2Original,
	 P0.[t_cdt2] AS CDT2Original
	
	FROM [dbo].[PCR500] AS P0
	INNER JOIN [dbo].[PCR504] AS P4
	ON P0.[t_pcln] = P4.[t_pcln] 
	INNER JOIN [dbo].[PCR505] AS P
	ON P.[t_pcln] = P0.[t_pcln] AND P.[t_revn] = P4.[t_revn]
	WHERE P4.[t_pcln] = @PCLNumber 	
	AND P4.[t_revn] = @Revision
   
END
-------------------------------
--32
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_Total_NestedQuantity]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_Total_NestedQuantity]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_Total_NestedQuantity]
 @PCRN NVARCHAR(100),
 @PONO INT,
 @StockNumber NVARCHAR(100),
 @PCRRevision INT
AS
BEGIN 
    DECLARE @DeletedStatus INT 
	DECLARE @Value INT = 0
	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE UPPER([LNConstant]) = 'DELETE'

	SELECT
		@Value = SUM(ISNULL(P.[t_nqty], 0))
	FROM 
	  [dbo].[PCR503] AS P
	  WHERE 
	P.[t_pcrn] = @PCRN 
		AND 
	P.[t_pono] = @PONO 
		AND
	P.[t_stkn] = @StockNumber 
		AND 
	P.[t_revn] >= @PCRRevision
	   AND 
	P.[t_lsta] <> @DeletedStatus
	  
   RETURN @Value
	
END
------------------------
--33
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_Last_PCR_Sequence_Details]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_Last_PCR_Sequence_Details]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Get_Last_PCR_Sequence_Details]
 @PCRN NVARCHAR(100),
 @PONO INT
AS
BEGIN 
	SELECT
	TOP 1
	  P.[t_revn],
	  P.[t_pcln],
	  P.[t_stkn],
	  P.[t_stkr],
	  P.[t_nqty],
	  P.[t_npcs]
	FROM 
	  [dbo].[PCR503] AS P
	  WHERE P.[t_pcrn] = @PCRN AND P.[t_pono] = @PONO
	  ORDER BY P.[t_revn] DESC
	
END
--------------------
--34
GO
 IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_RD_Area]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_RD_Area]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_RD_Area]
   @PCLNumber NVARCHAR(100),
   @Revision INT,
   @TotalArea FLOAT
AS
BEGIN 
	SET NOCOUNT ON
	
	DECLARE @MSG NVARCHAR(1000)
	DECLARE @Index INT
	DECLARE @Total INT
	DECLARE @IORN NVARCHAR(100)
	DECLARE @Qoro FLOAT
	DECLARE @TBL AS TABLE(Qoro FLOAT)
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @StockArea FLOAT
	DECLARE @Factor FLOAT
	DECLARE @Thickness FLOAT
	DECLARE @CCD1 FLOAT
	DECLARE @CDT1 FLOAT
	DECLARE @CCD2 FLOAT
	DECLARE @CDT2 FLOAT
	DECLARE @QOFRArea FLOAT
	DECLARE @Query NVARCHAR(MAX)
	BEGIN TRY
	   SELECT TOP 1 @IORN = P.[t_iorn]
	   FROM [dbo].[PCR504] AS P WHERE P.[t_pcln] = @PCLNumber AND P.[t_revn] = @Revision
	   IF(@IORN IS NOT NULL AND LEN(@IORN) > 0)
	    BEGIN
		  SET @Query =  ' SELECT SUM([t_qoro]) FROM ' + @LNLinkedServer + '.dbo.twhinh220'+ @LNCompanyId + ' WHERE [t_oorg] = 16 AND [t_orno] = ''' + @IORN + ''''
		  INSERT INTO @TBL(Qoro)
		  EXEC(@Query)
		  IF EXISTS(SELECT Qoro FROM @TBL)
		   BEGIN
		     SELECT TOP 1 @Qoro = Qoro FROM @TBL
			 SELECT TOP 1 
			    @StockArea = ROUND(P0.[t_area], 2),
			    @Factor = P0.[t_cnvf],
			    @Thickness = P0.[t_thic],
				@CCD1 = P0.[t_ccd1],
				@CDT1 = P0.[t_cdt1],
				@CCD2 = P0.[t_ccd2],
				@CDT2 = P0.[t_cdt2] 
			  FROM [dbo].[PCR500] AS P0 WHERE P0.[t_pcln] = @PCLNumber AND P0.[t_stkr] = @Revision

			  SET @QOFRArea = ROUND((@Qoro * 1000) / ((@Factor * @Thickness) + (@CCD1 * @CDT1) + (@CCD2 * @CDT2)), 2)

			   IF((@StockArea - @QOFRArea) < ROUND(@TotalArea, 2))
			    BEGIN
				  SET @MSG = 'Total Area in Distribution must be equal or less than '
				  SET @MSG = CONCAT(@MSG, ROUND(@StockArea - @QOFRArea, 2))
				  SET @MSG = CONCAT(@MSG, ' & m2. Reason ')
				  SET @MSG = CONCAT(@MSG, @QOFRArea)				  
				  SET @MSG = CONCAT(@MSG, ' m2 is already consumed in Project (Manual) Order: ')
				  SET @MSG = CONCAT(@MSG, @IORN)	
				  RAISERROR(@MSG, 11, 1)
				END
		   END
	    END
	   SELECT 1
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END
-------------------------
--35
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_ReleasePCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_ReleasePCL]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

Alter Procedure [dbo].[SP_PCR_Validate_ReleasePCL]
(
   @Id INT 
)
AS
 BEGIN
  BEGIN TRY 
   DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
   DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
   DECLARE @Query NVARCHAR(MAX)
   DECLARE @StatusDeleted INT
   DECLARE @StatusPCLReleased INT
   DECLARE @StatusPCRReleasedToMP INT
   DECLARE @ItemTypePipeValue INT
   DECLARE @ItemTypePlateValue INT
   DECLARE @Area FLOAT
   DECLARE @RTAR FLOAT
   DECLARE @SQTY FLOAT
   DECLARE @EMNO NVARCHAR(100)
   DECLARE @MVTP INT
   DECLARE @RBAL INT
   DECLARE @PRDT DATETIME
   DECLARE @Project NVARCHAR(100)
   DECLARE @Element NVARCHAR(100)
   DECLARE @ChildItem NVARCHAR(100)
   DECLARE @PRTN INT
   DECLARE @PCLN NVARCHAR(1000)
   DECLARE @REVN INT
   DECLARE @MSG NVARCHAR(1000)
   DECLARE @DA FLOAT
   DECLARE @C INT
   DECLARE @DIST INT = 0
   DECLARE @FilesCount INT
   DECLARE @AttachmentTableName NVARCHAR(100) = 'PCR504'
   SELECT @FilesCount = COUNT(0) from [dbo].[FCS001] AS F WHERE F.[TableName] = @AttachmentTableName AND TableId = @Id
   IF(@FilesCount = 0)
	 BEGIN
	    SET @MSG = 'Please provide attachment before releasing PCL'
		RAISERROR(@MSG, 11, 1)
	 END
   SELECT
       @RBAL =  ISNULL(P.[t_rbal], 0),
	   @RTAR = ROUND(ISNULL(P.[t_rtar], 0), 2) ,
	   @Area = ROUND(ISNULL(P.[t_area], 0), 2),
	   @SQTY = ROUND(ISNULL(P.[t_sqty], 0), 2) ,
	   @EMNO = P.[t_emno] ,
	   @MVTP = P.[t_mvtp],
	   @PCLN = P.[t_pcln],
	   @REVN = P.[t_revn]
   FROM [dbo].[PCR504] AS P WHERE P.Id = @Id
   --SELECT CASE WHEN ROUND(12.345, 2) - ROUND(12.23,2) > 0.1 THEN 1 ELSE 0 END AS A
   IF(@Area <= 0)
   BEGIN
      SET @MSG = 'Total consumed area must be greater than zero'
	  RAISERROR(@MSG, 11, 1)
   END
   IF(@SQTY < 0)
   BEGIN
      SET @MSG = 'Scrap quantity must not be less than zero'
	  RAISERROR(@MSG, 11, 1)
   END
   IF(@EMNO IS NULL)
   BEGIN
      SET @MSG = 'Please enter planner'
	  RAISERROR(@MSG, 11, 1)
   END
   IF(@MVTP IS NULL)
   BEGIN
      SET @MSG = 'Please select Movement Type'
	  RAISERROR(@MSG, 11, 1)
   END  
       IF(@RBAL > 0)
	    BEGIN		
		  IF EXISTS(SELECT 0  FROM [dbo].[PCR505] AS P5 WHERE P5.[t_pcln] = @PCLN AND P5.[t_revn] = @REVN)
		  BEGIN
		   SELECT 
			@DA = ROUND(SUM(ISNULL(P5.[t_area], 0)), 2) 
		   FROM 
			[dbo].[PCR505] AS P5 
		   WHERE 
			P5.[t_pcln] = @PCLN AND P5.[t_revn] = @REVN
		   
		   SELECT 
			@DIST = COUNT(0) 
		   FROM 
			[dbo].[PCR505] AS P5 
		   WHERE 
			P5.[t_pcln] = @PCLN AND P5.[t_revn] = @REVN

		   IF(@RBAL <> @DIST)
	   	    BEGIN
     		   SET @MSG = 'Distribution for return balance incomplete'
			   RAISERROR(@MSG, 11, 1)
			   --PRINT 1
		    END
		  END
		  ELSE
		   BEGIN
		    SET @MSG = 'Please maintain distribution for return balance'
			RAISERROR(@MSG, 11, 1)
		   END
		  --TODO Test
		  IF(@RTAR - @DA > 0.1)
		   BEGIN
		    SET @MSG = 'Total area in distribution must be equal to Total return area for PCL'
			RAISERROR(@MSG, 11, 1)
			  
		   END
	    END
		SELECT
			TOP 1 
			@PRDT = P3.[t_pcr_crdt] 
		FROM 
			[dbo].[PCR503] AS P3 
		WHERE 
			P3.[t_pcln] = @PCLN AND P3.[t_stkn] IS NOT NULL 
		ORDER BY P3.[t_pcr_crdt] DESC
        
		SELECT
		 TOP 1
		 @Project = P3.[t_cprj] ,
		 @Element = P3.[t_cspa],
		 @ChildItem = P3.[t_sitm],
		 @PRTN = P3.[t_prtn] 
		 FROM 
			[dbo].[PCR503] AS P3 
		 WHERE 
			P3.[t_pcln] = @PCLN AND 
			P3.[t_stkn] IS NOT NULL AND 
			CAST(@PRDT AS DATE) = CAST(P3.[t_pcr_crdt] AS DATE)

		 DECLARE @PrevPCRN NVARCHAR(100)
		 DECLARE @PrevRevn INT
		 DECLARE @PONO INT

		 SELECT
		 TOP 1
		  @PrevPCRN = P3.[t_pcrn],
		  @PrevRevn = P3.[t_revn],
		  @PONO = P3.[t_pono]
		 FROM 
			[dbo].[PCR503] AS P3 
		 WHERE 
			P3.[t_cprj] = @Project AND P3.[t_cspa] = @Element AND P3.[t_prtn] = @PRTN AND P3.[t_sitm] = @ChildItem AND
		 CAST(P3.[t_pcr_crdt] AS DATE) < CAST(@PRDT AS DATE) AND P3.[t_stkn] IS NULL AND P3.[t_lsta] <> 10
		 
		 IF (@PrevPCRN IS NOT NULL AND @PrevRevn IS NOT NULL AND @PONO IS NOT NULL)
		 BEGIN
		   SET @MSG = 'Please Proceed previous PCR No.: ' + @PrevPCRN +  ',PCR Line: '
		   SET @MSG = CONCAT(@MSG, @Pono)
		   SET @MSG  = CONCAT(@MSG,   ' and Revision:')
		   SET @MSG  = CONCAT(@MSG, @PrevRevn)  
		   RAISERROR(@MSG, 11, 1)
		 END
		 
		 SELECT 1

	END TRY
	BEGIN CATCH				
		DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(), 
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 			
	END CATCH
END
---------------------------------
--36
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Insert_Update_Delete_ReturnDistribution_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Insert_Update_Delete_ReturnDistribution_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Insert_Update_Delete_ReturnDistribution_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @NextSequence INT
	 DECLARE @Sequence INT
	 DECLARE @PCLNumber NVARCHAR(100)
	 DECLARE @Revision INT
	 DECLARE @Id INT
	 DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [PCLNumber] NVARCHAR(100), 
		 [ReturnBalance] FLOAT,
		 [Length] FLOAT,
		 [Width] FLOAT,
		 [MaximumReturnArea] FLOAT,
		 [Area] FLOAT,
		 [Sequence] INT,
		 [OddShape] BIT,
		 [Revision] INT,
		 [ItemType] INT,
		 [Remark] NVARCHAR(MAX),
		 [StockNumber] NVARCHAR(100),
		 [StockRevision] INT
	 )
	DECLARE @SequencesToBeAdjusted AS TABLE(Id INT)   
	INSERT INTO @TBL
	(
		 [Id], 
		 [PCLNumber], 
		 [ReturnBalance],
		 [Length],
		 [Width],
		 [MaximumReturnArea],
		 [Area],
		 [Sequence],
		 [OddShape],
		 [Revision],
		 [ItemType],
		 [Remark]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('PCLNumber').value('.', 'NVARCHAR(100)') AS [PCLNumber], 
		Array.Entity.query('ReturnBalance').value('.', 'FLOAT') AS [ReturnBalance],  
		Array.Entity.query('Length').value('.', 'FLOAT') AS [Length],	
		Array.Entity.query('Width').value('.', 'FLOAT') AS [Width],	
		Array.Entity.query('MaximumReturnArea').value('.', 'FLOAT') AS [MaximumReturnArea],
		Array.Entity.query('Area').value('.', 'FLOAT') AS [Area],
		Array.Entity.query('Sequence').value('.', 'INT') AS [Sequence],
		Array.Entity.query('OddShape').value('.', 'BIT') AS [OddShape],
		Array.Entity.query('Revision').value('.', 'INT') AS [Revision],
		Array.Entity.query('ItemType').value('.', 'BIT') AS [ItemType],	
		Array.Entity.query('Remark').value('.', 'NVARCHAR(100)') AS [Remark]	
			
	FROM @Data.nodes('/ArrayOfRETURN_BALANCE_DISTRIBUTION_Result/RETURN_BALANCE_DISTRIBUTION_Result') AS Array(Entity)
    
	UPDATE T
		SET 
		T.[StockNumber] = P0.[t_stkn], 
		T.[StockRevision] = P0.[t_stkr]
	FROM @TBL AS T
	JOIN [dbo].[PCR500] AS P0 ON T.PCLNumber COLLATE Latin1_General_100_CS_AS_KS_WS = P0.[t_pcln]
		
	IF(@Mode = 0) --INSERT
	 BEGIN
	   IF EXISTS(
				  SELECT 0 FROM [dbo].[PCR505] AS P 
				  INNER JOIN  @TBL AS T ON T.PCLNumber COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcln]  AND P.[t_revn] = T.Revision
				)
	    BEGIN
		   SELECT @NextSequence = MAX(P.[t_seqn]) + 1 FROM [dbo].[PCR505] AS P 
		   INNER JOIN  @TBL AS T 
		   ON T.PCLNumber COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcln]  AND P.[t_revn] = T.Revision
	    END
		ELSE
		 BEGIN
		   SET @NextSequence = 1
		 END
	   INSERT INTO 
		[dbo].[PCR505]
		(
			[t_pcln], [t_leng], [t_widt], [t_area], [t_odds], [t_revn], [t_ityp], [t_rmrk], [t_seqn], [t_stkn], [t_stkr]
		)
	    SELECT 
		  T.PCLNumber, T.[Length], T.Width, 
		  ROUND(ISNULL(T.Area, 0), 2), T.OddShape, T.Revision, 
		  T.ItemType, T.Remark, @NextSequence, T.StockNumber, T.StockRevision
	    FROM @TBL AS T
	 END
    ELSE IF(@Mode = 1) --UPDATE
	 BEGIN
	  UPDATE tbl505
		SET 
		  tbl505.[t_leng] = T.[Length],
		  tbl505.[t_widt] = T.[Width],
		  tbl505.[t_area] = ROUND(ISNULL(T.[Area], 0), 2),
		  tbl505.[t_odds] = T.[OddShape],
		  tbl505.[t_rmrk] = T.[Remark]
		FROM [dbo].[PCR505] AS tbl505
		INNER JOIN @TBL AS T ON T.[Id] = tbl505.[Id]
		
	 END	 
	ELSE IF(@Mode = 2) --DELETE
	 BEGIN
	 	   
	   SELECT TOP 1 @Id = T.[Id] FROM @TBL AS T 
       SELECT
	    TOP 1
		 @PCLNumber = P.[t_pcln],
		 @Revision = P.[t_revn],
		 @Sequence = P.[t_seqn]
	    FROM [dbo].[PCR505] AS P WHERE P.[Id] = @Id
	   INSERT INTO @SequencesToBeAdjusted(Id)
	   SELECT 		
		P.[Id] 
	   FROM 
	    [dbo].[PCR505] AS P 
	   WHERE 
	    P.[t_pcln] = @PCLNumber AND P.[t_revn] = @Revision AND P.[t_seqn] > @Sequence
	   
	   UPDATE P
	     SET
		   P.[t_seqn] = P.[t_seqn] - 1
	    FROM [dbo].[PCR505] AS P
		INNER JOIN @SequencesToBeAdjusted AS T ON P.[Id] = [T].Id
		DELETE FROM [dbo].[PCR505] WHERE [Id] = @Id
	 END
	COMMIT
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END 
--------------------------
--37
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_ReleasePCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_ReleasePCL]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

Alter Procedure [dbo].[SP_PCR_ReleasePCL]
(
   @Id INT ,
   @PSNo NVARCHAR(100),
   @UpdateScrap BIT = 0   
)
AS
 BEGIN
   DECLARE @Project NVARCHAR(100)
   DECLARE @Element NVARCHAR(100)
   DECLARE @PCRN NVARCHAR(100)
   DECLARE @Pono INT
   DECLARE @PCLN NVARCHAR(100)
   DECLARE @Npcs INT
   DECLARE @Nqty INT
   DECLARE @Sqty INT
   DECLARE @Revn INT
   DECLARE @NewRevn INT
   DECLARE @SSPA NVARCHAR(100)
   DECLARE @ItemType INT
   DECLARE @MaxRDate DATETIME
   DECLARE @Tbl AS TABLE(PMGC INT, STGI INT, SSPA_1 NVARCHAR(100), SSPA_2 NVARCHAR(100), SSPA_3 NVARCHAR(100), SSPA_4 NVARCHAR(100))
   DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
   DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
   DECLARE @RowIndex INT
   DECLARE @Total INT
   DECLARE @IdsTable AS Table(RowIndex INT IDENTITY(1,1), Id INT)
   DECLARE @Query NVARCHAR(MAX)
   DECLARE @YesValue INT = 1
   DECLARE @NoValue INT = 2
   DECLARE @ParentId INT
   DECLARE @PMGC INT
   DECLARE @STGI INT
   DECLARE @Stat INT
   DECLARE @STGI504 INT
   DECLARE @Loc NVARCHAR(100)
   DECLARE @StatusDeleted INT
   DECLARE @StatusPCLReleased INT
   DECLARE @StatusPCRReleasedToMP INT
   DECLARE @ItemTypePipeValue INT
   DECLARE @ItemTypePlateValue INT

   BEGIN TRAN
     BEGIN TRY
	   EXEC [dbo].[SP_PCR_Validate_ReleasePCL] @Id
	   --print 'ok'
	  SELECT
	  TOP 1
	   @PCLN = P.[t_pcln] ,
	   @Revn = P.[t_revn],
	   @Sqty = P.[t_sqty],
	   @STGI504 = P.[t_stgi]
	   FROM [dbo].[PCR504] AS P WHERE P.[Id] = @Id
	 IF(@UpdateScrap = 1)
	  BEGIN
	    UPDATE [dbo].[PCR504] SET [t_sqty] = [t_rtar], [t_rtar] = 0 WHERE [Id] = @Id
	  END
	  SELECT
	    TOP 1 @StatusPCRReleasedToMP = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'
	   SELECT
	    TOP 1 @StatusDeleted = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'DELETE'
	  SELECT
	    TOP 1 @StatusPCLReleased = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'PCL.REL'
	  SELECT
	    TOP 1 @ItemTypePipeValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PIPE' 
	  SELECT
	    TOP 1 @ItemTypePlateValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PLATE'
		
		UPDATE
			tbl503   
				SET 
				tbl503.[t_pcl_rldt] = GETUTCDATE(),
				tbl503.[t_mvtp] = tbl504.[t_mvtp],
				tbl503.[t_cwoc] = tbl504.[t_cwoc],
				tbl503.[t_lsta] = @StatusPCLReleased,
				tbl503.[t_sqty] = (tbl503.[t_arcs] * tbl504.[t_sqty]) / tbl504.[t_area]
		FROM 
			[dbo].[PCR503] AS tbl503	
			JOIN [dbo].[PCR504] AS tbl504	 
		ON tbl504.[t_pcln] = tbl503.[t_pcln]
		WHERE 
			tbl503.[t_lsta] <> @StatusDeleted AND tbl504.[Id] = @Id

	  IF EXISTS(SELECT 0 FROM [dbo].[PCR503] AS P0 WHERE P0.[t_pcln] = @PCLN AND P0.[t_lsta] <> @StatusDeleted AND ISNULL(P0.[t_nqty], 0) <> P0.[t_npcs])
	    BEGIN
		 INSERT INTO 
			@IdsTable
		 SELECT 
			P0.[Id]
		 FROM 
			[dbo].[PCR503] AS P0 
		WHERE 
			P0.[t_pcln] = @PCLN AND 
			P0.[t_lsta] <> @StatusDeleted AND 
			ISNULL(P0.[t_nqty], 0) <> P0.[t_npcs]
		 
		 SET @RowIndex = 1
		 SELECT @Total = COUNT(0) FROM @IdsTable
		 WHILE(@RowIndex <= @Total)
		  BEGIN
		    SELECT 
			 @ParentId = T.[Id] 
			FROM 
			 @IdsTable AS T 
			WHERE 
			 T.[RowIndex] = @RowIndex
			
		    SELECT TOP 1
				 @PCRN = P.[t_pcrn], 
				 @Pono = P.[t_pono], 
				 @Npcs = P.[t_npcs], 
				 @Nqty = ISNULL(P.[t_nqty], 0)		 
			 FROM 
				[dbo].[PCR503] AS P
			 WHERE 
				P.[Id] = @ParentId
		  
		 
			 SELECT TOP 1 
				@NewRevn = ISNULL(P0.[t_revn], 0)
			 FROM 
				[dbo].[PCR503] AS P0 
			WHERE 
				P0.[t_pcrn] = @PCRN AND 
				P0.[t_pono] = @Pono
			 ORDER BY P0.[t_revn] DESC
									
			SET @NewRevn += 1
		  
			 INSERT INTO [dbo].[PCR503]
			 (
				[t_pcrn], [t_pono], [t_revn], [t_leng], [t_widt], [t_npcs], [t_pcrt], 
				[t_prtn], [t_mitm], [t_sitm], [t_revi], [t_cprj], [t_cspa], [t_nqty], [t_sqty], [t_arcs],
				[t_thic], [t_pcr_crdt], [t_prdt], [t_cwoc], [t_prmk], [t_pcr_rldt], [t_lsta], [t_area], [t_ityp], [t_init]
			)
			 SELECT 
				@PCRN, @Pono, @NewRevn, P.[t_leng], P.[t_widt], @Npcs - @Nqty, P.[t_pcrt],
				P.[t_prtn], P.[t_mitm], P.[t_sitm], P.[t_revi], P.[t_cprj], P.[t_cspa], 0, 0, 0,
				P.[t_thic], P.[t_pcr_crdt], P.[t_prdt], P.[t_cwoc], P.[t_prmk], GETUTCDATE(), P.[t_lsta], P.[t_area], @ItemTypePlateValue, @PSNo
			 FROM 
				[dbo].[PCR503] AS P 
			 WHERE 
				P.[Id] = @ParentId

          SET @RowIndex += 1
	  END          
	END
	    
	    UPDATE [dbo].[PCR500] SET [t_qfre] = [t_qfre] - @Sqty WHERE [t_pcln] = @PCLN
	    
		IF EXISTS(SELECT 0 FROM [dbo].[PCR504] AS P WHERE P.[t_pcln] = @PCLN AND P.[t_revn] = @Revn)
		 BEGIN
		  IF EXISTS(SELECT 0 FROM [dbo].[PCR500] AS P0 WHERE P0.[t_pcln] = @PCLN)
		   BEGIN
		       SELECT
				 TOP 1
				   @Loc = P0.[t_loca] ,
				   @ItemType = P0.[t_ityp],
				   @SSPA = P0.[t_sspa]
				   FROM [dbo].[PCR500] AS P0 WHERE P0.[t_pcln] = @PCLN
			   SET @Query = 'SELECT TOP 1 [t_pmgc], [t_stgi], [t_sspa_1], [t_sspa_2], [t_sspa_3], [t_sspa_4] FROM ' + @LNLinkedServer + '.dbo.'+ 'tltcom000' + @LNCompanyId + ' WHERE [t_dimx] = ''' + @Loc + ''''
			   INSERT INTO @Tbl(PMGC, STGI, SSPA_1, SSPA_2, SSPA_3, SSPA_4)
			   EXEC(@Query)
		   END
		  ELSE
		   BEGIN
		     SET @PMGC = @YesValue
			 SET @STGI = @NoValue
			 SET @SSPA = ''
		   END
		  IF(@SSPA IS NOT NULL AND LEN(@SSPA) >0 AND EXISTS(SELECT 0 FROM @TBL AS T WHERE T.[SSPA_1] = @SSPA OR T.[SSPA_2] = @SSPA OR T.[SSPA_3] = @SSPA OR T.[SSPA_4] = @SSPA))
		    BEGIN
			    SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'APPR.QA'
			END
		   ELSE
			 BEGIN
			  IF(@PMGC = @NoValue AND @STGI = @YesValue)
			    BEGIN
			      SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'APPR.QA'
			    END
			  ELSE
			    BEGIN
				  IF(@PMGC = @NoValue AND @STGI = @NoValue)
					   BEGIN
						  IF(@STGI504 = @NoValue)
						   BEGIN
							 SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'APPR.QA'
						   END
						   ELSE
							 BEGIN
							   SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'CONF.PMG'
							 END
					   END
					   ELSE
							BEGIN
							   SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'PCL.REL'
							END
                  
				  IF(@ItemType = @ItemTypePipeValue)
				   BEGIN
				      SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'PCL.REL'
				   END    
				END
			 END
		    
			SELECT
			TOP 1
			  @MaxRDate = P0.[t_prdt]
			FROM [dbo].[PCR503] AS P0 WHERE P0.[t_pcln] = @PCLN ORDER BY [t_prdt] DESC
			
			SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'CONF.PMG'
			UPDATE [dbo].[PCR504]
			 SET 
			  [t_aarc] = [t_area], 
			  [t_asqt] = [t_sqty], 
			  [t_pcl_rldt] = GETUTCDATE(), 
			  [t_stat] = @Stat,
			  [t_ctdt] = @MaxRDate
			WHERE 
			  [t_pcln] = @PCLN 
			   AND 
			  [t_revn] = @Revn 
		 
		 END

	   COMMIT
	   SELECT 1
    END TRY
	BEGIN CATCH
	  IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	END CATCH
END 
------------------------------
--38
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Generate_New_PCR_Revision]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Generate_New_PCR_Revision]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Generate_New_PCR_Revision]
 @PCRN NVARCHAR(100),
 @PONO INT,
 @PCR_Revision INT,
 @StockNumber NVARCHAR(100),
 @StockRevision INT,
 @Nqty INT,
 @Arcs FLOAT,
 @N_Npcs INT, 
 @Last_PCR_Revision INT,
 @PSNo NVARCHAR(100)
AS
BEGIN
 BEGIN TRAN
  BEGIN TRY 
    DECLARE @Status INT 
	SELECT
	    TOP 1 
		@Status = [Value] 	
	FROM
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE 
		UPPER([LNConstant]) = 'PCL.GEN'

    INSERT INTO [dbo].[PCR503]
           (
		    [t_pcrn], [t_pono], [t_revn], [t_prtn], [t_mitm]
           ,[t_sitm], [t_leng], [t_widt], [t_npcs], [t_pcrt]
           ,[t_prdt], [t_cwoc], [t_cwar], [t_cloc], [t_pcln]
           ,[t_lsta], [t_sqty], [t_nqty], [t_arcs], [t_stkn]
           ,[t_stkr], [t_mvtp], [t_prmk], [t_mrmk], [t_pcr_crdt]
           ,[t_pcr_rldt], [t_pcl_crdt], [t_pcl_rldt], [t_cprj], [t_cspa]
           ,[t_mpos], [t_thic], [t_gorn], [t_shtp], [t_shfl]
           ,[t_area], [t_pitm], [t_hcno], [t_alts], [t_armk]
           ,[t_oitm], [t_qapr], [t_iqty], [t_revi], [t_lscr]
           ,[t_scrn], [t_rqty], [t_mcrp], [t_rrmk], [t_pr01]
           ,[t_pr02], [t_pr03], [t_pr04], [t_pr05], [t_pr06]
           ,[t_pr07], [t_pr08], [t_pr09], [t_pr10], [t_ityp]
           ,[t_Refcntd], [t_Refcntu], [t_init]
		   )
     SELECT
	        P.[t_pcrn], P.[t_pono], @Last_PCR_Revision + 1, P.[t_prtn], P.[t_mitm]
           ,P.[t_sitm], P.[t_leng], P.[t_widt], @N_Npcs, P.[t_pcrt]
           ,P.[t_prdt], P.[t_cwoc], P.[t_cwar], P.[t_cloc], P.[t_pcln]
           ,@Status, 0, @Nqty, @Arcs, @StockNumber
           ,@StockRevision, P.[t_mvtp], P.[t_prmk], P.[t_mrmk], P.[t_pcr_crdt]
           ,P.[t_pcr_rldt], GETUTCDATE(), NULL, P.[t_cprj], P.[t_cspa]
           ,P.[t_mpos], P.[t_thic], P.[t_gorn], P.[t_shtp], P.[t_shfl]
           ,P.[t_area], P.[t_pitm], P.[t_hcno], P.[t_alts], P.[t_armk]
           ,P.[t_oitm], P.[t_qapr], P.[t_iqty], P.[t_revi], P.[t_lscr]
           ,P.[t_scrn], P.[t_rqty], P.[t_mcrp], P.[t_rrmk], P.[t_pr01]
           ,P.[t_pr02], P.[t_pr03], P.[t_pr04], P.[t_pr05], P.[t_pr06]
           ,P.[t_pr07], P.[t_pr08], P.[t_pr09], P.[t_pr10], P.[t_ityp]
           ,P.[t_Refcntd], P.[t_Refcntu], @PSNo

	 FROM
		[dbo].[PCR503] AS P
	 WHERE 
	  P.[t_pcrn] = @PCRN
	    AND
	  P.[t_pono] = @PONO
	    AND
      P.[t_revn] = @PCR_Revision

		
	COMMIT TRAN							 
 END TRY 
 BEGIN CATCH 
  IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(), 
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END
 END CATCH 
END
------------------------------
--39
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_PCRLineStatus _For_Last_PCRRevision]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_PCRLineStatus _For_Last_PCRRevision]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Update_PCRLineStatus _For_Last_PCRRevision]
 @PCRN NVARCHAR(100),
 @PONO INT,
 @Last_PCR_Revision INT 
AS
BEGIN
 BEGIN TRAN
  BEGIN TRY 
    DECLARE @Id INT
	DECLARE @Item NVARCHAR(100)
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @StockRevision INT
    DECLARE @DeletedStatus INT
	DECLARE @Arcs FLOAT
	
	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE 
		UPPER([LNConstant]) = 'DELETE'
    
	SELECT
	  TOP 1
		@Id = P.[Id],
		@Arcs = P.[t_arcs],
		@Item = P.[t_sitm],
		@StockNumber = P.[t_stkn],
		@StockRevision = P.[t_stkr]
    FROM
	    [dbo].[PCR503] AS P
	WHERE
		P.[t_pcrn] = @PCRN
		  AND
		P.[t_pono] = @PONO
		  AND
        P.[t_revn] = @Last_PCR_Revision
	
	UPDATE
	   [dbo].[PCR500]
	SET
	   [t_qall] = [t_qall] -@Arcs,
	   [t_qfre] = [t_qfre] + @Arcs
	WHERE
		[t_item] = @Item
		  AND
        [t_stkn] = @StockNumber
		  AND
        [t_stkr] = @StockRevision

	UPDATE 
		[dbo].[PCR503]
	SET 
		[t_lsta] = @DeletedStatus
	WHERE 
		[Id] = @Id

	COMMIT TRAN							 
 END TRY 
 BEGIN CATCH 
  IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(), 
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END
 END CATCH 
END
----------------------------
--40
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_PCRLineStatus]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_PCRLineStatus]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Update_PCRLineStatus]
 @PCRN NVARCHAR(100),
 @PONO INT,
 @PCR_Revision INT,
 @StockNumber NVARCHAR(100)
AS
BEGIN
 BEGIN TRAN
  BEGIN TRY 
    DECLARE @DeletedStatus INT 
	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE 
		UPPER([LNConstant]) = 'DELETE'

	UPDATE 
		[dbo].[PCR503]
	SET 
		[t_lsta] = @DeletedStatus
	WHERE 
		[t_pcrn] = @PCRN 
			AND 
		[t_pono] = @PONO 
	        AND
		[t_stkn]= @StockNumber 
	        AND 
		[t_revn] >= @PCR_Revision
	        AND 
		[t_lsta] <> @DeletedStatus	
	COMMIT TRAN							 
 END TRY 
 BEGIN CATCH 
  IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(), 
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END
 END CATCH 
END
--------------------------------
--41
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_StockStatus]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_StockStatus]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Update_StockStatus]
 @Item NVARCHAR(100),  
 @StockNumber NVARCHAR(100),
 @StockRevision INT
AS
BEGIN
 BEGIN TRAN
  BEGIN TRY 
    DECLARE @DeletedStatus INT 
	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('Stock Status','ltsfc.stk.stat') 
	WHERE 
		UPPER([LNConstant]) = 'DELETE'

	UPDATE 
		[dbo].[PCR500]
	SET 
		[t_stat] = @DeletedStatus
	WHERE 
		[t_item] = @Item
			AND 
		[t_stkn] = @StockNumber
	        AND
		[t_stkr] > @StockRevision
	        
	COMMIT TRAN							 
 END TRY 
 BEGIN CATCH 
  IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(), 
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END
 END CATCH 
END
------------------------------------
--42
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_ModifyPCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_ModifyPCL]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_ModifyPCL]
    @Id INT,
	@RemarkReason NVARCHAR(100),
    @PCLModifiedRemark NVARCHAR(100),
	@PSNo NVARCHAR(20)
AS
 BEGIN
 BEGIN TRAN
  BEGIN TRY
   DECLARE @PCLN NVARCHAR(100)
   DECLARE @Revision INT
   DECLARE @CTable AS TABLE(
    RowIndex INT IDENTITY(1,1),
    PCRN NVARCHAR(100),
    PCRN_Line INT,
    PCRN_Revision INT,
    StockNumber NVARCHAR(100),
    StockRevision INT,
    PCLN NVARCHAR(100),
    Item NVARCHAR(100),
    Arcs FLOAT,
    Nqty FLOAT
	)
   DECLARE @L_Table AS TABLE(
   [PCR_Revision] INT, 
   [PCLN] NVARCHAR(100), 
   [StockNumber] NVARCHAR(100), 
   [StockRevision] INT,
   [Nqty] FLOAT,
   [Npcs] INT
   )
   DECLARE @Query NVARCHAR(MAX)
   DECLARE @C_PCRN NVARCHAR(100)
   DECLARE @C_PCRN_Line INT
   DECLARE @C_PCRN_Revision INT
   DECLARE @C_StockNumber NVARCHAR(100)
   DECLARE @C_StockRevision INT
   DECLARE @C_PCLN NVARCHAR(100)
   DECLARE @C_Item NVARCHAR(100)
   DECLARE @C_Arcs FLOAT
   DECLARE @C_Nqty FLOAT
   DECLARE @L_PCR_Revision INT
   DECLARE @L_PCLN NVARCHAR(100)
   DECLARE @L_StockNumber NVARCHAR(100)
   DECLARE @L_StockRevision INT
   DECLARE @L_Nqty FLOAT
   DECLARE @L_Npcs INT
   DECLARE @RowIndex INT
   DECLARE @Total INT
   DECLARE @SQty FLOAT
   DECLARE @DeletedStatus INT 
   DECLARE @Mode INT = 0
   DECLARE @TotalNestedQuantity FLOAT
   DECLARE @N_Npcs INT
   DECLARE @PId INT
   IF(@RemarkReason IS NULL OR LEN(@RemarkReason) = 0)
    BEGIN
     RAISERROR('Remark Reason is mandatory for Modified PCL', 11, 1)
    END
	
   IF(@PCLModifiedRemark IS NULL OR LEN(@PCLModifiedRemark) = 0)
    BEGIN
     RAISERROR('PCL modified remark is mandatory for Modified PCL', 11, 1)
    END
	

	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE 
		UPPER([LNConstant]) = 'DELETE'
   
   SELECT 
    TOP 1
	  @PCLN = P.[t_pcln] , 
	  @Revision = P.[t_revn]
    FROM 
	 [dbo].[PCR504] AS P
    WHERE 
	 P.[Id] = @Id

	INSERT INTO 
	 @CTable
	 (
	  [PCRN], [PCRN_Line], [PCRN_Revision],
	  [StockNumber], [StockRevision], [Item],
	  [PCLN], [Arcs], [Nqty]
	 )
	SELECT 
	    P3.[t_pcrn], P3.[t_pono], P3.[t_revn],
		P3.[t_stkn], P3.[t_stkr], P3.[t_sitm],
		P3.[t_pcln], P3.[t_arcs], P3.[t_nqty]
	FROM
	    [dbo].[PCR503] AS P3
	WHERE
		P3.[t_pcln] = @PCLN
		 AND
        P3.[t_lsta] <> @DeletedStatus
	 
	SET @RowIndex = 1
	SELECT @Total = COUNT(0) FROM @CTable
	WHILE(@RowIndex <= @Total)
	  BEGIN
	   SELECT
	    @C_Arcs = C.[Arcs],
		@C_Item = C.[Item],
		@C_Nqty = C.[Nqty],
		@C_PCLN = C.[PCLN],
		@C_PCRN = C.[PCRN],
		@C_PCRN_Line = C.[PCRN_Line],
		@C_PCRN_Revision = C.[PCRN_Revision],
		@C_StockNumber = C.[StockNumber],
		@C_StockRevision = C.[StockRevision]
	   FROM
	    @CTable AS C 
       WHERE 
	    C.[RowIndex] = @RowIndex
		
		DELETE FROM @L_Table
		
		SET @Query = CONCAT('[dbo].[SP_PCR_Get_last_PCR_Sequence_Details] ','''' + @C_PCRN + '''',', ', @C_PCRN_Line)
		
		INSERT INTO @L_Table([PCR_Revision], [PCLN], [StockNumber], [StockRevision], [Nqty], [Npcs])
		EXEC(@Query)

		SELECT
		  TOP 1
			  @L_PCR_Revision = L.[PCR_Revision],
			  @L_PCLN = L.[PCLN],
			  @L_Nqty = L.[Nqty],
			  @L_Npcs = L.[Npcs],
			  @L_StockNumber = L.[StockNumber],
			  @L_StockRevision = L.[StockRevision]
		FROM 
		 @L_Table AS L
		 
		 IF(@L_PCLN IS NULL OR LEN(@L_PCLN) = 0)
		  BEGIN
		    SET @Mode = 3  
		  END
		 ELSE
		  BEGIN
		   IF(@L_StockNumber = @C_StockNumber)
		    BEGIN
			  SET @Mode = 1
		    END
		   ELSE
		    BEGIN
			  SET @Mode = 2
			END
		  END
		  IF(@Mode <> 0)
		   BEGIN
		     EXECUTE @TotalNestedQuantity = [dbo].[SP_PCR_Get_Total_NestedQuantity] @C_PCRN, @C_PCRN_Line , @C_StockNumber, @C_PCRN_Revision			 
		   END
		  IF(@Mode = 1)
		   BEGIN
			 SET @N_Npcs = @L_Npcs + @TotalNestedQuantity - @L_Nqty
			 EXEC [dbo].[SP_PCR_Update_PCRLineStatus] @C_PCRN, @C_PCRN_Line, @C_PCRN_Revision, @C_StockNumber 
			 EXEC [dbo].[SP_PCR_Update_StockStatus] @C_Item, @C_StockNumber, @C_StockRevision
			 EXEC [dbo].[SP_PCR_Generate_New_PCR_Revision] @C_PCRN , @C_PCRN_Line , 
			      @C_PCRN_Revision, @C_StockNumber, @C_StockRevision, @C_Nqty, @C_Arcs, @N_Npcs, @L_PCR_Revision, @PSNo
 
 		   END
		   ELSE IF(@Mode = 2)
		    BEGIN			
			  SET @N_Npcs = @TotalNestedQuantity
			  EXEC [dbo].[SP_PCR_Update_PCRLineStatus] @C_PCRN, @C_PCRN_Line, @C_PCRN_Revision, @C_StockNumber 
			  EXEC [dbo].[SP_PCR_Update_StockStatus] @C_Item, @C_StockNumber, @C_StockRevision
			  EXEC [dbo].[SP_PCR_Generate_New_PCR_Revision] @C_PCRN , @C_PCRN_Line , 
			       @C_PCRN_Revision, @C_StockNumber, @C_StockRevision, @C_Nqty, @C_Arcs, @N_Npcs, @L_PCR_Revision, @PSNo
		    END
			ELSE IF(@Mode = 3)
			 BEGIN			   
			   SET @N_Npcs = @L_Npcs + @TotalNestedQuantity			
			   EXEC [dbo].[SP_PCR_Update_PCRLineStatus] @C_PCRN, @C_PCRN_Line, @C_PCRN_Revision, @C_StockNumber 
			   EXEC [dbo].[SP_PCR_Update_StockStatus] @C_Item, @C_StockNumber, @C_StockRevision
			   EXEC [dbo].[SP_PCR_Generate_New_PCR_Revision] @C_PCRN , @C_PCRN_Line , 
			        @C_PCRN_Revision, @C_StockNumber, @C_StockRevision, @C_Nqty, @C_Arcs, @N_Npcs, @L_PCR_Revision, @PSNo			   
			    EXEC [dbo].[SP_PCR_Update_PCRLineStatus _For_Last_PCRRevision] @C_PCRN, @C_PCRN_Line, @L_PCR_Revision 
			 END
	   SET @RowIndex += 1
	 END
     
	 DELETE FROM [dbo].[PCR505] WHERE [t_pcln] = @C_PCLN
	 
	 SELECT 
	   TOP 1
	   @PId = P.[Id],
	   @SQty = P.[t_sqty]
	 FROM 
	  [dbo].[PCR504] AS P
	 WHERE 
	   P.[t_pcln] = @C_PCLN
     ORDER BY 
	   P.[t_revn] DESC

      UPDATE P
	  SET 
	    P.[t_resn] = @RemarkReason,
	    P.[t_pmrk] = @PCLModifiedRemark,
		P.[t_rtar] = P.[t_rtar] + @SQty,
		P.[t_sqty] = 0,
		P.[t_stat] = NULL,
		P.[t_pcl_rldt] = NULL,
		P.[t_pmg_cdat] = NULL,
		P.[t_qa_adat] = NULL
	  FROM 
	   [dbo].[PCR504] AS P	  
	  WHERE 
	   P.[Id] = @PId

	  UPDATE P0
	  SET
	    P0.[t_qfre] = P0.[t_qfre] + @SQty
	  FROM 
		[dbo].[PCR500] AS P0
	  JOIN 
		[dbo].[PCR504] AS P ON P.[t_pcln] = P0.[t_pcln]
	  WHERE 
	    P.[Id] = @PId
	 
	 COMMIT TRAN
	 SELECT 1

	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(), 
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END
----------------------------
--43

GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_Release_PCL_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_Release_PCL_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Update_Release_PCL_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @PCRN NVARCHAR(100)
	 DECLARE @WCDeliver NVARCHAR(100)
	 DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [TotalCuttingLength] FLOAT, 
		 [WCDeliver] NVARCHAR(100), 
		 --[Planner] NVARCHAR(100), 
		 [Remark] NVARCHAR(MAX),
		 [MovementType] INT,
		 [ReturnBalance] INT,
		 [AllowedStage1] INT
	 )
	    
	INSERT INTO @TBL
	(
		 [Id], 
		 [TotalCuttingLength], 
		 [WCDeliver], 
		 --[Planner], 
		 [Remark],
		 [MovementType],
		 [ReturnBalance],
		 [AllowedStage1]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('TotalCuttingLength').value('.', 'FLOAT') AS [TotalCuttingLength], 
		Array.Entity.query('WCDeliver').value('.', 'NVARCHAR(100)') AS [WCDeliver],  
		--Array.Entity.query('Planner').value('.', 'NVARCHAR(100)') AS [Planner],	
		Array.Entity.query('Remark').value('.', 'NVARCHAR(MAX)') AS [Remark],	
		Array.Entity.query('MovementType').value('.', 'INT') AS [MovementType],
		Array.Entity.query('ReturnBalance').value('.', 'INT') AS [ReturnBalance],
		Array.Entity.query('AllowedStage1').value('.', 'INT') AS [AllowedStage1]	
			
	FROM @Data.nodes('/ArrayOfRELEASE_PCL_DATA_Result/RELEASE_PCL_DATA_Result') AS Array(Entity)
	
    IF(@Mode = 1) --UPDATE
	 BEGIN
	  
	  SELECT
	  TOP 1 
	    @PCRN = P3.[t_pcrn],
	    @WCDeliver = T.[WCDeliver]
	  FROM [dbo].[PCR503] AS P3 
	   INNER JOIN [dbo].[PCR504] AS P
	  ON P3.[t_pcln] = P.[t_pcln]
	   INNER JOIN @TBL AS T ON T.[Id] = P.[Id]

	  EXEC [dbo].[SP_PCR_Validate_WorkCenter] @PCRN, @WCDeliver

	  UPDATE tbl504
		SET 
		  tbl504.[t_tclt] = ROUND(ISNULL(B.TotalCuttingLength, 0), 2),
		  tbl504.[t_cwoc] = B.WCDeliver,
		  --tbl504.[t_emno] = B.Planner,
		  tbl504.[t_mrmk] = B.Remark,
		  tbl504.[t_mvtp] = B.MovementType,
		  tbl504.[t_rbal] = B.ReturnBalance,
		  tbl504.[t_stgi] = B.AllowedStage1
		FROM [dbo].[PCR504] AS tbl504
		INNER JOIN @TBL AS B ON B.Id = tbl504.Id
		COMMIT
	 END	 
	
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END 

--------------------
--44
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Authenticate_Workflow_Status]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Authenticate_Workflow_Status]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Authenticate_Workflow_Status]
    @PSNo NVARCHAR(100),
	@ScreenName NVARCHAR(100)
AS
BEGIN
    
	DECLARE @Exists BIT 
	DECLARE @Count INT
    SELECT @Count = COUNT(0) 
	FROM [dbo].[ATH001] AS U
	INNER JOIN [dbo].[ATH004] AS RolesTable ON RolesTable.[Id] = U.[Role]
	INNER JOIN [dbo].[ATH003] AS RoleProcessesTable ON RolesTable.[Id] = RoleProcessesTable.[RoleId]	
	INNER JOIN [dbo].[ATH002] AS ProcessesTable ON RoleProcessesTable.[ProcessId] = ProcessesTable.[Id]
	WHERE U.[Employee] = @PSNo AND ProcessesTable.[Area] = 'PCR' AND ProcessesTable.[IsActive] = 1 AND 
	ProcessesTable.[Process]  = @ScreenName AND RoleProcessesTable.[IsActive] = 1
    RETURN (CASE WHEN @Count > 0 THEN 1 ELSE 0 END)
	
END
----------------------------
--45
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_ENUM_LIST]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_ENUM_LIST]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_GET_ENUM_LIST]
    @PSNo NVARCHAR(MAX)
AS
BEGIN
	SELECT 
	  DISTINCT 
		ProcessesTable.[Process] AS Code, 
		ProcessesTable.[Description] AS [Description], 
		ProcessesTable.[Process] AS CodeDescription, 
		ProcessesTable.[OrderNo]
	FROM [dbo].[ATH001] AS U
		INNER JOIN [dbo].[ATH004] AS RolesTable ON RolesTable.[Id] = U.[Role]
		INNER JOIN [dbo].[ATH003] AS RoleProcessesTable ON RolesTable.[Id] = RoleProcessesTable.[RoleId]	
		INNER JOIN [dbo].[ATH002] AS ProcessesTable ON RoleProcessesTable.[ProcessId] = ProcessesTable.[Id]
	WHERE 
	    U.[Employee] = @PSNo 
		  AND 
	    ProcessesTable.[Area] = 'PCR' 
		  AND 
	    ProcessesTable.[IsActive] = 1 
		  AND 
	    ProcessesTable.[Controller] = 'PCLWorkflow'
	      AND
	    RoleProcessesTable.[IsActive] = 1
	ORDER BY ProcessesTable.[OrderNo]
END
---------------------
--46

GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_PCL_Workflow_DATA]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_PCL_Workflow_DATA]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
--EXEC [dbo].[SP_PCR_GET_PCL_Workflow_DATA] '122963', 'Receive Plate'
ALTER PROCEDURE [dbo].[SP_PCR_GET_PCL_Workflow_DATA]
    @PSNo NVARCHAR(100),
	@ScreenName NVARCHAR(100)
AS
BEGIN 
    SET NOCOUNT ON
	DECLARE @IsAuthorized BIT
    EXECUTE @IsAuthorized =  [dbo].[SP_PCR_Authenticate_Workflow_Status] @PSNo, @ScreenName
    IF(@IsAuthorized = 0)
	 BEGIN
	   RAISERROR('You are not authorized to see data for this screen', 11, 1)
	 END
	
    DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');
	DECLARE @Collation NVARCHAR(MAX) = ' COLLATE Latin1_General_100_CS_AS_KS_WS';
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @YesValue INT = 1
	DECLARE @IsProcessPCR BIT = 0
	DECLARE @IsSecondStageClearance BIT = 0
	DECLARE @IsResolvePCLDiscrepancy BIT  = 0
	DECLARE @StatusPerScreen AS TABLE
	(
	  [t_cnst] NVARCHAR(100)	      
	)
	DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	
	IF(@ScreenName = 'First Stage Clearance') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('conf.pmg')
	 END
	 IF(@ScreenName = 'Confirm Plate Cutting Request') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('appr.qa')
	 END
	 IF(@ScreenName = 'Receive Plate') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('plt.issued'),	  
	  ('rej.qc')
	 END
	 IF(@ScreenName = 'Process Plate Cutting Request') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('apprv.qc'),
	  ('plt.cut'),
	  ('rej.qc')
	  SET @IsProcessPCR = 1
	 END
	 IF(@ScreenName = 'Resolve PCL Discrepancy' )
	 BEGIN
	  INSERT INTO @StatusPerScreen
	   VALUES	   
	   ('plt.cut')	   
	   SET @IsResolvePCLDiscrepancy = 1
	 END
	 IF(@ScreenName = 'Second Stage Clearance') 
	 BEGIN
	  INSERT INTO @StatusPerScreen
	  VALUES
	  ('plt.mark'),
	  ('plt.cut')
	  SET @IsSecondStageClearance = 1
	 END
	 
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')

	DECLARE @tblWCLocation TABLE
	(
		[t_cadr] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_cwoc] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[WCDeliverName] NVARCHAR(MAX)
	)
	INSERT INTO @tblWCLocation([t_cadr], [t_cwoc], [t_dsca], [WCDeliverName])
	EXEC [dbo].[SP_PCR_Get_WorkCenter_With_Location] 1 

	DECLARE @tblLocationAddress AS TABLE 
	(
		[t_pcrn] NVARCHAR(100), 
		[t_loca] NVARCHAR(100),
		[t_ladr] NVARCHAR(100)
	)
	SET @SqlQuery = 
	' SELECT  
		A.[t_pcrn] , 
		A.[t_loca],
		C.[t_ladr] 
	    FROM ' + @lnlinkedserver + '.dbo.tltlnt000'+ @lncompanyid  + ' AS C 
	    INNER JOIN PCR501 AS A 
	      ON A.[t_loca] = C.[t_dimx]'

    INSERT INTO @tblLocationAddress
	(
		[t_pcrn],
		[t_loca],
		[t_ladr]
	)
	EXEC(@SqlQuery)
	
    SELECT
	    DISTINCT
		P.[Id] AS Id,
		P.[t_rlto] AS ReleaseTo,
		P.[t_pcln] AS PCLNumber,
		P.[t_revn] AS Revision,
		P.[t_mvtp] AS MovementType,
		PS.[t_txt] AS PCLStatus,
		P3.[t_prdt] AS PCRRequirementDate,
		P3.[t_pcrn] AS PCRNumber,
		WC.WCDeliverName AS WCDeliverName,
		NULL AS StockPhysical,
		P0.[t_stkn] AS StockNumber,
		P0.[t_area] AS TotalArea,
		P.[t_rbal] AS ReturnBalance,
		P.[t_rtar] AS ReturnArea,
		P.[t_ftra] AS FinalTotalReturnArea,
		NULL AS Project
		FROM [dbo].[PCR504] AS P
		INNER JOIN [dbo].[PCR500] AS P0 
		  ON P.[t_pcln] = P0.[t_pcln]
		INNER JOIN [dbo].[PCR503] AS P3 
		  ON P.[t_pcln] = P3.[t_pcln]
		INNER JOIN [dbo].[PCR501] AS P1
		  ON P3.[t_pcrn] = P1.[t_pcrn]
		LEFT OUTER JOIN @tblPCLStatus AS PS
		  ON P.[t_stat] = PS.[t_val]
		LEFT OUTER JOIN @tblLocationAddress AS LA
		  ON P3.[t_pcrn] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_pcrn] AND P1.[t_loca] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_loca]
		LEFT OUTER JOIN @tblWCLocation AS WC
		  ON P.[t_cwoc]  COLLATE Latin1_General_100_CS_AS_KS_WS = WC.[t_cwoc]
		AND WC.[t_cadr] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_ladr]
		WHERE 
		 (
		  (
			@IsProcessPCR = 1 
			AND 
			  (
				(UPPER(PS.[t_cnst]) IN (SELECT UPPER(SS.[t_cnst]) FROM @StatusPerScreen AS SS))
					OR  
				(UPPER(PS.[t_cnst]) = 'PLT.MARK' )--AND P.[t_new_case] = @YesValue)
			   
              )
		  )
		  OR
		  (	
			@IsSecondStageClearance = 1 
				AND 
			(P.[t_qc_adat] IS NULL OR P.[t_qc_adat] = 0)
				AND 
			(P.[t_qc_rdat] IS NULL OR P.[t_qc_rdat] = 0)
				AND 
			UPPER(PS.[t_cnst]) IN (SELECT UPPER(SS.[t_cnst]) FROM @StatusPerScreen AS SS)
		  )
		 OR
		 (
		  @IsResolvePCLDiscrepancy = 1
		     AND
           UPPER(PS.[t_cnst]) IN (SELECT UPPER(SS.[t_cnst]) FROM @StatusPerScreen AS SS)
		     AND
          P.[t_rtar] <> P.[t_ftra]
		 )
		 OR
		 (
		   @IsSecondStageClearance = 0
		    AND
           @IsProcessPCR = 0
		    AND
           @IsResolvePCLDiscrepancy = 0
		    AND
		   UPPER(PS.[t_cnst]) IN (SELECT UPPER(SS.[t_cnst]) FROM @StatusPerScreen AS SS)
		 )
	  )

END
-----------------------------
--47
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Generate_New_Stock_Revision]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Generate_New_Stock_Revision]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Generate_New_Stock_Revision]
    @Id INT,
	@PSNo NVARCHAR(20)
AS
 BEGIN
 BEGIN TRAN
  BEGIN TRY
   SET NOCOUNT ON
   DECLARE @A AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(1))
   DECLARE @T AS TABLE(RowIndex INT IDENTITY(1,1), [Length] FLOAT, [Width] FLOAT, Area FLOAT, [OddShape] INT)
   DECLARE @RowIndex INT
   DECLARE @PId INT
   DECLARE @Total INT
   DECLARE @Length FLOAT
   DECLARE @Width FLOAT
   DECLARE @Area500 FLOAT
   DECLARE @Area505 FLOAT
   DECLARE @Thickness FLOAT
   DECLARE @CNVF FLOAT
   DECLARE @CNVB FLOAT
   DECLARE @CDT1 FLOAT
   DECLARE @CCD1 FLOAT
   DECLARE @CDT2 FLOAT
   DECLARE @CCD2 FLOAT
   DECLARE @PCLN NVARCHAR(100)
   DECLARE @Revision INT
   DECLARE @CurrentStockNumber NVARCHAR(100)
   DECLARE @CurrentStockRevision INT
   DECLARE @PrevStockNumber NVARCHAR(100)
   DECLARE @PrevStockRevision INT
   DECLARE @StockNumber NVARCHAR(100)
   DECLARE @StockRevision INT
   DECLARE @Status INT
   DECLARE @Quantity FLOAT
   DECLARE @OddShape INT
   DECLARE @TWGT FLOAT
   DECLARE @Item NVARCHAR(100)
   DECLARE @RBAL INT
   DECLARE @SR INT
   DECLARE @Alpha NVARCHAR(1)
   INSERT INTO @A([Value])
   VALUES
	('A'), ('B'), ('C'), ('D'), ('E'), ('F'), ('G'), ('H'), ('I'), ('J'), ('K'), ('L'), ('M'), 
	('N'), ('O'), ('P'), ('Q'), ('R'), ('S'), ('T'), ('U'), ('V'), ('W'), ('X'), ('Y'), ('Z')
	SELECT
	 @PCLN = P.[t_pcln],
	 @Revision = P.[t_revn],
	 @RBAL = P.[t_rbal]
	FROM 
	 [dbo].[PCR504] AS P
	WHERE 
	 P.[Id] = @Id

	SELECT
	 TOP 1
	  @CurrentStockNumber = P0.[t_stkn] ,
	  @CurrentStockRevision = P0.[t_stkr],
	  @Item = P0.[t_item],
	  @Area500 = P0.[t_area],
	  @CNVF = P0.[t_cnvf],
	  @Thickness = P0.[t_thic],
	  @CDT1= P0.[t_cdt1],
	  @CDT2 = P0.[t_cdt2],
	  @CCD1 = P0.[t_ccd1],
	  @CCD2 = P0.[t_ccd2],
	  @CNVB = P0.[t_cnvb],
	  @PId = P0.[Id]
	FROM
	 [dbo].[PCR500] AS P0
	WHERE 
	 P0.[t_pcln] = @PCLN
	 
	SELECT 
	   TOP 1
	   @StockRevision = P0.[t_stkr]
	FROM 
	  [dbo].[PCR500] AS P0
	 WHERE 
	  LTRIM(RTRIM(P0.[t_item])) = LTRIM(RTRIM(@Item))
	    AND 
	  P0.[t_stkn] = @CurrentStockNumber
	 ORDER BY P0.[t_stkn] DESC

	SELECT @StockRevision += 1
	SET @SR = @StockRevision
	
	INSERT INTO
	 @T
	  (
	    [Length],
		[Width],
		[Area],
		[OddShape]
      )
	 SELECT
	   P.[t_leng],
	   P.[t_widt],
	   P.[t_area],
	   P.[t_odds]
	 FROM
	   [dbo].[PCR505] AS P
	 WHERE
	    P.[t_pcln] = @PCLN 
		 AND
        P.[t_revn] = @Revision
    
   SELECT @RowIndex = 1
   SELECT @Total = COUNT(0) FROM @T
	
   IF(@RBAL = 1)
	 BEGIN
	   SET @PrevStockNumber = @CurrentStockNumber
	   SET @PrevStockRevision = @CurrentStockRevision
	 END
	
	SELECT
	    	TOP 1 @Status = [Value] 
	 FROM 
			[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('Stock Status','ltsfc.stk.stat') 
	 WHERE 
	        UPPER([LNConstant]) = 'CREATED'
	 
	WHILE(@RowIndex <= @Total)
	 BEGIN
	  SELECT 
	    @Area505 = T.[Area] ,
	    @Length = T.[Length] ,
	    @Width = T.[Width],
		@OddShape = T.[OddShape]
	  FROM
	    @T AS T
	  WHERE 
		T.[RowIndex] = @RowIndex
	   IF(@RBAL = 1)
	    BEGIN
		  SET @StockNumber = @CurrentStockNumber
		  SET @StockRevision = @SR
		  SET @SR += 1
	    END
	   ELSE
		BEGIN
		  SELECT TOP  1
			@Alpha = A.[Value] 
		  FROM 
			@A AS A 
		  WHERE 
			A.[RowIndex] = @RowIndex
		  SET @StockRevision = @CurrentStockRevision
		  SET @StockNumber = CONCAT(@CurrentStockNumber, @Alpha)
		END
		
		SET @Quantity = (@Area505/1000) * ((@Thickness * @CNVF) + ((@CDT1 * @CCD1) + (@CDT2 * @CCD2)))		
		SET @TWGT = ((@Area505) * ((@CNVB * @Thickness) + (@CCD1 * @CDT1) + (@CCD2 * @CDT2)))/1000
		INSERT INTO 
			[dbo].[PCR500]
			(
				[t_pstk], [t_prev], [t_stkn], [t_stkr],
				[t_leng], [t_widt],	[t_area], [t_arcs],
				[t_pcln], [t_qall],	[t_qfre], [t_qnty],
				[t_stat], [t_twgt],	[t_odds],
				[t_item], [t_loca], [t_arms], [t_mtrl],
				[t_qres], [t_qcut], [t_orno], [t_pono],
				[t_rcno], [t_rcln], [t_shtp], [t_shfl],
				[t_size], [t_cwar], [t_wloc], [t_cnvf],
				[t_htno], [t_tcno], [t_gorn], [t_mchn], 
				[t_armv], [t_qapr], [t_cono], [t_cdt1], 
				[t_cdt2], [t_cnvb], [t_ccd1], [t_ccd2], 
				[t_psta], [t_clot], [t_idsf], [t_cprj],
				[t_cspa], [t_rmrk], [t_cwoc], [t_amod],
				[t_rorn], [t_rrst], [t_srmk], [t_sono],
				[t_runn], [t_cons], [t_sprj], [t_okey],
				[t_sspa], [t_sact], [t_scrn], [t_shpm],
				[t_rqno], [t_ncra], [t_ncrn], [t_ncrr], 
				[t_rsfp], [t_rsby], [t_rsdt], [t_ityp], 
				[t_remk], [t_Refcntd], [t_Refcntu]
			 )		
			
			 SELECT 
			    @PrevStockNumber, @PrevStockRevision, @StockNumber, @StockRevision,
			    @Length, @Width, @Area505, 0, 
				NULL, 0, @Area500, @Quantity, 
				@Status, @TWGT, @OddShape,
				P0.[t_item], P0.[t_loca], P0.[t_arms], P0.[t_mtrl],
				P0.[t_qres], P0.[t_qcut], P0.[t_orno], P0.[t_pono],
				P0.[t_rcno], P0.[t_rcln], P0.[t_shtp], P0.[t_shfl],
				P0.[t_size], P0.[t_cwar], P0.[t_wloc], P0.[t_cnvf],
				P0.[t_htno], P0.[t_tcno], P0.[t_gorn], P0.[t_mchn], 
				P0.[t_armv], P0.[t_qapr], P0.[t_cono], P0.[t_cdt1], 
				P0.[t_cdt2], P0.[t_cnvb], P0.[t_ccd1], P0.[t_ccd2], 
				P0.[t_psta], P0.[t_clot], P0.[t_idsf], P0.[t_cprj],
				P0.[t_cspa], P0.[t_rmrk], P0.[t_cwoc], P0.[t_amod],
				P0.[t_rorn], P0.[t_rrst], P0.[t_srmk], P0.[t_sono],
				P0.[t_runn], P0.[t_cons], P0.[t_sprj], P0.[t_okey],
				P0.[t_sspa], P0.[t_sact], P0.[t_scrn], P0.[t_shpm],
				P0.[t_rqno], P0.[t_ncra], P0.[t_ncrn], P0.[t_ncrr], 
				P0.[t_rsfp], P0.[t_rsby], P0.[t_rsdt], P0.[t_ityp], 
				P0.[t_remk], P0.[t_Refcntd], [t_Refcntu]				
			  FROM 
				 [dbo].[PCR500] AS P0
               WHERE 
				  P0.[Id] = @PId
	    SET @RowIndex += 1	  
	 END
	 COMMIT TRAN							 
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(), 
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END
------------------------
--48
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Approve_Reject_PCL_Workflow]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Approve_Reject_PCL_Workflow]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Approve_Reject_PCL_Workflow]
    @PSNo NVARCHAR(100),
	@Id INT,
	@CurrentScreen NVARCHAR(100),
	@Approve BIT = 1
AS
 BEGIN
    DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');
	DECLARE @Query NVARCHAR(MAX)
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @TempTableQuery NVARCHAR(MAX)
	DECLARE @NextStatus NVARCHAR(100)
	DECLARE @NextStatusValue INT
	DECLARE @InsertInLN504Table BIT  = 0
	DECLARE @InsertInLN505Table BIT  = 0
	DECLARE @InsertIn500Table BIT = 0
	DECLARE @UpdateInLN504Table BIT = 0
	DECLARE @YesValue INT = 1
	DECLARE @ReadyForReleased INT = 2
	DECLARE @Status BIT
	DECLARE @PCLNumber NVARCHAR(100)
	DECLARE @Revision INT
	SELECT 
	 TOP 1 
       @PCLNumber = P.[t_pcln] , 
	   @Revision = P.[t_revn] 
	FROM [dbo].[PCR504] AS P 
	  WHERE P.[Id] = @Id
	
	IF(@CurrentScreen = 'First Stage Clearance') 
	 BEGIN
	   SET @NextStatus = 'appr.qa'
	 END
	IF(@CurrentScreen = 'Confirm Plate Cutting Request') 
	 BEGIN
	   SET @NextStatus = 'plt.issued'
	   SET @InsertInLN504Table = 1
	   SET @InsertIn500Table = 1
	 END
	 IF(@CurrentScreen = 'Receive Plate') 
	 BEGIN
	   SET @NextStatus = 'plt.mark'
	   SET @UpdateInLN504Table = 1 
	   SET @ReadyForReleased = 1
	 END
	 IF(@CurrentScreen = 'Process Plate Cutting Request') 
	 BEGIN
	   SET @NextStatus = 'plt.cut'
	   SET @UpdateInLN504Table = 1 
	 END
	 IF(@CurrentScreen = 'Resolve PCL Discrepancy') 
	 BEGIN
	   SET @NextStatus = 'pcl.close'
	   SET @UpdateInLN504Table = 1 
	 END
	 IF(@CurrentScreen = 'Second Stage Clearance') 
	 BEGIN
	   SET @NextStatus = 'apprv.qc'
	   SET @UpdateInLN504Table = 1 
	 END
	SELECT 
	TOP 1 
	  @NextStatusValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = UPPER(@NextStatus)
	 BEGIN TRAN
	   BEGIN TRY
	            UPDATE [dbo].[PCR504] 
			    SET [t_stat] =  @NextStatusValue , 
			     WHERE [Id] = @Id,
				 IF(@InsertIn500Table  = 1 )
				    BEGIN
				        EXEC [dbo].[SP_PCR_Generate_New_Stock_Revision] @Id = @Id, @PSNo =  @PSNo
				     END 
		         SELECT 1 AS [Status], NULL AS [MESSAGE] 
       COMMIT TRAN
	END TRY
	BEGIN CATCH
	  SELECT  0 AS [Status],  ERROR_MESSAGE() AS [MESSAGE]
	  IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(), 
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				--PRINT @ErrorMessage 
				SELECT  0 AS [Status],  @ErrorMessage AS [MESSAGE]
			END
	END CATCH

	
END
-----------------------------
--49
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_Workflow_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_Workflow_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Update_Workflow_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT  = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @Id INT
	 DECLARE @StatusValue NVARCHAR(20)	 
	 DECLARE @tbl AS TABLE
	 (
		 [Id] INT, 
		 [ReleaseTo] NVARCHAR(100),
		 [FinalTotalReturnArea] FLOAT
	 )
	    
	INSERT INTO @tbl
	(
	[Id], 
	[ReleaseTo],
	[FinalTotalReturnArea]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('ReleaseTo').value('.', 'NVARCHAR(100)') AS [ReleaseTo],
		Array.Entity.query('FinalTotalReturnArea').value('.', 'FLOAT') AS [ReleaseTo]
	FROM 
		@Data.nodes('/ArrayOfPCL_WORKFLOW_Result/PCL_WORKFLOW_Result') AS Array(Entity)
   
   DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')
    
	IF(@Mode = 1) --UPDATE
	 BEGIN
	  SELECT 
		TOP 1 @StatusValue = UPPER(TP.[t_cnst])
	  FROM 
		[dbo].[PCR504] AS P 
	  INNER JOIN @tbl AS T ON T.Id = P.[Id]
	  INNER JOIN @tblPCLStatus AS TP ON TP.[t_val] = P.[t_stat]
	 
	 
	 

	--IF(@StatusValue <> @ApprovedStatusValue)
	-- BEGIN
	--	 RAISERROR('Release to value cannot be applied to this screen', 12, 1)
	-- END
	 IF(@StatusValue =  'APPR.QA')
	  BEGIN
	    UPDATE tbl504
		SET 
		  tbl504.[t_rlto] = B.ReleaseTo
		FROM 
		 [dbo].[PCR504] AS tbl504
		INNER JOIN @tbl AS B ON B.Id = tbl504.Id
	  END
	 ELSE IF(@StatusValue = 'APPRV.QC' OR @StatusValue = 'PLT.CUT' OR @StatusValue = 'REJ.QC' OR @StatusValue = 'PLT.MARK')
	   BEGIN
	     UPDATE tbl504
		SET 
		  tbl504.[t_ftra] = B.FinalTotalReturnArea
		FROM 
		 [dbo].[PCR504] AS tbl504
		INNER JOIN @tbl AS B ON B.Id = tbl504.Id
	   END		
	  COMMIT
	 END	
	  
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE() ,
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END 







 

 
 






















