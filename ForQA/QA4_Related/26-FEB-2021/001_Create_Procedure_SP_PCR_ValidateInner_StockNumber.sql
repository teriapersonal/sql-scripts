IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_ValidateInner_StockNumber]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_ValidateInner_StockNumber]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_ValidateInner_StockNumber]
   @PSNo NVARCHAR(100),
   @Id INT,   
   @StockNumber NVARCHAR(100),
   @StockRevision INT,
   @UseAlternateStock INT,
   @Location NVARCHAR(100),   
   @Project NVARCHAR(100), 
   @ChildItem NVARCHAR(100),
   @PartNumber INT,
   @Element NVARCHAR(100),
   @PCRN NVARCHAR(100),
   @PONO INT, 
   @Revision INT
		
AS
BEGIN 
	SET NOCOUNT ON
	BEGIN TRY
	     DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	     DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');
		 DECLARE @Sql NVARCHAR(MAX)		 
		 DECLARE @Status INT 
		 DECLARE @RCNO NVARCHAR(100)
		 DECLARE @RCLN INT 
		 DECLARE @Quantity FLOAT
		 DECLARE @PCLN NVARCHAR(100)
		 DECLARE @PSTA INT
		 DECLARE @Item NVARCHAR(100)
		 DECLARE @CLOT NVARCHAR(100)
		 DECLARE @CWAR NVARCHAR(100)
	     DECLARE @ContractTable AS TABLE([Value] NVARCHAR(100))		
		 DECLARE @Contract NVARCHAR(100)
		 DECLARE @OContract NVARCHAR(100)
		 DECLARE @R AS TABLE([CNT] INT)
		 DECLARE @AA AS TABLE([V] INT)
		 DECLARE @VV INT			   
		 DECLARE @CNT INT
		 DECLARE @YesValue INT
		 DECLARE @NoValue INT 
		 DECLARE @S INT
		 DECLARE @Message NVARCHAR(MAX)
		 DECLARE @StatusReleasedValue INT
		 DECLARE @StockPhysicalStatusConsumed INT
		 
		 SELECT
			TOP 1 @YesValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		 WHERE UPPER([LNConstant]) = 'YES'

		SELECT
			TOP 1 @NoValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		WHERE UPPER([LNConstant]) = 'NO'

		 IF(@UseAlternateStock = @NoValue) -- No
		   BEGIN		       
			   SELECT    
			     TOP 1
				  @Status = P0.[t_stat],
				  @RCNO = P0.[t_rcno],
				  @RCLN = P0.[t_rcln],
				  @PCLN = P0.[t_pcln],
				  @Item = LTRIM(RTRIM(P0.[t_item])),
				  @Quantity = P0.[t_qnty],
				  @PSTA = P0.[t_psta],
				  @CLOT = P0.[t_clot],
				  @CWAR = P0.[t_cwar],
				  @Contract = P0.[t_cono]			   
			 FROM 
				[dbo].[PCR500] AS P0 
			 WHERE 
			   P0.[t_loca] = @Location 
			  AND 
			   LTRIM(RTRIM(P0.[t_item])) = @ChildItem 
			  AND 
			   P0.[t_stkn] = @StockNumber 
			  AND 
			   P0.[t_stkr] = @StockRevision

			END
		 ELSE
			BEGIN
			  SELECT    
			     TOP 1
				  @Status = P0.[t_stat],
				  @RCNO = P0.[t_rcno],
				  @RCLN = P0.[t_rcln],
				  @PCLN = P0.[t_pcln],
				  @Item = LTRIM(RTRIM(P0.[t_item])),
				  @Quantity = P0.[t_qnty],
				  @PSTA = P0.[t_psta],
				  @CLOT = P0.[t_clot],
				  @CWAR = P0.[t_cwar],
				  @Contract = P0.[t_cono]
			   /*ltsfc500.stkn, ltsfc500.stat, ltsfc500.rcno, ltsfc500.rcln, ltsfc500.qnty, 
               ltsfc500.cono, ltsfc500.pcln, ltsfc500.psta, ltsfc500.item, ltsfc500.clot, ltsfc500.cwar*/
			 FROM 
				[dbo].[PCR500] AS P0 
			 WHERE
			   P0.[t_stkn] = @StockNumber 
			    AND
			   P0.[t_stkr] = @StockRevision
			    AND
			   P0.[t_loca] = @Location 			  
			   
		 END

		SET @Sql = 
		 ' SELECT 
		   TOP 1 A.[t_cono] 
			FROM ' + @LNLinkedServer + '.dbo.ttpctm110' + @LNCompanyId + ' AS A 
			WHERE A.[t_cprj] = ''' + @Project + ''''
		 
		INSERT INTO @ContractTable([Value])
		EXEC(@Sql)
		 
		SELECT 
			TOP 1 
			@OContract = C.[Value] 
		FROM 
			@ContractTable AS C
	    IF(@Contract <> @OContract)
		  BEGIN
		   -- RAISERROR('PCR Line does not belong to contract mentioned in stock', 12, 1)
		   PRINT 1
		  END

		 
		 SET @Sql = 'SELECT COUNT(0) AS CNT FROM ' + @LNLinkedServer + '.dbo.tltmig601' + @LNCompanyId + ' AS A 
		 WHERE LTRIM(RTRIM(A.[t_eitm])) = ''' + @Item + ''' AND A.[t_elot] = ''' + @CLOT + ''''
		 INSERT INTO @R([CNT])
		 EXEC(@Sql)
		 SELECT @CNT = R.[CNT] FROM @R AS R
		 IF(@CNT = 0)
		   BEGIN
		     IF(@RCNO IS NULL OR LEN(@RCNO) = 0)
			   BEGIN
			     --RAISERROR('Receipt not generated for specified stock', 12, 1)
				 PRINT 2
			   END
			 SET @Sql = 
			   CONCAT(' SELECT 
					TOP 1 A.[t_conf] 
				FROM ' + @LNLinkedServer + '.dbo.twhinh312' + @LNCompanyId + ' AS A 
			    WHERE A.[t_rcln] = ', @RCLN , ' AND A.[t_rcno] = ''' + @RCNO + '''')
			 
			 --DELETE FROM @AA 
			 --INSERT INTO @AA([V])
			 --EXEC(@Sql)
			   
			 --SELECT 
				--TOP 1 @VV = A.[V] 
			 --FROM @AA AS A
			 
			 IF(@VV <> @YesValue) -- Not Yes
			    BEGIN
				 -- RAISERROR('Receipt not Confirmed for specified stock', 12, 1)
				 PRINT 3
			    END
				
			 IF(@Quantity = 0)
				 BEGIN
				   --SET @Message = CONCAT('Receipt', @RCNO, ' for stock number ', @StockNumber , ' is yet to be inspected')
				   --RAISERROR(@Message, 12, 1)
				   PRINT 4
				 END
           END
	  IF(@PCLN IS NOT NULL AND LEN(@PCLN) > 0)
		 BEGIN
			SELECT 
				TOP 1 @S = P.[t_stat]  
			FROM [dbo].[PCR504] AS P 
			WHERE P.[t_pcln] = @PCLN AND P.[t_revn] = @Revision
					
			SELECT
				TOP 1 @StatusReleasedValue = [Value] 
			FROM 
				[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
			WHERE 
				UPPER([LNConstant]) = 'PCL.REL'
					
			IF(@S = @StatusReleasedValue) 
				BEGIN
					SET @Message = CONCAT(
					'PCL ', 
					@PCLN, 
					' already released for stock number ', 
					@StockNumber, 
					' and revision', 
					@Revision)
					RAISERROR(@Message, 12, 1)
				END
		 END
				  
      SELECT
		TOP 1 @StockPhysicalStatusConsumed = [Value] 
	  FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('Stock Physical Status','ltsfc.p.stat') 
	  WHERE 
		UPPER([LNConstant]) = 'CONSUMED'
	  IF(@PSTA = @StockPhysicalStatusConsumed)
		BEGIN
			SET @Message = CONCAT('Stock number ', @StockNumber, ' and revision', @Revision, ' is already consumed')
			RAISERROR(@Message, 12, 1)
		END
		 
	  DELETE FROM @R
	  SET @Sql = 
				' SELECT 
					COUNT(0) AS CNT 
					FROM ' + @LNLinkedServer + '.dbo.twhinr140' + @LNCompanyId +
					' AS A 
					WHERE 
						A.[t_cwar] = ''' + @CWAR + ''' AND 
						LTRIM(RTRIM( A.[t_item])) = ''' + @Item + ''' AND
					   ( A.[t_clot] = ''' + @StockNumber + ''' OR A.[t_clot] = ''' + @StockNumber + ''')'
	  INSERT INTO @R([CNT])
	  EXEC(@Sql)
		 
	  SELECT @CNT = R.[CNT] FROM @R AS R 
		 
	  IF(@CNT = 0)
		 BEGIN
			--RAISERROR('Mismatch in stock inventory and actual stock point inventory', 12, 1)
			PRINT 5
		 END
		  
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END
