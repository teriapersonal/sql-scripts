--USE [IEMQS]
USE [IEMQS_QA]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Reset_All_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Reset_All_Data]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Reset_All_Data]
AS
BEGIN 
	SET NOCOUNT ON
	BEGIN TRAN
       BEGIN TRY 
			DELETE FROM [dbo].[PCR506]
			DELETE FROM [dbo].[PCR501]
			DELETE FROM [dbo].[PCR503]
			DELETE FROM [dbo].[PCR504]
			DELETE FROM [dbo].[PCR505]
			DELETE FROM [dbo].[PCR500]			
			INSERT INTO [dbo].[PCR500]
			SELECT * FROM [PhzpdSqldb2k12].[Lnappqa4db].dbo.tltsfc500175
			COMMIT
			SELECT 1
	  END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END