 USE [IEMQS]
 GO
 BEGIN TRAN
       BEGIN TRY 
		 IF NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_PCR500_pcln')   
				 BEGIN
				   CREATE NONCLUSTERED INDEX IX_PCR500_pcln ON [dbo].[PCR500]([t_pcln] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
						 DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
				 END  
		IF NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_PCR500_item_stkn_stkr')   
				 BEGIN
				   CREATE UNIQUE NONCLUSTERED INDEX IX_PCR500_item_stkn_stkr ON [dbo].[PCR500]([t_item] ASC, [t_stkn] ASC, [t_stkr] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
						 DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
				 END  
		IF NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = 'IX_PCR500_loca_item_stkn_stkr')   
				 BEGIN
				   CREATE UNIQUE NONCLUSTERED INDEX IX_PCR500_loca_item_stkn_stkr ON [dbo].[PCR500]([t_loca] ASC, [t_item] ASC, [t_stkn] ASC, [t_stkr] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
						 DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
				 END 
			COMMIT
	  END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 

