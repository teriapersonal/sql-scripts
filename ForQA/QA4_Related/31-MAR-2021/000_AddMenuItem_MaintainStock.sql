USE [IEMQS_QA]
GO
BEGIN
DECLARE @ParentID INT
DECLARE @RoleID INT
DECLARE @ProcessID INT
DECLARE @Process NVARCHAR(MAX) = 'Display Stock'
SELECT TOP 1 @ParentID = Id FROM [dbo].[ATH002] WHERE [Area] = 'PCR' and [ParentId] IS NULL
IF NOT EXISTS(SELECT 0 FROM [dbo].[ATH002] WHERE [Area] = 'PCR' AND [ParentId] = @ParentID AND [Process] = @Process)
 BEGIN
  INSERT INTO [dbo].[ATH002] ([Process], [Description], [Area], [Controller], [Action], [ParentId], [MainMenuOrderNo], 
  [OrderNo], [IsDisplayInMenu], [IconClass], [IsActive], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn], [Help], [FAQ], [IsMobileMenu])
   VALUES (@Process, 'Stock screen', 'PCR', 'MaintainStock', 'Index', @ParentID, NULL, 23, 1, NULL, 1, '122963', GETUTCDATE(), NULL, NULL, NULL, NULL, NULL)
 END
 SELECT @ProcessID = Id FROM [dbo].[ATH002] WHERE [Area] = 'PCR' AND  [Process] = @Process
 DECLARE @T AS TABLE(RoleId INT, RowNumber INT IDENTITY(1,1) PRIMARY KEY)
 DECLARE @Counter INT
 DECLARE @Total INT
 INSERT INTO @T(RoleId)
 SELECT A.[Id] FROM [dbo].[ATH004] AS A WHERE A.[Role] IN ('IT1', 'IT2', 'IT3', 'MATL3', 'ITA1')
 SELECT @Total = COUNT(0) FROM @T
 SELECT @Counter = 1
 WHILE(@Counter <= @Total)
  BEGIN
    SELECT @RoleID = T.[RoleId] FROM @T AS T WHERE T.[RowNumber] = @Counter
	IF NOT EXISTS (SELECT 0 FROM  [dbo].[ATH003] AS A WHERE A.[RoleId] = @RoleID AND A.[ProcessId] = @ProcessID)
	 BEGIN
	   INSERT INTO [dbo].[ATH003]([ProcessId], [RoleId], [IsActive], [IsMobileMenu], [CreatedBy], [CreatedOn], [EditedBy], [EditedOn])
	   VALUES(@ProcessID, @RoleID, 1, 0, '122963', GETUTCDATE(), NULL, NULL)
	   --PRINT @Counter
	 END
	SET @Counter += 1
  END
--SELECT * from ATH003 WHERE ProcessId = 5348
END


--select * from ath002 where Area = 'PCR' Process='Display Stock'

