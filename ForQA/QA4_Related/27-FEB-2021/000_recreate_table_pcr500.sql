USE [IEMQS_QA]
	
BEGIN TRAN
   BEGIN TRY 
	   IF OBJECT_ID('[dbo].[PCR500]', 'u') IS NOT NULL 
	     BEGIN
           DROP TABLE [dbo].[PCR500]
		 END
		CREATE TABLE [dbo].[PCR500]
		  (
			Id INT IDENTITY(1,1) PRIMARY KEY, 
			t_item NVARCHAR(94) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_stkn NVARCHAR(40) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_stkr INT NULL , 
			t_loca NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_leng FLOAT NULL , 
			t_widt FLOAT NULL , 
			t_arms NVARCHAR(60) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_qnty FLOAT NULL , 
			t_mtrl NVARCHAR(60) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_thic FLOAT NULL , 
			t_qres FLOAT NULL , 
			t_qall FLOAT NULL , 
			t_qcut FLOAT NULL , 
			t_qfre FLOAT NULL , 
			t_orno NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_pono INT NULL , 
			t_rcno NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_rcln INT NULL , 
			t_shtp NVARCHAR(40) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_shfl NVARCHAR(40) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_size NVARCHAR(60) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_cwar NVARCHAR(12) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_wloc NVARCHAR(20) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_cnvf FLOAT NULL , 
			t_stat INT NULL , 
			t_htno NVARCHAR(40) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_tcno NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_gorn NVARCHAR(40) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_mchn NVARCHAR(60) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_area FLOAT NULL , 
			t_arcs FLOAT NULL , 
			t_pcln NVARCHAR(60) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_armv NVARCHAR(12) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_qapr FLOAT NULL , 
			t_cono NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_cdt1 FLOAT NULL , 
			t_cdt2 FLOAT NULL , 
			t_cnvb FLOAT NULL , 
			t_ccd1 FLOAT NULL , 
			t_ccd2 FLOAT NULL , 
			t_twgt FLOAT NULL , 
			t_psta INT NULL , 
			t_clot NVARCHAR(40) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_pstk NVARCHAR(40) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_prev INT NULL , 
			t_odds INT NULL , 
			t_idsf INT NULL , 
			t_cprj NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_cspa NVARCHAR(16) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_rmrk NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_cwoc NVARCHAR(12) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_amod INT NULL , 
			t_rorn NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_rrst INT NULL , 
			t_srmk NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_sono NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_runn NVARCHAR(24) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_cons INT NULL , 
			t_sprj NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_okey NVARCHAR(20) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_sspa NVARCHAR(16) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_sact NVARCHAR(16) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_scrn NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_shpm NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_rqno NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_ncra INT NULL , 
			t_ncrn NVARCHAR(30) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_ncrr NVARCHAR(1000) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_rsfp INT NULL , 
			t_rsby NVARCHAR(18) COLLATE Latin1_General_100_CS_AS_KS_WS NULL , 
			t_rsdt DATETIME NULL , 
			t_ityp INT NULL , 
			t_Refcntd INT NULL , 
			t_Refcntu INT NULL , 
			t_remk NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS NULL 
		  )
		COMMIT
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
		
		
		

		