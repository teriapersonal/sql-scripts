--USE [IEMQS]
USE [IEMQS_QA]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Insert_Update_Material_Planner_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Insert_Update_Material_Planner_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Insert_Update_Material_Planner_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @ChildItem NVARCHAR(100)
	 DECLARE @OtherItem NVARCHAR(100)
	 DECLARE @AreaConsumed FLOAT
	 DECLARE @UseAlternativeStock INT
	 DECLARE @StockNumber NVARCHAR(100)
	 DECLARE @StockRevision INT
	 DECLARE @OldStockNumber NVARCHAR(100)
	 DECLARE @OldStockRevision INT
	 DECLARE @OldAreaConsumed FLOAT
	 DECLARE @Id INT
	 DECLARE @SQall FLOAT
	 DECLARE @SArea FLOAT
	 DECLARE @SQCut FLOAT
	 DECLARE @YesValue INT 
	 DECLARE @AA FLOAT
	 DECLARE @StockArea FLOAT
	 DECLARE @PCLN NVARCHAR(100)
	 DECLARE @ApprovedQuantity INT
	 DECLARE @RejectedQuantity INT
	 DECLARE @IsMain BIT
	 SELECT
		TOP 1 @YesValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') 
		WHERE UPPER([LNConstant]) = 'YES'
	  
     DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [PCRRequirementDate] DATETIME, 
		 [ManufacturingItem] NVARCHAR(100), 
		 [ChildItem] NVARCHAR(100), 
		 [NestedQuantity] INT,
		 [AreaConsumed] FLOAT,
		 --[LengthModified] FLOAT,
		 --[WidthModified] FLOAT, 
		 [StockNumber] NVARCHAR(100),
		 [PCLNumber] NVARCHAR(100),
		 --[MovementType] INT,
		 [PCRPlannerRemark] NVARCHAR(100),
		 [StockRevision] INT,
		 [MaterialPlanner] NVARCHAR(100),
		 --[GrainOrientation] NVARCHAR(100),
		 --[ShapeType] NVARCHAR(100),
		 [ShapeFile] NVARCHAR(100),
		 [WCDeliver] NVARCHAR(100),
		 [UseAlternateStock] INT,
		 [AlternateStockRemark] NVARCHAR(MAX),
		 [CuttingLocationModified] NVARCHAR(100),
		 [ApprovedQuantity] INT,
		 [RejectedQuantity] INT,
		 [IsMain] BIT
	 )
	    
	INSERT INTO @TBL
	(
		 [Id],
		 [PCRRequirementDate], 
		 [ManufacturingItem], 
		 [ChildItem], 
		 [NestedQuantity],
		 [AreaConsumed],
		 --[LengthModified],
		 --[WidthModified], 
		 [StockNumber],
		 [PCLNumber],
		 --[MovementType],
		 [PCRPlannerRemark],
		 [StockRevision],
		 [MaterialPlanner],
		 --[GrainOrientation],
		 --[ShapeType],
		 [ShapeFile],
		 [WCDeliver],
		 [UseAlternateStock],
		 [AlternateStockRemark],
		 [CuttingLocationModified],
		 [ApprovedQuantity],
		 [RejectedQuantity],
		 [IsMain]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('PCRRequirementDate').value('.', 'DATETIME') AS [PCRRequirementDate], 
		Array.Entity.query('ManufacturingItem').value('.', 'NVARCHAR(100)') AS [ManufacturingItem],  
		Array.Entity.query('ChildItem').value('.', 'NVARCHAR(100)') AS [ChildItem],	
		Array.Entity.query('NestedQuantity').value('.', 'INT') AS [NestedQuantity],	
		Array.Entity.query('AreaConsumed').value('.', 'FLOAT') AS [AreaConsumed],
		--Array.Entity.query('LengthModified').value('.', 'FLOAT') AS [LengthModified],
		--Array.Entity.query('WidthModified').value('.', 'FLOAT') AS [WidthModified],
		Array.Entity.query('StockNumber').value('.', 'NVARCHAR(100)') AS [StockNumber],	
		Array.Entity.query('PCLNumber').value('.', 'NVARCHAR(100)') AS [PCLNumber],	
		--Array.Entity.query('MovementType').value('.', 'INT') AS [MovementType],	
		Array.Entity.query('PCRPlannerRemark').value('.', 'NVARCHAR(100)') AS [PCRPlannerRemark],	
		Array.Entity.query('StockRevision').value('.', 'INT') AS [StockRevision],	
		Array.Entity.query('MaterialPlanner').value('.', 'NVARCHAR(100)') AS [MaterialPlanner],	
		--Array.Entity.query('GrainOrientation').value('.', 'NVARCHAR(100)') AS [GrainOrientation],	
		--Array.Entity.query('ShapeType').value('.', 'NVARCHAR(100)') AS [ShapeType],	
		Array.Entity.query('ShapeFile').value('.', 'NVARCHAR(100)') AS [ShapeFile],	
		Array.Entity.query('WCDeliver').value('.', 'NVARCHAR(100)') AS [WCDeliver],	
		Array.Entity.query('UseAlternateStock').value('.', 'INT') AS [UseAlternateStock],	
		Array.Entity.query('AlternateStockRemark').value('.', 'NVARCHAR(MAX)') AS [AlternateStock],	
		Array.Entity.query('CuttingLocationModified').value('.', 'NVARCHAR(100)') AS [CuttingLocationModified],
		Array.Entity.query('ApprovedQuantity').value('.', 'INT') AS [ApprovedQuantity],	
		Array.Entity.query('RejectedQuantity').value('.', 'INT') AS [RejectedQuantity],
		Array.Entity.query('IsMain').value('.', 'BIT') AS [IsMain]
			
	FROM @Data.nodes('/ArrayOfSP_PCR_GET_MATERIAL_PLANNER_DATA_Result/SP_PCR_GET_MATERIAL_PLANNER_DATA_Result') AS Array(Entity)
	
    IF(@Mode = 1) --UPDATE
	  SELECT 
		TOP 1 @IsMain = T.[IsMain] 
	  FROM @TBL AS T 
	 BEGIN
	  IF(@IsMain = 0)
	   BEGIN
	    UPDATE tbl503
		SET 
		  tbl503.[t_iqty] = B.ApprovedQuantity,
		  tbl503.[t_rqty] = B.RejectedQuantity
		FROM [dbo].[PCR503] AS tbl503
		INNER JOIN @TBL AS B ON B.[Id] = tbl503.[Id]
	  END
	  ELSE
	   BEGIN
	    SELECT
	    TOP 1 
		  @UseAlternativeStock = T.[UseAlternateStock],
		  @StockNumber = T.[StockNumber] ,
		  @StockRevision = T.[StockRevision],
		  @AreaConsumed = T.[AreaConsumed],
		  @Id = T.[Id]
	  FROM @TBL AS T
	  
	    SELECT 
	    @OldStockNumber = P.[t_stkn],
		@OldStockRevision = P.[t_stkr],
		@OldAreaConsumed = ISNULL(P.[t_arcs], 0),
		@ChildItem = P.[t_sitm]
	  FROM [dbo].[PCR503] AS P WHERE P.[Id] = @Id

	    IF(@UseAlternativeStock = @YesValue) 
	   BEGIN
	     SELECT 
	     @OtherItem = @ChildItem,
	     @ChildItem = P.[t_item]
	     FROM [dbo].PCR500 AS P WHERE P.[t_stkn] = @StockNumber AND P.[t_stkr] = @StockRevision
	   END
	     UPDATE tbl503
		  SET 
		    tbl503.[t_prdt] = B.PCRRequirementDate,
		   --tbl503.[t_mitm] = B.ManufacturingItem,
		   tbl503.[t_sitm] = CASE WHEN @UseAlternativeStock <> @YesValue THEN tbl503.[t_sitm] ELSE @ChildItem END,
		   tbl503.[t_oitm] = CASE WHEN @UseAlternativeStock <> @YesValue THEN tbl503.[t_oitm] ELSE @OtherItem END,
		   tbl503.[t_nqty] = B.NestedQuantity,
		   tbl503.[t_arcs] = B.AreaConsumed,
		   --tbl503.[t_leng] = B.LengthModified,
		   --tbl503.[t_widt] = B.WidthModified,
		   tbl503.[t_stkn] = B.StockNumber,
		   tbl503.[t_pcln] = B.PCLNumber,
		   --tbl503.[t_mvtp] = B.MovementType,
		   tbl503.[t_prmk] = B.PCRPlannerRemark,
		   tbl503.[t_stkr] = B.StockRevision,
		   tbl503.[t_mrmk] = B.MaterialPlanner,
		   --tbl503.[t_gorn] = B.GrainOrientation,
		   --tbl503.[t_shtp] = B.ShapeType,
		   tbl503.[t_shfl] = B.ShapeFile,
		   tbl503.[t_cwoc] = B.WCDeliver,
		   tbl503.[t_alts] = B.UseAlternateStock,
		   tbl503.[t_armk] = B.AlternateStockRemark--,
		   --tbl503.[t_cloc] = B.CuttingLocationModified
		 FROM [dbo].[PCR503] AS tbl503
		  INNER JOIN @TBL AS B ON B.[Id] = tbl503.[Id]
		
		 IF(@StockNumber <> @OldStockNumber OR @OldStockRevision <> @StockRevision)
		 BEGIN
		   UPDATE [dbo].[PCR500] 
		   SET 
		    [t_qall] = [t_qall] - @OldAreaConsumed,
		    [t_qfre] = [t_qfre] + @OldAreaConsumed
		   WHERE 
		    LTRIM(RTRIM([t_item])) = LTRIM(RTRIM(@ChildItem)) AND [t_stkn] = @OldStockNumber AND [t_stkr] = @OldStockRevision
		 END
		  SELECT 
		 TOP 1
			 @SQall = P.[t_qall] ,
			 @SArea = P.[t_area],
			 @SQCut = P.[t_qcut]
		 FROM [dbo].[PCR500] AS P WHERE LTRIM(RTRIM([t_item])) = LTRIM(RTRIM(@ChildItem)) AND P.[t_stkn] = @StockNumber AND P.[t_stkr] = @StockRevision
		 
		  UPDATE [dbo].[PCR500] 
		   SET 
		    [t_qall] = @SQall + @AreaConsumed - @OldAreaConsumed,
		    [t_qfre] = @SArea - @SQall - @SQCut			  
		   WHERE 
		    LTRIM(RTRIM([t_item])) = LTRIM(RTRIM(@ChildItem)) AND [t_stkn] = @StockNumber AND [t_stkr] = @StockRevision

			SELECT 
				TOP 1 @PCLN = T.PCLNumber 
			FROM @TBL AS T

			SELECT 
			   TOP 1 @StockArea = P.[t_area]
			FROM 
				[dbo].[PCR500] AS P 
			WHERE 
				P.[t_pcln] = @PCLN

			DELETE FROM [dbo].[PCR505] WHERE [t_pcln] = @PCLN
			
			SELECT 
				TOP 1 @AA = P.[t_area] 
			FROM 
				[dbo].[PCR504] AS P 
			WHERE 
				P.[t_pcln] = @PCLN
			
			SET @AA = @AA + @AreaConsumed - @OldAreaConsumed
			
			UPDATE 
				[dbo].[PCR504] 
			SET 
				[t_area] = @AA,
				[t_rtar] = @StockArea - @AA,
				[t_sqty] = 0
			WHERE 
				[t_pcln] = @PCLN
       END
	  COMMIT
	END	 
	
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE() ,
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END