USE [IEMQS_QA]
--USE [IEMQS]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_Workflow_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_Workflow_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Update_Workflow_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT  = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @Id INT
	 DECLARE @StatusValue NVARCHAR(20)	 
	 DECLARE @tbl AS TABLE
	 (
		 [Id] INT, 
		 [ReleaseTo] NVARCHAR(100),
		 [FinalTotalReturnArea] FLOAT
	 )
	    
	INSERT INTO @tbl
	(
	[Id], 
	[ReleaseTo],
	[FinalTotalReturnArea]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('ReleaseTo').value('.', 'NVARCHAR(100)') AS [ReleaseTo],
		Array.Entity.query('FinalTotalReturnArea').value('.', 'FLOAT') AS [ReleaseTo]
	FROM 
		@Data.nodes('/ArrayOfPCL_WORKFLOW_Result/PCL_WORKFLOW_Result') AS Array(Entity)
   
   DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')
    
	IF(@Mode = 1) --UPDATE
	 BEGIN
	  SELECT 
		TOP 1 @StatusValue = UPPER(TP.[t_cnst])
	  FROM 
		[dbo].[PCR504] AS P 
	  INNER JOIN @tbl AS T ON T.Id = P.[Id]
	  INNER JOIN @tblPCLStatus AS TP ON TP.[t_val] = P.[t_stat]
	 
	 
	 

	--IF(@StatusValue <> @ApprovedStatusValue)
	-- BEGIN
	--	 RAISERROR('Release to value cannot be applied to this screen', 12, 1)
	-- END
	 IF(@StatusValue =  'APPR.QA')
	  BEGIN
	    UPDATE tbl504
		SET 
		  tbl504.[t_rlto] = B.ReleaseTo
		FROM 
		 [dbo].[PCR504] AS tbl504
		INNER JOIN @tbl AS B ON B.Id = tbl504.Id
	  END
	 ELSE IF(@StatusValue = 'APPRV.QC' OR @StatusValue = 'PLT.CUT' OR @StatusValue = 'REJ.QC' OR @StatusValue = 'PLT.MARK')
	   BEGIN
	     UPDATE tbl504
		SET 
		  tbl504.[t_ftra] = B.FinalTotalReturnArea
		FROM 
		 [dbo].[PCR504] AS tbl504
		INNER JOIN @tbl AS B ON B.Id = tbl504.Id
	   END		
	  COMMIT
	 END	
	  
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE() ,
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END 
