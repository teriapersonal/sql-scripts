--USE [IEMQS]
USE [IEMQS_QA]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_Update_Material_Planner_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_Update_Material_Planner_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

ALTER PROCEDURE [dbo].[SP_PCR_Validate_Update_Material_Planner_Data]
	@UserId NVARCHAR(20),
	@Data XML
AS
BEGIN
 BEGIN TRY
    DECLARE @Id INT
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @StockRevision INT
	DECLARE @ToleranceLimit FLOAT = 0.1
	DECLARE @UseAlternateStock INT
	DECLARE @AreaConsumed FLOAT
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @Revision INT
	DECLARE @Length FLOAT
	DECLARE @Width FLOAT
	DECLARE @PONO INT
	DECLARE @OldAreaConsumed FLOAT
	DECLARE @DArea FLOAT
	DECLARE @DAreaU FLOAT
	DECLARE @DAreaL FLOAT
	DECLARE @ChildItem NVARCHAR(100)
	DECLARE @Denom DECIMAL = 1000000
	DECLARE @OtherItem NVARCHAR(100)
	DECLARE @CItem NVARCHAR(100)
	DECLARE @StockQall FLOAT
	DECLARE @StockArea FLOAT
	DECLARE @ItemTypeValuePipe INT
	DECLARE @ItemType INT
	DECLARE @WCDeliver NVARCHAR(100)
	DECLARE @Message NVARCHAR(1000)
	DECLARE @YesValue INT	 
	DECLARE @NestedQuantity INT
	DECLARE @ApprovedQuantity INT
	DECLARE @RejectedQuantity INT
	SELECT
		TOP 1 @YesValue = [Value] 
	FROM 
	    [dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('Yes No Choice','tppdm.yeno') 
	WHERE 
	   UPPER([LNConstant]) = 'YES'
	DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [PCRRequirementDate] DATETIME, 
		 [ManufacturingItem] NVARCHAR(100), 
		 [ChildItem] NVARCHAR(100), 
		 [NestedQuantity] INT,
		 [AreaConsumed] FLOAT,
		 [LengthModified] FLOAT,
		 [WidthModified] FLOAT, 
		 [StockNumber] NVARCHAR(100),
		 [PCLNumber] NVARCHAR(100),
		 [MovementType] INT,
		 [PCRPlannerRemark] NVARCHAR(100),
		 [StockRevision] INT,
		 [MaterialPlanner] NVARCHAR(100),
		 [GrainOrientation] NVARCHAR(100),
		 [ShapeType] NVARCHAR(100),
		 [ShapeFile] NVARCHAR(100),
		 [WCDeliver] NVARCHAR(100),
		 [UseAlternateStock] INT,
		 [AlternateStockRemark] NVARCHAR(MAX),
		 [CuttingLocationModified] NVARCHAR(100),
		 [ApprovedQuantity] INT,
		 [RejectedQuantity] INT
	 )
	    
	INSERT INTO @TBL
	(
		 [Id],
		 [PCRRequirementDate], 
		 [ManufacturingItem], 
		 [ChildItem], 
		 [NestedQuantity],
		 [AreaConsumed],
		 [LengthModified],
		 [WidthModified], 
		 [StockNumber],
		 [PCLNumber],
		 [MovementType],
		 [PCRPlannerRemark],
		 [StockRevision],
		 [MaterialPlanner],
		 [GrainOrientation],
		 [ShapeType],
		 [ShapeFile],
		 [WCDeliver],
		 [UseAlternateStock],
		 [AlternateStockRemark],
		 [CuttingLocationModified],
		 [ApprovedQuantity],
		 [RejectedQuantity]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('PCRRequirementDate').value('.', 'DATETIME') AS [PCRRequirementDate], 
		Array.Entity.query('ManufacturingItem').value('.', 'NVARCHAR(100)') AS [ManufacturingItem],  
		Array.Entity.query('ChildItem').value('.', 'NVARCHAR(100)') AS [ChildItem],	
		Array.Entity.query('NestedQuantity').value('.', 'INT') AS [NestedQuantity],	
		Array.Entity.query('AreaConsumed').value('.', 'FLOAT') AS [AreaConsumed],
		Array.Entity.query('LengthModified').value('.', 'FLOAT') AS [LengthModified],
		Array.Entity.query('WidthModified').value('.', 'FLOAT') AS [WidthModified],
		Array.Entity.query('StockNumber').value('.', 'NVARCHAR(100)') AS [StockNumber],	
		Array.Entity.query('PCLNumber').value('.', 'NVARCHAR(100)') AS [PCLNumber],	
		Array.Entity.query('MovementType').value('.', 'INT') AS [MovementType],	
		Array.Entity.query('PCRPlannerRemark').value('.', 'NVARCHAR(100)') AS [PCRPlannerRemark],	
		Array.Entity.query('StockRevision').value('.', 'INT') AS [StockRevision],	
		Array.Entity.query('MaterialPlanner').value('.', 'NVARCHAR(100)') AS [MaterialPlanner],	
		Array.Entity.query('GrainOrientation').value('.', 'NVARCHAR(100)') AS [GrainOrientation],	
		Array.Entity.query('ShapeType').value('.', 'NVARCHAR(100)') AS [ShapeType],	
		Array.Entity.query('ShapeFile').value('.', 'NVARCHAR(100)') AS [ShapeFile],	
		Array.Entity.query('WCDeliver').value('.', 'NVARCHAR(100)') AS [WCDeliver],	
		Array.Entity.query('UseAlternateStock').value('.', 'INT') AS [UseAlternateStock],	
		Array.Entity.query('AlternateStockRemark').value('.', 'NVARCHAR(MAX)') AS [AlternateStock],	
		Array.Entity.query('CuttingLocationModified').value('.', 'NVARCHAR(100)') AS [CuttingLocationModified],
		Array.Entity.query('ApprovedQuantity').value('.', 'INT') AS [ApprovedQuantity],	
		Array.Entity.query('RejectedQuantity').value('.', 'INT') AS [RejectedQuantity]
	FROM @Data.nodes('/ArrayOfSP_PCR_GET_MATERIAL_PLANNER_DATA_Result/SP_PCR_GET_MATERIAL_PLANNER_DATA_Result') AS Array(Entity)
	
	SELECT 
		TOP 1 @ItemTypeValuePipe = [Value] 
	FROM 
		dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PIPE'
	SELECT 
	 TOP 1 
	     @Id = T.[Id],
		 @UseAlternateStock = T.[UseAlternateStock],  
		 @StockNumber = T.[StockNumber],
		 @StockRevision = ISNULL(T.[StockRevision], 0),		 
		 @AreaConsumed = ROUND(ISNULL(T.[AreaConsumed], 0), 3),
		 @OldAreaConsumed = ROUND(ISNULL(P.[t_arcs], 0), 3),
		 @PCRN = P.[t_pcrn],
		 @PONO = P.[t_pono],
		 @Revision = P.[t_revn],
		 @ItemType = P.[t_ityp],
		 @CItem = P.[t_sitm],
		 @Length = P.[t_leng],
		 @Width = P.[t_widt],
		 @WCDeliver = T.[WCDeliver],
		 @NestedQuantity = T.[NestedQuantity],
		 @ApprovedQuantity = T.[ApprovedQuantity],
		 @RejectedQuantity = T.[RejectedQuantity]
	 FROM @TBL AS T 
	   INNER JOIN [dbo].[PCR503] AS P ON T.[Id] = P.[Id]
	   
	EXEC [dbo].[SP_PCR_Validate_StockNumber]
	   @PSNo = @UserId,
	   @Id = @Id,   
	   @StockNumber = @StockNumber,
	   @StockRevision = @StockRevision,
	   @UseAlternateStock = @UseAlternateStock
	
	IF(@UseAlternateStock = @YesValue)
	   BEGIN
	    SET @OtherItem =  @CItem  --, @CItem = P.[t_item]  FROM [dbo].PCR500 AS P WHERE P.[t_stkn] = @StockNumber AND P.[t_stkr] = @StockRevision	  
	   END
	
	 EXEC [dbo].[SP_PCR_Validate_WorkCenter] @PCRN, @WCDeliver
     
	 IF(@AreaConsumed = 0)
	   BEGIN
		 RAISERROR('Please enter value for Area Consumed', 12, 1)
	   END
     
	 IF(@StockArea <= (@AreaConsumed + @StockQall - @OldAreaConsumed))
		BEGIN
		 SET @Message =  'Area Consumed : '
		 SET @Message = CONCAT(@Message, ROUND((@AreaConsumed + @StockQall - @OldAreaConsumed), 3))
		 SET @Message = CONCAT(@Message, ' for stock ')
		 SET @Message = CONCAT(@Message, @StockNumber)
		 SET @Message = CONCAT(@Message, ' in PCR Lines exceed total available area : ')
		 SET @Message = CONCAT(@Message, ROUND(@StockArea, 3))		 
		 RAISERROR(@Message, 12, 1)
		END
      IF(@ApprovedQuantity IS NOT NULL AND @RejectedQuantity IS NOT NULL)
	   BEGIN
	     IF(@NestedQuantity <> @ApprovedQuantity + @RejectedQuantity)
		   BEGIN
		    RAISERROR('Total of approved quantity and rejected quantity should be equal to nested quantity', 11, 1)
		   END
	   END
      IF(@ItemType <> @ItemTypeValuePipe)
	   BEGIN
		 SET @DArea = (@Width * @Length)/@Denom
		 SET @DAreaU = ROUND(@DArea + (@DArea * @ToleranceLimit), 3)
		 SET @DAreaL = ROUND(@DArea - (@DArea * @ToleranceLimit), 3)
		 --select @Width, @Length, @DareaL, @DAreaU
		 IF(@AreaConsumed < @DAreaL OR @AreaConsumed > @DAreaU)
		   BEGIN
			SET @Message = CONCAT('Area maintained : ', @AreaConsumed)
			SET @Message = CONCAT(@Message, ' is out side ')
			SET @Message = CONCAT(@Message, @ToleranceLimit * 100)
			SET @Message = CONCAT(@Message, ' percent tolerance limit of ')
			SET @Message = CONCAT(@Message, ROUND(@DArea, 3))
			SET @Message = CONCAT(@Message, ' (ie from ') 
			SET @Message = CONCAT(@Message, @DAreaL)
			SET @Message = CONCAT(@Message, ' to ')
			SET @Message = CONCAT(@Message, @DAreaU)
			SET @Message = CONCAT(@Message, '). Would you like to proceed?')
		    RAISERROR(@Message, 11, 1)
		   END
		 END

	     SELECT 1 AS [Status], NULL AS [MESSAGE]
  END TRY
  BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(), 
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		IF(@ErrorSeverity = 11) --> Warning for showing dialog box
		  BEGIN
		   SELECT 0 AS [Status], @ErrorMessage AS [MESSAGE]
		  END
		 ELSE --> 11 means error
		  BEGIN
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		  END
   END CATCH 	
END 