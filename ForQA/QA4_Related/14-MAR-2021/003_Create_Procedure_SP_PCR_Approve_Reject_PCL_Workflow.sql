--USE [IEMQS]
USE [IEMQS_QA]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Approve_Reject_PCL_Workflow]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Approve_Reject_PCL_Workflow]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Approve_Reject_PCL_Workflow]
    @PSNo NVARCHAR(100),
	@Id INT,
	@CurrentScreen NVARCHAR(100),
	@Approve BIT = 1
AS
 BEGIN
    DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');
	DECLARE @Query NVARCHAR(MAX)
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @TempTableQuery NVARCHAR(MAX)
	DECLARE @NextStatus NVARCHAR(100)
	DECLARE @NextStatusValue INT
	DECLARE @InsertInLN504Table BIT  = 0
	DECLARE @InsertInLN505Table BIT  = 0
	DECLARE @InsertIn500Table BIT = 0
	DECLARE @UpdateInLN504Table BIT = 0
	DECLARE @YesValue INT = 1
	DECLARE @ReadyForReleased INT = 2
	DECLARE @Status BIT
	DECLARE @PCLNumber NVARCHAR(100)
	DECLARE @Revision INT
	SELECT 
	 TOP 1 
       @PCLNumber = P.[t_pcln] , 
	   @Revision = P.[t_revn] 
	FROM [dbo].[PCR504] AS P 
	  WHERE P.[Id] = @Id
	
	IF(@CurrentScreen = 'First Stage Clearance') 
	 BEGIN
	   SET @NextStatus = 'appr.qa'
	 END
	IF(@CurrentScreen = 'Confirm Plate Cutting Request') 
	 BEGIN
	   SET @NextStatus = 'plt.issued'
	   SET @InsertInLN504Table = 1
	   SET @InsertIn500Table = 1
	 END
	 IF(@CurrentScreen = 'Receive Plate') 
	 BEGIN
	   SET @NextStatus = 'plt.mark'
	   SET @UpdateInLN504Table = 1 
	   SET @ReadyForReleased = 1
	 END
	 IF(@CurrentScreen = 'Process Plate Cutting Request') 
	 BEGIN
	   SET @NextStatus = 'plt.cut'
	   SET @UpdateInLN504Table = 1 
	 END
	 IF(@CurrentScreen = 'Second Stage Clearance') 
	 BEGIN
	   SET @NextStatus = 'apprv.qc'
	   SET @UpdateInLN504Table = 1 
	 END
	SELECT 
	TOP 1 
	  @NextStatusValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = UPPER(@NextStatus)
	
	IF(@InsertInLN504Table = 1)
	 BEGIN	   
	   IF EXISTS(SELECT 0 FROM [dbo].[PCR505] AS P5 WHERE P5.[t_pcln] = @PCLNumber AND P5.[t_revn] = @Revision)
	    BEGIN
		  SET @InsertInLN505Table = 1
	    END
	 END

	SET @Query = CONCAT(' 
	  BEGIN 
	     BEGIN TRY
	        DECLARE @I INT = ', @Id,
		   'DECLARE @Invalid BIT = 0 ')
		   
	SET @Query = CONCAT(@Query,
	' IF(', @InsertInLN504Table , ' = 1 )',
		'BEGIN			
			IF NOT EXISTS
			(
			    SELECT A.[t_pcln] FROM ' + @LNLinkedServer + '.dbo.tltsfc504' + @LnCompanyId + ' A
	   	        INNER JOIN [dbo].[PCR504] AS P ON A.[t_pcln] = P.[t_pcln] AND A.[t_revn] = P.[t_revn] WHERE P.[Id] = @I 
			)
			   BEGIN                  
					INSERT INTO ' + @LNLinkedServer + '.dbo.tltsfc504' + @LNCompanyId + '
					(
					    [t_imqs], [t_rele], [t_reld], [t_prcs], [t_prcd], [t_rler], [t_prer],
						[t_pcln], [t_revn], [t_mvtp], [t_emno], [t_area], [t_date], [t_stat], [t_iorn],
						[t_orno], [t_rorn], [t_sqty], [t_rbal], [t_pcl_rldt], [t_mcsp],
						[t_pmg_cdat], [t_pmg_rdat], [t_qa_adat], [t_qa_rdat], [t_sfc_cdat],
						[t_plt_idat], [t_plt_rdat], [t_plt_mdat], [t_qc_adat], [t_qc_rdat],
						[t_plt_cdat], [t_plt_rtdt], [t_pcl_cndt], [t_pcl_cldt], [t_cwoc],
						[t_aarc], [t_asqt], [t_rtar], [t_pcl_dscr], [t_plt_trdt], [t_ctdt],
						[t_rrmk], [t_odds], [t_tclt], [t_mrmk], [t_atqa], [t_atqc], [t_rqrd], [t_actl], [t_txta],
						[t_pibp], [t_pips], [t_pirk], [t_pibp_s], [t_pips_s], [t_pirk_s],
						[t_srmk], [t_fvdt], [t_rrmk_qc], [t_pmrk], [t_resn], [t_rloc], [t_stgi],
						[t_ityp], [t_mpln], [t_new_case], [t_Refcntd], [t_Refcntu]
					) 
				SELECT 
					1, 2, 2, 2, 2, '''', '''', 
					T.[t_pcln], T.[t_revn], T.[t_mvtp], T.[t_emno], T.[t_area], GETUTCDATE(), ', @NextStatusValue  , ' , '''',
					'''', '''', T.[t_sqty], T.[t_rbal], GETUTCDATE(),
					GETUTCDATE(), GETUTCDATE(), GETUTCDATE(), GETUTCDATE(), GETUTCDATE(), '''',
					GETUTCDATE(), GETUTCDATE(), GETUTCDATE(), GETUTCDATE(), GETUTCDATE(),
					GETUTCDATE(), GETUTCDATE(), GETUTCDATE(), GETUTCDATE(), T.[t_cwoc],
					T.[t_aarc], T.[t_asqt], T.[t_rtar], GETUTCDATE(), GETUTCDATE(), GETUTCDATE(),
					'''', [t_odds], [t_tclt], T.[t_mrmk], '''', '''', '''', '''', '''',
					'''', '''', '''', '''', '''', '''',
					'''', GETUTCDATE(), '''', '''', '''', '''', 1,
					T.[t_ityp], '''',  '''', 1, 1 
				FROM [dbo].[PCR504] AS T WHERE T.[Id] = @I
				      IF(', @InsertInLN505Table , '= 1 )',
						'BEGIN			  
							 INSERT INTO ' + @LNLinkedServer + '.dbo.tltsfc505' + @LNCompanyId + '
								 (
									[t_pcln], [t_revn], [t_seqn], [t_leng], 
									[t_widt], [t_area], [t_odds], [t_stkn], 
									[t_stkr], [t_rmrk], [t_ityp], [t_Refcntd], [t_Refcntu]
								 )
							 SELECT 
									T.[t_pcln], T.[t_revn], T.[t_seqn], T.[t_leng], 
									T.[t_widt], T.[t_area], T.[t_odds], T.[t_stkn], 
									T.[t_stkr], T.[t_rmrk], T.[t_ityp], 1, 1 
							 FROM [dbo].[PCR505] AS T WHERE T.[t_pcln] = ''' + @PCLNumber + ''' AND T.[t_revn] = ', @Revision,
						' END
						END
			ELSE
				 BEGIN 
					SET @Invalid = 1
					SELECT 0 AS [Status], ''Already exists in LN'' AS [MESSAGE]
				 END
		END
		IF(', @UpdateInLN504Table , ' = 1 )',
		' BEGIN
				 UPDATE ' + @LNLinkedServer + '.dbo.tltsfc504' + @LNCompanyId + 
				 ' SET 
				    [t_rele] = ', @ReadyForReleased,
				 ', [t_stat] = ', @NextStatusValue,
				 ', [t_new_case] = ', @YesValue,
				 ' WHERE [t_imqs] = 1  AND [t_pcln] = ''' + @PCLNumber + ''' AND [t_revn] = ', @Revision,				    
	    ' END
        IF(@Invalid = 0)
		   BEGIN
			    UPDATE [dbo].[PCR504] 
			    SET [t_stat] = ', @NextStatusValue , 
			    ' WHERE [Id] = @I',
				' IF(', @InsertIn500Table , ' = 1 )',
				   ' BEGIN
				        EXEC [dbo].[SP_PCR_Generate_New_Stock_Revision] @Id = @I, @PSNo = ''',
					   @PSNo + '''',
				    ' END ',
		        ' SELECT 1 AS [Status], NULL AS [MESSAGE] 			  
		   END			  
		END TRY 
		BEGIN CATCH 				 
				SELECT  0 AS [Status],  ERROR_MESSAGE() AS [MESSAGE]							   
		END CATCH 
	  END
	')
	PRINT(@Query)
	EXEC(@Query)	
END