USE [IEMQS_QA]
--USE [IEMQS]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_ReleasePCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_ReleasePCL]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

Alter Procedure [dbo].[SP_PCR_Validate_ReleasePCL]
(
   @Id INT 
)
AS
 BEGIN
  BEGIN TRY 
   DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
   DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
   DECLARE @Query NVARCHAR(MAX)
   DECLARE @StatusDeleted INT
   DECLARE @StatusPCLReleased INT
   DECLARE @StatusPCRReleasedToMP INT
   DECLARE @ItemTypePipeValue INT
   DECLARE @ItemTypePlateValue INT
   DECLARE @Area FLOAT
   DECLARE @RTAR FLOAT
   DECLARE @SQTY FLOAT
   DECLARE @EMNO NVARCHAR(100)
   DECLARE @MVTP INT
   DECLARE @RBAL INT
   DECLARE @PRDT DATETIME
   DECLARE @Project NVARCHAR(100)
   DECLARE @Element NVARCHAR(100)
   DECLARE @ChildItem NVARCHAR(100)
   DECLARE @PRTN INT
   DECLARE @PCLN NVARCHAR(1000)
   DECLARE @REVN INT
   DECLARE @MSG NVARCHAR(1000)
   DECLARE @DA FLOAT
   DECLARE @C INT
   DECLARE @DIST INT = 0
   DECLARE @FilesCount INT
   DECLARE @AttachmentTableName NVARCHAR(100) = 'PCR504'
   SELECT @FilesCount = COUNT(0) from [dbo].[FCS001] AS F WHERE F.[TableName] = @AttachmentTableName AND TableId = @Id
   IF(@FilesCount = 0)
	 BEGIN
	    SET @MSG = 'Please provide attachment before releasing PCL'
		--RAISERROR(@MSG, 11, 1)
	 END
   SELECT
       @RBAL =  ISNULL(P.[t_rbal], 0),
	   @RTAR = ROUND(ISNULL(P.[t_rtar], 0), 2) ,
	   @Area = ROUND(ISNULL(P.[t_area], 0), 2),
	   @SQTY = ROUND(ISNULL(P.[t_sqty], 0), 2) ,
	   @EMNO = P.[t_emno] ,
	   @MVTP = P.[t_mvtp],
	   @PCLN = P.[t_pcln],
	   @REVN = P.[t_revn]
   FROM [dbo].[PCR504] AS P WHERE P.Id = @Id
   --SELECT CASE WHEN ROUND(12.345, 2) - ROUND(12.23,2) > 0.1 THEN 1 ELSE 0 END AS A
   IF(@Area <= 0)
   BEGIN
      SET @MSG = 'Total consumed area must be greater than zero'
	  RAISERROR(@MSG, 11, 1)
   END
   IF(@SQTY < 0)
   BEGIN
      SET @MSG = 'Scrap quantity must not be less than zero'
	  RAISERROR(@MSG, 11, 1)
   END
   IF(@EMNO IS NULL)
   BEGIN
      SET @MSG = 'Please enter planner'
	  RAISERROR(@MSG, 11, 1)
   END
   IF(@MVTP IS NULL)
   BEGIN
      SET @MSG = 'Please select Movement Type'
	  RAISERROR(@MSG, 11, 1)
   END  
       IF(@RBAL > 0)
	    BEGIN		
		  IF EXISTS(SELECT 0  FROM [dbo].[PCR505] AS P5 WHERE P5.[t_pcln] = @PCLN AND P5.[t_revn] = @REVN)
		  BEGIN
		   SELECT 
			@DA = ROUND(SUM(ISNULL(P5.[t_area], 0)), 2) 
		   FROM 
			[dbo].[PCR505] AS P5 
		   WHERE 
			P5.[t_pcln] = @PCLN AND P5.[t_revn] = @REVN
		   
		   SELECT 
			@DIST = COUNT(0) 
		   FROM 
			[dbo].[PCR505] AS P5 
		   WHERE 
			P5.[t_pcln] = @PCLN AND P5.[t_revn] = @REVN

		   IF(@RBAL <> @DIST)
	   	    BEGIN
     		   SET @MSG = 'Distribution for return balance incomplete'
			   RAISERROR(@MSG, 11, 1)
			   --PRINT 1
		    END
		  END
		  ELSE
		   BEGIN
		    SET @MSG = 'Please maintain distribution for return balance'
			RAISERROR(@MSG, 11, 1)
		   END
		  --TODO Test
		  IF(@RTAR - @DA > 0.1)
		   BEGIN
		    SET @MSG = 'Total area in distribution must be equal to Total return area for PCL'
			RAISERROR(@MSG, 11, 1)
			  
		   END
	    END
		SELECT
			TOP 1 
			@PRDT = P3.[t_pcr_crdt] 
		FROM 
			[dbo].[PCR503] AS P3 
		WHERE 
			P3.[t_pcln] = @PCLN AND P3.[t_stkn] IS NOT NULL 
		ORDER BY P3.[t_pcr_crdt] DESC
        
		SELECT
		 TOP 1
		 @Project = P3.[t_cprj] ,
		 @Element = P3.[t_cspa],
		 @ChildItem = P3.[t_sitm],
		 @PRTN = P3.[t_prtn] 
		 FROM 
			[dbo].[PCR503] AS P3 
		 WHERE 
			P3.[t_pcln] = @PCLN AND 
			P3.[t_stkn] IS NOT NULL AND 
			CAST(@PRDT AS DATE) = CAST(P3.[t_pcr_crdt] AS DATE)

		 DECLARE @PrevPCRN NVARCHAR(100)
		 DECLARE @PrevRevn INT
		 DECLARE @PONO INT

		 SELECT
		 TOP 1
		  @PrevPCRN = P3.[t_pcrn],
		  @PrevRevn = P3.[t_revn],
		  @PONO = P3.[t_pono]
		 FROM 
			[dbo].[PCR503] AS P3 
		 WHERE 
			P3.[t_cprj] = @Project AND P3.[t_cspa] = @Element AND P3.[t_prtn] = @PRTN AND P3.[t_sitm] = @ChildItem AND
		 CAST(P3.[t_pcr_crdt] AS DATE) < CAST(@PRDT AS DATE) AND P3.[t_stkn] IS NULL AND P3.[t_lsta] <> 10
		 
		 IF (@PrevPCRN IS NOT NULL AND @PrevRevn IS NOT NULL AND @PONO IS NOT NULL)
		 BEGIN
		   SET @MSG = 'Please Proceed previous PCR No.: ' + @PrevPCRN +  ',PCR Line: '
		   SET @MSG = CONCAT(@MSG, @Pono)
		   SET @MSG  = CONCAT(@MSG,   ' and Revision:')
		   SET @MSG  = CONCAT(@MSG, @PrevRevn)  
		   RAISERROR(@MSG, 11, 1)
		 END
		 
		 SELECT 1

	END TRY
	BEGIN CATCH				
		DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(), 
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 			
	END CATCH
END