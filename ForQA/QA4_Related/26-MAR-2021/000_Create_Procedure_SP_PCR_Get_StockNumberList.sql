--USE [IEMQS]
USE [IEMQS_QA]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_StockNumberList]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_StockNumberList]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_StockNumberList] 
 @PSNo NVARCHAR(100),
 @Id INT
AS
BEGIN 
	DECLARE @Location NVARCHAR(100)
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @LNLinkedServer NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(100) = [dbo].[FN_COMMON_GET_PCR_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @StockPhysicalStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
	DECLARE @StockStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
	
	DECLARE @ResultTable AS TABLE(
		[Id] INT, 
		[Contract] NVARCHAR(100), 
		[Project] NVARCHAR(100),
		[ChildItem] NVARCHAR(100), 
		[StockNumber] NVARCHAR(100), 
		[StockRevision] INT, 						
		[UseAlternateStock] BIT,		
		[FreeArea] FLOAT,		
		[TotalArea] FLOAT,
		[AreaAllocated] FLOAT
	)
	
	IF (@Id > 0)
	BEGIN
		SELECT 
			TOP 1 @Location = P.[t_loca]
		FROM 
			[dbo].[PCR501] AS P INNER JOIN [dbo].[PCR503] AS P3 ON P3.[t_pcrn] = P.[t_pcrn]
		WHERE 
			P3.[Id] = @Id
	END

    --SELECT @Location = 'PEW'
	INSERT INTO 
		@StockPhysicalStatusTable
		(
			[LNConstant], 
			[Text], 
			[Value]
		)
    SELECT 
		Enums.[LNConstant], 
		Enums.[Text], 
		Enums.[Value] 
	FROM 
		[dbo].[PCR000] AS Enums 
	WHERE 
		UPPER(Enums.[EnumName]) = UPPER('Stock Physical Status')
		AND
		(
			(
				@Id > 0 
				AND 
				UPPER(Enums.[LNConstant]) IN 
				(
					UPPER('virtual'), 
					UPPER('at.store'), 
					UPPER('at.pps'),
					UPPER('ret.pps')
				)
			)
				
			OR 
			(
				@Id = 0
			)
		
		)

		INSERT INTO 
			@StockStatusTable
			(
				[LNConstant], 
				[Text], 
				[Value]
			)
		SELECT 
			Enums.[LNConstant], 
			Enums.[Text], 
			Enums.[Value] 
		FROM 
			[dbo].[PCR000] AS Enums 
		WHERE 
			UPPER(Enums.[EnumName]) = UPPER('Stock Status') 
			AND
			UPPER(Enums.[LNConstant]) <> UPPER('delete')
	
	--SET @Id = 1219
    IF(@Id > 0)
	 BEGIN
	 
	   SET @SqlQuery =
		   'DECLARE @StockStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
		    DECLARE @StockPhysicalStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)			
			INSERT INTO @StockPhysicalStatusTable([LNConstant], [Text], [Value])
			SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums 
			WHERE UPPER(Enums.[EnumName]) = UPPER(''Stock Physical Status'')
				AND	UPPER(Enums.[LNConstant]) IN (UPPER(''virtual''), UPPER(''at.store''), UPPER(''at.pps''), UPPER(''ret.pps''))

			INSERT INTO @StockStatusTable([LNConstant], [Text], [Value])
			SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums WHERE 
			UPPER(Enums.[EnumName]) = UPPER(''Stock Status'') AND UPPER(Enums.[LNConstant]) <> UPPER(''delete'') '
	   SET @SqlQuery += 
		   CONCAT(' DECLARE @I INT = ', @Id)
       SET @SqlQuery += 
			  ' SELECT		        
				TOP 1000
				P.[Id] AS Id, 
				P.[t_cono] AS [Contract],
				A.[t_cprj] AS [Project],
				LTRIM(RTRIM(P.[t_item])) AS ChildItem,
				LTRIM(RTRIM(P.[t_stkn])) AS StockNumber, 
				P.[t_stkr] AS StockRevision, 
				P.[t_area] AS TotalArea, 
				P.[t_qall] AS AreaAllocated, 
				P.[t_qfre] As FreeArea,
				1 AS UseAlternateStock
			FROM 
				[dbo].[PCR500] AS P 
			INNER JOIN ' +
			  @LNLinkedServer + '.dbo.ttpctm110' + @LNCompanyId + ' AS A 
			ON A.[t_cono] = P.[t_cono]
			 INNER JOIN
			    [dbo].[PCR503] AS P3 ON P3.[t_cprj] = A.[t_cprj] 
			WHERE 
			  P.[t_loca] = ''' + @Location + '''' + ' ' + '			   
			   AND 
			  P3.[Id] = @I
			   AND
			  P.[t_stat] IN 
			  (
				SELECT 
					SS.[Value] 
				FROM @StockStatusTable AS SS
			  )
			  AND
			  P.[t_psta] IN 
			  (
				SELECT 
					SPS.[Value] 
				FROM @StockPhysicalStatusTable AS SPS WHERE UPPER(SPS.LNConstant) <> UPPER(''VIRTUAL'')
			  ) '
    
		 INSERT INTO 
		  @ResultTable
		  (
			[Id], 
			[Contract], 
			[Project], 
			[ChildItem], 
			[StockNumber], 
			[StockRevision], 
			[TotalArea], 
			[AreaAllocated], 
			[FreeArea],
			[UseAlternateStock]
		  )
		 EXEC(@SqlQuery)
	

		 INSERT INTO 
		  @ResultTable
		  (
			[Id], 
			[Contract], 
			[Project], 
			[ChildItem], 
			[StockNumber], 
			[StockRevision], 
			[TotalArea], 
			[AreaAllocated], 
			[FreeArea],
			[UseAlternateStock]
		  )
		  SELECT
				DISTINCT
				P.[Id] AS Id, 
				P.[t_cono] AS [Contract],
				NULL AS [Project],
				LTRIM(RTRIM(P.[t_item])) AS [ChildItem],
				LTRIM(RTRIM(P.[t_stkn])) AS [StockNumber],  
				P.[t_stkr] AS [StockRevision], 
				P.[t_area] AS [TotalArea], 
				P.[t_qall] AS [AreaAllocated], 
				P.[t_qfre] As [FreeArea],
				0 AS [UseAlternateStock]
		  FROM 
				[dbo].[PCR500] AS P
		  INNER JOIN 
				[dbo].[PCR503] AS P3 ON LTRIM(RTRIM(P.[t_item])) = LTRIM(RTRIM(P3.[t_sitm]))  
		  WHERE 
			P.[t_loca] = @Location 
			 AND
			P3.[Id] = @Id
			 AND
			P.[t_stat] IN 
			(
			  SELECT 
				SS.[Value] 
			  FROM @StockStatusTable AS SS
			)
			 AND
			P.[t_psta] IN 
			(
			  SELECT 
				SPS.[Value] 
			  FROM @StockPhysicalStatusTable AS SPS 
			)	
			SELECT * from @ResultTable
	END
	 ELSE
	  BEGIN
	      /*
		  select * from [PhzpdSqldb2k12].[Lnappqa4db].dbo.ttpctm100175 --contract desc
select distinct * from [PhzpdSqldb2k12].[Lnappqa4db].dbo.ttcmcs003175 -- wharehouse desc
select distinct * from [PhzpdSqldb2k12].[Lnappqa4db].dbo.ttcibd001175 where t_item in (select distinct t_item from pcr500) -- item desc
		  */
		  SELECT
				TOP 1000
				[Id], [t_cono] AS [Contract], [t_cprj] AS [Project], [t_item] AS [ChildItem], [t_stkn] [StockNumber], [t_stkr] AS [StockRevision],
				[t_area] AS [TotalArea], [t_arcs] AS [AreaAllocated], [t_loca] AS [Location], [t_leng] AS [Length], [t_widt] AS [Width], [t_qfre] AS [FreeArea], [t_qnty] AS [Quantity],
				[t_mtrl] AS [Materials], [t_thic] AS [Thickness], [t_orno] AS [PurchaseOrder], [t_pono] AS [POPosition], [t_rcno] AS [ReceiptNumber], [t_rcln] AS [ReceiptLine],
				[t_shtp] AS [ShapeType], [t_shfl] AS [ShapeFile], [t_size] AS [SizeCode], [t_cwar] AS [Warehouse], [t_wloc] AS [WarehouseLocation], [t_cnvf] AS [DerivedConversion],
				[t_stat] AS [StockStatus], [t_htno] AS [HeatNo], [t_tcno] AS [TCNo], [t_gorn] AS [GraiOrientation], [t_mchn] AS [CuttingMachine], [t_pcln] AS [PCLNumber],
				[t_armv] AS [ARMVersion], [t_cdt1] AS [CladThickness1], [t_cdt2] AS [CladThickness2], [t_ccd1] AS [TheoreticalCon1], [t_ccd2] AS [TheoreticalCon2], [t_twgt] AS [TheoreticalWeight],
				[t_psta] AS [StockPhysicalStatus], [t_clot] AS [ActualLot], [t_pstk] AS [ParentStockNumber], [t_prev] AS [ParentStockRevision], [t_odds] AS [OddShape], [t_idsf] AS [IssueDirectly],
				[t_cspa] AS [Element], [t_rmrk] AS [Remark], [t_srmk] AS [StockRelatedRevision], [t_sprj] AS [StockProject], [t_ncra] AS [NCRApplicable], [t_ncrr] AS [NCRRemark],
				[t_ncrn] AS [NCRNumber], [t_rsfp] AS [ReservedForProject], [t_rsby] AS [ReservedByWho], [t_rsdt] AS [ReservationDate], [t_arms] AS [ARMSet], [t_cnvb] AS [TheoreticalCon]
		  FROM 
				[dbo].[PCR500] AS P		   
		  WHERE 
			P.[t_stkn] IS NOT NULL AND LEN(P.[t_stkn]) > 0
			 AND
			P.[t_stat] IN 
			(
			  SELECT 
				SS.[Value] 
			  FROM @StockStatusTable AS SS
			)
			 AND
			P.[t_psta] IN 
			(
			  SELECT 
				SPS.[Value] 
			  FROM @StockPhysicalStatusTable AS SPS 
			)
			ORDER BY P.[t_qfre] DESC
	  END
	
	
END