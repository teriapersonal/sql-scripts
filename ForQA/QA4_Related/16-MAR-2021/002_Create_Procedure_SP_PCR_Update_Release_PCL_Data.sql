--USE [IEMQS]
USE [IEMQS_QA]
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_Release_PCL_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_Release_PCL_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Update_Release_PCL_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @PCRN NVARCHAR(100)
	 DECLARE @WCDeliver NVARCHAR(100)
	 DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [TotalCuttingLength] FLOAT, 
		 [WCDeliver] NVARCHAR(100), 
		 --[Planner] NVARCHAR(100), 
		 [Remark] NVARCHAR(MAX),
		 [MovementType] INT,
		 [ReturnBalance] INT,
		 [AllowedStage1] INT
	 )
	    
	INSERT INTO @TBL
	(
		 [Id], 
		 [TotalCuttingLength], 
		 [WCDeliver], 
		 --[Planner], 
		 [Remark],
		 [MovementType],
		 [ReturnBalance],
		 [AllowedStage1]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('TotalCuttingLength').value('.', 'FLOAT') AS [TotalCuttingLength], 
		Array.Entity.query('WCDeliver').value('.', 'NVARCHAR(100)') AS [WCDeliver],  
		--Array.Entity.query('Planner').value('.', 'NVARCHAR(100)') AS [Planner],	
		Array.Entity.query('Remark').value('.', 'NVARCHAR(MAX)') AS [Remark],	
		Array.Entity.query('MovementType').value('.', 'INT') AS [MovementType],
		Array.Entity.query('ReturnBalance').value('.', 'INT') AS [ReturnBalance],
		Array.Entity.query('AllowedStage1').value('.', 'INT') AS [AllowedStage1]	
			
	FROM @Data.nodes('/ArrayOfRELEASE_PCL_DATA_Result/RELEASE_PCL_DATA_Result') AS Array(Entity)
	
    IF(@Mode = 1) --UPDATE
	 BEGIN
	  
	  SELECT
	  TOP 1 
	    @PCRN = P3.[t_pcrn],
	    @WCDeliver = T.[WCDeliver]
	  FROM [dbo].[PCR503] AS P3 
	   INNER JOIN [dbo].[PCR504] AS P
	  ON P3.[t_pcln] = P.[t_pcln]
	   INNER JOIN @TBL AS T ON T.[Id] = P.[Id]

	  EXEC [dbo].[SP_PCR_Validate_WorkCenter] @PCRN, @WCDeliver

	  UPDATE tbl504
		SET 
		  tbl504.[t_tclt] = ROUND(ISNULL(B.TotalCuttingLength, 0), 2),
		  tbl504.[t_cwoc] = B.WCDeliver,
		  --tbl504.[t_emno] = B.Planner,
		  tbl504.[t_mrmk] = B.Remark,
		  tbl504.[t_mvtp] = B.MovementType,
		  tbl504.[t_rbal] = B.ReturnBalance,
		  tbl504.[t_stgi] = B.AllowedStage1
		FROM [dbo].[PCR504] AS tbl504
		INNER JOIN @TBL AS B ON B.Id = tbl504.Id
		COMMIT
	 END	 
	
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END 
