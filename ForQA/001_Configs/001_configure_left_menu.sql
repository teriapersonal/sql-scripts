USE [IEMQS_QA]
GO
BEGIN
 BEGIN TRY
  BEGIN TRAN
  DECLARE @UserId NVARCHAR(100) = '122963'
  DECLARE @ParentId INT 
  DECLARE @ProcessName NVARCHAR(100)
  DECLARE @ProcessDesc NVARCHAR(100)
  DECLARE @ControllerName NVARCHAR(100)
  DECLARE @ActionName NVARCHAR(100)
  DECLARE @ActualRoleId INT
  DECLARE @RoleId INT
  DECLARE @ProcessId INT
  DECLARE @Area NVARCHAR(3) ='PCR'
  DECLARE @MaxOrderNumber INT = 0
  DECLARE @RowIndex INT = 1
  DECLARE @TotalRoles INT
  DECLARE @TotalProcesses INT  
  DECLARE @RoleName NVARCHAR(100)  
  DECLARE @RoleDescription NVARCHAR(100)
  DECLARE @RoleGroup NVARCHAR(100)
  DECLARE @Level INT
  DECLARE @RolesTable AS TABLE(RowIndex INT IDENTITY(1,1), Id INT, RoleName NVARCHAR(100), RoleDescription NVARCHAR(100), RoleGroup NVARCHAR(100), [Level] INT)
  DECLARE @ProcessesTable AS TABLE(RowIndex INT IDENTITY(1,1), RoleId INT, ProcessName NVARCHAR(100), ProcessDescription NVARCHAR(100), ControllerName NVARCHAR(100), ActionName NVARCHAR(100))
  
  DELETE FROM [dbo].[ATH003] WHERE ProcessId IN
	(
		SELECT 
			Processes.Id 
		FROM 
			[dbo].[ATH002] AS Processes 
		WHERE 
			Processes.[Area] = @Area
	)
  DELETE FROM [dbo].[ATH002] WHERE [Area] = @Area
  
  INSERT INTO @RolesTable
  (
	Id, RoleName, RoleDescription, RoleGroup, [Level]
  )
   VALUES 
    (1, 'PLNG3', 'Planning Engineer', 'PLNG', 3),
	(2, 'MATL3', 'Material Engineer', 'MATL', 3),
	(3, 'QC2', 'QC2', 'QC', 2),
	(4, 'SHOP', 'Shop User', 'SHOP', NULL)
	
  SELECT @TotalRoles = COUNT(0) FROM @RolesTable
  WHILE(@RowIndex <= @TotalRoles)
   BEGIN
     SELECT 
	   @RoleName = R.RoleName,
	   @RoleDescription = R.RoleDescription,
	   @RoleGroup = R.RoleGroup,
	   @Level = R.[Level]
	  FROM @RolesTable AS R WHERE R.[RowIndex] = @RowIndex
	 IF NOT EXISTS (SELECT 0 FROM [dbo].[ATH004] AS R WHERE R.[Role] = @RoleName)
	  BEGIN
	    INSERT INTO [dbo].[ATH004]
		(
		  [RoleGroup], [Role], [Description], [Level]
		)
		VALUES 
		(
		  @RoleGroup, @RoleName, @RoleDescription, @Level
		)
	  END
	 SET @RowIndex += 1
  END
  INSERT INTO @ProcessesTable
  (
	RoleId, ProcessName, ProcessDescription, ControllerName, ActionName)
  VALUES
   (1, 'Generate PCR', 'Generate PCR', 'MaintainPCR', 'GeneratePCR'),
   (2, 'Material Planner PCR', 'Material Planner PCR', 'MaterialPlanner', 'Index'),
   (2, 'Release PCL', 'Release PCL PCR', 'ReleasePCL', 'Index'),
   (3, 'First Stage Clearance', 'First Stage Clearance QA/QC PCR', 'PCLWorkflow', 'FirstStageClearance'),
   (4, 'Confirm Plate Cutting Request', 'Confirm Plate Cutting Request PCR', 'PCLWorkflow', 'ConfirmPlateCuttingRequest'),
   (4, 'Receive Plate', 'Receive or Mark Plate PCR', 'PCLWorkflow', 'ReceivePlate'),
   (4, 'Process Plate Cutting Request', 'Process Plate Cutting Request PCR', 'PCLWorkflow', 'ProcessPlateCuttingRequest'),
   (3, 'Second Stage Clearance', 'Second Stage Clearance QA/QC PCR', 'PCLWorkflow', 'SecondStageClearance'),
   (2, 'Resolve PCL Discrepancy', 'Resolve PCL Discrepancy PCR', 'ResolvePCLDiscrepancy', 'Index')
   
  SELECT TOP 1 @ParentId = A.[Id] from [dbo].[ATH002] AS A WHERE A.[Area] = @Area AND A.[ParentId] IS NULL
  
  
  IF(@ParentId IS NULL)
   BEGIN
		DECLARE @MainMenuOrder INT = 0
		SELECT @MainMenuOrder = MAX(A.[MainMenuOrderNo]) FROM [dbo].[ATH002] AS A WHERE A.[ParentId] IS NULL
		INSERT INTO [dbo].[ATH002]
		(
		  Process, [Description], Area, Controller, [Action], ParentId, MainMenuOrderNo, 
		  OrderNo, IsDisplayInMenu, IconClass,IsActive, CreatedBy, CreatedOn, EditedBy, EditedOn, Help, FAQ, IsMobileMenu
		 )
		 Values(
		   @Area, @Area, @Area,	NULL, NULL,	NULL, @MainMenuOrder + 1,	
		   1, 1, NULL,  1, @UserId, GETUTCDATE(), NULL, NULL, NULL, NULL, 0 
		 )
	    SELECT @ParentId = @@IDENTITY  
  END

	
 SELECT @RowIndex  = 1
 SELECT @TotalProcesses = COUNT(0) FROM @ProcessesTable
 WHILE(@RowIndex <= @TotalProcesses)
  BEGIN
   SELECT 
     @ProcessName = S.ProcessName ,
	 @ProcessDesc = S.ProcessDescription ,
	 @ControllerName = S.ControllerName ,
	 @ActionName = S.ActionName,
	 @RoleId = S.RoleId
	FROM 
	 @ProcessesTable AS S WHERE S.[RowIndex] = @RowIndex
	  
	 IF NOT EXISTS(SELECT 0 FROM [dbo].[ATH002] AS A WHERE A.[Area] = @Area AND A.[Process] = @ProcessName)
	  BEGIN
	    SELECT @MaxOrderNumber = MAX(A.[OrderNo]) FROM [dbo].[ATH002] AS A WHERE A.[Area] = @Area
		INSERT INTO [dbo].[ATH002]
		(
			Process, [Description], Area, Controller, [Action], 
			ParentId, MainMenuOrderNo, OrderNo, 
			IsDisplayInMenu, IconClass, IsActive, CreatedBy, CreatedOn, EditedBy, EditedOn, Help, FAQ, IsMobileMenu
		)
		VALUES
		(
		   @ProcessName, @ProcessDesc, @Area, @ControllerName, @ActionName, 
		   @ParentId, NULL, @MaxOrderNumber + 1,
		   1, NULL, 1, @UserId, GETUTCDATE(), NULL, NULL, NULL, NULL, 0
		 )
		SET @ProcessId = @@IDENTITY
      END
	  ELSE
	   BEGIN
	     SELECT TOP 1 @ProcessId = A.[Id] FROM [dbo].[ATH002] AS A WHERE A.[Area] = @Area AND A.[Process] = @ProcessName AND A.[ParentId] = @ParentId
	   END
	  SELECT @RoleName = R.[RoleName], @RoleGroup = R.[RoleGroup] FROM @RolesTable AS R WHERE R.[Id] = @RoleId
	  SELECT TOP 1 @ActualRoleId = R.[Id] FROM [dbo].[ATH004] AS R WHERE R.[Role] = @RoleName AND R.[RoleGroup] = @RoleGroup
	  IF NOT EXISTS(SELECT 0 FROM [dbo].[ATH003] AS A WHERE A.[ProcessId] = @ProcessId AND A.[RoleId] = @ActualRoleId)
	   BEGIN
	     INSERT INTO [dbo].[ATH003]
		 (
			[RoleId], [ProcessId], [IsActive], [CreatedBy], [CreatedOn]
		 )
		 VALUES
		 (
			@ActualRoleId, @ProcessId, 1, @UserId, GETUTCDATE()
		 )
	   END
    SET @RowIndex += 1
  END
 COMMIT TRAN
END TRY
   BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 
   END CATCH
END


