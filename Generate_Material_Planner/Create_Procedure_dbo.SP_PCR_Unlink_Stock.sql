USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Unlink_Stock]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Unlink_Stock]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Unlink_Stock]
   @Id INT,
   @PSNo NVARCHAR(100)
AS
BEGIN 
    DECLARE @PCRN NVARCHAR(100)
	DECLARE @PCLN NVARCHAR(100)
	DECLARE @ARCS FLOAT
	DECLARE @ChildItem NVARCHAR(100)
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @STKR INT
	DECLARE @Pono INT
	DECLARE @Revn INT
	DECLARE @RecordCount INT
	DECLARE @Area FLOAT
	DECLARE @StockArea FLOAT
	DECLARE @IsDeleted BIT = 0
	DECLARE @ReleasedValue INT
	DECLARE @Allow BIT = 1
	DECLARE @ErrMessage NVARCHAR(100) = ''
	DECLARE @CompleteErrorMessage NVARCHAR(100)
	BEGIN TRAN
	 BEGIN TRY
	  /*

	   If STKN not empty for Id in 503 
		  If PCLN exists for Id in 503
		   and  
		   When data found in 504 for PCLN and t_stat = NULL
			 then Enable
		   else 
			 Disable
		  Else  -- PCLN not exists
			Enable
	   Else   -- STKN empty
		   Disable 



	  */
	   SELECT
	      @PCLN = P.[t_pcln],
		  @ARCS = P.[t_arcs],
		  @ChildItem = P.[t_sitm],
		  @STKR = P.[t_stkr],
		  @StockNumber = P.[t_stkn],
		  @PCRN = P.[t_pcrn],
		  @Pono = P.[t_pono] ,
		  @Revn = P.[t_revn]
	     FROM [dbo].[PCR503] AS P WHERE P.[Id] = @Id
	  
	  IF(@StockNumber IS NULL OR LEN(@StockNumber) = 0)
	   BEGIN
	    SET @Allow = 0
		SET @ErrMessage = 'Stock number does not exist'
	   END
	  ELSE -- Stock number exists
	   BEGIN
	     IF(@PCLN IS NULL OR LEN(@PCLN) = 0)
		  BEGIN
		   SET @Allow = 1
		  END
		  ELSE -- PCLN exists
		   BEGIN
		    SELECT @RecordCount = COUNT(0) FROM [dbo].[PCR504] AS PP WHERE PP.[t_pcln] = @PCLN
		    IF @RecordCount = 0
			  BEGIN
			   SET @Allow = 1
			  END
			ELSE
			  BEGIN
			   SET @Allow = 0
			   SET @ErrMessage = 'PCL is either generated or released'
			  END
		   END
	   END
	  --SET @Allow = 1 --TODO remove after validating
	  IF(@Allow = 0)
	   BEGIN
	      SET @CompleteErrorMessage = 'Unlink stock is not allowed'
		  SET @CompleteErrorMessage = CASE WHEN LEN(@ErrMessage) > 0 THEN @CompleteErrorMessage + ' because ' + @ErrMessage ELSE @CompleteErrorMessage END
	      RAISERROR(@CompleteErrorMessage , 11, 1)
	   END
	  
	  SELECT
	    TOP 1 @ReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'

	   
	   SELECT TOP 1 @Area = P.[t_area]
	   FROM [dbo].[PCR504] AS P WHERE P.[t_pcln] = @PCLN
	   IF(@PCLN IS NOT NULL AND LEN(@PCLN) > 0)
	    BEGIN
		  DELETE FROM [dbo].[PCR505] WHERE [t_pcln] = @PCLN
	      IF(@ARCS = @Area) 
		   BEGIN
		      DELETE FROM [dbo].[PCR504] WHERE [t_pcln] = @PCLN
			  SET @IsDeleted = 1
		   END
		  ELSE
			 BEGIN
			    SET @StockArea = 0
				SELECT TOP 1 @StockArea = P.[t_area]
				 FROM [dbo].[PCR500] AS P WHERE P.[t_pcln] = @PCLN

				 UPDATE
				 [dbo].[PCR504] 
				  SET 
				   [t_area] = @Area - @ARCS,
				   [t_rtar] = @StockArea - @Area,
				   [t_cwoc] = NULL,
				   [t_sqty] = 0,
				   [t_emno] = NULL,
				   [t_rbal] = 0,
				   [t_tclt] = 0,
				   [t_mvtp] = NULL
				 WHERE [t_pcln] = @PCLN
			 END
	     END
		 UPDATE [dbo].[PCR500]
		  SET 
		   [t_qall] = [t_qall] - @ARCS,
		   [t_qfre] = [t_qfre] + @ARCS,
		   [t_stat] = CASE WHEN @IsDeleted = 1 THEN 1 ELSE [t_stat] END,
		   [t_pcln] = CASE WHEN @IsDeleted = 1 THEN NULL ELSE [t_pcln] END
         WHERE [t_item] = @ChildItem AND [t_stkn] = @StockNumber AND [t_stkr] = @STKR

		 UPDATE [dbo].[PCR503]
		  SET 
		   [t_stkn] = NULL,
		   [t_nqty] = 0,
		   [t_arcs] = 0,
		   [t_lsta] = @ReleasedValue,
		   [t_pcln] = NULL,
		   [t_pcl_crdt] = 0,
		   [t_pcl_rldt] = 0

		  WHERE [t_pcrn]= @PCRN AND [t_pono] = @PONO AND [t_revn] = @Revn

		 COMMIT TRAN						
	  SELECT 1
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END

