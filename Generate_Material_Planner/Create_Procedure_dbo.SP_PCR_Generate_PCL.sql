USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Generate_PCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Generate_PCL] 
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
--Exec [dbo].[SP_PCR_Generate_PCL] 72, '122963'
ALTER PROCEDURE  [dbo].[SP_PCR_Generate_PCL]
   @Id INT,
   @PSNo NVARCHAR(100) 
AS
BEGIN 
    SET NOCOUNT ON
    DECLARE @Sql NVARCHAR(MAX)
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @CurrentDate DATETIME
	Declare @H as Table(RowIndex INT IDENTITY(1,1),[t_pcln] NVARCHAR(100),[t_sitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,[t_stkn] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,[t_stkr] INT,[t_ityp] INT)
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @PCLN NVARCHAR(100)
	DECLARE @CWOC NVARCHAR(100)
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)
	DECLARE @ValueTable Table ([Value] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS)
	DECLARE @IPCL NVARCHAR(100)
	DECLARE @LOC NVARCHAR(100)
	DECLARE @PCRReleasedValue INT
	DECLARE @PCLGeneratedValue INT
	DECLARE @PipeTypeValue INT
	DECLARE @MovementTypeValue INT
	DECLARE @StockStatusAllocatedValue INT
	BEGIN TRAN
	 BEGIN TRY
	  SET @CurrentDate = GETUTCDATE() 
	  
	   SELECT
	    	TOP 1 @PCRReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'
	   SELECT
	    	TOP 1 @PCLGeneratedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'PCL.GEN'
	 
	    SELECT
	    	TOP 1 @PipeTypeValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PIPE'
	 
	   SELECT
	    	TOP 1 @MovementTypeValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Movement Type','ltmov.type') WHERE UPPER([LNConstant]) = 'PROFILE.CUT'

	   SELECT
	    	TOP 1 @StockStatusAllocatedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Stock Status','ltsfc.stk.stat') WHERE UPPER([LNConstant]) = 'ALLOCATED'
	 
	  SELECT 
	    @StockNumber = P.[t_stkn], 
	    @PCRN = P.[t_pcrn] 
	   FROM [dbo].[PCR503] AS P WHERE P.Id = @Id
	   SELECT
	    @LOC = P.[t_loca]
		FROM [dbo].[PCR501] AS P WHERE P.[t_pcrn] = @PCRN
	  
	  SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer')
	  SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId')
	  SET @Sql = 'SELECT TOP 1 [t_ipcl] FROM ' + @LNLinkedServer + '.dbo.tltlnt000' + @LNCompanyId + ' WHERE [t_dimx] = ''' + @LOC + ''''
	  INSERT INTO @ValueTable
	  EXEC(@Sql)
	  SELECT TOP 1 @IPCL = [Value] FROM @ValueTable
	  --print @ipcl
	  --print @stocknumber
	  UPDATE [dbo].[PCR503] SET
	  [t_lsta] = @PCLGeneratedValue,
	  [t_pcln] = CONCAT(CONCAT(@IPCL,LTRIM(RTRIM(@StockNumber))),LTRIM(RTRIM(ISNULL([t_stkr], 0)))) ,
	  [t_pcl_crdt] = @CurrentDate
	  WHERE [t_stkn] = @StockNumber AND [t_lsta] = @PCRReleasedValue 

      INSERT 
	   INTO 
	    @H([t_pcln], [t_sitm], [t_stkn], [t_stkr], [t_ityp])
	  SELECT P.[t_pcln], P.[t_sitm], P.[t_stkn], ISNULL(P.[t_stkr], 0), P.[t_ityp]
        FROM     [dbo].[PCR503] AS P
       WHERE   P.[t_lsta] = @PCLGeneratedValue
        AND     CAST(P.[t_pcl_crdt] AS DATE) = CAST(@CurrentDate AS DATE)
		AND P.[t_pcln] = CONCAT(CONCAT(@IPCL,LTRIM(RTRIM(@StockNumber))),LTRIM(RTRIM(ISNULL([t_stkr], 0)))) 
        GROUP BY P.[t_pcln], P.[t_sitm], P.[t_stkn], P.[t_stkr], P.[t_ityp]
        
		INSERT INTO [dbo].[PCR504](
		[t_pcln], [t_revn], [t_emno], [t_area], [t_rtar], [t_stat], [t_orno], [t_sqty], [t_rbal], 
		[t_pcl_rldt], [t_pmg_cdat], [t_pmg_rdat], [t_qa_adat], [t_qa_rdat], [t_sfc_cdat], [t_plt_idat],  [t_plt_rdat],  [t_plt_mdat], [t_qc_adat], [t_qc_rdat], [t_plt_cdat],
		[t_plt_rtdt], [t_pcl_cndt], [t_pcl_cldt], 
		[t_ityp], [t_mvtp], [t_odds])
		SELECT 
		
		
		  P.[t_pcln], 0, @PSNo, P0.[t_qall], P0.[t_area] - P0.[t_qall], NULL, NULL, 0, 0, 
		  NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
		  P.[t_ityp], CASE WHEN  P.[t_ityp] = @PipeTypeValue THEN @MovementTypeValue ELSE NULL END, 0 --TODO check 
		FROM @H AS P
		INNER JOIN [dbo].[PCR500] AS P0 ON 
		LTRIM(RTRIM(P0.[t_item])) = LTRIM(RTRIM(P.[t_sitm])) 
		AND P0.[t_stkn] = P.[t_stkn] 
		AND ISNULL(P0.[t_stkr], 0) = ISNULL(P.[t_stkr], 0)
		
       DELETE FROM @H
	    
	    INSERT INTO @H([t_pcln],[t_sitm],[t_stkn], [t_stkr])
	    SELECT P.[t_pcln], P.[t_sitm], P.[t_stkn], P.[t_stkr]
		 FROM  [dbo].[PCR503] AS P
			WHERE   P.[t_lsta] = @PCLGeneratedValue
		  AND  CAST(P.[t_pcl_crdt] AS DATE) = CAST(@CurrentDate AS DATE)
		  AND P.[t_pcln] = CONCAT(CONCAT(@IPCL, LTRIM(RTRIM(@StockNumber))),LTRIM(RTRIM(ISNULL(P.[t_stkr], 0))))
        GROUP BY P.[t_pcln], P.[t_sitm], P.[t_stkn], P.[t_stkr]

		UPDATE P
		 SET 
		   P.[t_stat] = @StockStatusAllocatedValue , 
		   P.[t_pcln] = H.[t_pcln]
		 FROM
		 [dbo].[PCR500] AS P
		 JOIN @H AS H ON 
		  LTRIM(RTRIM(H.[t_sitm]))  = LTRIM(RTRIM(P.[t_item])) 
		   AND 
		  H.[t_stkn]  = P.[t_stkn] 
		   AND
		  ISNULL(H.[t_stkr], 0) = ISNULL(P.[t_stkr], 0)

		  SELECT 
		    TOP 1
			 @PCLN = P.[t_pcln],
		     @CWOC = P.[t_cwoc]
		  
		   FROM [dbo].[PCR503] AS P 
		   WHERE 
		   CAST(P.[t_pcl_crdt] AS DATE) = CAST(@CurrentDate AS DATE) 
		   AND
		   P.[t_pcln] = CONCAT(CONCAT(@IPCL, LTRIM(RTRIM(@StockNumber))),LTRIM(RTRIM(ISNULL(P.[t_stkr], 0))))
	
	       UPDATE [dbo].[PCR504]
		   SET [t_cwoc] = @CWOC
		   WHERE [t_cwoc] IS NULL AND [t_pcln] = @PCLN

	      COMMIT TRAN						
	  SELECT 1
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(), 
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END

