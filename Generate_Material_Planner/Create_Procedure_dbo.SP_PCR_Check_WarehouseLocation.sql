USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Check_WarehouseLocation]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Check_WarehouseLocation]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE  [dbo].[SP_PCR_Check_WarehouseLocation]
   @Id INT
AS
BEGIN 
	SET NOCOUNT ON
	DECLARE @IsAvailableInINS BIT
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @t_cwar NVARCHAR(100)
	DECLARE @t_sitm NVARCHAR(100)
	DECLARE @t_item NVARCHAR(100)
	DECLARE @t_stkn NVARCHAR(100)
	DECLARE @Total INT
	DECLARE @tbl AS TABLE(RowIndex INT IDENTITY(1,1), t_cwar NVARCHAR(100), t_item NVARCHAR(100), t_stkn NVARCHAR(100))
	DECLARE @R AS TABLE([Value] NVARCHAR(100))
	DECLARE @V NVARCHAR(100)
	DECLARE @Sql NVARCHAR(MAX)
	INSERT INTO @tbl(t_cwar,t_item,t_stkn)
	SELECT
	TOP 1 P0.[t_cwar], P0.[t_item], P0.[t_stkn]
	  FROM
	  [dbo].[PCR500] AS P0 WITH (NOLOCK) INNER JOIN [dbo].[PCR503] AS P ON LTRIM(RTRIM(P0.[t_item])) = LTRIM(RTRIM(P.[t_sitm])) AND P0.[t_stkn] = P.[t_stkn] WHERE P.[Id] = @Id
	  ORDER BY P0.[t_stkr] DESC

    SELECT @Total = COUNT(0) FROM @tbl
	IF(@Total = 0)
	 BEGIN
	   SET @IsAvailableInINS = 0
	 END
	 ELSE
	  BEGIN
	    SELECT 
		       @t_cwar = T.[t_cwar],
		       @t_item = T.[t_item],
			   @t_stkn = T.[t_stkn]			   
	    FROM @tbl AS T WHERE T.RowIndex = 1
		SET @Sql = 
		' SELECT C.[t_loca] FROM ' 
		+ @LNLinkedServer + '.dbo.twhinr140'+ @LNCompanyId + 
		' AS C WHERE C.[t_cwar] = ''' + @t_cwar + ''' AND LTRIM(RTRIM(C.[t_item])) = ''' + 
		LTRIM(RTRIM(@t_item)) + ''' AND C.[t_clot] = ''' + @t_stkn + 
		''' AND C.[t_qhnd] > 0 AND UPPER(LTRIM(RTRIM(C.[t_loca]))) = ''INS'' '
	    
		PRINT(@Sql)
	    INSERT INTO @R
		EXEC(@Sql)
		SELECT @Total = COUNT(0) FROM @R
		IF(@Total > 0)
		BEGIN
		 SET @IsAvailableInINS = 1
		END
		ELSE
		BEGIN
		   SET @IsAvailableInINS = 0
		END
	  END
	  RETURN @IsAvailableInINS
	 
END

