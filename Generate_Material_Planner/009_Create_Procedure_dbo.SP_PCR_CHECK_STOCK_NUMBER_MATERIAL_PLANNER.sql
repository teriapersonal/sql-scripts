USE [IEMQS]
GO
/****** Object:  StoredProcedure [dbo].[SP_PCR_CHECK_STOCK_NUMBER_MATERIAL_PLANNER]    Script Date: 12-12-2020 18:58:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[SP_PCR_CHECK_STOCK_NUMBER_MATERIAL_PLANNER]
AS
BEGIN 
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @SQlQuery NVARCHAR(MAX) 
	DECLARE @SQlQuery1 NVARCHAR(MAX) 
	DECLARE @SQlQuery2 NVARCHAR(MAX) 

	DECLARE @Collation NVARCHAR(100) = ''
	SET @Collation = ' COLLATE ' + 'Latin1_General_100_CS_AS_KS_WS';

	SET @SQlQuery = 
		'SELECT CASE WHEN EXISTS (
			SELECT *
			FROM  ' + @LNLinkedServer  + '.dbo.tltfsc500' + @LNCompanyId + ' AS A
			LEFT JOIN [dbo].[PCR503] AS P ON A.[t_sitm]=P.[t_sitm] AND A.[t_stkn]=P.[t_stkn]
			AND A.[t_stkr]=P.[t_stkr]
			WHERE A.[t_cwar] = "SC"
		)
		THEN CAST(0 AS BIT)
		ELSE CAST(1 AS BIT)'
END