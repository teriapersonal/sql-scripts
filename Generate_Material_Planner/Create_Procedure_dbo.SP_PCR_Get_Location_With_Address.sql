USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_Location_With_Address]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_Location_With_Address] 
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Get_Location_With_Address]   
AS
BEGIN 
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @LNLinkedServer NVARCHAR(100) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(100) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	

	SET @SqlQuery = 'SELECT  
							A.[t_ladr] AS ParentID,
							C.[t_dimx] AS Code, 
						    C.[t_desc] AS Description, 
						    CONCAT(C.[t_dimx], ' + '''-'' + C.[t_desc]) AS CodeDescription							
					FROM  [dbo].[COM002] AS C INNER JOIN '+ @LNLinkedServer +'.[dbo].[tltlnt000'+ @LNCompanyId + '] AS A 
					ON A.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx] WHERE C.[t_dimx] IS NOT NULL AND C.[t_dtyp] = 1 
					ORDER BY A.[t_ladr], C.[t_dimx], C.[t_desc]'

	EXEC (@SqlQuery)
END

