USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_Generate_PCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_Generate_PCL]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_Generate_PCL]
   @Id INT
AS
BEGIN 
	SET NOCOUNT ON
	DECLARE @IsWarehouseInINS BIT = 0
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @LastPCRDate DATETIME
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)
	DECLARE @ChildItem NVARCHAR(100)
	DECLARE @PartNumber NVARCHAR(100)
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @PCRDATE DATETIME
	DECLARE @PONO INT
	DECLARE @Revn INT
	DECLARE @NQty INT
	DECLARE @ARCS FLOAT
	DECLARE @PrevPCRN NVARCHAR(100)
	DECLARE @PrevPONO INT
	DECLARE @PrevREVN INT
	DECLARE @MSG NVARCHAR(1000)
	DECLARE @Index INT
	DECLARE @Total INT
	DECLARE @PCRReleasedValue INT
	DECLARE @PCRDeletedValue INT
	DECLARE @Tbl AS TABLE(RowIndex INT IDENTITY(1,1), [t_arcs] FLOAT, [t_nqty] INT, [t_pcrn] NVARCHAR(100), [t_pono] INT)	
	BEGIN TRY
	 
	 EXECUTE @IsWarehouseInINS = [dbo].[SP_PCR_Check_WarehouseLocation] @Id
	 
	 IF(@IsWarehouseInINS = 1)
	  BEGIN
	    RAISERROR('Inventory is available in Location : INS, So you cant generate PCL', 11, 1)
	  END

	 SELECT @StockNumber = P.[t_stkn] FROM [dbo].[PCR503] AS P WHERE P.[Id] = @Id
	 IF(@StockNumber IS NULL OR LEN(@StockNumber) = 0)
	  BEGIN
	   RAISERROR('Stock Number should not be blank for selected line', 11, 1)
	  END
	  SELECT
	    	TOP 1 @PCRReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'
      SELECT
	    	TOP 1 @PCRDeletedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'DELETE'

	  INSERT INTO @Tbl([t_arcs], [t_nqty], [t_pcrn], [t_pono])
	  SELECT
	   P.[t_arcs], P.[t_nqty], P.[t_pcrn], [t_pono] 
	   FROM [dbo].[PCR503] AS P
	   WHERE P.[t_lsta] = @PCRReleasedValue
	    AND P.[t_stkn] = @StockNumber
      SET @Index = 1
	  SELECT @Total = COUNT(0) FROM @Tbl
	  WHILE(@Index <= @Total)
	   BEGIN
	     SELECT 
		  @NQty = ISNULL(T.[t_nqty], 0),
		  @PCRN = T.[t_pcrn],
		  @ARCS = ISNULL(T.[t_arcs], 0),
		  @PONO = T.[t_pono]
		  FROM @Tbl AS T WHERE T.[RowIndex] = @Index
		 IF(@NQty = 0)
		  BEGIN
		   SET @MSG = 'Nested Quantity for PCR Number '
		   SET @MSG = CONCAT(@MSG, @PCRN)
		   SET @MSG  = CONCAT(@MSG, ' and PCR Line ')
		   SET @MSG  = CONCAT(@MSG, @PONO)
		   SET @MSG  = CONCAT(@MSG, ' is zero ')
		   BREAK;
		  END
		 IF(@ARCS = 0)
		   BEGIN
		      SET @MSG = 'Area Consumed for PCR Number '
			  SET @MSG = CONCAT(@MSG, @PCRN)
			  SET @MSG  = CONCAT(@MSG, ' and PCR Line ')
			  SET @MSG  = CONCAT(@MSG, @PONO)
			  SET @MSG  = CONCAT(@MSG, ' is zero ')
		      BREAK;
		    END
		    SET @Index += 1
	    END		
	 
        IF(@Total = 0)
	      BEGIN
	        SET @MSG = 'No Records found to generate PCL'
	      END
	     IF(@MSG IS NOT NULL)
	      BEGIN
		    RAISERROR(@MSG, 11, 1)
		  END

		  SELECT
		 
		    @Project = P.[t_cprj], 
		    @Element = P.[t_cspa],
		    @ChildItem = P.[t_sitm],
		    @PCRN = P.[t_pcrn], 
		    @PONO = P.[t_pono], 
		    @Revn = P.[t_revn],
		    @PCRDATE = P.[t_pcr_crdt],
		    @PartNumber = P.[t_prtn]

		     FROM [dbo].[PCR503] AS P
		     WHERE P.[Id] = @Id

		     IF EXISTS(
					SELECT 
						P.[t_pcr_crdt] 
					FROM 
						[dbo].[PCR503] AS P 
					WHERE P.[t_cprj] = @Project AND P.[t_cspa] = @Element AND P.[t_prtn] = @PartNumber AND P.[t_stkn] IS NOT NULL
		      )
		        BEGIN
		           SELECT TOP 1 @LastPCRDate = P.[t_pcr_crdt] 
		           FROM [dbo].[PCR503] AS P WHERE P.[t_cprj] = @Project AND P.[t_cspa] = @Element AND P.[t_prtn] = @PartNumber AND P.[t_stkn] IS NOT NULL
			       ORDER BY P.[t_pcr_crdt] DESC
		        END
		     ELSE
		       BEGIN
		        SELECT @LastPCRDate = @PCRDATE
		     END
		 
		
		    SELECT       
			   TOP 1 
				@PrevPCRN = P.[t_pcrn],
				@PrevPONO = P.[t_pono],
				@PrevREVN = P.[t_revn]
			FROM        
			  [dbo].[PCR503] AS P
			WHERE
				P.[t_cprj] =  @Project
				 AND        
				P.[t_sitm] =  @ChildItem
				 AND        
				P.[t_prtn] =  @PartNumber
				 AND
				P.[t_cspa] = @Element
				 AND
				 (
				   P.[t_pcrn] <> @PCRN
						OR
					P.[t_pono] <> @PONO
						OR
					P.[t_revn] <> @Revn
				 )
				 AND
    			CAST(P.[t_pcr_crdt] AS DATE) < CAST(@LastPCRDate AS DATE)
		  		 AND
	    		P.[t_stkn] IS NULL
				 AND
			    P.[t_lsta] <> @PCRDeletedValue 
 
          IF(@PrevPCRN IS NOT NULL AND @PrevPONO IS NOT NULL AND @PrevPONO IS NOT NULL)
	       BEGIN
	          SET @MSG = 'Please proceed previous PCR No.: '
		      SET @MSG = CONCAT(@MSG, @PrevPCRN)
		      SET @MSG = CONCAT(@MSG, ', PCR Line: ')
		      SET @MSG = CONCAT(@MSG, @PrevPONO)
		      SET @MSG = CONCAT(@MSG, 'and Revision: ')
		      SET @MSG = CONCAT(@MSG, @PrevREVN)
              RAISERROR(@MSG, 11, 1)
	       END

	      SELECT 1
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END

