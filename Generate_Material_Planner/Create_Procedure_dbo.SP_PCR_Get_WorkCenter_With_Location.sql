USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_WorkCenter_With_Location]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_WorkCenter_With_Location]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
--exec [dbo].[SP_PCR_Get_WorkCenter_With_Location] 0
ALTER PROCEDURE [dbo].[SP_PCR_Get_WorkCenter_With_Location]
 @ShowAll BIT   = 1
AS
BEGIN 
	
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @LNLinkedServer NVARCHAR(100) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(100) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	

	/*SET @SqlQuery = '	Select  
	                        [t_cadr] AS ParentID ,
							[t_cwoc] AS Code, 
						    [t_dsca] AS Description, 
						    CONCAT([t_cwoc], ' + '''-'' + [t_dsca]) AS CodeDescription							
					   FROM '+ @LNLinkedServer + '.dbo.ttcmcs065'+ @LNCompanyId + ' ORDER BY [t_cadr], [t_cwoc], [t_dsca]'*/
     IF(@ShowAll = 1)
	  BEGIN 
	   SET @SqlQuery = '	SELECT  
	                        DISTINCT
	                        B.[t_cadr] AS ParentID ,
							B.[t_cwoc] AS Code, 
						    B.[t_dsca] AS Description, 
						    CONCAT(B.[t_cwoc], ' + '''-'' + B.[t_dsca]) AS CodeDescription							
					   FROM [dbo].[COM002] AS C
					   INNER JOIN '+ @LNLinkedServer +'.[dbo].[tltlnt000'+ @LNCompanyId + '] AS A 
					ON A.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					INNER JOIN '+ @LNLinkedServer + '.dbo.ttcmcs065'+ @LNCompanyId + ' AS B 
					   ON B.[t_cadr] = A.[t_ladr] WHERE C.[t_dimx] IS NOT NULL AND C.[t_dtyp] = 1 ORDER BY B.[t_cadr], B.[t_cwoc], B.[t_dsca]'
     END
	 ELSE
	  BEGIN
	    SET @SqlQuery = '	SELECT  
	                        DISTINCT
	                        B.[t_cadr] AS ParentID ,
							B.[t_cwoc] AS Code, 
						    B.[t_dsca] AS Description, 
						    CONCAT(B.[t_cwoc], ' + '''-'' + B.[t_dsca]) AS CodeDescription							
					   FROM [dbo].[COM002] AS C
					   INNER JOIN '+ @LNLinkedServer +'.[dbo].[tltlnt000'+ @LNCompanyId + '] AS A 
					ON A.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					INNER JOIN [dbo].[PCR501] AS P ON P.[t_loca] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					INNER JOIN '+ @LNLinkedServer + '.dbo.ttcmcs065'+ @LNCompanyId + ' AS B 
					   ON B.[t_cadr] = A.[t_ladr] WHERE C.[t_dimx] IS NOT NULL AND C.[t_dtyp] = 1 ORDER BY B.[t_cadr], B.[t_cwoc], B.[t_dsca]'
	  END
	EXEC (@SqlQuery)
END

