USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_StockNumberList]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_StockNumberList]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
--select * from pcr500 where Id =1281476
--update pcr500 set t_psta = 5 where Id =1281476
 --select distinct p.Id, p1.Id as IId,p1.t_loca, p1.t_item, p1.t_psta , p1.t_stat from pcr503 as p join pcr500 as p1 on p1.t_item = p.t_sitm
--exec [dbo].[SP_PCR_Get_StockNumberList] '122963', 2237
ALTER PROCEDURE [dbo].[SP_PCR_Get_StockNumberList] 
 @PSNo NVARCHAR(100),
 @Id INT
AS
BEGIN 
	DECLARE @Location NVARCHAR(100)
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @LNLinkedServer NVARCHAR(100) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(100) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @StockPhysicalStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
	DECLARE @StockStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
	
	DECLARE @ResultTable AS TABLE([Id] INT, [Contract] NVARCHAR(100), [Project] NVARCHAR(100),
	[ChildItem] NVARCHAR(100), [StockNumber] NVARCHAR(100), [StockRevision] INT, [TotalArea] FLOAT, [AreaAllocated] FLOAT,
	[FreeArea] FLOAT, [UseAlternateStock] BIT)
	
	
	SELECT 
		TOP 1 @Location = P.[t_loca]
    FROM 
		[dbo].[PCR501] AS P INNER JOIN [dbo].[PCR503] AS P3 ON P3.[t_pcrn] = P.[t_pcrn]
	WHERE 
		P3.[Id] = @Id

    SELECT @Location = 'PEW'
	INSERT INTO 
		@StockPhysicalStatusTable
		(
			[LNConstant], 
			[Text], 
			[Value]
		)
    SELECT 
		Enums.[LNConstant], 
		Enums.[Text], 
		Enums.[Value] 
	FROM 
		[dbo].[PCR000] AS Enums 
	WHERE 
		UPPER(Enums.[EnumName]) = UPPER('Stock Physical Status')
		AND
		UPPER(Enums.[LNConstant]) IN 
		(
			UPPER('virtual'), 
			UPPER('at.store'), 
			UPPER('at.pps'),
			UPPER('ret.pps')
		)

	INSERT INTO 
		@StockStatusTable
		(
			[LNConstant], 
			[Text], 
			[Value]
		)
    SELECT 
		Enums.[LNConstant], 
		Enums.[Text], 
		Enums.[Value] 
	FROM 
		[dbo].[PCR000] AS Enums 
	WHERE 
		UPPER(Enums.[EnumName]) = UPPER('Stock Status') 
	    AND
		UPPER(Enums.[LNConstant]) <> UPPER('delete')
	
	--SET @Id = 1219
    
	 
	SET @SqlQuery =
		   'DECLARE @StockStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)
		    DECLARE @StockPhysicalStatusTable AS TABLE(LNConstant NVARCHAR(100), [Text] NVARCHAR(100), [Value] INT)			
			INSERT INTO @StockPhysicalStatusTable([LNConstant], [Text], [Value])
			SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums 
			WHERE UPPER(Enums.[EnumName]) = UPPER(''Stock Physical Status'')
				AND	UPPER(Enums.[LNConstant]) IN (UPPER(''virtual''), UPPER(''at.store''), UPPER(''at.pps''), UPPER(''ret.pps''))

			INSERT INTO @StockStatusTable([LNConstant], [Text], [Value])
			SELECT Enums.[LNConstant], Enums.[Text], Enums.[Value] FROM [dbo].[PCR000] AS Enums WHERE 
			UPPER(Enums.[EnumName]) = UPPER(''Stock Status'') AND UPPER(Enums.[LNConstant]) <> UPPER(''delete'') '
		SET @SqlQuery += 
		   CONCAT(' DECLARE @I INT = ', @Id)
		   SET @SqlQuery += 
			  ' SELECT		        
				TOP 1000
				P.[Id] AS Id, 
				P.[t_cono] AS [Contract],
				A.[t_cprj] AS [Project],
				LTRIM(RTRIM(P.[t_item])) AS ChildItem,
				LTRIM(RTRIM(P.[t_stkn])) AS StockNumber, 
				P.[t_stkr] AS StockRevision, 
				P.[t_area] AS TotalArea, 
				P.[t_qall] AS AreaAllocated, 
				P.[t_qfre] As FreeArea,
				1 AS UseAlternateStock
			FROM 
				[dbo].[PCR500] AS P 
			INNER JOIN ' +
			  @LNLinkedServer + '.dbo.ttpctm110' + @LNCompanyId + ' AS A 
			ON A.[t_cono] = P.[t_cono]
			 INNER JOIN
			    [dbo].[PCR503] AS P3 ON P3.[t_cprj] = A.[t_cprj] 
			WHERE 
			  P.[t_loca] = ''' + @Location + '''' + ' ' + '			   
			   AND 
			  P3.[Id] = @I
			   AND
			  P.[t_stat] IN 
			  (
				SELECT 
					SS.[Value] 
				FROM @StockStatusTable AS SS
			  )
			  AND
			  P.[t_psta] IN 
			  (
				SELECT 
					SPS.[Value] 
				FROM @StockPhysicalStatusTable AS SPS WHERE UPPER(SPS.LNConstant) <> UPPER(''VIRTUAL'')
			  ) '
    
	 INSERT INTO 
	  @ResultTable
	  (
		[Id], 
		[Contract], 
		[Project], 
		[ChildItem], 
		[StockNumber], 
		[StockRevision], 
		[TotalArea], 
		[AreaAllocated], 
		[FreeArea],
		[UseAlternateStock]
	  )
	 EXEC(@SqlQuery)
	

	  INSERT INTO 
	  @ResultTable
	  (
		[Id], 
		[Contract], 
		[Project], 
		[ChildItem], 
		[StockNumber], 
		[StockRevision], 
		[TotalArea], 
		[AreaAllocated], 
		[FreeArea],
		[UseAlternateStock]
	  )
	  SELECT
		    DISTINCT
			P.[Id] AS Id, 
			P.[t_cono] AS [Contract],
			NULL AS [Project],
			LTRIM(RTRIM(P.[t_item])) AS ChildItem,
			LTRIM(RTRIM(P.[t_stkn])) AS StockNumber,  
			P.[t_stkr] AS StockRevision, 
			P.[t_area] AS TotalArea, 
			P.[t_qall] AS AreaAllocated, 
			P.[t_qfre] As FreeArea,
			0 AS UseAlternateStock
	  FROM 
			[dbo].[PCR500] AS P
	  INNER JOIN 
			[dbo].[PCR503] AS P3 ON LTRIM(RTRIM(P.[t_item])) = LTRIM(RTRIM(P3.[t_sitm]))  
	  WHERE 
		P.[t_loca] = @Location 
		 AND
		P3.[Id] = @Id
		 AND
		P.[t_stat] IN 
		(
		  SELECT 
			SS.[Value] 
		  FROM @StockStatusTable AS SS
		)
		 AND
		P.[t_psta] IN 
		(
		  SELECT 
			SPS.[Value] 
		  FROM @StockPhysicalStatusTable AS SPS 
		)	
  SELECT * from @ResultTable
END

