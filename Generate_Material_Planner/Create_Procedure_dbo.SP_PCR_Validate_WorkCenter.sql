USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_WorkCenter]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_WorkCenter]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_WorkCenter]
   @WorkCenter NVARCHAR(100),
   @LocationAddress NVARCHAR(100)
AS
BEGIN 
	SET NOCOUNT ON
	
	BEGIN TRY
	     DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	     DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	 
		 DECLARE @Sql NVARCHAR(MAX)
		 DECLARE @ResultTable AS TABLE(WorkCenter NVARCHAR(100))
		 DECLARE @Count INT
		 SET @Sql = ' SELECT [t_cwoc] FROM ' + @LNLinkedServer + '.dbo.ttcmcs065' + @LNCompanyId + ' WHERE [t_cwoc] = ''' + @WorkCenter + ''' AND [t_cadr] = ''' + @LocationAddress + ''''
	     INSERT INTO @ResultTable(WorkCenter)
		  EXEC(@Sql)
		 SELECT @Count = COUNT(0) FROM @ResultTable
		 IF(@Count = 0)
		  BEGIN
		    RAISERROR('Specified Work Center does not belong to PCR Location. Enter another Work center',12, 1);
		  END
		  SELECT 1
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END

