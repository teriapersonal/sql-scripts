USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_Return_PCR]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_Return_PCR]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_Return_PCR]
   @Id INT
AS
BEGIN 
	DECLARE @Allow BIT = 1
	DECLARE @PCRN NVARCHAR(100)
	DECLARE @PONO INT
	DECLARE @Revn INT
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)
	DECLARE @SerialNumber INT
	DECLARE @Sequence INT
	DECLARE @tbl AS TABLE(RowINDEX INT IDENTITY(1,1), Project NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, Element NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, SerialNumber INT, Sequence INT)
	DECLARE @PCRLineStatusDeletedValue INT
	BEGIN TRY
	   SELECT 
		TOP 1  @PCRLineStatusDeletedValue =
		[Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'DELETE'
	  
	   SELECT 	   
	     @PCRN = P.[t_pcrn],
	     @PONO = P.[t_pono],
	     @Revn = P.[t_revn]
	   FROM 
	    [dbo].[PCR503] AS P 
	   WHERE 
	    P.Id = @Id

		IF EXISTS (SELECT 0 FROM [dbo].[PCR503] AS P WHERE P.[t_pono] = @PONO AND P.[t_pcrn] = @PCRN AND P.[t_revn] <> @Revn AND P.[t_lsta] <> @PCRLineStatusDeletedValue)
		 BEGIN
		   SET @Allow = 0
		 END
		 ELSE
		  BEGIN
		    INSERT INTO @tbl(Project, Element, SerialNumber, [Sequence])
			SELECT 
			P0.[t_cprj], P0.[t_cspa], P0.[t_sern], P0.[t_seqn]
			FROM [dbo].[PCR506] AS P0 WHERE P0.[t_pcrn] = @PCRN AND P0.[t_pono] = @PONO  
			IF EXISTS (
			 SELECT 0 FROM [dbo].[PCR506] AS P0 INNER JOIN @tbl AS T ON T.Project = P0.[t_cprj] AND T.Element = P0.[t_cspa] 
			  AND T.[SerialNumber] = P0.[t_sern] AND T.[Sequence] > P0.[t_seqn])
			   BEGIN
			     SET @Allow = 0
			   END
			ELSE
			   BEGIN
			    SET @Allow = 1
			  END
		  END		
		  SET @Allow = 1 --TODO remove after confirmation
		IF(@Allow = 0)
		 BEGIN
		   RAISERROR('Return PCR not allowed', 11, 1)
		 END
		SELECT 
		TOP 1 
		@StockNumber = P.[t_stkn] 
		FROM [dbo].[PCR503] AS P 
		WHERE P.[t_pcrn] = @PCRN AND P.[t_pono] = @PONO AND P.[t_revn] = @Revn
		IF(@StockNumber IS NOT NULL AND LEN(@StockNumber) > 0)
		BEGIN
		 RAISERROR('Please unlink stock first.', 11, 1)
		END
	   SELECT 1
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END

