USE [IEMQS]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_MATERIAL_PLANNER_DATA]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_MATERIAL_PLANNER_DATA]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
--EXEC [dbo].[SP_PCR_GET_MATERIAL_PLANNER_DATA]  @StartIndex  = 1,    @EndIndex  =220,    @SortingFields  = ''
ALTER PROCEDURE [dbo].[SP_PCR_GET_MATERIAL_PLANNER_DATA]
    @StartIndex INT = 1,
    @EndIndex INT = 10,
    @SortingFields VARCHAR(50) = '',
	@PCR504Id INT = 0
AS
BEGIN 
	DECLARE @RowFilterQuery NVARCHAR(MAX) = '';
	DECLARE @DeclareQuery NVARCHAR(MAX) = ''
	DECLARE @Spacer NVARCHAR(100) 
    DECLARE @SegmentLength INT = 9
    DECLARE @i INT =0
    WHILE(@i < @SegmentLength)
    BEGIN
      SELECT @Spacer = CASE WHEN @i = 0 THEN ' ' ELSE   @Spacer + ' ' END 
      SET @i += 1
    END
	
	IF LEN(ISNULL(@SortingFields, '')) > 0
	BEGIN
		SET @SortingFields = SUBSTRING(LTRIM(RTRIM(@SortingFields)), 9, LEN(LTRIM(RTRIM(@SortingFields)))-8)
	END

	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @SQlQuery NVARCHAR(MAX) 
	DECLARE @SQlQuery1 NVARCHAR(MAX) 
	DECLARE @SQlQuery2 NVARCHAR(MAX) 
	DECLARE @SQlQuery3 NVARCHAR(MAX) 
	DECLARE @SQlQuery4 NVARCHAR(MAX) 
	DECLARE @SQlQuery5 NVARCHAR(MAX)
	DECLARE @SQlQuery6 NVARCHAR(MAX)
	--SET @SqlQuery = 
	
    
	--EXEC(@SqlQuery)
	DECLARE @Collation NVARCHAR(100) = ''
	SET @Collation = ' COLLATE ' + 'Latin1_General_100_CS_AS_KS_WS';
	IF LEN(ISNULL(@SortingFields, '')) > 0
	 BEGIN
	 	SET @SortingFields = SUBSTRING(LTRIM(RTRIM(@SortingFields)), 9, LEN(LTRIM(RTRIM(@SortingFields)))-8)
	 END

	 DECLARE @ConditionalJoin NVARCHAR(MAX) = ''
	 IF (@PCR504Id <> 0)
		BEGIN
		  SET @ConditionalJoin = @ConditionalJoin + ' INNER JOIN [dbo].[PCR504] AS P4 ON P4.[t_pcln] = P.[t_pcln] '
		  SET @ConditionalJoin = @ConditionalJoin + ' AND P4.[t_revn] = P.[t_revn] '
		END
		ELSE
		BEGIN
		  SET @ConditionalJoin= ''
	END;

	DECLARE @ConditionalWhere NVARCHAR(MAX) = ''
	IF (@PCR504Id <> 0)
		BEGIN
		  SET @ConditionalWhere = @ConditionalWhere + ' AND P4.[Id] = ' 
		  SET @ConditionalWhere = CONCAT(@ConditionalWhere, @PCR504Id)
		END
		ELSE
		BEGIN
		  SET @ConditionalWhere= ''
	END;

	SET @SQlQuery1 = 
	'
	 DECLARE @tblName AS TABLE
	 (
	   [t_emno] NVARCHAR(100),
	   [t_nama] NVARCHAR(200)
	 ) 
	 DECLARE @tblAllProjects TABLE
	 (
		[Id] NVARCHAR(100) , 
		[Description] NVARCHAR(MAX) , 
		[CodeDescription] NVARCHAR(MAX)
	 )
	 
	  INSERT INTO @tblAllProjects
	  (
		[Id], 
		[Description], 
		[CodeDescription]
	  )
	   EXEC [dbo].[SP_PCR_Get_ProjectList] NULL	 
	 DECLARE @tblProjects TABLE
	 (
		[Id] NVARCHAR(100) , 
		[Description] NVARCHAR(MAX) , 
		[CodeDescription] NVARCHAR(MAX)
	 )
	 DECLARE @tblElements TABLE
	 (
		Id NVARCHAR(100) , 
		[Description] NVARCHAR(MAX)
	 )
	 DECLARE @tblParameter AS TABLE 
	(
		PCRN NVARCHAR(100), 
		Parameter NVARCHAR(100)
	)
	DECLARE @tblLocationAddress AS TABLE 
	(
		PCRN NVARCHAR(100), 
		LocationAddress NVARCHAR(100)
	)
	INSERT INTO @tblProjects
	(
		[Id], 
		[Description], 
		[CodeDescription]
	)
	SELECT * 
	FROM @tblAllProjects 
	WHERE [Id] COLLATE Latin1_General_100_CS_AS_KS_WS IN 
	(
			SELECT DISTINCT Proj.[t_cprj] FROM [dbo].[PCR503] AS Proj WITH (NOLOCK)
	)
	INSERT INTO @tblElements
	(
			[Id], 
			[Description]
	)
	SELECT 
	  [t_cspa], 
	  [t_dsca] 
	FROM ' + @LNLinkedServer  + '.dbo.ttppdm600' + @LNCompanyId +  ' AS tbl 
	WHERE tbl.[t_cprj] COLLATE Latin1_General_100_CS_AS_KS_WS IN 
	(
			SELECT [Id] FROM @tblProjects
	)
	INSERT INTO @tblParameter
	(
		PCRN, 
		[Parameter]
	)
	SELECT  
		A.[t_pcrn], 
		C.[t_pcr_cuni]  
	FROM ' + @lnlinkedserver + '.dbo.tltlnt000'+ @lncompanyid  + ' AS C 
	 INNER JOIN PCR501 AS A 
	   ON  A.[t_loca] = C.[t_dimx] 
    INSERT INTO @tblLocationAddress
	(
		PCRN, 
		LocationAddress
	)
	SELECT  
		A.[t_pcrn] , 
		C.[t_ladr] 
	FROM ' + @lnlinkedserver + '.dbo.tltlnt000'+ @lncompanyid  + ' AS C 
	 INNER JOIN PCR501 AS A 
	   ON  A.[t_loca] = C.[t_dimx]
	INSERT INTO @tblName([t_emno], [t_nama])
	 SELECT  
	    DISTINCT
		A.[t_emno] , 
		A.[t_nama]
	  FROM ' + @LNLinkedServer + '.dbo.ttccom001'+ @LNCompanyId  + ' AS A
	   INNER JOIN [dbo].[PCR503] AS C 
	     ON C.[t_init] COLLATE Latin1_General_100_CS_AS_KS_WS = A.[t_emno] 
	DECLARE @tblManufacturingItem TABLE
	(
		[t_cprj] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_pitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_mitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS
	)	
	DECLARE @tblChildItem TABLE
	(
		[t_cprj] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_sern] INT, 
		[t_item] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_desc] NVARCHAR(1000) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dscb] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dscd] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_mtrl] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS
	)	
	DECLARE @tblWCLocation TABLE
	(
		[t_cadr] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_cwoc] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[WCDeliverName] NVARCHAR(MAX)
	)
	DECLARE @Spacer NVARCHAR(100) = ''' + @Spacer + '''
    DECLARE @SegmentLength INT = ' 
	SET @SQlQuery1 = CONCAT(@SqlQuery1, @SegmentLength)
	SET @SQlQuery1 = @SQlQuery1 + ' ' +
	'INSERT INTO @tblManufacturingItem
	(
		[t_cprj], 
		[t_cspa], 
		[t_pitm], 
		[t_dsca]
	)
	SELECT 
		DISTINCT 
		B.[t_cprj], 
		B.[t_cspa], 
		A.[t_item], 
		A.[t_dsca]
	FROM ' + @LNLinkedServer  + '.dbo.tltibd911' + @LNCompanyId + ' AS tbl911 
		INNER JOIN ' + @LNLinkedServer  + '.dbo.ttpptc120' + @LNCompanyId + ' AS B WITH (NOLOCK) 
			ON B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern]
		INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
	        ON A.[t_item]= tbl911.[t_pitm] 
	     WHERE B.[t_cprj]' + @Collation +' IN 
		  (
			SELECT Id FROM @tblProjects
		  ) 
	
	INSERT INTO @tblChildItem
	(
		[t_cprj], 
		[t_cspa], 
		[t_sern], 
		[t_item], 
		[t_dsca], 
		[t_desc], 
		[t_dscd], 
		[t_dscb],
		[t_mtrl]
	)
	SELECT 
	  DISTINCT 
		B.[t_cprj], 
		B.[t_cspa],
		B.[t_sern], 
		A.[t_item], 
		A.[t_dsca], 
		'''', 
		A.[t_dscd], 
		A.[t_dscb],
		T.[t_mtrl]
		FROM ' + @LNLinkedServer   + '.dbo.tltibd911' + @LnCompanyId + ' AS tbl911 
		INNER JOIN ' +@LNLinkedServer + '.dbo.ttpptc120' + @LNCompanyId  + ' AS B WITH (NOLOCK) 
			ON B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern] 
		INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
	        ON A.[t_item]= tbl911.[t_citm] 
		
		INNER JOIN (SELECT distinct [t_mtrl] FROM   ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ') AS T ON T.[t_mtrl] = A.[t_dscb]
		INNER JOIN ' + @LNLinkedServer + '.dbo.tltibd903' + @LNCompanyId + ' AS tbl903 WITH (NOLOCK) 
		   ON tbl903.[t_item] = tbl911.[t_citm]
		WHERE B.[t_cprj]' + @Collation + ' IN 
			(
				SELECT Id FROM @tblProjects
			)
		UPDATE T
			SET T.[t_desc] = A.[t_desc]
	    FROM @tblChildItem AS T
		JOIN ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ' AS A
		ON A.[t_mtrl] = T.[t_mtrl]
	    
		INSERT INTO @tblWCLocation([t_cadr], [t_cwoc], [t_dsca], [WCDeliverName])
		EXEC [dbo].[SP_PCR_Get_WorkCenter_With_Location] 1 '
		
		
	    SET @SqlQuery2 = ' 
							SELECT 
							   DISTINCT
								P.[Id],
								P.[t_pcrn] AS PCRNumber,
								P.[t_pono] AS PCRLineNo,
								P.[t_revn] AS PCRLineRevision,
								P.[t_pcrt] AS PCRType,
								P.[t_prdt] AS PCRRequirementDate,
								P.[t_lsta] AS PCRLineStatus,
								P.[t_prtn] AS PartNumber,
								P.[t_mitm] AS ManufacturingItem,
                                ManufacturingItemTable.[t_dsca] AS ManufacturingItemDesc,
								@Spacer AS [ChildItemSegment],
								CASE WHEN SUBSTRING(P.[t_sitm], 1, @SegmentLength ) = @Spacer THEN SUBSTRING(P.[t_sitm], @SegmentLength + 1, LEN(P.[t_sitm]) - @SegmentLength) ELSE P.[t_sitm] END AS [ChildItem], 
								ChildItemTable.[t_dsca] AS ChildItemDesc,
								P.[t_sqty] AS ScrapQuantity,
								P.[t_nqty] AS NestedQuantity,								
								ROUND(P.[t_arcs], 2) AS AreaConsumed,
                                P2.[t_leng_m] AS [Length],
								P2.[t_widt_m] AS [Width],
								P.[t_npcs] AS NoOfPieces,
								P.[t_stkn] AS StockNumber,
								P.[t_pcln] AS PCLNumber,
								P.[t_ityp] AS ItemType,
								P.[t_mvtp] AS MovementType,
								P.[t_prmk] AS PCRPlannerRemark,
								P.[t_stkr] AS StockRevision,
								P.[t_mrmk] AS MaterialPlanner,
								P.[t_pcr_crdt] AS PCRCreationDate,
								P.[t_pcr_rldt] AS PCRReleaseDate,
								P.[t_pcl_crdt] AS PCLCreationDate,
								P.[t_pcl_rldt] AS PCLReleaseDate,
								P.[t_cprj] AS Project,
								P.[t_cspa] AS Element,
								P.[t_mpos] AS ManufacturingItemPosition,
                                Projects.Description AS ProjectDesc,
                                Elements.Description AS ElementDesc,
								PR.[Parameter] AS PCRParameter,
								LA.LocationAddress AS LocationAddress,
								P.[t_gorn] AS GrainOrientation,
								P.[t_shtp] AS ShapeType,
								P.[t_shfl] AS ShapeFile,
								P.[t_thic] AS Thickness,
								P.[t_cwoc] AS WCDeliver,'
       SET @SQLQuery3 =
							  ' WCLOCA.[WCDeliverName] AS WCDeliverName,
                                ChildItemTable.[t_dscb] AS MaterialSpecification,
								P.[t_hcno] AS PCRHardCopyNo,
                                P2.[t_cloc] AS CuttingLocation,
                                P.[t_alts] AS UseAlternateStock,
                                P.[t_armk] AS AlternateStockRemark,
								P.[t_leng] AS [LengthModified],
								P.[t_widt] AS [WidthModified],
								P.[t_cloc] AS CuttingLocationModified,
                                P.[t_oitm] AS OriginalItem,
								P.[t_rqty],
								P.[t_qapr],
								P.[t_pr01],
								P.[t_pr02],
								P.[t_pr03],
								P.[t_pr04],
								P.[t_pr05],
								P.[t_pr06],
								P.[t_pr07],
								P.[t_pr08],
								P.[t_pr09],
								P.[t_pr10],'

	  SET @SQlQuery4 =    			                    
							  ' NULL AS EmployeeName,
                                CONCAT(N.[t_emno], ''-'' + N.[t_nama]) AS PCRInitiator,							
                                P.[t_revi] AS ItemRevision,
                                P.[t_lscr] AS LinkToSCR,
                                P.[t_scrn] AS SCRNumber,
                                ChildItemTable.[t_dscd] AS ARMCode
			                    FROM [dbo].[PCR503] AS P 
								INNER JOIN @tblProjects AS Projects ON Projects.[Id] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_cprj]								
								LEFT JOIN [dbo].[PCR506] AS P2 ON P2.[t_cprj] = P.[t_cprj] AND 
								P2.[t_cspa] = P.[t_cspa] AND P2.[t_pcrn] = P.[t_pcrn] AND P2.[t_pono] = P.[t_pono]
								INNER JOIN @tblParameter AS PR ON PR.[PCRN] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcrn] 
								INNER JOIN @tblLocationAddress AS LA ON LA.[PCRN] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcrn] 
								LEFT JOIN @tblManufacturingItem AS ManufacturingItemTable ON ManufacturingItemTable.[t_cprj] = P.[t_cprj] 
								AND ManufacturingItemTable.[t_cspa] = P.[t_cspa]';
						
	  SET @SQlQuery5 =       '  AND ManufacturingItemTable.[t_pitm] = P.[t_mitm]
								LEFT JOIN @tblChildItem AS ChildItemTable ON ChildItemTable.[t_cspa] = P.[t_cspa] AND ChildItemTable.[t_cprj] = P.[t_cprj]
								AND ChildItemTable.[t_item] = P.[t_sitm] 
								LEFT JOIN @tblWCLocation AS WCLOCA ON P.[t_cwoc] = WCLOCA.[t_cwoc] 
								LEFT JOIN @tblElements AS Elements ON Elements.[Id] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_cspa]
								LEFT OUTER JOIN @tblName AS N ON P.[t_init] COLLATE Latin1_General_100_CS_AS_KS_WS = N.[t_emno]' + @ConditionalJoin
								
	
	  SET @SqlQuery6 =       ' WHERE P.[t_lsta] NOT IN 
								(
								   SELECT
								    [Value] 
								  FROM 
									[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS](''PCR Line Status'',''ltpcr.lsta'') 
								  WHERE 
									UPPER([LNConstant]) = ''DELETE''
                                 ) '  + @ConditionalWhere
	--SELECT CAST('<![CDATA[' + @SQlQuery1 + @SQlQuery2  + @SQlQuery3 + @SQlQuery4 + @SQlQuery5 + @SqlQuery6 + ']]>' AS XML)
	EXEC(@SQlQuery1 + @SQlQuery2 + @SQlQuery3 + @SQlQuery4 + @SQlQuery5 + @SqlQuery6)
END

