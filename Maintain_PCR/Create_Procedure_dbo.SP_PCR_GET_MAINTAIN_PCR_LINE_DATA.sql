USE [IEMQS]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC [dbo].[SP_PCR_GET_MAINTAIN_PCR_LINE_DATA]  'HPC000001', 0, '1'
ALTER PROCEDURE  [dbo].[SP_PCR_GET_MAINTAIN_PCR_LINE_DATA]
    @PCRNumber NVARCHAR(20),
	@State INT,
	@PSNo NVARCHAR(20)
AS
BEGIN 
  BEGIN TRY
    SET NOCOUNT ON
    DECLARE @Tbl AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(20))
	DECLARE @LatestPCRNumber NVARCHAR(20)
	DECLARE @CurrentIndex INT
	DECLARE @Total INT	
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @SQlQuery1 NVARCHAR(MAX) 
	DECLARE @SQlQuery2 NVARCHAR(MAX) 
	DECLARE @SQlQuery3 NVARCHAR(MAX) 
	DECLARE @SQlQuery4 NVARCHAR(MAX) 
	DECLARE @SQlQuery5 NVARCHAR(MAX)

	INSERT INTO @Tbl([Value])
	SELECT P.[t_pcrn] FROM [dbo].[PCR501] AS P ORDER BY P.[t_pcrn]	
	SELECT @Total = COUNT(0) FROM @Tbl
	
	IF(@State = 0)
	 BEGIN
	  SET @LatestPCRNumber = @PCRNumber
	 END
	 ELSE IF(@State = -2) --First
	  BEGIN
	    SELECT @LatestPCRNumber = T.[Value] FROM @Tbl AS T WHERE T.RowIndex = 1		
	  END
	 ELSE IF(@State = 2) --Last
	  BEGIN
	    SELECT @LatestPCRNumber = T.[Value] FROM @Tbl AS T WHERE T.RowIndex = @Total
	  END
	 ELSE IF(@State = -1 OR @State = 1) --Prev (-1) or Next (1)
	  BEGIN
	    SELECT @CurrentIndex = T.RowIndex FROM @Tbl AS T WHERE T.[Value] = @PCRNumber
		IF(@State = -1)
		 BEGIN
			SELECT @LatestPCRNumber = T.[Value] FROM @Tbl AS T WHERE T.RowIndex = @CurrentIndex - 1
		 END
		 ELSE
		  BEGIN
		    SELECT @LatestPCRNumber = T.[Value] FROM @Tbl AS T WHERE T.RowIndex = @CurrentIndex + 1
		  END	    
	  END
	  IF(@LatestPCRNumber IS NULL)
	  BEGIN
	    RAISERROR('Invalid PCRNumber', 11, 1)
	  END
	DECLARE @Collation NVARCHAR(100) = ''
	SET @Collation = ' COLLATE ' + 'Latin1_General_100_CS_AS_KS_WS';
	
	SET @SQlQuery1 = 
	 '
	 DECLARE @tblAllProjects TABLE
	 (
		Id NVARCHAR(100) , 
		[Description] NVARCHAR(MAX) , 
		[CodeDescription] NVARCHAR(MAX)
	 ) 
	 
	 INSERT INTO @tblAllProjects([Id], [Description], [CodeDescription])
	 EXEC [dbo].[SP_PCR_Get_ProjectList] NULL	
	 
	 DECLARE @tblProjects TABLE
	 (
		Id NVARCHAR(100) , 
		[Description] NVARCHAR(MAX) , 
		[CodeDescription] NVARCHAR(MAX)
	 )
	 DECLARE @tblElements TABLE
	 (
		Id NVARCHAR(100) , 
		[Description] NVARCHAR(MAX)
	 )	
	 DECLARE @tblLocationAddress AS TABLE 
	 (
		PCRN NVARCHAR(100), 
		LocationAddress NVARCHAR(100)
	 )
	 INSERT INTO @tblProjects
	 (
		[Id], 
		[Description], 
		[CodeDescription]
	  )
	   SELECT * FROM @tblAllProjects 
		WHERE [Id] COLLATE Latin1_General_100_CS_AS_KS_WS IN 
			(
				SELECT DISTINCT Proj.[t_cprj] FROM [dbo].[PCR503] AS Proj WITH (NOLOCK) WHERE Proj.[t_pcrn] = ''' + @LatestPCRNumber + '''
			)
	 
	 INSERT INTO @tblElements
	 (
		[Id], 
		[Description]
	  )
		SELECT [t_cspa], [t_dsca] 
		FROM ' + @LNLinkedServer  + '.dbo.ttppdm600' + @LNCompanyId + ' AS tbl 	  
		WHERE tbl.[t_cprj] COLLATE Latin1_General_100_CS_AS_KS_WS IN 
			(
				SELECT [Id] FROM @tblProjects
			)
	 
	 INSERT INTO @tblLocationAddress
	 (
		PCRN, 
		LocationAddress
	 )
	   SELECT  A.[t_pcrn] , C.[t_ladr] 
		FROM ' + @lnlinkedserver + '.dbo.tltlnt000'+ @lncompanyid  + ' AS C 
		INNER JOIN PCR501 AS A ON  A.[t_loca] = C.[t_dimx]
	 
	 DECLARE @tblManufacturingItem TABLE
	 (
		 [t_cprj] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_pitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_mitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_dsca] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS,
		 [t_fnno] INT
	 )	
	 
	 DECLARE @tblChildItem TABLE
	 (
		 [t_cprj] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_sern] INT, 
		 [t_item] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_dsca] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_desc] NVARCHAR(1000) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_dscb] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		 [t_dscd] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS
	 )	
	 
	 DECLARE @tblWCLocation TABLE
	 (
			[t_cwoc] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
			[WCDeliverName] NVARCHAR(MAX)
	 )
	 DECLARE @SegmentLength  INT = 9
	 
	 INSERT INTO @tblManufacturingItem
	 (
			[t_cprj], 
			[t_cspa], 
			[t_pitm], 
			[t_dsca], 
			[t_fnno]
	  )
	    SELECT 
			DISTINCT 
				B.[t_cprj], 
				B.[t_cspa], 
				A.[t_item], 
				A.[t_dsca], 
				tbl911.[t_fnno]
			FROM ' + @LNLinkedServer  + '.dbo.tltibd911' + @LNCompanyId + ' AS tbl911 
			INNER JOIN ' + @LNLinkedServer  + '.dbo.ttpptc120' + @LNCompanyId + ' AS B WITH (NOLOCK)
				ON B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern]
			INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
				ON A.[t_item]= tbl911.[t_pitm] 
		    INNER JOIN [dbo].[pcr503] AS P ON P.[t_prtn] = tbl911.[t_sern]
	        WHERE B.[t_cprj]' + @Collation +' IN 
			(
				SELECT Id FROM @tblProjects
			) 
	
	    INSERT INTO @tblChildItem
		(
		  [t_cprj], 
		  [t_cspa], 
		  [t_sern], 
		  [t_item], 
		  [t_dsca], 
		  [t_desc], 
		  [t_dscd], 
		  [t_dscb]
		)
	     SELECT 
		  DISTINCT 
		    B.[t_cprj], 
			B.[t_cspa],
			B.[t_sern], 
			A.[t_item], 
			A.[t_dsca], 
			tbl901.[t_desc], 
			A.[t_dscd], A.[t_dscb]
		 FROM ' + @LNLinkedServer   + '.dbo.tltibd911' + @LnCompanyId + ' AS tbl911 
		 INNER JOIN ' +	@LNLinkedServer + '.dbo.ttpptc120' + @LNCompanyId  + ' AS B WITH (NOLOCK) 
		  ON B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern] 
		 INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
		  ON A.[t_item]= tbl911.[t_citm] 
		INNER JOIN  ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ' AS tbl901 WITH (NOLOCK) 
		  ON tbl901.[t_mtrl] = A.[t_dscb]
		INNER JOIN ' + @LNLinkedServer + '.dbo.tltibd903' + @LNCompanyId + ' AS tbl903 WITH (NOLOCK) 
		  ON tbl903.[t_item] = tbl911.[t_citm]
		WHERE B.[t_cprj]' + @Collation +' IN 
		 (
		   SELECT Id FROM @tblProjects
		 )
	
	   INSERT INTO @tblWCLocation
		SELECT 
		 LTRIM(RTRIM([t_cwoc])) AS [t_cwoc], 
		 LTRIM(RTRIM([t_cwoc])) + ''-'' + [t_dsca] 
		 FROM ' + @LNLinkedServer + '.dbo.ttirou001' + @LNCompanyId + ' WITH (NOLOCK)'
		
	SET @SqlQuery2 = 
					   '
					     SELECT 
					            DISTINCT
		                        P.[Id],
								P.[t_pcrn] AS PCRNumber,
								P.[t_pono] AS PCRLineNo,
								P.[t_revn] AS PCRLineRevision,
								P.[t_pcrt] AS PCRType,
								P.[t_pcr_crdt] AS PCRCreationDate,
								P.[t_pcr_rldt] AS PCRReleaseDate,
								P.[t_pcl_crdt] AS PCLCreationDate,
								P.[t_pcl_rldt] AS PCLReleaseDate,
								P.[t_cprj] AS Project,																
                                Projects.Description AS ProjectDesc,
								P.[t_cspa] AS Element,
                                Elements.Description AS ElementDesc,
								P.[t_hcno] AS PCRHardCopyNo,
								P.[t_sqty] AS ScrapQuantity,
								P.[t_nqty] AS NestedQuantity,
								P.[t_arcs] AS AreaConsumed,
								P.[t_leng] AS [Length],
								P.[t_widt] AS [Width],
								P.[t_npcs] AS NoOfPieces,
								P.[t_area] AS AreaOfPieces,'
								
            SET @SQLQuery3 =
							 '	((P.[t_leng] * P.[t_widt]) / 1000000) AS TotalArea,
								P.[t_cwoc] AS WCDeliver,
								P.[t_stkn] AS StockNumber,
								P.[t_mvtp] AS MovementType,
								P.[t_prmk] AS PCRPlannerRemark,
								P.[t_prtn] AS PartNumber,
								ManufacturingItemTable.[t_fnno] AS FindNo,
								P.[t_mitm] AS ManufacturingItem,
                                ManufacturingItemTable.[t_dsca] AS ManufacturingItemDesc,
								CASE WHEN LEN(ISNULL(P.[t_sitm],'''')) >  @SegmentLength THEN SUBSTRING(P.[t_sitm], 1, @SegmentLength) ELSE '''' END  AS [ChildItemSegment],
								CASE WHEN LEN(ISNULL(P.[t_sitm],'''')) >  @SegmentLength THEN SUBSTRING(P.[t_sitm], @SegmentLength, LEN(P.[t_sitm])) ELSE '''' END AS [ChildItem],
								ChildItemTable.[t_dsca] AS ChildItemDesc,
								ChildItemTable.[t_dscb] AS ItemMaterialGrade,								
								P.[t_prdt] AS PCRRequirementDate,								
								P.[t_cloc] AS CuttingLocation,
								P.[t_lsta] AS PCRLineStatus,
								P.[t_pcln] AS PCLNumber,
							    P.[t_pr01] AS P1,
							    P.[t_pr02] AS P2,
							    P.[t_pr03] AS P3,
							    P.[t_pr04] AS P4,
							    P.[t_pr05] AS P5,
							    P.[t_pr06] AS P6,
							    P.[t_pr07] AS P7,
							    P.[t_pr08] AS P8,
							    P.[t_pr09] AS P9,
							    P.[t_pr10] AS P10'

			SET @SQlQuery4 =    			                    
							  ' FROM [dbo].[PCR503] AS P 
								INNER JOIN @tblProjects AS Projects ON Projects.[Id] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_cprj]								
								INNER JOIN [dbo].[PCR506] AS P2 ON P2.[t_cprj] = P.[t_cprj] AND 
								P2.[t_cspa] = P.[t_cspa] AND P2.[t_pcrn] = P.[t_pcrn] AND P2.[t_pono] = P.[t_pono]
								INNER JOIN @tblLocationAddress AS LA ON LA.[PCRN] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcrn] 
								LEFT JOIN @tblManufacturingItem AS ManufacturingItemTable ON ManufacturingItemTable.[t_cprj] = P.[t_cprj] 
								AND ManufacturingItemTable.[t_cspa] = P.[t_cspa]';
						
			SET @SQlQuery5 = '  AND ManufacturingItemTable.[t_pitm] = P.[t_mitm]
								LEFT JOIN @tblChildItem AS ChildItemTable ON ChildItemTable.[t_cspa] = P.[t_cspa] AND ChildItemTable.[t_cprj] = P.[t_cprj]
								AND ChildItemTable.[t_item] = P.[t_sitm] 
								LEFT JOIN @tblWCLocation AS WCLOCA ON P.[t_cwoc] = WCLOCA.[t_cwoc] 
								LEFT JOIN @tblElements AS Elements ON Elements.[Id] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_cspa]
								WHERE P.[t_pcrn] = ''' + @LatestPCRNumber + ''''

	  --SELECT CAST('<root><![CDATA[' + @SQlQuery1 + @SQlQuery2  + @SQlQuery3 + @SQlQuery4 + @SQlQuery5  + ']]></root>' AS XML)
	  EXEC(@SQlQuery1 + @SQlQuery2 + @SQlQuery3 + @SQlQuery4 + @SQlQuery5)
	END TRY
	BEGIN CATCH
	   DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
	     SELECT  
			@ErrorMessage = ERROR_MESSAGE() + 'Line ' + CAST(ERROR_LINE() as nvarchar(5)),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	END CATCH
END

