USE [IEMQS]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--EXEC [dbo].[SP_PCR_GET_MAINTAIN_PCR_HEADER_DATA]  'HPC000001', 2, '2'
ALTER PROCEDURE  [dbo].[SP_PCR_GET_MAINTAIN_PCR_HEADER_DATA]
    @PCRNumber NVARCHAR(20),
	@State INT,
	@PSNo NVARCHAR(20)
AS
BEGIN 
   
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @Sql NVARCHAR(MAX)
	DECLARE @Tbl AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(20))
	DECLARE @LatestPCRNumber NVARCHAR(20)
	DECLARE @CurrentIndex INT
	DECLARE @Total INT
	DECLARE @StateFirst BIT = 1
	DECLARE @StateLast BIT = 1
	DECLARE @StatePrev BIT = 1
	DECLARE @StateNext BIT = 1
	
	INSERT INTO @Tbl([Value])
	SELECT P.[t_pcrn] FROM [dbo].[PCR501] AS P ORDER BY P.[t_pcrn]
	
	SELECT @Total = COUNT(0) FROM @Tbl
	
	IF(@State = 0)
	 BEGIN
		 IF(@PCRNumber IS NULL OR UPPER(@PCRNumber) = 'NULL' OR LEN(@PCRNumber) = 0)
		  BEGIN
		   SELECT @PCRNumber = T.[Value] FROM @Tbl AS T WHERE T.RowIndex = 1		
		  END	 
	  SET @LatestPCRNumber = @PCRNumber
	 END
	 ELSE IF(@State = -2) --First
	  BEGIN
	    SELECT @LatestPCRNumber = T.[Value] FROM @Tbl AS T WHERE T.RowIndex = 1		
	  END
	 ELSE IF(@State = 2) --Last
	  BEGIN
	    SELECT @LatestPCRNumber = T.[Value] FROM @Tbl AS T WHERE T.RowIndex = @Total
	  END
	 ELSE IF(@State = -1 OR @State = 1) --Prev (-1) or Next (1)
	  BEGIN
	    SELECT @CurrentIndex = T.RowIndex FROM @Tbl AS T WHERE T.[Value] = @PCRNumber
		IF(@State = -1)
		 BEGIN
			SELECT @LatestPCRNumber = T.[Value] FROM @Tbl AS T WHERE T.RowIndex = @CurrentIndex - 1
		 END
		 ELSE
		  BEGIN
		    SELECT @LatestPCRNumber = T.[Value] FROM @Tbl AS T WHERE T.RowIndex = @CurrentIndex + 1
		  END	    
	  END
	   
	   SELECT @CurrentIndex = T.RowIndex FROM @Tbl AS T WHERE T.[Value] = @PCRNumber
	   
	   IF(@CurrentIndex > 1 AND @CurrentIndex < @Total)
	   BEGIN
	     SET @StateFirst = 1
		 SET @StateLast = 1
		 SET @StatePrev = 1
		 SET @StateNext = 1
	   END
	   ELSE 
	     BEGIN
		   IF(@Total = 1 AND @CurrentIndex = 1)
		    BEGIN
			  SET @StateFirst = 0
			  SET @StateLast = 0
			  SET @StatePrev = 0
			  SET @StateNext = 0
		    END
			ELSE IF(@Total > 1 AND @CurrentIndex = 1)
			 BEGIN
			  SET @StateFirst = 0
			  SET @StateLast = 1
			  SET @StatePrev = 0
			  SET @StateNext = 1
			 END
			ELSE IF(@Total = @CurrentIndex)
			 BEGIN
			  SET @StateFirst = 1
			  SET @StateLast = 0
			  SET @StatePrev = 1
			  SET @StateNext = 0
			 END
		 END

	    SELECT [Value] AS PCRNumber FROM @Tbl
		SELECT @StateFirst AS FirstEnabled, @StateLast AS LastEnabled, @StatePrev AS PrevEnabled, @StateNext AS NextEnabled
	    SET @Sql = 
			'SELECT  
				 A.[t_loca] + ''-'' + C.[t_desc] AS [Location] , 
				 A.[t_ityp] AS ItemType, 
				 ItemTypes.[Text] AS ItemTypeName, 
				 A.[t_cdat] AS CreationDate, 
				 A.[t_rdat] AS ReleaseDate, 
				 A.[t_init] AS Initiator,
				 N.[t_nama] AS InitiatorName
			  FROM ' + @LNLinkedServer + '.dbo.ttfgld010'+ @LNCompanyId  + ' AS C 
			   INNER JOIN [dbo].[PCR501] AS A 
				 ON  A.[t_loca] = C.[t_dimx] 
			   INNER JOIN ' + @LNLinkedServer + '.dbo.ttccom001' + @LNCompanyId + ' AS N ON N.[t_emno] = A.[t_init]
			   LEFT JOIN (Select [Value], [Text]  FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS(''Item Type'',''ltsfc.ityp'')) AS ItemTypes
				 ON ItemTypes.[Value] = A.[t_ityp] 
			   WHERE A.[t_pcrn] = ''' + @LatestPCRNumber + ''''
			
		EXEC(@Sql)
	
	
END

