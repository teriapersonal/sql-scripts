USE [IEMQS]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [dbo].[SP_PCR_Get_ReleasePCL_Data] '122963'
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_ReleasePCL_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_ReleasePCL_Data]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Get_ReleasePCL_Data]
    @PSNo NVARCHAR(100)
AS
BEGIN 
	
    DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @YesValue INT
	DECLARE @NoValue INT  
	DECLARE @Collation NVARCHAR(100) = ''
	SET @Collation = ' COLLATE Latin1_General_100_CS_AS_KS_WS';
	
	DECLARE @tblSTGI AS TABLE
	(
	  [t_loca] NVARCHAR(100),
	  [t_pcln] NVARCHAR(100),
	  [t_stgi] INT
	)
	DECLARE @tblName AS TABLE
	(
	 [t_emno] NVARCHAR(100),
	 [t_nama] NVARCHAR(200)
	)

	SELECT 
		@YesValue = [Value] FROM  [dbo].FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice', 'tppdm.yeno') WHERE UPPER([LNConstant]) = 'YES'
    
	SELECT 
		@NoValue = [Value] FROM  [dbo].FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice', 'tppdm.yeno') WHERE UPPER([LNConstant]) = 'NO'
	

	SET @SqlQuery =
	' SELECT 
	  P0.[t_loca],
	  P.[t_pcln],
	  A.[t_stgi]
	  FROM ' + @LNLinkedServer + '.dbo.tltcom000'+ @LNCompanyId + ' AS A
	  INNER JOIN [dbo].[PCR500] AS P0 ON P0.[t_loca] = A.[t_dimx]
	  INNER JOIN [dbo].[PCR504] AS P ON P.[t_pcln] = P0.[t_pcln]
	  '
    INSERT INTO @tblSTGI([t_loca], [t_pcln], [t_stgi])
	EXEC(@SqlQuery)

	SET @SqlQuery = 
	' SELECT  
		A.[t_emno] , 
		A.[t_nama]
		FROM ' + @LNLinkedServer + '.dbo.ttccom001'+ @LNCompanyId  + ' AS A
	    INNER JOIN [dbo].[PCR504] AS C 
	      ON C.[t_emno] = A.[t_emno] '
    INSERT INTO @tblName([t_emno], [t_nama])
	EXEC(@SqlQuery)

	DECLARE @tblLocationAddress AS TABLE 
	(
		[t_pcrn] NVARCHAR(100), 
		[t_loca] NVARCHAR(100),
		[t_ladr] NVARCHAR(100)
	)
	SET @SqlQuery = 
	' SELECT  
		A.[t_pcrn] , 
		A.[t_loca],
		C.[t_ladr] 
	    FROM ' + @lnlinkedserver + '.dbo.tltlnt000'+ @lncompanyid  + ' AS C 
	    INNER JOIN PCR501 AS A 
	      ON A.[t_loca] = C.[t_dimx]'

    INSERT INTO @tblLocationAddress
	(
		[t_pcrn],
		[t_loca],
		[t_ladr]
	)
	EXEC(@SqlQuery)
	
	DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')

	DECLARE @tblItemType AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblItemType([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp')
		
	DECLARE @tblWCLocation TABLE
	(
		[t_cadr] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_cwoc] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[WCDeliverName] NVARCHAR(MAX)
	)
	INSERT INTO @tblWCLocation([t_cadr], [t_cwoc], [t_dsca], [WCDeliverName])
	EXEC [dbo].[SP_PCR_Get_WorkCenter_With_Location] 0
	
    SELECT
	    DISTINCT
		P.[Id] AS Id,
		P.[t_rbal] AS ReturnBalance,
		P.[t_pcln] AS PCLNumber,
		P.[t_revn] AS Revision,
		P.[t_area] AS TotalConsumedArea,
		P.[t_sqty] AS ScrapQuantity,
		P.[t_rtar] AS ReturnArea,
		PS.[t_txt] AS PCLStatus,  
		P.[t_rrmk] AS ReturnRemark,
		P.[t_tclt] AS TotalCuttingLength,
		P0.[t_stkn] AS StockNumber,
		P0.[t_area] AS TotalArea,
		WC.WCDeliverName AS WCDeliverName,
		CONCAT(N.[t_emno], '-' + N.[t_nama]) AS Planner,							
		--N.[t_nama] AS Planner,
		P.[t_mrmk] AS Remark,
		P.[t_mvtp] AS MovementType,
		P.[t_ityp] AS ItemType,
		IT.[t_txt] AS ItemTypeName,
		P.[t_pcl_rldt] AS PCLReleaseDate,
		P.[t_stgi] AS AllowedStage1,
		CASE WHEN S.[t_stgi] = @NoValue THEN 0 ELSE 1 END AS DisableSTGI,
		--0 AS DisableReturnBalance
		CASE WHEN P5.[t_pcln] IS NOT NULL AND LEN(P5.[t_pcln]) > 0 AND P5.[t_revn] IS NOT NULL THEN 1 ELSE 0 END AS DisableReturnBalance
		FROM [dbo].[PCR504] AS P
		INNER JOIN [dbo].[PCR500] AS P0 
		  ON P.[t_pcln] = P0.[t_pcln]
		INNER JOIN [dbo].[PCR503] AS P3 
		  ON P.[t_pcln] = P3.[t_pcln]
		INNER JOIN [dbo].[PCR501] AS P1
		  ON P3.[t_pcrn] = P1.[t_pcrn] 
		LEFT OUTER JOIN [dbo].[PCR505] AS P5
		  ON P.[t_pcln] = P5.[t_pcln] AND P.[t_revn] = P5.[t_revn]
		LEFT OUTER JOIN @tblName AS N
		  ON P.[t_emno] COLLATE Latin1_General_100_CS_AS_KS_WS = N.[t_emno]
		LEFT OUTER JOIN @tblSTGI AS S
		  ON S.[t_pcln] COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcln]
		LEFT OUTER JOIN @tblPCLStatus AS PS 
		  ON P.[t_stat] = PS.[t_val]
		LEFT OUTER JOIN @tblItemType AS IT 
		  ON P.[t_ityp] = IT.[t_val]		
		LEFT OUTER JOIN @tblLocationAddress AS LA
		  ON P3.[t_pcrn] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_pcrn] AND P1.[t_loca] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_loca]
		LEFT OUTER JOIN @tblWCLocation AS WC
		  ON P.[t_cwoc]  COLLATE Latin1_General_100_CS_AS_KS_WS = WC.[t_cwoc]
		AND WC.[t_cadr] COLLATE Latin1_General_100_CS_AS_KS_WS = LA.[t_ladr]
		--WHERE 1 = 1
		 --UPPER(PS.[t_cnst]) IN ('CONF.PMG','PCL.REL','APPR.QA','CONF.SFC','REJ.QC') OR P.[t_stat] IS NULL

		 
	
END

