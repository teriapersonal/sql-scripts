USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Dec 11th 2020
-- Description:	
-- =============================================

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_ReleasePCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_ReleasePCL]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

--exec [dbo].[SP_PCR_ReleasePCL] 1056, '122963', 0
Alter Procedure [dbo].[SP_PCR_ReleasePCL]
(
   @Id INT ,
   @PSNo NVARCHAR(100),
   @UpdateScrap BIT = 0   
)
AS
 BEGIN
   DECLARE @Project NVARCHAR(100)
   DECLARE @Element NVARCHAR(100)
   DECLARE @PCRN NVARCHAR(100)
   DECLARE @Pono INT
   DECLARE @PCLN NVARCHAR(100)
   DECLARE @Npcs INT
   DECLARE @Nqty INT
   DECLARE @Sqty INT
   DECLARE @Revn INT
   DECLARE @NewRevn INT
   DECLARE @SSPA NVARCHAR(100)
   DECLARE @ItemType INT
   DECLARE @MaxRDate DATETIME
   DECLARE @Tbl AS TABLE(PMGC INT, STGI INT, SSPA_1 NVARCHAR(100), SSPA_2 NVARCHAR(100), SSPA_3 NVARCHAR(100), SSPA_4 NVARCHAR(100))
   DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
   DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
   DECLARE @RowIndex INT
   DECLARE @Total INT
   DECLARE @IdsTable AS Table(RowIndex INT IDENTITY(1,1), Id INT)
   DECLARE @Query NVARCHAR(MAX)
   DECLARE @YesValue INT = 1
   DECLARE @NoValue INT = 2
   DECLARE @ParentId INT
   DECLARE @PMGC INT
   DECLARE @STGI INT
   DECLARE @Stat INT
   DECLARE @STGI504 INT
   DECLARE @Loc NVARCHAR(100)
   DECLARE @StatusDeleted INT
   DECLARE @StatusPCLReleased INT
   DECLARE @StatusPCRReleasedToMP INT
   DECLARE @ItemTypePipeValue INT
   DECLARE @ItemTypePlateValue INT

   BEGIN TRAN
     BEGIN TRY
	   EXEC [dbo].[SP_PCR_Validate_ReleasePCL] @Id
	   --print 'ok'
	  SELECT
	  TOP 1
	   @PCLN = P.[t_pcln] ,
	   @Revn = P.[t_revn],
	   @Sqty = P.[t_sqty],
	   @STGI504 = P.[t_stgi]
	   FROM [dbo].[PCR504] AS P WHERE P.[Id] = @Id
	 IF(@UpdateScrap = 1)
	  BEGIN
	    UPDATE [dbo].[PCR504] SET [t_sqty] = [t_rtar], [t_rtar] = 0 WHERE [Id] = @Id
	  END
	  SELECT
	    TOP 1 @StatusPCRReleasedToMP = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'
	   SELECT
	    TOP 1 @StatusDeleted = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'DELETE'
	  SELECT
	    TOP 1 @StatusPCLReleased = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'PCL.REL'
	  SELECT
	    TOP 1 @ItemTypePipeValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PIPE' 
	  SELECT
	    TOP 1 @ItemTypePlateValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PLATE'
		
		UPDATE
			tbl503   
				SET 
				tbl503.[t_pcl_rldt] = GETUTCDATE(),
				tbl503.[t_mvtp] = tbl504.[t_mvtp],
				tbl503.[t_cwoc] = tbl504.[t_cwoc],
				tbl503.[t_lsta] = @StatusPCLReleased,
				tbl503.[t_sqty] = (tbl503.[t_arcs] * tbl504.[t_sqty]) / tbl504.[t_area]
		FROM 
			[dbo].[PCR503] AS tbl503	
			JOIN [dbo].[PCR504] AS tbl504	 
		ON tbl504.[t_pcln] = tbl503.[t_pcln]
		WHERE 
			tbl503.[t_lsta] <> @StatusDeleted AND tbl504.[Id] = @Id

	  IF EXISTS(SELECT 0 FROM [dbo].[PCR503] AS P0 WHERE P0.[t_pcln] = @PCLN AND P0.[t_lsta] <> @StatusDeleted AND ISNULL(P0.[t_nqty], 0) <> P0.[t_npcs])
	    BEGIN
		 INSERT INTO 
			@IdsTable
		 SELECT 
			P0.[Id]
		 FROM 
			[dbo].[PCR503] AS P0 
		WHERE 
			P0.[t_pcln] = @PCLN AND 
			P0.[t_lsta] <> @StatusDeleted AND 
			ISNULL(P0.[t_nqty], 0) <> P0.[t_npcs]
		 
		 SET @RowIndex = 1
		 SELECT @Total = COUNT(0) FROM @IdsTable
		 WHILE(@RowIndex <= @Total)
		  BEGIN
		    SELECT 
			 @ParentId = T.[Id] 
			FROM 
			 @IdsTable AS T 
			WHERE 
			 T.[RowIndex] = @RowIndex
			
		    SELECT TOP 1
				 @PCRN = P.[t_pcrn], 
				 @Pono = P.[t_pono], 
				 @Npcs = P.[t_npcs], 
				 @Nqty = ISNULL(P.[t_nqty], 0)		 
			 FROM 
				[dbo].[PCR503] AS P
			 WHERE 
				P.[Id] = @ParentId
		  
		 
			 SELECT TOP 1 
				@NewRevn = ISNULL(P0.[t_revn], 0)
			 FROM 
				[dbo].[PCR503] AS P0 
			WHERE 
				P0.[t_pcrn] = @PCRN AND 
				P0.[t_pono] = @Pono
			 ORDER BY P0.[t_revn] DESC
									
			SET @NewRevn += 1
		  
			 INSERT INTO [dbo].[PCR503]
			 (
				[t_pcrn], [t_pono], [t_revn], [t_leng], [t_widt], [t_npcs], [t_pcrt], 
				[t_prtn], [t_mitm], [t_sitm], [t_revi], [t_cprj], [t_cspa], [t_nqty], [t_sqty], [t_arcs],
				[t_thic], [t_pcr_crdt], [t_prdt], [t_cwoc], [t_prmk], [t_pcr_rldt], [t_lsta], [t_area], [t_ityp], [t_init]
			)
			 SELECT 
				@PCRN, @Pono, @NewRevn, P.[t_leng], P.[t_widt], @Npcs - @Nqty, P.[t_pcrt],
				P.[t_prtn], P.[t_mitm], P.[t_sitm], P.[t_revi], P.[t_cprj], P.[t_cspa], 0, 0, 0,
				P.[t_thic], P.[t_pcr_crdt], P.[t_prdt], P.[t_cwoc], P.[t_prmk], GETUTCDATE(), P.[t_lsta], P.[t_area], @ItemTypePlateValue, @PSNo
			 FROM 
				[dbo].[PCR503] AS P 
			 WHERE 
				P.[Id] = @ParentId

          SET @RowIndex += 1
	  END          
	END
	    
	    UPDATE [dbo].[PCR500] SET [t_qfre] = [t_qfre] - @Sqty WHERE [t_pcln] = @PCLN
	    
		IF EXISTS(SELECT 0 FROM [dbo].[PCR504] AS P WHERE P.[t_pcln] = @PCLN AND P.[t_revn] = @Revn)
		 BEGIN
		  IF EXISTS(SELECT 0 FROM [dbo].[PCR500] AS P0 WHERE P0.[t_pcln] = @PCLN)
		   BEGIN
		       SELECT
				 TOP 1
				   @Loc = P0.[t_loca] ,
				   @ItemType = P0.[t_ityp],
				   @SSPA = P0.[t_sspa]
				   FROM [dbo].[PCR500] AS P0 WHERE P0.[t_pcln] = @PCLN
			   SET @Query = 'SELECT TOP 1 [t_pmgc], [t_stgi], [t_sspa_1], [t_sspa_2], [t_sspa_3], [t_sspa_4] FROM ' + @LNLinkedServer + '.dbo.'+ 'tltcom000' + @LNCompanyId + ' WHERE [t_dimx] = ''' + @Loc + ''''
			   INSERT INTO @Tbl(PMGC, STGI, SSPA_1, SSPA_2, SSPA_3, SSPA_4)
			   EXEC(@Query)
		   END
		  ELSE
		   BEGIN
		     SET @PMGC = @YesValue
			 SET @STGI = @NoValue
			 SET @SSPA = ''
		   END
		  IF(@SSPA IS NOT NULL AND LEN(@SSPA) >0 AND EXISTS(SELECT 0 FROM @TBL AS T WHERE T.[SSPA_1] = @SSPA OR T.[SSPA_2] = @SSPA OR T.[SSPA_3] = @SSPA OR T.[SSPA_4] = @SSPA))
		    BEGIN
			    SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'APPR.QA'
			END
		   ELSE
			 BEGIN
			  IF(@PMGC = @NoValue AND @STGI = @YesValue)
			    BEGIN
			      SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'APPR.QA'
			    END
			  ELSE
			    BEGIN
				  IF(@PMGC = @NoValue AND @STGI = @NoValue)
					   BEGIN
						  IF(@STGI504 = @NoValue)
						   BEGIN
							 SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'APPR.QA'
						   END
						   ELSE
							 BEGIN
							   SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'CONF.PMG'
							 END
					   END
					   ELSE
							BEGIN
							   SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'PCL.REL'
							END
                  
				  IF(@ItemType = @ItemTypePipeValue)
				   BEGIN
				      SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'PCL.REL'
				   END    
				END
			 END
		    
			SELECT
			TOP 1
			  @MaxRDate = P0.[t_prdt]
			FROM [dbo].[PCR503] AS P0 WHERE P0.[t_pcln] = @PCLN ORDER BY [t_prdt] DESC
			
			SELECT @Stat = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat') WHERE UPPER([LNConstant]) = 'CONF.PMG'
			UPDATE [dbo].[PCR504]
			 SET 
			  [t_aarc] = [t_area], 
			  [t_asqt] = [t_sqty], 
			  [t_pcl_rldt] = GETUTCDATE(), 
			  [t_stat] = @Stat,
			  [t_ctdt] = @MaxRDate
			WHERE 
			  [t_pcln] = @PCLN 
			   AND 
			  [t_revn] = @Revn 
		 
		 END

	   COMMIT
	   SELECT 1
    END TRY
	BEGIN CATCH
	  IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	END CATCH
END 

