USE [IEMQS]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_ModifyPCL]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_ModifyPCL]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_ModifyPCL]
    @Id INT,
	@RemarkReason NVARCHAR(100),
    @PCLModifiedRemark NVARCHAR(100),
	@PSNo NVARCHAR(20)
AS
 BEGIN
 BEGIN TRAN
  BEGIN TRY
   DECLARE @PCLN NVARCHAR(100)
   DECLARE @Revision INT
   DECLARE @CTable AS TABLE(
    RowIndex INT IDENTITY(1,1),
    PCRN NVARCHAR(100),
    PCRN_Line INT,
    PCRN_Revision INT,
    StockNumber NVARCHAR(100),
    StockRevision INT,
    PCLN NVARCHAR(100),
    Item NVARCHAR(100),
    Arcs FLOAT,
    Nqty FLOAT
	)
   DECLARE @L_Table AS TABLE(
   [PCR_Revision] INT, 
   [PCLN] NVARCHAR(100), 
   [StockNumber] NVARCHAR(100), 
   [StockRevision] INT,
   [Nqty] FLOAT,
   [Npcs] INT
   )
   DECLARE @Query NVARCHAR(MAX)
   DECLARE @C_PCRN NVARCHAR(100)
   DECLARE @C_PCRN_Line INT
   DECLARE @C_PCRN_Revision INT
   DECLARE @C_StockNumber NVARCHAR(100)
   DECLARE @C_StockRevision INT
   DECLARE @C_PCLN NVARCHAR(100)
   DECLARE @C_Item NVARCHAR(100)
   DECLARE @C_Arcs FLOAT
   DECLARE @C_Nqty FLOAT
   DECLARE @L_PCR_Revision INT
   DECLARE @L_PCLN NVARCHAR(100)
   DECLARE @L_StockNumber NVARCHAR(100)
   DECLARE @L_StockRevision INT
   DECLARE @L_Nqty FLOAT
   DECLARE @L_Npcs INT
   DECLARE @RowIndex INT
   DECLARE @Total INT
   DECLARE @SQty FLOAT
   DECLARE @DeletedStatus INT 
   DECLARE @Mode INT = 0
   DECLARE @TotalNestedQuantity FLOAT
   DECLARE @N_Npcs INT
   DECLARE @PId INT
   IF(@RemarkReason IS NULL OR LEN(@RemarkReason) = 0)
    BEGIN
     RAISERROR('Remark Reason is mandatory for Modified PCL', 11, 1)
    END
	
   IF(@PCLModifiedRemark IS NULL OR LEN(@PCLModifiedRemark) = 0)
    BEGIN
     RAISERROR('PCL modified remark is mandatory for Modified PCL', 11, 1)
    END
	

	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE 
		UPPER([LNConstant]) = 'DELETE'
   
   SELECT 
    TOP 1
	  @PCLN = P.[t_pcln] , 
	  @Revision = P.[t_revn]
    FROM 
	 [dbo].[PCR504] AS P
    WHERE 
	 P.[Id] = @Id

	INSERT INTO 
	 @CTable
	 (
	  [PCRN], [PCRN_Line], [PCRN_Revision],
	  [StockNumber], [StockRevision], [Item],
	  [PCLN], [Arcs], [Nqty]
	 )
	SELECT 
	    P3.[t_pcrn], P3.[t_pono], P3.[t_revn],
		P3.[t_stkn], P3.[t_stkr], P3.[t_sitm],
		P3.[t_pcln], P3.[t_arcs], P3.[t_nqty]
	FROM
	    [dbo].[PCR503] AS P3
	WHERE
		P3.[t_pcln] = @PCLN
		 AND
        P3.[t_lsta] <> @DeletedStatus
	 
	SET @RowIndex = 1
	SELECT @Total = COUNT(0) FROM @CTable
	WHILE(@RowIndex <= @Total)
	  BEGIN
	   SELECT
	    @C_Arcs = C.[Arcs],
		@C_Item = C.[Item],
		@C_Nqty = C.[Nqty],
		@C_PCLN = C.[PCLN],
		@C_PCRN = C.[PCRN],
		@C_PCRN_Line = C.[PCRN_Line],
		@C_PCRN_Revision = C.[PCRN_Revision],
		@C_StockNumber = C.[StockNumber],
		@C_StockRevision = C.[StockRevision]
	   FROM
	    @CTable AS C 
       WHERE 
	    C.[RowIndex] = @RowIndex
		
		DELETE FROM @L_Table
		
		SET @Query = CONCAT('[dbo].[SP_PCR_Get_last_PCR_Sequence_Details] ','''' + @C_PCRN + '''',', ', @C_PCRN_Line)
		
		INSERT INTO @L_Table([PCR_Revision], [PCLN], [StockNumber], [StockRevision], [Nqty], [Npcs])
		EXEC(@Query)

		SELECT
		  TOP 1
			  @L_PCR_Revision = L.[PCR_Revision],
			  @L_PCLN = L.[PCLN],
			  @L_Nqty = L.[Nqty],
			  @L_Npcs = L.[Npcs],
			  @L_StockNumber = L.[StockNumber],
			  @L_StockRevision = L.[StockRevision]
		FROM 
		 @L_Table AS L
		 
		 IF(@L_PCLN IS NULL OR LEN(@L_PCLN) = 0)
		  BEGIN
		    SET @Mode = 3  
		  END
		 ELSE
		  BEGIN
		   IF(@L_StockNumber = @C_StockNumber)
		    BEGIN
			  SET @Mode = 1
		    END
		   ELSE
		    BEGIN
			  SET @Mode = 2
			END
		  END
		  IF(@Mode <> 0)
		   BEGIN
		     EXECUTE @TotalNestedQuantity = [dbo].[SP_PCR_Get_Total_NestedQuantity] @C_PCRN, @C_PCRN_Line , @C_StockNumber, @C_PCRN_Revision			 
		   END
		  IF(@Mode = 1)
		   BEGIN
			 SET @N_Npcs = @L_Npcs + @TotalNestedQuantity - @L_Nqty
			 EXEC [dbo].[SP_PCR_Update_PCRLineStatus] @C_PCRN, @C_PCRN_Line, @C_PCRN_Revision, @C_StockNumber 
			 EXEC [dbo].[SP_PCR_Update_StockStatus] @C_Item, @C_StockNumber, @C_StockRevision
			 EXEC [dbo].[SP_PCR_Generate_New_PCR_Revision] @C_PCRN , @C_PCRN_Line , 
			      @C_PCRN_Revision, @C_StockNumber, @C_StockRevision, @C_Nqty, @C_Arcs, @N_Npcs, @L_PCR_Revision, @PSNo
 
 		   END
		   ELSE IF(@Mode = 2)
		    BEGIN			
			  SET @N_Npcs = @TotalNestedQuantity
			  EXEC [dbo].[SP_PCR_Update_PCRLineStatus] @C_PCRN, @C_PCRN_Line, @C_PCRN_Revision, @C_StockNumber 
			  EXEC [dbo].[SP_PCR_Update_StockStatus] @C_Item, @C_StockNumber, @C_StockRevision
			  EXEC [dbo].[SP_PCR_Generate_New_PCR_Revision] @C_PCRN , @C_PCRN_Line , 
			       @C_PCRN_Revision, @C_StockNumber, @C_StockRevision, @C_Nqty, @C_Arcs, @N_Npcs, @L_PCR_Revision, @PSNo
		    END
			ELSE IF(@Mode = 3)
			 BEGIN			   
			   SET @N_Npcs = @L_Npcs + @TotalNestedQuantity			
			   EXEC [dbo].[SP_PCR_Update_PCRLineStatus] @C_PCRN, @C_PCRN_Line, @C_PCRN_Revision, @C_StockNumber 
			   EXEC [dbo].[SP_PCR_Update_StockStatus] @C_Item, @C_StockNumber, @C_StockRevision
			   EXEC [dbo].[SP_PCR_Generate_New_PCR_Revision] @C_PCRN , @C_PCRN_Line , 
			        @C_PCRN_Revision, @C_StockNumber, @C_StockRevision, @C_Nqty, @C_Arcs, @N_Npcs, @L_PCR_Revision, @PSNo			   
			    EXEC [dbo].[SP_PCR_Update_PCRLineStatus _For_Last_PCRRevision] @C_PCRN, @C_PCRN_Line, @L_PCR_Revision 
			 END
	   SET @RowIndex += 1
	 END
     
	 DELETE FROM [dbo].[PCR505] WHERE [t_pcln] = @C_PCLN
	 
	 SELECT 
	   TOP 1
	   @PId = P.[Id],
	   @SQty = P.[t_sqty]
	 FROM 
	  [dbo].[PCR504] AS P
	 WHERE 
	   P.[t_pcln] = @C_PCLN
     ORDER BY 
	   P.[t_revn] DESC

      UPDATE P
	  SET 
	    P.[t_resn] = @RemarkReason,
	    P.[t_pmrk] = @PCLModifiedRemark,
		P.[t_rtar] = P.[t_rtar] + @SQty,
		P.[t_sqty] = 0,
		P.[t_stat] = NULL,
		P.[t_pcl_rldt] = NULL,
		P.[t_pmg_cdat] = NULL,
		P.[t_qa_adat] = NULL
	  FROM 
	   [dbo].[PCR504] AS P	  
	  WHERE 
	   P.[Id] = @PId

	  UPDATE P0
	  SET
	    P0.[t_qfre] = P0.[t_qfre] + @SQty
	  FROM 
		[dbo].[PCR500] AS P0
	  JOIN 
		[dbo].[PCR504] AS P ON P.[t_pcln] = P0.[t_pcln]
	  WHERE 
	    P.[Id] = @PId
	 
	 COMMIT TRAN
	 SELECT 1

	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(), 
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END