USE [IEMQS]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_PlannerNames]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_PlannerNames]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
--exec [dbo].[SP_PCR_Get_PlannerNames] '122963'
ALTER PROCEDURE [dbo].[SP_PCR_Get_PlannerNames]
    @PSNo NVARCHAR(100)
AS
BEGIN 
	DECLARE @TableName NVARCHAR(MAX)='ttccom001'
	DECLARE @CodeColName NVARCHAR(MAX)='t_emno'
	DECLARE @DescColName NVARCHAR(MAX)='t_nama'

	DECLARE @SqlQuery NVARCHAR(MAX)=Null
	DECLARE @LNLinkedServer NVARCHAR(max) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(max) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	

	SET @SqlQuery='	Select TOP 1000 '+@CodeColName+' as Code, 
						   '+@DescColName+' as Description, 
						   '+@CodeColName+'+''-''+'+@DescColName+' as CodeDescription 
					FROM '+@LNLinkedServer+'.dbo.'+@TableName+@LNCompanyId

	
	PRINT (@SqlQuery)
	EXEC (@SqlQuery)
	
    
END

