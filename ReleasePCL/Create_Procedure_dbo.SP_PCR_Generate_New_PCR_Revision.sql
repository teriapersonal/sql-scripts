USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Generate_New_PCR_Revision]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Generate_New_PCR_Revision]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Generate_New_PCR_Revision]
 @PCRN NVARCHAR(100),
 @PONO INT,
 @PCR_Revision INT,
 @StockNumber NVARCHAR(100),
 @StockRevision INT,
 @Nqty INT,
 @Arcs FLOAT,
 @N_Npcs INT, 
 @Last_PCR_Revision INT,
 @PSNo NVARCHAR(100)
AS
BEGIN
 BEGIN TRAN
  BEGIN TRY 
    DECLARE @Status INT 
	SELECT
	    TOP 1 
		@Status = [Value] 	
	FROM
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE 
		UPPER([LNConstant]) = 'PCL.GEN'

    INSERT INTO [dbo].[PCR503]
           (
		    [t_pcrn], [t_pono], [t_revn], [t_prtn], [t_mitm]
           ,[t_sitm], [t_leng], [t_widt], [t_npcs], [t_pcrt]
           ,[t_prdt], [t_cwoc], [t_cwar], [t_cloc], [t_pcln]
           ,[t_lsta], [t_sqty], [t_nqty], [t_arcs], [t_stkn]
           ,[t_stkr], [t_mvtp], [t_prmk], [t_mrmk], [t_pcr_crdt]
           ,[t_pcr_rldt], [t_pcl_crdt], [t_pcl_rldt], [t_cprj], [t_cspa]
           ,[t_mpos], [t_thic], [t_gorn], [t_shtp], [t_shfl]
           ,[t_area], [t_pitm], [t_hcno], [t_alts], [t_armk]
           ,[t_oitm], [t_qapr], [t_iqty], [t_revi], [t_lscr]
           ,[t_scrn], [t_rqty], [t_mcrp], [t_rrmk], [t_pr01]
           ,[t_pr02], [t_pr03], [t_pr04], [t_pr05], [t_pr06]
           ,[t_pr07], [t_pr08], [t_pr09], [t_pr10], [t_ityp]
           ,[t_Refcntd], [t_Refcntu], [t_init]
		   )
     SELECT
	        P.[t_pcrn], P.[t_pono], @Last_PCR_Revision + 1, P.[t_prtn], P.[t_mitm]
           ,P.[t_sitm], P.[t_leng], P.[t_widt], @N_Npcs, P.[t_pcrt]
           ,P.[t_prdt], P.[t_cwoc], P.[t_cwar], P.[t_cloc], P.[t_pcln]
           ,@Status, 0, @Nqty, @Arcs, @StockNumber
           ,@StockRevision, P.[t_mvtp], P.[t_prmk], P.[t_mrmk], P.[t_pcr_crdt]
           ,P.[t_pcr_rldt], GETUTCDATE(), NULL, P.[t_cprj], P.[t_cspa]
           ,P.[t_mpos], P.[t_thic], P.[t_gorn], P.[t_shtp], P.[t_shfl]
           ,P.[t_area], P.[t_pitm], P.[t_hcno], P.[t_alts], P.[t_armk]
           ,P.[t_oitm], P.[t_qapr], P.[t_iqty], P.[t_revi], P.[t_lscr]
           ,P.[t_scrn], P.[t_rqty], P.[t_mcrp], P.[t_rrmk], P.[t_pr01]
           ,P.[t_pr02], P.[t_pr03], P.[t_pr04], P.[t_pr05], P.[t_pr06]
           ,P.[t_pr07], P.[t_pr08], P.[t_pr09], P.[t_pr10], P.[t_ityp]
           ,P.[t_Refcntd], P.[t_Refcntu], @PSNo

	 FROM
		[dbo].[PCR503] AS P
	 WHERE 
	  P.[t_pcrn] = @PCRN
	    AND
	  P.[t_pono] = @PONO
	    AND
      P.[t_revn] = @PCR_Revision

		
	COMMIT TRAN							 
 END TRY 
 BEGIN CATCH 
  IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(), 
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END
 END CATCH 
END
