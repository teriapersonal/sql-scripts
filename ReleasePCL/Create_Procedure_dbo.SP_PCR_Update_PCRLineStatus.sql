USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_PCRLineStatus]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_PCRLineStatus]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Update_PCRLineStatus]
 @PCRN NVARCHAR(100),
 @PONO INT,
 @PCR_Revision INT,
 @StockNumber NVARCHAR(100)
AS
BEGIN
 BEGIN TRAN
  BEGIN TRY 
    DECLARE @DeletedStatus INT 
	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE 
		UPPER([LNConstant]) = 'DELETE'

	UPDATE 
		[dbo].[PCR503]
	SET 
		[t_lsta] = @DeletedStatus
	WHERE 
		[t_pcrn] = @PCRN 
			AND 
		[t_pono] = @PONO 
	        AND
		[t_stkn]= @StockNumber 
	        AND 
		[t_revn] >= @PCR_Revision
	        AND 
		[t_lsta] <> @DeletedStatus	
	COMMIT TRAN							 
 END TRY 
 BEGIN CATCH 
  IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(), 
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END
 END CATCH 
END
