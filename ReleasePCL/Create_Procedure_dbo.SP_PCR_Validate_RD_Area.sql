USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --exec [dbo].[SP_PCR_Validate_Generate_PCL] 69
 IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_RD_Area]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_RD_Area]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_RD_Area]
   @PCLNumber NVARCHAR(100),
   @Revision INT,
   @TotalArea FLOAT
AS
BEGIN 
	SET NOCOUNT ON
	
	DECLARE @MSG NVARCHAR(1000)
	DECLARE @Index INT
	DECLARE @Total INT
	DECLARE @IORN NVARCHAR(100)
	DECLARE @Qoro FLOAT
	DECLARE @TBL AS TABLE(Qoro FLOAT)
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @StockArea FLOAT
	DECLARE @Factor FLOAT
	DECLARE @Thickness FLOAT
	DECLARE @CCD1 FLOAT
	DECLARE @CDT1 FLOAT
	DECLARE @CCD2 FLOAT
	DECLARE @CDT2 FLOAT
	DECLARE @QOFRArea FLOAT
	DECLARE @Query NVARCHAR(MAX)
	BEGIN TRY
	   SELECT TOP 1 @IORN = P.[t_iorn]
	   FROM [dbo].[PCR504] AS P WHERE P.[t_pcln] = @PCLNumber AND P.[t_revn] = @Revision
	   IF(@IORN IS NOT NULL AND LEN(@IORN) > 0)
	    BEGIN
		  SET @Query =  ' SELECT SUM([t_qoro]) FROM ' + @LNLinkedServer + '.dbo.twhinh220'+ @LNCompanyId + ' WHERE [t_oorg] = 16 AND [t_orno] = ''' + @IORN + ''''
		  INSERT INTO @TBL(Qoro)
		  EXEC(@Query)
		  IF EXISTS(SELECT Qoro FROM @TBL)
		   BEGIN
		     SELECT TOP 1 @Qoro = Qoro FROM @TBL
			 SELECT TOP 1 
			    @StockArea = ROUND(P0.[t_area], 2),
			    @Factor = P0.[t_cnvf],
			    @Thickness = P0.[t_thic],
				@CCD1 = P0.[t_ccd1],
				@CDT1 = P0.[t_cdt1],
				@CCD2 = P0.[t_ccd2],
				@CDT2 = P0.[t_cdt2] 
			  FROM [dbo].[PCR500] AS P0 WHERE P0.[t_pcln] = @PCLNumber AND P0.[t_stkr] = @Revision

			  SET @QOFRArea = ROUND((@Qoro * 1000) / ((@Factor * @Thickness) + (@CCD1 * @CDT1) + (@CCD2 * @CDT2)), 2)

			   IF((@StockArea - @QOFRArea) < ROUND(@TotalArea, 2))
			    BEGIN
				  SET @MSG = 'Total Area in Distribution must be equal or less than '
				  SET @MSG = CONCAT(@MSG, ROUND(@StockArea - @QOFRArea, 2))
				  SET @MSG = CONCAT(@MSG, ' & m2. Reason ')
				  SET @MSG = CONCAT(@MSG, @QOFRArea)				  
				  SET @MSG = CONCAT(@MSG, ' m2 is already consumed in Project (Manual) Order: ')
				  SET @MSG = CONCAT(@MSG, @IORN)	
				  RAISERROR(@MSG, 11, 1)
				END
		   END
	    END
	   SELECT 1
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END

