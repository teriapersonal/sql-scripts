USE [IEMQS]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec [dbo].[SP_PCR_Get_ReturnDistribution_Data] '122963', 38
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_ReturnDistribution_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_ReturnDistribution_Data]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

ALTER PROCEDURE [dbo].[SP_PCR_Get_ReturnDistribution_Data]
    @PSNo NVARCHAR(100),
	@Id INT
AS
BEGIN 
    --SET @PCLNumber = 'PCLCA13-8440'
	DECLARE @PCLNumber NVARCHAR(100)
	DECLARE @Revision INT
	SELECT
	TOP 1
	  @PCLNumber = P.[t_pcln] ,
	  @Revision = P.[t_revn]
	FROM [dbo].[PCR504] AS P WHERE P.[Id] = @Id

	DECLARE @TotalArea FLOAT
	SELECT @TotalArea = SUM(P.[t_area]) FROM [dbo].[PCR505] AS P WHERE P.[t_pcln] = @PCLNumber AND P.[t_revn] = @Revision
	SET @TotalArea = CASE WHEN @TotalArea IS NULL THEN 0 ELSE ROUND(@TotalArea, 2) END
	SELECT 
	DISTINCT
	  P.[Id] AS Id,
	  P0.[t_pcln] AS PCLNumber,	
	 P.[t_widt] AS Width,	
	 P.[t_leng] AS [Length],	
	 P.[t_area] AS Area,	
	 P4.[t_ityp] AS ItemType,	
	 P.[t_odds] AS OddShape,	
	 P.[t_rmrk] AS Remark,	
	 P.[t_seqn]  AS [Sequence],	
	 ROUND(P0.[t_area] - P4.[t_area], 2) AS MaximumReturnArea,	
	 P.[t_stkr] AS StockRevision,	
	 P4.[t_revn] AS Revision,	
	 P4.[t_rbal] AS ReturnBalance,	
	 @TotalArea AS TotalArea,
	 P0.[t_leng] AS LengthOriginal,
	 P0.[t_widt] AS WidthOriginal,
	 P0.[t_loca] AS LocationOriginal,
	 P0.[t_area] AS AreaOriginal,
	 P0.[t_odds] AS OddShapeOriginal,
	 P0.[t_cnvf] AS FactorOriginal,
	 P0.[t_thic] AS ThicknessOriginal,
	 P0.[t_ccd1] AS CCD1Original,
	 P0.[t_cdt1] AS CDT1Original,
	 P0.[t_ccd2] AS CCD2Original,
	 P0.[t_cdt2] AS CDT2Original
	
	FROM [dbo].[PCR500] AS P0
	INNER JOIN [dbo].[PCR504] AS P4
	ON P0.[t_pcln] = P4.[t_pcln] 
	INNER JOIN [dbo].[PCR505] AS P
	ON P.[t_pcln] = P0.[t_pcln] AND P.[t_revn] = P4.[t_revn]
	WHERE P4.[t_pcln] = @PCLNumber 	
	AND P4.[t_revn] = @Revision
   
END

