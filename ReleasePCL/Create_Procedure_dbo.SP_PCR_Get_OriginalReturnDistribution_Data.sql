USE [IEMQS]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_OriginalReturnDistribution_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_OriginalReturnDistribution_Data]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO

--exec [dbo].[SP_PCR_Get_OriginalReturnDistribution_Data] '122963', 2059

ALTER PROCEDURE [dbo].[SP_PCR_Get_OriginalReturnDistribution_Data]
    @PSNo NVARCHAR(100),
	@Id INT
AS
BEGIN 
    --SET @PCLNumber = 'PCLCA13-8440'
	DECLARE @PCLNumber NVARCHAR(100)
	DECLARE @Revision INT
	DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	SELECT
	TOP 1
	  @PCLNumber = P.[t_pcln] ,
	  @Revision = P.[t_revn]
	FROM [dbo].[PCR504] AS P WHERE P.[Id] = @Id

	DECLARE @TotalArea FLOAT
	SELECT @TotalArea = SUM(P.[t_area]) FROM [dbo].[PCR505] AS P WHERE P.[t_pcln] = @PCLNumber AND P.[t_revn] = @Revision
	SET @TotalArea = CASE WHEN @TotalArea IS NULL THEN 0 ELSE ROUND(@TotalArea, 2) END
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')
	SELECT 
	DISTINCT
	 P0.[t_pcln] AS PCLNumber,	
	 PS.[t_txt] AS PCLStatus,  
	 P4.[t_ityp] AS ItemType,
	 ROUND(P0.[t_area] - P4.[t_area], 2) AS MaximumReturnArea,	
	 @TotalArea AS TotalArea,
	 P0.[t_leng] AS LengthOriginal,
	 P0.[t_widt] AS WidthOriginal,
	 P0.[t_loca] AS LocationOriginal,
	 P0.[t_area] AS AreaOriginal,
	 P0.[t_odds] AS OddShapeOriginal,
	 P0.[t_cnvf] AS FactorOriginal,
	 P0.[t_thic] AS ThicknessOriginal,
	 P0.[t_ccd1] AS CCD1Original,
	 P0.[t_cdt1] AS CDT1Original,
	 P0.[t_ccd2] AS CCD2Original,
	 P0.[t_cdt2] AS CDT2Original,
	 P4.[t_revn] AS Revision,
	 P4.[t_rbal] AS ReturnBalance
	FROM [dbo].[PCR500] AS P0
	LEFT OUTER JOIN [dbo].[PCR504] AS P4
	ON P0.[t_pcln] = P4.[t_pcln] 
	LEFT OUTER JOIN @tblPCLStatus AS PS 
    ON P4.[t_stat] = PS.[t_val]
	LEFT OUTER JOIN [dbo].[PCR505] AS P
	ON P.[t_pcln] = P0.[t_pcln] AND P.[t_revn] = P4.[t_revn]
	WHERE P4.[t_pcln] = @PCLNumber 	
	AND P4.[t_revn] = @Revision
   
END

--select top 10 [t_pcln], t_stkn, t_stkr from pcr500 where t_leng >0
--PCL01000357480	0100035748	0
--update pcr503 set t_pcln = 'PCL01000357480' , t_stkn = '0100035748', t_stkr = 0
--select [t_pcln], t_stkn, t_stkr from pcr503
--select [t_pcln] from pcr504
--update pcr504 set t_pcln = 'PCL01000357480'
--select * from pcr503
 --select * from pcr000 where EnumName ='PCL Status'
--delete from pcr503 where Id = 
--t_pcln='PCLCA13-8440'
--select [t_pcrn] from pcr506
--delete from pcr503 where t_pcrn='HPC000001'