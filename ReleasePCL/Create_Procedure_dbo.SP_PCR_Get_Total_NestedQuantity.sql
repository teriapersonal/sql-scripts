USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_Total_NestedQuantity]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_Total_NestedQuantity]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Get_Total_NestedQuantity]
 @PCRN NVARCHAR(100),
 @PONO INT,
 @StockNumber NVARCHAR(100),
 @PCRRevision INT
AS
BEGIN 
    DECLARE @DeletedStatus INT 
	DECLARE @Value INT = 0
	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE UPPER([LNConstant]) = 'DELETE'

	SELECT
		@Value = SUM(ISNULL(P.[t_nqty], 0))
	FROM 
	  [dbo].[PCR503] AS P
	  WHERE 
	P.[t_pcrn] = @PCRN 
		AND 
	P.[t_pono] = @PONO 
		AND
	P.[t_stkn] = @StockNumber 
		AND 
	P.[t_revn] >= @PCRRevision
	   AND 
	P.[t_lsta] <> @DeletedStatus
	  
   RETURN @Value
	
END