USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_PCRLineStatus _For_Last_PCRRevision]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_PCRLineStatus _For_Last_PCRRevision]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Update_PCRLineStatus _For_Last_PCRRevision]
 @PCRN NVARCHAR(100),
 @PONO INT,
 @Last_PCR_Revision INT 
AS
BEGIN
 BEGIN TRAN
  BEGIN TRY 
    DECLARE @Id INT
	DECLARE @Item NVARCHAR(100)
	DECLARE @StockNumber NVARCHAR(100)
	DECLARE @StockRevision INT
    DECLARE @DeletedStatus INT
	DECLARE @Arcs FLOAT
	
	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM 
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('PCR Line Status','ltpcr.lsta') 
	WHERE 
		UPPER([LNConstant]) = 'DELETE'
    
	SELECT
	  TOP 1
		@Id = P.[Id],
		@Arcs = P.[t_arcs],
		@Item = P.[t_sitm],
		@StockNumber = P.[t_stkn],
		@StockRevision = P.[t_stkr]
    FROM
	    [dbo].[PCR503] AS P
	WHERE
		P.[t_pcrn] = @PCRN
		  AND
		P.[t_pono] = @PONO
		  AND
        P.[t_revn] = @Last_PCR_Revision
	
	UPDATE
	   [dbo].[PCR500]
	SET
	   [t_qall] = [t_qall] -@Arcs,
	   [t_qfre] = [t_qfre] + @Arcs
	WHERE
		[t_item] = @Item
		  AND
        [t_stkn] = @StockNumber
		  AND
        [t_stkr] = @StockRevision

	UPDATE 
		[dbo].[PCR503]
	SET 
		[t_lsta] = @DeletedStatus
	WHERE 
		[Id] = @Id

	COMMIT TRAN							 
 END TRY 
 BEGIN CATCH 
  IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(), 
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END
 END CATCH 
END
