USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Insert_Update_Delete_ReturnDistribution_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Insert_Update_Delete_ReturnDistribution_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Insert_Update_Delete_ReturnDistribution_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @NextSequence INT
	 DECLARE @Sequence INT
	 DECLARE @PCLNumber NVARCHAR(100)
	 DECLARE @Revision INT
	 DECLARE @Id INT
	 DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [PCLNumber] NVARCHAR(100), 
		 [ReturnBalance] FLOAT,
		 [Length] FLOAT,
		 [Width] FLOAT,
		 [MaximumReturnArea] FLOAT,
		 [Area] FLOAT,
		 [Sequence] INT,
		 [OddShape] BIT,
		 [Revision] INT,
		 [ItemType] INT,
		 [Remark] NVARCHAR(MAX),
		 [StockNumber] NVARCHAR(100),
		 [StockRevision] INT
	 )
	DECLARE @SequencesToBeAdjusted AS TABLE(Id INT)   
	INSERT INTO @TBL
	(
		 [Id], 
		 [PCLNumber], 
		 [ReturnBalance],
		 [Length],
		 [Width],
		 [MaximumReturnArea],
		 [Area],
		 [Sequence],
		 [OddShape],
		 [Revision],
		 [ItemType],
		 [Remark]
	)
	SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('PCLNumber').value('.', 'NVARCHAR(100)') AS [PCLNumber], 
		Array.Entity.query('ReturnBalance').value('.', 'FLOAT') AS [ReturnBalance],  
		Array.Entity.query('Length').value('.', 'FLOAT') AS [Length],	
		Array.Entity.query('Width').value('.', 'FLOAT') AS [Width],	
		Array.Entity.query('MaximumReturnArea').value('.', 'FLOAT') AS [MaximumReturnArea],
		Array.Entity.query('Area').value('.', 'FLOAT') AS [Area],
		Array.Entity.query('Sequence').value('.', 'INT') AS [Sequence],
		Array.Entity.query('OddShape').value('.', 'BIT') AS [OddShape],
		Array.Entity.query('Revision').value('.', 'INT') AS [Revision],
		Array.Entity.query('ItemType').value('.', 'BIT') AS [ItemType],	
		Array.Entity.query('Remark').value('.', 'NVARCHAR(100)') AS [Remark]	
			
	FROM @Data.nodes('/ArrayOfRETURN_BALANCE_DISTRIBUTION_Result/RETURN_BALANCE_DISTRIBUTION_Result') AS Array(Entity)
    
	UPDATE T
		SET 
		T.[StockNumber] = P0.[t_stkn], 
		T.[StockRevision] = P0.[t_stkr]
	FROM @TBL AS T
	JOIN [dbo].[PCR500] AS P0 ON T.PCLNumber COLLATE Latin1_General_100_CS_AS_KS_WS = P0.[t_pcln]
		
	IF(@Mode = 0) --INSERT
	 BEGIN
	   IF EXISTS(
				  SELECT 0 FROM [dbo].[PCR505] AS P 
				  INNER JOIN  @TBL AS T ON T.PCLNumber COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcln]  AND P.[t_revn] = T.Revision
				)
	    BEGIN
		   SELECT @NextSequence = MAX(P.[t_seqn]) + 1 FROM [dbo].[PCR505] AS P 
		   INNER JOIN  @TBL AS T 
		   ON T.PCLNumber COLLATE Latin1_General_100_CS_AS_KS_WS = P.[t_pcln]  AND P.[t_revn] = T.Revision
	    END
		ELSE
		 BEGIN
		   SET @NextSequence = 1
		 END
	   INSERT INTO 
		[dbo].[PCR505]
		(
			[t_pcln], [t_leng], [t_widt], [t_area], [t_odds], [t_revn], [t_ityp], [t_rmrk], [t_seqn], [t_stkn], [t_stkr]
		)
	    SELECT 
		  T.PCLNumber, T.[Length], T.Width, 
		  ROUND(ISNULL(T.Area, 0), 2), T.OddShape, T.Revision, 
		  T.ItemType, T.Remark, @NextSequence, T.StockNumber, T.StockRevision
	    FROM @TBL AS T
	 END
    ELSE IF(@Mode = 1) --UPDATE
	 BEGIN
	  UPDATE tbl505
		SET 
		  tbl505.[t_leng] = T.[Length],
		  tbl505.[t_widt] = T.[Width],
		  tbl505.[t_area] = ROUND(ISNULL(T.[Area], 0), 2),
		  tbl505.[t_odds] = T.[OddShape],
		  tbl505.[t_rmrk] = T.[Remark]
		FROM [dbo].[PCR505] AS tbl505
		INNER JOIN @TBL AS T ON T.[Id] = tbl505.[Id]
		
	 END	 
	ELSE IF(@Mode = 2) --DELETE
	 BEGIN
	 	   
	   SELECT TOP 1 @Id = T.[Id] FROM @TBL AS T 
       SELECT
	    TOP 1
		 @PCLNumber = P.[t_pcln],
		 @Revision = P.[t_revn],
		 @Sequence = P.[t_seqn]
	    FROM [dbo].[PCR505] AS P WHERE P.[Id] = @Id
	   INSERT INTO @SequencesToBeAdjusted(Id)
	   SELECT 		
		P.[Id] 
	   FROM 
	    [dbo].[PCR505] AS P 
	   WHERE 
	    P.[t_pcln] = @PCLNumber AND P.[t_revn] = @Revision AND P.[t_seqn] > @Sequence
	   
	   UPDATE P
	     SET
		   P.[t_seqn] = P.[t_seqn] - 1
	    FROM [dbo].[PCR505] AS P
		INNER JOIN @SequencesToBeAdjusted AS T ON P.[Id] = [T].Id
		DELETE FROM [dbo].[PCR505] WHERE [Id] = @Id
	 END
	COMMIT
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END 


 

 
 