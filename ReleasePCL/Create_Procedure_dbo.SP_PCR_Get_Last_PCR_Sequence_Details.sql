USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_Last_PCR_Sequence_Details]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_Last_PCR_Sequence_Details]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Get_Last_PCR_Sequence_Details]
 @PCRN NVARCHAR(100),
 @PONO INT
AS
BEGIN 
	SELECT
	TOP 1
	  P.[t_revn],
	  P.[t_pcln],
	  P.[t_stkn],
	  P.[t_stkr],
	  P.[t_nqty],
	  P.[t_npcs]
	FROM 
	  [dbo].[PCR503] AS P
	  WHERE P.[t_pcrn] = @PCRN AND P.[t_pono] = @PONO
	  ORDER BY P.[t_revn] DESC
	
END
/*
DECLARE @L_Table AS TABLE(
   [PCR_Revision] INT, 
   [PCLN] NVARCHAR(100), 
   [StockNumber] NVARCHAR(100), 
   [StockRevision] INT,
   [Nqty] FLOAT,
   [Npcs] INT
   )
   declare @s nvarchar(max) = '[dbo].[SP_PCR_Get_last_PCR_Sequence_Details] ''HPC000001'', 20'
   insert into @L_Table
    exec(@s)
  select * from @L_Table
  */