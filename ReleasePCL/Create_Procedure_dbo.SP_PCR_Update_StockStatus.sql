USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Update_StockStatus]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Update_StockStatus]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


ALTER PROCEDURE [dbo].[SP_PCR_Update_StockStatus]
 @Item NVARCHAR(100),  
 @StockNumber NVARCHAR(100),
 @StockRevision INT
AS
BEGIN
 BEGIN TRAN
  BEGIN TRY 
    DECLARE @DeletedStatus INT 
	SELECT
	    TOP 1 
		@DeletedStatus = [Value] 
	FROM
		[dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS]('Stock Status','ltsfc.stk.stat') 
	WHERE 
		UPPER([LNConstant]) = 'DELETE'

	UPDATE 
		[dbo].[PCR500]
	SET 
		[t_stat] = @DeletedStatus
	WHERE 
		[t_item] = @Item
			AND 
		[t_stkn] = @StockNumber
	        AND
		[t_stkr] > @StockRevision
	        
	COMMIT TRAN							 
 END TRY 
 BEGIN CATCH 
  IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE(), 
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END
 END CATCH 
END
