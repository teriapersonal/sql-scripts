USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_PCL_Number_List]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_PCL_Number_List]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
--EXEC [dbo].[SP_PCR_Get_PCL_Number_List]
ALTER PROCEDURE [dbo].[SP_PCR_Get_PCL_Number_List]
AS
BEGIN 
	
	DECLARE @tblPCLStatus AS TABLE
	(
	   [t_val] INT NULL,
	   [t_txt] NVARCHAR(100)	,
	   [t_cnst] NVARCHAR(100)   
	)
	INSERT INTO @tblPCLStatus([t_val], [t_txt], [t_cnst])
	SELECT [Value], [Text], UPPER([LNConstant]) FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCL Status','ltpcl.stat')

	

	SELECT  
			ROW_NUMBER() OVER (
			ORDER BY P.[t_pcln]
			) Code,
	        P.[t_pcln] AS CodeDescription							
	FROM [dbo].[PCR504] AS P 
	LEFT OUTER JOIN @tblPCLStatus AS PS ON PS.[t_val] = P.[t_stat]
	WHERE UPPER(PS.[t_cnst]) IN ('CONF.PMG','PCL.REL','APPR.QA','CONF.SFC','REJ.QC') OR P.[t_stat] IS NULL
	
END

