USE [IEMQS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Create_PCR_Table]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Create_PCR_Table] 
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
 
ALTER PROCEDURE [dbo].[SP_PCR_Create_PCR_Table]
(
	 @LNTableName NVARCHAR(20),
	 @IEMQSTableName NVARCHAR(20)
)
AS
BEGIN
 DECLARE @query NVARCHAR(max)
 DECLARE @rowindex INT
 DECLARE @total INT
 SET @rowindex =1
 DECLARE @tbl TABLE(Id INT IDENTITY(1,1), columnName NVARCHAR(100),data_type NVARCHAR(200), collation_name NVARCHAR(1000), is_nullable bit,is_identity bit, max_length int)
 DECLARE @columnName NVARCHAR(100)
 DECLARE @data_type NVARCHAR(200)
 DECLARE @collation_name NVARCHAR(1000)
 DECLARE @is_nullable BIT
 DECLARE @is_identity BIT
 DECLARE @max_length INT
--declare @tablename nvarchar(100) = 'tltsfc505175'
--declare @equivalent nvarchar(100) ='PCR505'
 DECLARE @colquery NVARCHAR(max)
 INSERT into @tbl(columnName,data_type, collation_name,is_nullable,is_identity,max_length)



 SELECT 
	c.[name], 
	tt.[name] AS DataType, 
	c.collation_name, 
	c.is_nullable, 
	c.is_identity, 
	c.max_length 
 FROM 
	[HZPDSQL2K12\IN02].[lnappqa3db].sys.tables AS t 
 JOIN 
	[HZPDSQL2K12\IN02].[lnappqa3db].sys.schemas AS s ON t.schema_id = s.schema_id
 JOIN 
	[HZPDSQL2K12\IN02].[lnappqa3db].sys.columns AS c ON t.object_id = c.object_id
 JOIN 
	[HZPDSQL2K12\IN02].[lnappqa3db].sys.types AS tt ON tt.user_type_id = c.user_type_id
 WHERE 
    t.[name] = @LNTableName

  SET @query ='create table ' + @IEMQSTableName + '(Id INT IDENTITY(1,1) PRIMARY KEY,'
  SELECT @total = count(0) from @tbl
  WHILE(@rowindex <= @total)
   BEGIN
    SELECT 
	 @columnName = t.columnName,
	 @data_type = t.data_type, 
	 @collation_name  = t.collation_name,
	 @is_nullable = t.is_nullable,
	 @is_identity = t.is_identity,
	 @max_length = t.max_length 
   FROM @tbl AS t 
   WHERE t.Id = @rowindex

   SET @colquery = @columnName
   IF(@data_type ='nvarchar')
    BEGIN
	  SET @colquery = concat(@colquery + ' ' + @data_type + '(',@max_length)
	  SET @colquery = concat(@colquery,')')	
    END
  ELSE
    BEGIN
      SET @colquery = concat(@colquery + ' ', @data_type)
    END
  IF(@data_type ='nvarchar' OR @data_type = 'varchar')
   BEGIN
     SET @colquery = concat(@colquery, ' COLLATE ' + @collation_name) 
   END
  IF(@is_nullable = 0)
   BEGIN
    SET @colquery += ' ' + 'NULL'
   END
  ELSE
   BEGIN
    SET @colquery += ' ' + ' NOT NULL'
   END
  
  SET @query = @query + ' ' + @colquery
  IF(@rowindex = @total)
   BEGIN
    SET @query += ' )'
   END
  ELSE
   BEGIN
     SET @query += ' ,'
   END
   SET @rowindex += 1
END
 PRINT(@query)
--print(@total)
--EXEC(@Query)
END



