-- [tltsfc506510]
USE [IEMQS]

USE [IEMQS]
GO

/****** Object:  Table [dbo].[PCR506]    Script Date: 11/27/2020 1:05:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PCR506](
	[t_cprj] [nvarchar](9) NOT NULL,
	[t_cspa] [nvarchar](8) NOT NULL,
	[t_sern] [int] NOT NULL,
	[t_seqn] [int] NOT NULL,
	[t_sitm] [nvarchar](47) NOT NULL,
	[t_revi] [nvarchar](6) NOT NULL,
	[t_mitm] [nvarchar](47) NOT NULL,
	[t_leng] [float] NOT NULL,
	[t_widt] [float] NOT NULL,
	[t_npcs] [int] NOT NULL,
	[t_leng_m] [float] NOT NULL,
	[t_widt_m] [float] NOT NULL,
	[t_npcs_m] [int] NOT NULL,
	[t_loca] [nvarchar](9) NOT NULL,
	[t_init] [nvarchar](9) NOT NULL,
	[t_pcrn] [nvarchar](9) NULL,
	[t_pono] [int] NOT NULL,
	[t_pcrt] [int] NULL,
	[t_refcntd] [int] NULL,
	[t_refcntu] [int] NULL,
	[t_cwoc] [nvarchar](6) NULL,
	[t_prdt] [datetime] NULL,
	[t_prmk] [nvarchar](50) NULL,
	[t_cloc] [nvarchar](20) NULL,
	[t_rrmk] [nvarchar](30) NULL,
	[t_rtby] [nvarchar](9) NULL,
	[t_rtdt] [datetime] NULL,
	[t_prio] [int] NULL,
	[t_ityp] [int] NULL,
	[t_sloc] [nvarchar](30) NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](100) NOT NULL
) ON [PRIMARY]

GO



GO
  CREATE UNIQUE INDEX UniqueIndex_PCR006 ON PCR506 ([t_cprj],[t_cspa],[t_sern],[t_seqn])
GO