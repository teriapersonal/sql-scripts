/*  
Created By : Anil Hadiyal  
Created On : 21-08-2019  
Description : Get PCR Enum Details  
  
Select * from dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Type','ltsfc.pcr.type')  
*/  
ALTER Function [dbo].[FN_GET_PCR_ENUM_DESCRIPTIONS](   
@EnumName varchar(max),  
@LNDomainName nvarchar(max)  
)  
returns @tblEnum table (Id int,EnumName nvarchar(max),LNTableName nvarchar(max),LNDomainName nvarchar(max),LNConstant nvarchar(max),Text nvarchar(max),Value int)  
BEGIN  
  
insert into @tblEnum  
  
SELECT * FROM PCR000  
WHERE EnumName=@EnumName   
 AND LNDomainName=@LNDomainName  
  
return  
END