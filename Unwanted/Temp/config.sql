-- =============================================  
-- Author:  TRISHUL TANDEL  
-- Create date: 9 MAY 2018  
-- Description: GET CONFIG VALUE  
-- =============================================  
CREATE FUNCTION [dbo].[FN_COMMON_GET_CONFIG_VALUE]  
(  
 @pKey NVARCHAR(100)  
)  
RETURNS NVARCHAR(max)  
AS  
BEGIN  
   
 DECLARE @ReturnValue NVARCHAR(max)  
  
 SELECT @ReturnValue=Value FROM Config WHERE [Key]=@pKey  
  
 RETURN @ReturnValue;  
  
END