USE [IEMQS]
GO
/****** Object:  Table [dbo].[PCR000]    Script Date: 16-02-2021 20:28:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PCR000](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EnumName] [nvarchar](max) NULL,
	[LNTableName] [nvarchar](max) NULL,
	[LNDomainName] [nvarchar](max) NULL,
	[LNConstant] [nvarchar](max) NULL,
	[Text] [nvarchar](max) NULL,
	[Value] [int] NULL,
 CONSTRAINT [PK_PCR000] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
