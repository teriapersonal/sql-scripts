BEGIN
DECLARE @tbl table (Id int,Process nvarchar(100),[Description] nvarchar(100),Area nvarchar(25),Controller nvarchar(100),  
     [Action] nvarchar(100),ParentId int,IsDisplayInMenu bit,IconClass nvarchar(50),IsActive nchar(10),  
     CreatedBy  nvarchar(9),CreatedOn datetime,EditedBy  nvarchar(9),EditedOn datetime,MainMenuOrder int,SubMenuOrder int)  

   
 insert into @tbl  
 Select * from (  
 Select Id,Process,Description,Area,Controller,Action,ISNULL(ParentId,0)ParentId,IsDisplayInMenu,IconClass,IsActive,CreatedBy,CreatedOn,EditedBy,EditedOn,  
  case when ParentId is null and MainMenuOrderNo is not null then MainMenuOrderNo else 100000 end as MainMenuOrder,  
  case when ParentId is null and MainMenuOrderNo is not null then 0 else 100000 end as SubMenuOrder  
 from ATH002  
 Where /*IsDisplayInMenu=1 and*/ ParentId is null  
 --order by MainMenuOrder asc,SubMenuOrder  
  
 union all  
  
 Select Id,Process,Description,Area,Controller,Action,ISNULL(ParentId,0)ParentId,IsDisplayInMenu,IconClass,IsActive,CreatedBy,CreatedOn,EditedBy,EditedOn,  
  ISNULL((Select MainMenuOrderNo from ATH002 Where Id=ath2.ParentId),100000) as MainMenuOrder,  
  case when OrderNo is not null then OrderNo else 100000 end SubMenuOrder  
 from ATH002 as ath2  
 Where /*IsDisplayInMenu=1 and*/ ParentId is not null  
 --order by MainMenuOrder asc,SubMenuOrder  
 ) as ath  
 Order by MainMenuOrder,SubMenuOrder,ParentId  
select * from @tbl where Area='PCR'
end