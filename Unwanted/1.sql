USE [IEMQS]
GO
/**
  Change History  -
     Date - Nov 25th 2020 - Nishant Teria Zapimon Technologies.
	 Reason - Changed table name from PCR004 to PCR506, added input parameters @Project and @Element, since data will be fetched based upon these parameters.
    
**/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE  [dbo].[SP_PCR_GET_PCR_DATA]
    @StartIndex INT = 1,
    @EndIndex INT = 10,
    @SortingFields VARCHAR(50) = '', 
	@Project NVARCHAR(100) = NULL,
	@Element NVARCHAR(100) = NULL,
    @SqlQuery as NVARCHAR(MAX) = NULL  --PARAMETER USED FOR WHERE CONDITION
AS
BEGIN
	DECLARE @FilterQuery NVARCHAR(MAX)  = ' P.[t_cprj] = ''' + @Project + ''' AND P.[t_cspa] = ''' + @Element + ''''  
	DECLARE @RowFilterQuery NVARCHAR(MAX) = ''; 
	DECLARE @DeclareQuery NVARCHAR(MAX) = ''
	IF @ENDIndex <> 0
     BEGIN
        SET @RowFilterQuery
            = ' WHERE F.PartGroupNo BETWEEN ' + CAST(@StartIndex AS VARCHAR(10)) + ' AND ' + CAST(@ENDIndex AS VARCHAR(10))
              + '';
     END
	
	IF LEN(ISNULL(@SortingFields, '')) > 0
	BEGIN
		SET @SortingFields = SUBSTRING(LTRIM(RTRIM(@SortingFields)), 9, LEN(LTRIM(RTRIM(@SortingFields)))-8)
	END
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	
	SET @DeclareQuery = '
	DECLARE @tblItemMaterial TABLE([t_cprj] NVARCHAR(100),[t_cspa] NVARCHAR(100), [t_item] NVARCHAR(1000), [t_dscb] NVARCHAR(1000))
	DECLARE @tblMaterialGrade TABLE([t_mtrl] NVARCHAR(MAX), [ItemMaterialGradeName] NVARCHAR(MAX))	
	DECLARE @tblWCLocation TABLE([t_cwoc] NVARCHAR(MAX), [WCDeliverName] NVARCHAR(MAX))
	
	INSERT INTO @tblItemMaterial
	SELECT A.[t_cprj], A.[t_cspa], LTRIM(RTRIM(A.[t_item])), LTRIM(RTRIM(B.[t_dscb]))   FROM ' + 
	@LNLinkedServer + '.dbo.ttpptc120' + @LNCompanyId + ' AS A WITH (NOLOCK) INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + '  AS B WITH (NOLOCK) ON A.[t_item] = B.[t_item] 
	  WHERE A.[t_cprj] = ''' + @Project + ''' AND  A.[t_cspa] = ''' + @Element + '''
	
	INSERT INTO @tblMaterialGrade
	SELECT LTRIM(RTRIM([t_mtrl])) t_mtrl, LTRIM(RTRIM([t_mtrl])) + ''-'' + [t_desc] FROM ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ' WITH (NOLOCK)  
	
	INSERT INTO @tblWCLocation
	SELECT LTRIM(RTRIM([t_cwoc])) AS t_cwoc, LTRIM(RTRIM([t_cwoc])) + ''-'' + [t_dsca] FROM ' + @LNLinkedServer + '.dbo.ttirou001' + @LNCompanyId + ' WITH (NOLOCK) '

	

	SET @SqlQuery
    = @DeclareQuery + '
		SELECT 
			ROW_NUMBER() OVER 
			( ORDER BY [PartNumber] ASC, [PartNumber] ASC) AS Row_No,					
			DENSE_RANK() OVER
			( ORDER BY [Project] ASC, [ChildItem] ASC, [PartNumber] ASC) AS PartGroupNo,
			* 
		   INTO #TempPCR
           FROM (
				  SELECT 
								[Id],
								P.[t_cprj] AS Project,
								P.[t_cspa] AS Element,
								[t_sern] AS PartNumber,
								[t_mitm] AS ManufacturingItem,
								[t_sitm] AS ChildItem,
								[t_leng] AS [Length],
								[t_widt] AS Width,
								[t_leng_m] AS LengthModified,
								[t_widt_m] AS WidthModified,
								[t_npcs] AS NoOfPieces,
								[t_npcs_m] AS RequiredQuantity,
								ISNULL([t_pcrn],'''') PCRNumber,
								[t_seqn] AS [Sequence],
								[t_pcrn] AS PCRLineNo,
								[t_pcrt] AS PCRType,
								[t_prdt] AS PCRRequirementDate,
								P.[t_cwoc] AS WCDeliver,
								[t_prmk] AS PlannerRemark,
								[t_cloc] AS CuttingLocation,
								[t_prio] AS [Priority],
								P.[t_loca] AS [Location],
								[t_revi] AS ItemRevision,
								[t_sloc] AS SubLocation,
								[CreatedBy],
								[CreatedOn],
								NULL AS EditedBy,
								NULL AS EditedOn,
								NULL AS ReturnBy,
								NULL AS ReturnDate,
								NULL AS ReturnRemark,
								0 AS RequiredForJIGFIX,
								NULL AS Description,
								0 AS Thickness,
								''PPT'' AS ItemType,																			
								'''' AS CuttingLocationName,								
								PCRT.[PCRTypeName],								
								IM.[t_dscb] AS ItemMaterialGrade,
								MG.[ItemMaterialGradeName],
								C2.[LocationName],
								P3.[PCRLineId],
								ISNULL(P3.[PCRLineStatus],'''') AS PCRLineStatus,
								WCLOCA.[WCDeliverName]								
								FROM [PCR506] AS P
								LEFT JOIN @tblItemMaterial AS IM ON IM.[t_cprj] = P.[t_cprj] AND IM.[t_cspa] = P.[t_cspa] AND IM.[t_item] = P.[t_sitm]
								LEFT JOIN (Select [LineId] AS PCRLineId, [RefId], [PCRLineStatus] FROM PCR003 WITH (NOLOCK)) AS P3 ON p.[Id] = P3.[RefId]
								LEFT JOIN (SELECT [t_dimx], [t_dimx] + ''-'' + [t_desc] AS LocationName FROM COM002 WITH (NOLOCK) WHERE [t_dtyp] = 1) 
								AS C2 ON P.[t_loca] = C2.[t_dimx]
								LEFT JOIN @tblMaterialGrade AS MG ON IM.[t_dscb] = MG.[t_mtrl]
								LEFT JOIN @tblWCLocation AS WCLOCA ON P.[t_cwoc] = WCLOCA.[t_cwoc]
								LEFT JOIN (Select [Value], [Text] AS PCRTypeName FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS(''PCR Type'',''ltsfc.pcr.type'')) AS PCRT 
								ON PCRT.[Value] = P.[t_pcrt]
  								WHERE ' + @FilterQuery + '
                        ) AS DATA
						
				
					SELECT  
						*, 
						CAST(ROW_NUMBER() OVER 
							( PARTITION BY [PartGroupNo] ORDER BY [Sequence] DESC, [Sequence] DESC) AS INT) AS SequenceSrNo,
						CAST((Select MAX([PartGroupNo]) FROM #TempPCR) AS INT) AS TotalCount 
						FROM #TempPCR F ' + @RowFilterQuery + ' ORDER BY [Row_No], [PartGroupNo]
					DROP TABLE #TempPCR';   
    EXEC(@SQlQuery)
END