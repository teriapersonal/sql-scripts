USE [IEMQS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
BEGIN 
BEGIN TRAN
  BEGIN TRY
	   CREATE TABLE [dbo].[PCR500](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[t_item] [nvarchar](94) NULL,
		[t_stkn] [nvarchar](40) NULL,
		[t_stkr] [int] NULL,
		[t_loca] [nvarchar](18) NULL,
		[t_leng] [float] NULL,
		[t_widt] [float] NULL,
		[t_arms] [nvarchar](60) NULL,
		[t_qnty] [float] NULL,
		[t_mtrl] [nvarchar](60) NULL,
		[t_thic] [float] NULL,
		[t_qres] [float] NULL,
		[t_qall] [float] NULL,
		[t_qcut] [float] NULL,
		[t_qfre] [float] NULL,
		[t_orno] [nvarchar](18) NULL,
		[t_pono] [int] NULL,
		[t_rcno] [nvarchar](18) NULL,
		[t_rcln] [int] NULL,
		[t_shtp] [nvarchar](40) NULL,
		[t_shfl] [nvarchar](40) NULL,
		[t_size] [nvarchar](60) NULL,
		[t_cwar] [nvarchar](12) NULL,
		[t_wloc] [nvarchar](20) NULL,
		[t_cnvf] [float] NULL,
		[t_stat] [int] NULL,
		[t_htno] [nvarchar](40) NULL,
		[t_tcno] [nvarchar](18) NULL,
		[t_gorn] [nvarchar](40) NULL,
		[t_mchn] [nvarchar](60) NULL,
		[t_area] [float] NULL,
		[t_arcs] [float] NULL,
		[t_pcln] [nvarchar](60) NULL,
		[t_armv] [nvarchar](12) NULL,
		[t_qapr] [float] NULL,
		[t_cono] [nvarchar](18) NULL,
		[t_cdt1] [float] NULL,
		[t_cdt2] [float] NULL,
		[t_cnvb] [float] NULL,
		[t_ccd1] [float] NULL,
		[t_ccd2] [float] NULL,
		[t_twgt] [float] NULL,
		[t_psta] [int] NULL,
		[t_clot] [nvarchar](40) NULL,
		[t_pstk] [nvarchar](40) NULL,
		[t_prev] [int] NULL,
		[t_odds] [int] NULL,
		[t_idsf] [int] NULL,
		[t_cprj] [nvarchar](18) NULL,
		[t_cspa] [nvarchar](16) NULL,
		[t_rmrk] [nvarchar](100) NULL,
		[t_cwoc] [nvarchar](12) NULL,
		[t_amod] [int] NULL,
		[t_rorn] [nvarchar](18) NULL,
		[t_rrst] [int] NULL,
		[t_srmk] [nvarchar](100) NULL,
		[t_sono] [nvarchar](18) NULL,
		[t_runn] [nvarchar](24) NULL,
		[t_cons] [int] NULL,
		[t_sprj] [nvarchar](18) NULL,
		[t_okey] [nvarchar](20) NULL,
		[t_sspa] [nvarchar](16) NULL,
		[t_sact] [nvarchar](16) NULL,
		[t_scrn] [nvarchar](18) NULL,
		[t_shpm] [nvarchar](18) NULL,
		[t_rqno] [nvarchar](18) NULL,
		[t_ncra] [int] NULL,
		[t_ncrn] [nvarchar](30) NULL,
		[t_ncrr] [nvarchar](1000) NULL,
		[t_rsfp] [int] NULL,
		[t_rsby] [nvarchar](18) NULL,
		[t_rsdt] [datetime] NULL,
		[t_ityp] [int] NULL,
		[t_remk] [nvarchar](200) NULL,
		[t_Refcntd] [int] NULL,
		[t_Refcntu] [int] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	CREATE TABLE [dbo].[PCR501](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[t_pcrn] [nvarchar](18) NULL,
		[t_cdat] [datetime] NULL,
		[t_rdat] [datetime] NULL,
		[t_loca] [nvarchar](18) NULL,
		[t_init] [nvarchar](18) NULL,
		[t_ityp] [int] NULL,
		[t_Refcntd] [int] NULL,
		[t_Refcntu] [int] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	CREATE TABLE [dbo].[PCR503](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[t_pcrn] [nvarchar](18) NULL,
		[t_pono] [int] NULL,
		[t_revn] [int] NULL,
		[t_prtn] [int] NULL,
		[t_mitm] [nvarchar](94) NULL,
		[t_sitm] [nvarchar](94) NULL,
		[t_leng] [float] NULL,
		[t_widt] [float] NULL,
		[t_npcs] [int] NULL,
		[t_pcrt] [int] NULL,
		[t_prdt] [datetime] NULL,
		[t_cwoc] [nvarchar](12) NULL,
		[t_cwar] [nvarchar](12) NULL,
		[t_cloc] [nvarchar](40) NULL,
		[t_pcln] [nvarchar](60) NULL,
		[t_lsta] [int] NULL,
		[t_sqty] [float] NULL,
		[t_nqty] [int] NULL,
		[t_arcs] [float] NULL,
		[t_stkn] [nvarchar](40) NULL,
		[t_stkr] [int] NULL,
		[t_mvtp] [int] NULL,
		[t_prmk] [nvarchar](100) NULL,
		[t_mrmk] [nvarchar](100) NULL,
		[t_pcr_crdt] [datetime] NULL,
		[t_pcr_rldt] [datetime] NULL,
		[t_pcl_crdt] [datetime] NULL,
		[t_pcl_rldt] [datetime] NULL,
		[t_cprj] [nvarchar](18) NULL,
		[t_cspa] [nvarchar](16) NULL,
		[t_mpos] [int] NULL,
		[t_thic] [float] NULL,
		[t_gorn] [nvarchar](60) NULL,
		[t_shtp] [nvarchar](40) NULL,
		[t_shfl] [nvarchar](40) NULL,
		[t_area] [float] NULL,
		[t_pitm] [nvarchar](94) NULL,
		[t_hcno] [nvarchar](60) NULL,
		[t_alts] [int] NULL,
		[t_armk] [nvarchar](100) NULL,
		[t_oitm] [nvarchar](94) NULL,
		[t_qapr] [int] NULL,
		[t_iqty] [int] NULL,
		[t_revi] [nvarchar](12) NULL,
		[t_lscr] [int] NULL,
		[t_scrn] [nvarchar](18) NULL,
		[t_rqty] [int] NULL,
		[t_mcrp] [int] NULL,
		[t_rrmk] [nvarchar](100) NULL,
		[t_pr01] [int] NULL,
		[t_pr02] [int] NULL,
		[t_pr03] [int] NULL,
		[t_pr04] [int] NULL,
		[t_pr05] [int] NULL,
		[t_pr06] [int] NULL,
		[t_pr07] [int] NULL,
		[t_pr08] [int] NULL,
		[t_pr09] [int] NULL,
		[t_pr10] [int] NULL,
		[t_ityp] [int] NULL,
		[t_Refcntd] [int] NULL,
		[t_Refcntu] [int] NULL,
		[t_init] [nvarchar](100) NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	CREATE TABLE [dbo].[PCR504](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[t_pcln] [nvarchar](60) NULL,
		[t_revn] [int] NULL,
		[t_mvtp] [int] NULL,
		[t_emno] [nvarchar](18) NULL,
		[t_area] [float] NULL,
		[t_date] [datetime] NULL,
		[t_stat] [int] NULL,
		[t_iorn] [nvarchar](18) NULL,
		[t_orno] [nvarchar](18) NULL,
		[t_rorn] [nvarchar](18) NULL,
		[t_sqty] [float] NULL,
		[t_rbal] [int] NULL,
		[t_pcl_rldt] [datetime] NULL,
		[t_pmg_cdat] [datetime] NULL,
		[t_pmg_rdat] [datetime] NULL,
		[t_qa_adat] [datetime] NULL,
		[t_qa_rdat] [datetime] NULL,
		[t_sfc_cdat] [datetime] NULL,
		[t_plt_idat] [datetime] NULL,
		[t_plt_rdat] [datetime] NULL,
		[t_plt_mdat] [datetime] NULL,
		[t_qc_adat] [datetime] NULL,
		[t_qc_rdat] [datetime] NULL,
		[t_plt_cdat] [datetime] NULL,
		[t_plt_rtdt] [datetime] NULL,
		[t_pcl_cndt] [datetime] NULL,
		[t_pcl_cldt] [datetime] NULL,
		[t_cwoc] [nvarchar](12) NULL,
		[t_aarc] [float] NULL,
		[t_asqt] [float] NULL,
		[t_rtar] [float] NULL,
		[t_pcl_dscr] [datetime] NULL,
		[t_plt_trdt] [datetime] NULL,
		[t_ctdt] [datetime] NULL,
		[t_rrmk] [nvarchar](100) NULL,
		[t_odds] [int] NULL,
		[t_tclt] [float] NULL,
		[t_mrmk] [nvarchar](60) NULL,
		[t_atqa] [nvarchar](18) NULL,
		[t_atqc] [nvarchar](18) NULL,
		[t_rqrd] [nvarchar](100) NULL,
		[t_actl] [nvarchar](100) NULL,
		[t_txta] [int] NULL,
		[t_pibp] [nvarchar](18) NULL,
		[t_pips] [nvarchar](18) NULL,
		[t_pirk] [nvarchar](60) NULL,
		[t_pibp_s] [nvarchar](18) NULL,
		[t_pips_s] [nvarchar](18) NULL,
		[t_pirk_s] [nvarchar](60) NULL,
		[t_srmk] [nvarchar](60) NULL,
		[t_fvdt] [datetime] NULL,
		[t_rrmk_qc] [nvarchar](60) NULL,
		[t_pmrk] [nvarchar](100) NULL,
		[t_resn] [nvarchar](12) NULL,
		[t_rloc] [nvarchar](20) NULL,
		[t_stgi] [int] NULL,
		[t_ityp] [int] NULL,
		[t_mpln] [nvarchar](18) NULL,
		[t_new_case] [int] NULL,
		[t_Refcntd] [int] NULL,
		[t_Refcntu] [int] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	CREATE TABLE [dbo].[PCR505](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[t_pcln] [nvarchar](60) NULL,
		[t_revn] [int] NULL,
		[t_seqn] [int] NULL,
		[t_leng] [float] NULL,
		[t_widt] [float] NULL,
		[t_area] [float] NULL,
		[t_odds] [int] NULL,
		[t_stkn] [nvarchar](40) NULL,
		[t_stkr] [int] NULL,
		[t_rmrk] [nvarchar](60) NULL,
		[t_ityp] [int] NULL,
		[t_Refcntd] [int] NULL,
		[t_Refcntu] [int] NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	CREATE TABLE [dbo].[PCR506](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[t_cprj] [nvarchar](18) NULL,
		[t_cspa] [nvarchar](16) NULL,
		[t_sern] [int] NULL,
		[t_mitm] [nvarchar](94) NULL,
		[t_sitm] [nvarchar](94) NULL,
		[t_leng] [float] NULL,
		[t_widt] [float] NULL,
		[t_leng_m] [float] NULL,
		[t_widt_m] [float] NULL,
		[t_npcs] [int] NULL,
		[t_npcs_m] [int] NULL,
		[t_pcrn] [nvarchar](18) NULL,
		[t_seqn] [int] NULL,
		[t_pono] [int] NULL,
		[t_pcrt] [int] NULL,
		[t_prdt] [datetime] NULL,
		[t_cwoc] [nvarchar](12) NULL,
		[t_prmk] [nvarchar](100) NULL,
		[t_cloc] [nvarchar](40) NULL,
		[t_prio] [int] NULL,
		[t_loca] [nvarchar](18) NULL,
		[t_init] [nvarchar](18) NULL,
		[t_rtby] [nvarchar](18) NULL,
		[t_rtdt] [datetime] NULL,
		[t_rrmk] [nvarchar](60) NULL,
		[t_revi] [nvarchar](12) NULL,
		[t_ityp] [int] NULL,
		[t_Refcntd] [int] NULL,
		[t_Refcntu] [int] NULL,
		[t_sloc] [nvarchar](60) NULL,
		[CreatedOn] [datetime] NULL,
		[CreatedBy] [nvarchar](100) NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
	
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PCR501_pcrn] ON [dbo].[PCR501]
	(
		[t_pcrn] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PCR503_cprj_cspa] ON [dbo].[PCR503]
	(
		[t_pcrn] ASC,
		[t_revn] ASC,
		[t_cprj] ASC,
		[t_cspa] ASC,
		[t_mitm] ASC,
		[t_sitm] ASC,
		[t_prtn] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE NONCLUSTERED INDEX [IX_PCR503_pcln] ON [dbo].[PCR503]
	(
		[t_pcln] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PCR503_pcrn_pono_revn] ON [dbo].[PCR503]
	(
		[t_pcrn] ASC,
		[t_pono] ASC,
		[t_revn] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PCR503_pcrn_revn_cprj_cspa_mitm_sitm_prtn] ON [dbo].[PCR503]
	(
		[t_pcrn] ASC,
		[t_revn] ASC,
		[t_cprj] ASC,
		[t_cspa] ASC,
		[t_mitm] ASC,
		[t_sitm] ASC,
		[t_prtn] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PCR504_pcln_revn] ON [dbo].[PCR504]
	(
		[t_pcln] ASC,
		[t_revn] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PCR505_pcln_revn_seqn] ON [dbo].[PCR505]
	(
		[t_pcln] ASC,
		[t_revn] ASC,
		[t_seqn] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE NONCLUSTERED INDEX [IX_PCR506_cprj] ON [dbo].[PCR506]
	(
		[t_cprj] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE NONCLUSTERED INDEX [IX_PCR506_cprj_cspa] ON [dbo].[PCR506]
	(
		[t_cprj] ASC,
		[t_cspa] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PCR506_cprj_cspa_seq_sern] ON [dbo].[PCR506]
	(
		[t_cprj] ASC,
		[t_cspa] ASC,
		[t_seqn] ASC,
		[t_sern] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
	CREATE UNIQUE NONCLUSTERED INDEX [IX_PCR506_cprj_cspa_sern_seq] ON [dbo].[PCR506]
	(
		[t_cprj] ASC,
		[t_cspa] ASC,
		[t_sern] ASC,
		[t_seqn] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	
   COMMIT
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE() ,
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END