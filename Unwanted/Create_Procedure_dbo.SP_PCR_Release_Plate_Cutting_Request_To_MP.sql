USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Nov 30th 2020
-- Description:	
-- =============================================

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Release_Plate_Cutting_Request_To_MP]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Release_Plate_Cutting_Request_To_MP]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
--Exec [dbo].[SP_PCR_Generate_Plate_Cutting_Request] '1664',',','122963'

Alter Procedure [dbo].[SP_PCR_Release_Plate_Cutting_Request_To_MP]
(
   @Id INT,   
   @PSNo NVARCHAR(100)
)
AS

BEGIN
	SET NOCOUNT ON
	DECLARE @PCRN NVARCHAR(100)	
	DECLARE @Pono INT = 0
	DECLARE @PCRRequirementDate DATETIME
	DECLARE @Table AS TABLE(RowIndex INT IDENTITY(1,1), PCRRequirementDate DATETIME, PONO INT)
	DECLARE @Index INT = 1
	DECLARE @Total INT = 1
	DECLARE @CreatedValue INT
	DECLARE @ReleasedValue INT
	DECLARE @IsError BIT = 0
	DECLARE @Message NVARCHAR(MAX)
	BEGIN TRAN
	   BEGIN TRY	     
	     
		  SELECT @PCRN = [t_pcrn] FROM [dbo].[PCR506] WHERE Id = @Id	 
		  IF(@PCRN IS NULL OR LEN(@PCRN) =0)
		   BEGIN
		     RAISERROR('Please generate PCR number first', 11, 1)
		   END
	      SELECT
	    	TOP 1 @CreatedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'CREATED'
	
	      SELECT
	    	TOP 1 @ReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'
	

		   IF NOT EXISTS 
		   (
			SELECT 0 FROM [dbo].[PCR503] AS P WHERE P.[t_pcrn] = @PCRN AND P.[t_lsta] = @CreatedValue
		   )
		   BEGIN
		     RAISERROR('Please maintain atleast one PCR Lines', 11, 1);
		   END  
		   INSERT INTO @Table(PCRRequirementDate, PONO)
		   SELECT [t_prdt], [t_pono] FROM [dbo].[PCR503] AS P WHERE P.[t_pcrn] = @PCRN AND P.[t_lsta] = @CreatedValue
		   SELECT @Total = COUNT(0) FROM @Table
		   WHILE(@Index <= @Total)
		   BEGIN
		     SELECT
			   @PCRRequirementDate = T.PCRRequirementDate ,
			   @Pono = T.PONO 
			   FROM @Table AS T WHERE T.RowIndex = @Index
			    IF(@PCRRequirementDate IS NULL)
				 BEGIN
				     SET @IsError = 1
					 BREAK
				 END
			   SET @Index += 1
		   END
		   IF(@IsError = 1)
		    BEGIN
			  SET @Message = CONCAT('PCR requirement date should not be blank for PCR Lines ', @Pono)
			  RAISERROR(@Message, 11,1)
		    END
		   SELECT 0 FROM [dbo].[PCR503] AS P WHERE P.[t_pcrn] = @PCRN AND P.[t_lsta] = @CreatedValue
		   UPDATE [dbo].[PCR501] SET [t_rdat] = GETUTCDATE() WHERE	[t_pcrn] = @PCRN
		   UPDATE [dbo].[PCR503] SET [t_pcr_rldt] = GETUTCDATE(), [t_lsta] = @ReleasedValue WHERE [t_pcrn] = @PCRN
		COMMIT TRAN
		SELECT 1
	END TRY 
	BEGIN CATCH 
	   IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRAN 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
					@ErrorMessage = ERROR_MESSAGE() ,  
					@ErrorSeverity = ERROR_SEVERITY(),  
					@ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	END CATCH 	
END 

