
BEGIN  
DECLARE @UserID NVARCHAR(10) = '122963'  
Declare @tmpTable table (  
       ProcessId int,   
       Process varchar(255),   
       Description varchar(255),  
       Area varchar(255),   
       Controller varchar(255),   
       Action varchar(255),  
       ParentId int,   
       MainMenuOrder int,   
       SubMenuOrder int,   
       IconClass varchar(255),  
       IsDisplayInMenu bit  
      )  
INSERT @tmpTable   
Select * from dbo.FN_GET_GET_ALL_USER_ACCESS_MENU(@UserID)  
  
Declare @ATH011 table(Id int,ProcessId int,OrderNo int)  
insert into @ATH011  
Select Id,ProcessId,OrderNo from ATH011 where Employee=@UserID  
  /*
insert into ATH011(Employee,ProcessId,OrderNo,CreatedBy,CreatedOn)  
Select @UserID,tt.ProcessId,tt.MainMenuOrder,@UserID,GETDATE() from @tmpTable as tt  
LEFT JOIN @ATH011 as a11 on tt.ProcessId=a11.ProcessId  
where a11.ProcessId is null and ParentId=0  
  
delete from ATH011 where Employee = @UserID and Id in (  
Select Id from @ATH011 as a11  
LEFT JOIN (Select * from @tmpTable where ParentId=0) as tt on a11.ProcessId=tt.ProcessId --and a11.Employee=@UserID  
where tt.ProcessId is null  
)  
  */
  
Declare @DefaultMenuOrder bit=0  
Select @DefaultMenuOrder=cast(Value as bit) from CONFIG where [Key]='DefaultMenuOrder'  
SET @DefaultMenuOrder=ISNULL(@DefaultMenuOrder,1)  
  
delete from @tmpTable  
IF @DefaultMenuOrder=0  
BEGIN  
   
 INSERT @tmpTable   
 Select * from dbo.FN_GET_GET_ALL_USER_ACCESS_MENU(@UserID)  
  
 Select distinct * from (  
 Select tt.ProcessId,tt.Process,tt.Description,tt.Area,tt.Controller,tt.Action,tt.ParentId,tt.SubMenuOrder,tt.IconClass,  
 case when a11.OrderNo is null then tt.MainMenuOrder else a11.OrderNo end as MainMenuOrder,IsDisplayInMenu  
 from @tmpTable as tt  
 INNER JOIN @ATH011 as a11 on tt.ProcessId=a11.ProcessId --and a11.Employee=@UserID  
  
 union all  
  
 Select tt.ProcessId,tt.Process,tt.Description,tt.Area,tt.Controller,tt.Action,tt.ParentId,tt.SubMenuOrder,tt.IconClass,  
 case when a11.OrderNo is null then tt.MainMenuOrder else a11.OrderNo end as MainMenuOrder,IsDisplayInMenu  
 from @tmpTable as tt  
 LEFT JOIN @ATH011 as a11 on tt.ProcessId=a11.ProcessId --and a11.Employee=@UserID  
 where tt.ParentId in (Select ProcessId from @ATH011)  
 )as finalmenu   
 Order by MainMenuOrder,SubMenuOrder  
END  
ELSE  
BEGIN  
 Select * from dbo.FN_GET_GET_ALL_USER_ACCESS_MENU(@UserID)  
END  
  
END