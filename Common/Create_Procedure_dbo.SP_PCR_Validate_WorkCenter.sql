USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_WorkCenter]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_WorkCenter]
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
GO
ALTER PROCEDURE [dbo].[SP_PCR_Validate_WorkCenter]
   @PCRN NVARCHAR(100),
   @WCDeliver NVARCHAR(100)
AS
BEGIN 
   BEGIN TRY
	SET NOCOUNT ON
	DECLARE @Message NVARCHAR(MAX)
	DECLARE @SqlQuery NVARCHAR(MAX)
	DECLARE @LNLinkedServer NVARCHAR(100) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(100) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @Result AS TABLE([Count] INT)
	DECLARE @Count INT
	SET @SqlQuery = '	SELECT  
	                        COUNT(0) AS CNT
					   FROM [dbo].[COM002] AS C
					   INNER JOIN '+ @LNLinkedServer +'.[dbo].[tltlnt000'+ @LNCompanyId + '] AS A 
					ON A.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					INNER JOIN '+ @LNLinkedServer + '.dbo.ttcmcs065'+ @LNCompanyId + ' AS B 
					   ON B.[t_cadr] = A.[t_ladr] 
					   INNER JOIN [dbo].[PCR501] AS P ON P.[t_loca] COLLATE Latin1_General_100_CS_AS_KS_WS = C.[t_dimx]
					   WHERE P.[t_pcrn] = ''' + @PCRN + ''' AND B.[t_cwoc] COLLATE Latin1_General_100_CS_AS_KS_WS = ''' + @WCDeliver + ''''
	 INSERT INTO @Result([Count])
	 EXEC(@SqlQuery)
	 SELECT @Count = R.[Count] FROM @Result AS R
	 IF(@Count = 0)
	  BEGIN
	    SET @Message = 'Selected Work center ' + @WCDeliver
		SET @Message += ' does not belong to location for which Plate cutting request ' + @PCRN + ' was generated for'
	    RAISERROR(@Message, 12, 1)
	  END
     END TRY
	 BEGIN CATCH
	    DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
		SELECT  
			@ErrorMessage = ERROR_MESSAGE(),  
			@ErrorSeverity = ERROR_SEVERITY(),  
			@ErrorState = ERROR_STATE() 
		PRINT @ErrorMessage 
		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	 END CATCH
END

