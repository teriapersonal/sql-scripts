USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Nov 2nd, 2020
-- Description:	
-- =============================================

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_MultiOccurence_Screen_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_MultiOccurence_Screen_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Get_MultiOccurence_Screen_Data]
	@UserId NVARCHAR(20),
	@TableName NVARCHAR(100),
	@IsLNTable BIT,
	@IndexName NVARCHAR(100),
	@IndexColumnNames NVARCHAR(500),
	@TabularColumnNames NVARCHAR(MAX),
	@ViewColumnNames NVARCHAR(1000) = NULL,	
	@ViewColumnValues NVARCHAR(1000) = NULL,
	@DescriptionColumnNames NVARCHAR(1000) = NULL,
	@DescriptionColumnTableNames NVARCHAR(1000) = NULL,	
	@NavigatorIndex INT = NULL,
	@NavigatorType NVARCHAR(10) = NULL,
	@MaxRows INT = 1000000
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Collation NVARCHAR(100) = ' '
	DECLARE @Index INT = 1
	DECLARE @IsViewColumnsProvided BIT = 0
	DECLARE @TempTableName NVARCHAR(100) = 
		'tempdb..##' + 'temp'+ '_' +
		@UserId + '_' +
		@TableName + '_' +
		@IndexName
	DECLARE @SQL NVARCHAR(MAX)
	DECLARE @Iterator INT
	DECLARE @Total INT
	DECLARE @TotalIndexes INT
	DECLARE @ColName NVARCHAR(100)
	DECLARE @ViewColumnNamesTable AS TABLE(RowIndex INT PRIMARY KEY IDENTITY(1,1), ColName NVARCHAR(100))
	DECLARE @ViewColumnCurrentValuesTable AS TABLE(RowIndex INT PRIMARY KEY IDENTITY(1,1), ColName NVARCHAR(100))
	DECLARE @ViewColumnNewValuesTable AS TABLE(RowIndex INT PRIMARY KEY IDENTITY(1,1), ColName NVARCHAR(100))
	DECLARE @DescriptionColumnNamesTable AS TABLE(RowIndex INT PRIMARY KEY IDENTITY(1,1), ColName NVARCHAR(100))
	DECLARE @DescriptionColumnTableNamesTable AS TABLE(RowIndex INT PRIMARY KEY IDENTITY(1,1), ColName NVARCHAR(100))
	DECLARE @AliasColumnNamesTable AS TABLE(RowIndex INT PRIMARY KEY IDENTITY(1,1), AliasColName NVARCHAR(100))
	DECLARE @ValueTable AS TABLE(RowIndex INT PRIMARY KEY IDENTITY(1,1), [Value] INT)
	DECLARE @DescriptionColumnName NVARCHAR(100)
	DECLARE @DescriptionColumnTableName NVARCHAR(100)
	DECLARE @DescriptionValue NVARCHAR(1000)
	DECLARE @TableAlias NVARCHAR(10) = 'A'
	DECLARE @IsEmpty BIT = 1
    DECLARE @First BIT, @Prev BIT, @Next BIT, @Last BIT
	SET @IsViewColumnsProvided = CASE WHEN @ViewColumnNames IS NULL THEN 0 ELSE 1 END
	INSERT INTO @AliasColumnNamesTable SELECT @TableAlias + '.' + [Value] FROM [dbo].[fn_Split] (@IndexColumnNames, ',') 
    SET @IndexColumnNames = NULL
	SELECT @IndexColumnNames = COALESCE(@IndexColumnNames + ',', '') + [AliasColName] FROM @AliasColumnNamesTable
	DELETE FROM @AliasColumnNamesTable
	INSERT INTO @AliasColumnNamesTable SELECT @TableAlias + '.' + [Value] FROM [dbo].[fn_Split] (@TabularColumnNames, ',') 
    SET @TabularColumnNames = NULL
	SELECT @TabularColumnNames = COALESCE(@TabularColumnNames + ',', '') + [AliasColName] FROM @AliasColumnNamesTable	
	IF(@IsLNTable = 1)
	 BEGIN
	   DECLARE @LNLinkedServer NVARCHAR(100)
	   DECLARE @LNCompanyId NVARCHAR(10)	
	   SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer')
	   SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId')
	   SELECT @TableName = @LnLinkedServer + '.[dbo].[' + @TableName + @LNCompanyId + ']'
	   SET @Collation = ' COLLATE ' + 'Latin1_General_100_CS_AS_KS_WS'
	 END
	
	IF(@IsViewColumnsProvided = 1)
	 BEGIN
		DECLARE @SQL1 NVARCHAR(MAX) 
		DECLARE @SQL2 NVARCHAR(MAX) 
		DECLARE @SQL3 NVARCHAR(MAX) 
		DECLARE @SQL4 NVARCHAR(MAX) 
		DECLARE @SQL5 NVARCHAR(MAX)
		DECLARE @SQL6 NVARCHAR(MAX)

		IF(OBJECT_ID(@TempTableName) IS  NOT NULL)
		  BEGIN
			  EXEC ('DROP TABLE ' + @TempTableName)
		  END

		INSERT INTO @ViewColumnNamesTable
			SELECT [VALUE]
		FROM   
			[dbo].[fn_Split] (@ViewColumnNames, ',') 
		SELECT @Total = COUNT(0) FROM @ViewColumnNamesTable		
		SET @Iterator = 1
	
		IF(OBJECT_ID(@TempTableName) IS  NULL)
		  BEGIN
				SET @SQL = 'CREATE TABLE ' + @TempTableName + '(
					RowIndex INT IDENTITY(1,1) PRIMARY KEY, ' ;

				WHILE(@Iterator <= @Total)
				  BEGIN
					SELECT 
						@ColName = VCNT.ColName 
					FROM 
						@ViewColumnNamesTable VCNT 
					WHERE 
						VCNT.RowIndex = @Iterator
					SET @SQL +=  @ColName + ' NVARCHAR(100)';
					IF(@Iterator <= @Total - 1)
						BEGIN
							SET @SQL += ', '
						END
					SET @Iterator += 1
				  END
		
				SET @SQL += ')'
		
				EXEC (@SQL)
		  END
		SET @SQL1 = 'BEGIN DELETE FROM ' + @TempTableName 
		SET @SQL2 = ' INSERT INTO ' + @TempTableName  + '(' 
		SET @SQL3 = ' SELECT DISTINCT ';
		SET @Iterator = 1
		WHILE(@Iterator <= @Total) 
			BEGIN
				SELECT 
					@ColName = VCNT.ColName 
				FROM 
					@ViewColumnNamesTable VCNT 
				WHERE VCNT.RowIndex = @Iterator
					
				SET @SQL2 +=  @ColName ;
				SET @SQL3 +=  @ColName ;
				IF(@Iterator <= @Total - 1)
					BEGIN
						SET @SQL2 += ', '
						SET @SQL3 += ', '
					END
				SET @Iterator += 1
					
			END
		SET @SQL2 += ')'
		SET @SQL3 += ' FROM ' + @TableName + ' WITH (NOLOCK) ORDER BY '
		SET @SQL = @SQL1 + @SQL2  + @SQL3
		SET @Iterator = 1
		WHILE(@Iterator <= @Total) 
			BEGIN
			SELECT 
				@ColName = VCNT.ColName 
			FROM 
				@ViewColumnNamesTable VCNT 
			WHERE 
				VCNT.RowIndex = @Iterator
					
			SET @SQL +=  @ColName ;
			IF(@Iterator <= @Total - 1)
				BEGIN
					SET @SQL += ', '
				END
			SET @Iterator += 1
			END
		SET @SQL += ' END'				
		EXEC(@SQL)
		IF(@ViewColumnValues IS NOT NULL) 
		  BEGIN
			INSERT INTO @ViewColumnCurrentValuesTable
			SELECT 
				[VALUE]
			FROM   
				[dbo].[fn_Split] (@ViewColumnValues, ',') 
		   END
		ELSE
		   BEGIN
		     SET @Iterator = 1
			 SELECT @Total = COUNT(0) FROM @ViewColumnNamesTable
             WHILE(@Iterator <= @Total)
				BEGIN
					SELECT @ColName = ColName FROM @ViewColumnNamesTable WHERE RowIndex = @Iterator
					SET @SQL = ' SELECT ' + @ColName +  ' FROM ' + @TempTableName + ' WHERE RowIndex = 1'		
					INSERT INTO @ViewColumnCurrentValuesTable
					EXEC(@SQL)
		 
					SET @Iterator += 1
				END
		   END	
		
		SET @Iterator = 1
		SELECT @Total = COUNT(0) FROM @ViewColumnNamesTable
		SET @SQL = 'SELECT RowIndex FROM ' + @TempTableName + ' WHERE '
		DECLARE @ColValue NVARCHAR(100)
		WHILE(@Iterator <= @Total) 
			BEGIN
				SELECT 
					@ColName = VCNT.ColName 
				FROM 
					@ViewColumnNamesTable VCNT 
				WHERE 
					VCNT.RowIndex = @Iterator
			
				SELECT 
					@ColValue = VCVT.ColName 
				FROM 
					@ViewColumnCurrentValuesTable VCVT 
				WHERE 
					VCVT.RowIndex = @Iterator
					
				SET @SQL +=  ' CONVERT(NVARCHAR(100), ' + @ColName + ')' + ' = ''' + @ColValue + ''''
				IF(@Iterator <= @Total - 1)
					BEGIN
						SET @SQL += ' AND '
					END
				SET @Iterator += 1
			END
		
		INSERT INTO @ValueTable		
		EXEC(@SQL)
		SELECT @Index = [Value] FROM @ValueTable		
		DELETE FROM @ValueTable

		SET @SQL = 'SELECT COUNT(0) FROM ' + @TempTableName
		INSERT INTO @ValueTable
		EXEC(@SQL)		
		SELECT @TotalIndexes = [Value] FROM @ValueTable
		
		SELECT @NavigatorIndex = CASE 
								   WHEN UPPER(@NavigatorType) = 'NEXT' THEN @Index + 1
								   WHEN UPPER(@NavigatorType) = 'PREV' THEN @Index - 1
								   WHEN UPPER(@NavigatorType) = 'FIRST' THEN 1
								   WHEN UPPER(@NavigatorType) = 'LAST' THEN @TotalIndexes 
								   ELSE @Index END

		IF(@NavigatorIndex >= 1 AND @NavigatorIndex <= @TotalIndexes)
		 BEGIN		
		     SET @Iterator = 1
			 SELECT @Total = COUNT(0) FROM @ViewColumnNamesTable
             WHILE(@Iterator <= @Total)
				BEGIN
					SELECT @ColName = ColName FROM @ViewColumnNamesTable WHERE RowIndex = @Iterator
					SET @SQL = ' SELECT ' + @ColName +  ' FROM ' + @TempTableName + ' WHERE RowIndex = ' 
					SELECT @SQL = CONCAT(@SQL, @NavigatorIndex)
					INSERT INTO @ViewColumnNewValuesTable
					EXEC(@SQL)
					SET @Iterator += 1
				END
			SET @SQL1  = ' BEGIN DECLARE @TempTable AS TABLE(RowIndex INT IDENTITY(1,1) PRIMARY KEY, '
			SET @SQL2  = ' INSERT INTO @TempTable('
			SET @SQL3  = ' SELECT '
			SET @SQL6  = ' DECLARE @ViewColumnsTable AS TABLE(RowIndex INT IDENTITY(1,1) PRIMARY KEY, ColName NVARCHAR(100), ColValue NVARCHAR(100),  DescriptionValue NVARCHAR(100))' 
			
			SELECT @Total = COUNT(0) FROM @ViewColumnNamesTable
			SET @Iterator = 1			
			WHILE(@Iterator <= @Total) 
				BEGIN
					SELECT 
						@ColName = VCNT.ColName 
					FROM 
						@ViewColumnNamesTable VCNT 
					WHERE 
						VCNT.RowIndex = @Iterator
					
					SELECT 
						@ColValue = VCVT.ColName 
					FROM 
						@ViewColumnNewValuesTable VCVT 
					WHERE 
						VCVT.RowIndex = @Iterator

					SET @SQL1 += @ColName + ' NVARCHAR(100)' ;
					SET @SQL2 += @ColName
					SET @SQL3 += @ColName
					SET @SQL6 += 
					' INSERT INTO @ViewColumnsTable(ColName, ColValue, DescriptionValue) VALUES (''' + @ColName + '''' + ',''' + @ColValue + '''' + ',NULL)' 
					IF(@Iterator <= @Total - 1)
						BEGIN
							SET @SQL1 += ', '
							SET @SQL2 += ', '
							SET @SQL3 += ', '
						END
					SET @Iterator += 1
				END
			SET @SQL1 += ') '
			SET @SQL2 += ') '
			SET @SQL3 += ' FROM ' + @TempTableName + ' WHERE RowIndex = '
			SELECT @SQL3 = CONCAT(@SQL3, @NavigatorIndex)	 
			SET @SQL4 = ' ' --SELECT * FROM @ViewColumnsTable '
			SET @SQL5 = ' SELECT ' + @TabularColumnNames	
			SET @SQL5 +=  ' FROM ' +  @TableName + ' ' + @TableAlias + ' JOIN @TempTable T ON '
			SET @Iterator = 1
			SELECT @Total = COUNT(0) FROM @ViewColumnNamesTable
			WHILE(@Iterator <= @Total) 
				BEGIN
					SELECT 
						@ColName = VCNT.ColName 
					FROM 
						@ViewColumnNamesTable VCNT 
					WHERE 
						VCNT.RowIndex = @Iterator
					
				  
				  SET @SQL5 +=  'CONVERT(NVARCHAR(100), ' + @TableAlias + '.' + @ColName + ') = CONVERT(NVARCHAR(100), T.' + @ColName + ')' + @Collation 
				  IF(@Iterator <= @Total - 1)
					BEGIN
						SET @SQL5 += ' AND '
					END
				  SET @Iterator += 1
				END
				SET @SQL5 += ' ORDER BY ' + @IndexColumnNames
	  
				SET @SQL = @SQL1 + @SQL2 + @SQL3 + @SQL6 + @SQL4 + @SQL5 + ' END'
				
				EXEC(@SQL)
		        IF(OBJECT_ID(@TempTableName) IS  NOT NULL)
				  BEGIN
					  EXEC ('DROP TABLE ' +  @TempTableName)
				  END
				  SET @IsEmpty = 0				  
		   END
		END
	IF(@IsEmpty = 1 OR @IsViewColumnsProvided = 0)
	 BEGIN
	 IF(@IsEmpty = 1)
	   BEGIN
			 --SELECT RowIndex, ColName, NULL AS ColValue, NULL AS DescriptionValue FROM  @ViewColumnNamesTable
			 PRINT 1
	   END
     ELSE
		BEGIN
			SET @SQL = '' --SELECT 0 AS RowIndex, NULL AS ColName, NULL AS ColValue, NULL AS DescriptionValue '    
		END
	
		SET @SQL = ' SELECT ' + @TabularColumnNames + ' FROM ' + @TableName + ' ' + @TableAlias
		SET @SQL += ' ORDER BY ' + @IndexColumnNames + ' OFFSET ' 
		SET @SQL = CONCAT(@SQL, @NavigatorIndex) 
		SET @SQL = CONCAT(@SQL, ' ROWS  FETCH NEXT ') 
		SET @SQL = CONCAT(@SQL, @MaxRows) 
		SET @SQL = CONCAT(@SQL, ' ROWS ONLY') 
		PRINT(@SQL)
		EXEC(@SQL)		  
     END
	 	
	IF(@NavigatorIndex = 1)
	  BEGIN
		SET @First = 0
		SET @Prev = 0
		IF(@TotalIndexes > @NavigatorIndex)
			BEGIN
			SET @Last = 1
			SET @Next = 1
			END
		ELSE
			BEGIN
			SET @Last = 0
			SET @Next = 0
			END
	  END
	ELSE IF(@NavigatorIndex = @TotalIndexes)
	  BEGIN
		SET @Last = 0
		SET @Next = 0
		If(@TotalIndexes > 1)
		  BEGIN
			SET @First = 1
			SET @Prev = 1				
		  END
		ELSE 
		  BEGIN
			SET @First = 0
			SET @Prev = 0					
		  END
	  END	
	ELSE IF(@NavigatorIndex > 1 AND @NavigatorIndex < @TotalIndexes)
	  BEGIN
		SET @Last = 1
		SET @Next = 1
		SET @First = 1
		SET @Prev = 1				
	  END	
	--SELECT @First AS FirstView, @Prev AS PreviousView, @Next AS NextView,@Last AS LastView
	
END 


/*
Exec [SP_PCR_Get_MultiOccurence_Screen_Data]
	@TableName = 'ttppdm600',
	@IsLNTable = 1,
	@UserId = '122963',
	@IndexName='Index1',
	@IndexColumnNames = 't_cprj,t_cspa',
	
	@TabularColumnNames ='t_cprj,t_dsca',
	@NavigatorIndex =1,
	@ViewColumnValues='0027003W,20004',
	@NavigatorIndex =1	
	@NavigatorType='CURRENT'
	

	
	
	*/