USE [IEMQS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Oct 26th 2020
-- Description:	This procedure returns the frequency column value
-- =============================================

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_FREQUENCY]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_FREQUENCY]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
 
Alter Procedure [dbo].[SP_PCR_GET_FREQUENCY]
(
	 @Project NVARCHAR(20),
	 @Element NVARCHAR(100),
	 @LNLinkedServer NVARCHAR(100),
	 @LnCompanyId NVARCHAR(10)	
)
AS

BEGIN
	DECLARE @Sql NVARCHAR(MAX)
	SET 
	@Sql =
		'BEGIN
 			SELECT	 
				tbl.t_qant AS [Value] 
			FROM	' + @LNLinkedServer + '.dbo.ttpptc101' + @LNCompanyId + '
			AS tbl 
			WHERE 	
				tbl.t_cprj = ''' + @Project	+ '''
		          AND 
				tbl.t_cspc = ''' + @Element + '''		
		END '
  EXEC (@Sql)

END
