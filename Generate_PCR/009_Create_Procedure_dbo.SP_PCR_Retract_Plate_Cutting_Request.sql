USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Dec 11th 2020
-- Description:	
-- =============================================

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Retract_Plate_Cutting_Request]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Retract_Plate_Cutting_Request]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


Alter Procedure [dbo].[SP_PCR_Retract_Plate_Cutting_Request]
(
   @Id INT ,
   @Project NVARCHAR(100),
   @Element NVARCHAR(100),
   @PSNo NVARCHAR(100)
)
AS
 BEGIN
   DECLARE @TempTable As table(RowIndex INT IDENTITY(1,1), [t_pcrn] NVARCHAR(100), [t_pono] INT,[t_lsta] INT, [t_sern] INT, [t_seqn] INT)
   DECLARE @QuantityTable AS TABLE([Value] FLOAT)
   DECLARE @tbl911 AS TABLE([t_sern] INT, [t_leng] INT, [t_widt] INT, [t_noun] FLOAT)
   DECLARE @LNLinkedServer NVARCHAR(100)
   DECLARE @LNCompanyId NVARCHAR(10)
   DECLARE @Loca NVARCHAR(100)
   DECLARE @Sql NVARCHAR(MAX) 
   DECLARE @t_pcrn NVARCHAR(100)
   DECLARE @t_pono INT
   DECLARE @t_sern INT
   DECLARE @t_seq INT
   DECLARE @t_lsta INT
   DECLARE @t_mrmk NVARCHAR(MAX)
   DECLARE @Quantity FLOAT
   DECLARE @t_noun Float
   DECLARE @Diff Float
   DECLARE @TotalConsumed FLOAT
   DECLARE @PCRCreatedValue INT
   DECLARE @PCRReleasedToNestingValue INT
   DECLARE @PCLGeneratedValue INT
   DECLARE @PCLReleasedValue INT
   DECLARE @PCRDeletedValue INT
   DECLARE @LineCount INT
   BEGIN TRAN
     BEGIN TRY
	   SELECT @Loca = [t_loca] FROM [dbo].[COM003] WITH (NOLOCK) WHERE  [t_psno] = @PSNo
	   SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer')
       SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId')
	   INSERT INTO @TempTable([t_pcrn], [t_pono], [t_sern] , [t_seqn])
	   SELECT P.[t_pcrn], P.[t_pono], P.[t_sern], P.[t_seqn] FROM [dbo].[PCR506] AS P WHERE P.[Id] = @Id
	   SELECT @t_pcrn = T.[t_pcrn], @t_pono = T.[t_pono], @t_sern = T.[t_sern] , @t_seq = T.[t_seqn] FROM @TempTable AS T
	   SELECT @t_lsta = P.[t_lsta], @t_mrmk = P.[t_mrmk] FROM [dbo].[PCR503] AS P WHERE P.[t_pcrn] = @t_pcrn AND P.[t_pono] = @t_pono
	   SELECT
	    	TOP 1 @PCRCreatedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'CREATED'
	   SELECT
	    	TOP 1 @PCRReleasedToNestingValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.NEST'
	   SELECT
	    	TOP 1 @PCLGeneratedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'PCL.GEN'
	   SELECT
	    	TOP 1 @PCLReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'PCL.REL'
	   SELECT
	    	TOP 1 @PCRDeletedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'DELETE'

	   IF(@t_lsta IS NULL)
	    BEGIN
			RAISERROR('PCR cannot be retracted as it is not created yet.', 11, 1)
	    END
	   IF(@t_lsta <= @PCRCreatedValue)
	    BEGIN
			RAISERROR('PCR cannot be retracted as it is not released to material planner yet.', 11, 1)
	    END
		IF(@t_lsta = @PCRReleasedToNestingValue)
	    BEGIN
			RAISERROR('PCR cannot be retracted as it is released to nesting.', 11, 1)
	    END
	   IF(@t_lsta = @PCLGeneratedValue OR @t_lsta = @PCLReleasedValue)
	    BEGIN
			RAISERROR('PCL is Generated/Released,cannot retract PCR.', 11, 1)
	    END
		IF(@t_lsta = @PCRDeletedValue)
	    BEGIN
			RAISERROR('PCR cannot be retracted as it is deleted.', 11, 1)
	    END
       SELECT @LineCount = COUNT(0) FROM [dbo].[PCR503] WHERE [t_pcrn] = @t_pcrn
	   DELETE FROM [dbo].[PCR503] WHERE [t_pcrn] = @t_pcrn AND [t_pono] = @t_pono
	   SET @Sql = ' SELECT [t_qant] FROM ' + @LNLinkedServer + '.dbo.ttpptc101' + @LNCompanyId + ' WHERE [t_cprj] = ''' + @Project + ''' AND [t_cspc] = ''' + @Element + ''''
	   INSERT INTO @QuantityTable([Value])
	   EXEC(@Sql)
	   SELECT @Quantity = [Value] FROM @QuantityTable
	   SELECT @TotalConsumed = SUM(P.[t_npcs_m]) FROM [dbo].[PCR506] AS P WHERE P.[t_cprj] = @Project AND P.[t_cspa] = @Element 
	   AND P.[t_sern] = @t_sern AND P.[t_seqn] <> @t_seq AND P.[t_pcrn] IS NOT NULL AND LEN(P.[t_pcrn]) > 0
	   SET @Sql = 'SELECT P.[t_sern], P.[t_leng], P.[t_widt], P.[t_noun]  FROM ' + @LNLinkedServer + '.dbo.tltibd911' 
	   + @LNCompanyId + ' AS P WHERE P.[t_cprj] = ''' + @Project + ''' AND P.[t_cspa] = ''' + @Element + ''' AND P.[t_sern] = ''' 
	   SET @Sql = CONCAT(@Sql, @t_sern)
	   Set @Sql = CONCAT(@Sql, '''')
	   INSERT INTO @tbl911([t_sern], [t_leng], [t_widt], [t_noun])
	   EXEC(@Sql)

	   UPDATE tbl506
	     SET 
		 tbl506.[t_leng] = T.[t_leng],
		 tbl506.[t_widt] = T.[t_widt],
		 tbl506.[t_npcs] = T.[t_noun] * @Quantity,
		 tbl506.[t_leng_m] = T.[t_leng],
		 tbl506.[t_widt_m] = T.[t_widt],
         tbl506.[t_loca] = @Loca,
		 tbl506.[t_npcs_m] = CASE WHEN tbl506.[t_npcs] > @TotalConsumed THEN   tbl506.[t_npcs] - @TotalConsumed  ELSE 0 END,
		 tbl506.[t_pcrn] = NULL,
		 tbl506.[t_pono] = 0,
		 tbl506.[t_rtby] = @PSNo,
		 tbl506.[t_rtdt] = GETUTCDATE(),
		 tbl506.[t_rrmk] = @t_mrmk                                 
		FROM 
		 [dbo].[PCR506] AS tbl506			
	    JOIN @tbl911 AS T 
		 ON tbl506.[t_cprj] = @Project AND tbl506.[t_cspa] = @Element AND tbl506.[t_sern] = T.[t_sern]

		 DELETE FROM [dbo].[PCR506] WHERE [t_cprj] = @Project AND [t_cspa]= @Element AND [t_sern] = @t_sern AND [t_seqn] <> @t_seq AND [t_pcrn] IS NULL
		 DELETE FROM [dbo].[PCR501] WHERE [t_pcrn] = @t_pcrn AND @LineCount <= 1
	   COMMIT
	   SELECT 1
    END TRY
	BEGIN CATCH
	  IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	END CATCH
END 

