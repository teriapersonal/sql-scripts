USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Oct 27th 2020
-- Description:	
-- =============================================
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Get_ProjectList]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Get_ProjectList]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
--Exec [dbo].[SP_PCR_Get_ProjectList] '122963'
ALTER Procedure [dbo].[SP_PCR_Get_ProjectList]
(
	 @PSNo NVARCHAR(100)
)
AS

BEGIN
	DECLARE @Sql NVARCHAR(MAX); 
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)	
	SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer')
	SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId')
	DECLARE @ProjectStatusValue INT
	DECLARE @ProjectTypeValue INT
	DECLARE @V INT
	SELECT	
		TOP 1 @ProjectStatusValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Project Status','tppdm.psts') WHERE UPPER([LNConstant]) = 'CONSTRUCTION'
	SELECT
		TOP 1 @ProjectTypeValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Project Type','tppdm.kopr') WHERE UPPER([LNConstant]) = 'SUB'
	SELECT
		 @V = 1
	SET @SQL = 
	'SELECT PP.[t_cprj], PP.[Description], PP.[CodeDescription] FROM
	(
	Select [t_cprj], [t_dsca] AS [Description], [t_cprj] + '' - '' + [t_dsca] AS CodeDescription  FROM ' +
	@LNLinkedServer + '.dbo.ttppdm600' + @LNCompanyId + ' AS P WITH (NOLOCK) 
	WHERE P.[t_psts] = '
	SET @Sql = CONCAT(@Sql, @ProjectStatusValue)
	SET @Sql = CONCAT(@Sql, 
	' AND P.[t_kopr] = ')
	SET @Sql = CONCAT(@Sql, @ProjectTypeValue)
	SET @Sql = CONCAT(@Sql, 
	' ) AS PP
	WHERE PP.[t_cprj] 
	NOT IN 
	 (SELECT  P2.[t_sprj] FROM ' + @LNLinkedServer + '.dbo.tltctm108' + @LNCompanyId + ' AS P2 WHERE P2.[t_serl] = ')
	 SET @Sql = CONCAT(@Sql, @V)
	 SET @Sql = CONCAT(@Sql, 
	 ')')


	--PRINT(@SQL)
	EXEC(@SQl)
	
END
