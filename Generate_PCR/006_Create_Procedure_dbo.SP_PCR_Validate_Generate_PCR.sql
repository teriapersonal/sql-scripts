USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Nov 29th 2020
-- Description:	
-- =============================================

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Validate_Generate_PCR]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Validate_Generate_PCR]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


Alter Procedure [dbo].[SP_PCR_Validate_Generate_PCR]
(
   @ProjectIds NVARCHAR(MAX),
   @ElementIds NVARCHAR(MAX),   
   @BudgetLineIds NVARCHAR(MAX),
   @ChildItemIds NVARCHAR(MAX),
   @Seperator NVARCHAR(10)  
)
AS

BEGIN
	SET NOCOUNT ON
	DECLARE @Query NVARCHAR(MAX)
	DECLARE @IsValid BIT = 0
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)
	DECLARE @ProjectsTable AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(100)) 
	DECLARE @ElementsTable AS TABLE(RowIndex INT IDENTITY(1,1),[Value] NVARCHAR(100)) 
	DECLARE @BudgetLinesTable AS TABLE(RowIndex INT IDENTITY(1,1),[Value] NVARCHAR(100)) 
	DECLARE @ChildItemsTable AS TABLE(RowIndex INT IDENTITY(1,1),[Value] NVARCHAR(100))
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)
	DECLARE @BudgetLine NVARCHAR(MAX)
	DECLARE @ChildItem NVARCHAR(MAX)
	DECLARE @Item NVARCHAR(MAX)
	DECLARE @RowIndex INT
	DECLARE @Total INT
	DECLARE @IsValidTable Table ([Value] BIT)
	DECLARE @Message NVARCHAR(MAX)
	SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer')
	SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId')
	INSERT INTO @ProjectsTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@ProjectIds, @Seperator) 
	INSERT INTO @ElementsTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@ElementIds, @Seperator) 
	INSERT INTO @BudgetLinesTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@BudgetLineIds, @Seperator) 
	INSERT INTO @ChildItemsTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@ChildItemIds, @Seperator) 
   
	
	BEGIN TRY
	     
		 SET @RowIndex = 1
		 SELECT @Total = COUNT(0) FROM @ProjectsTable
		 WHILE(@RowIndex <= @Total)
			 BEGIN
				 SELECT @Project = [VALUE] FROM @ProjectsTable WHERE RowIndex = @RowIndex
				 SELECT @Element = [VALUE] FROM @ElementsTable WHERE RowIndex = @RowIndex
				 SELECT @BudgetLine = [VALUE] FROM @BudgetLinesTable WHERE RowIndex = @RowIndex
				 SELECT @ChildItem = [VALUE] FROM @ChildItemsTable WHERE RowIndex = @RowIndex
				 PRINT 'C' 
				 PRINT @ChildItem
				 SET @Query = 'DECLARE @Item NVARCHAR(MAX)'
				 SET @Query +=  ' SELECT Top 1 @Item = tbl911.[t_citm] FROM ' + @LNLinkedServer + '.dbo.' + 'tltibd911' + @LNCompanyId + ' AS tbl911 WITH (NOLOCK) 
			                	WHERE tbl911.[t_cprj] = ''' + @Project + ''' AND tbl911.[t_cspa] = ''' + @Element + ''' AND CONVERT(NVARCHAR(100), tbl911.[t_sern]) = ''' + @BudgetLine + ''''
				 SET @Query += ' PRINT @Item IF(LTRIM(RTRIM(@Item)) <> ''' + LTRIM(RTRIM(@ChildItem)) + ''') BEGIN SELECT 0 END ELSE BEGIN SELECT 1 END '
				 DELETE FROM @IsValidTable
		         INSERT INTO @IsValidTable
		         EXEC (@Query)
		         SELECT @IsValid = [Value] FROM @IsValidTable

				 IF(@IsValid = 0)
				  BEGIN
				    SET @Message = CONCAT('Cant Generate PCR.\nReason : Mismatch in Budget Line Item ' , @BudgetLine)
					SET @Message = CONCAT(@Message,' and PCR Item')
		   			RAISERROR(@Message, 11, 1) 
				  END
				SET @RowIndex += 1
			END
			SELECT 1
	END TRY 
	BEGIN CATCH 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
					@ErrorMessage = ERROR_MESSAGE() ,  
					@ErrorSeverity = ERROR_SEVERITY(),  
					@ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
	END CATCH 
	
END 

/*
USE [IEMQS]
GO
DECLARE @Result INT
DECLARE @ProjectIds NVARCHAR(MAX) = '0017005B,0017005B'
DECLARE @ElementIds NVARCHAR(MAX) = '30006,30006'   
DECLARE @BudgetLineIds NVARCHAR(MAX) = '118,120'
DECLARE @ChildItems NVARCHAR(MAX) = '21470-A027,21470-A029'
DECLARE @Seperator NVARCHAR(10) = ',' 

EXECUTE [dbo].[SP_PCR_Validate_Generate_PCR]   @ProjectIds,   @ElementIds,   @BudgetLineIds,   @ChildItems,   @Seperator
GO



*/