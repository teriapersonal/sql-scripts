USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Oct 27th 2020
-- Description:	
-- =============================================
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_IS_PLATE]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_IS_PLATE]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO

ALTER Procedure [dbo].[SP_PCR_IS_PLATE]
(
	 @Item NVARCHAR(50),
	 @Pcrn NVARCHAR(9),
	 @Location NVARCHAR(9),
	 @LNLinkedServer NVARCHAR(100),
	 @LnCompanyId NVARCHAR(10)
)
AS

BEGIN
	DECLARE @Sql NVARCHAR(MAX); 
	
	SET @Sql =
	'BEGIN
	  DECLARE @PType NVARCHAR(100)  
	  DECLARE @CType NVARCHAR(100)
	  DECLARE @Loc NVARCHAR(9)
	  DECLARE @Returned BIT
	  SET @Returned = 0
	  SET @Loc = ' + '''' + @Location + '''
	  IF(@Loc IS NULL OR LTRIM(RTRIM(@Loc)) = '''') 
	    BEGIN
	     SELECT TOP 1 @Loc = tbl.[t_loca] FROM ' + @LNLinkedServer + '.[dbo].[tltsfc501' + @LNCompanyId + '] AS tbl WHERE tbl.[t_pcrn] = ''' + @Pcrn + '''
	    END 
	  SELECT TOP 1 @PType = tbl.[t_ptyp] FROM ' + @LNLinkedServer + '.[dbo].[tltlnt000' + @LNCompanyId + '] AS tbl WHERE tbl.[t_dimx] = @Loc 
	  IF(@PType IS NOT NULL AND LTRIM(RTRIM(@PType)) <> '''')
	    BEGIN
		  SELECT TOP 1 @CType = tbl.[t_ctyp] FROM ' + @LNLinkedServer + '.[dbo].[ttcibd001' + @LNCompanyId + '] AS tbl WHERE tbl.[t_item] = ''' + @Item + '''
		  SELECT @Returned = CASE WHEN @PType = @CType THEN 1 ELSE 0 END
	     END 	  	
	  SELECT @Returned AS [Value]
	 END ' 
	
  EXEC(@Sql)

END
