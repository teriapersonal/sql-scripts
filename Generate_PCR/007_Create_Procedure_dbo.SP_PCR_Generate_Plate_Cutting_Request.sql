USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Nov 30th 2020
-- Description:	
-- =============================================

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Generate_Plate_Cutting_Request]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Generate_Plate_Cutting_Request]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
--Exec [dbo].[SP_PCR_Generate_Plate_Cutting_Request] '1664',',','122963'

Alter Procedure [dbo].[SP_PCR_Generate_Plate_Cutting_Request]
(
   @Ids NVARCHAR(MAX) ,
   @Seperator NVARCHAR(10),
   @PSNo NVARCHAR(100)
)
AS

BEGIN
	SET NOCOUNT ON
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)
	DECLARE @IdsTable AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(1000)) 
	DECLARE @ValueTable Table ([Value] NVARCHAR(100))
	DECLARE @LocTable Table ([Value] NVARCHAR(100))
	DECLARE @FrequencyTable AS TABLE([Value] INT) 
	DECLARE @ThicknessTable Table ([Value] INT)
	DECLARE @Project NVARCHAR(100)
	DECLARE @Element NVARCHAR(100)
	DECLARE @SerialNumber INT
    DECLARE @Sql NVARCHAR(MAX)
	DECLARE @Value NVARCHAR(1000)
	DECLARE @Loc NVARCHAR(100)
	DECLARE @NumberOfPieces INT
	DECLARE @TotalConsumed INT	
    DECLARE @Frequency INT
	DECLARE @PCRN NVARCHAR(100)	
	DECLARE @PonoIncrement INT = 10
	DECLARE @Pono INT = 0
	DECLARE @ChildItem NVARCHAR(MAX)	
	DECLARE @Id INT
	DECLARE @NextSeq INT
	DECLARE @Diff INT
	DECLARE @CharValue NVARCHAR(9) 
	DECLARE @CharLength INT 
	DECLARE @StartLength INT = 0
	DECLARE @RowIndex INT
	DECLARE @Total INT
	DECLARE @Thickness INT
	DECLARE @EarlierValue NVARCHAR(9)
    DECLARE @MaxValue INT
	DECLARE @Message NVARCHAR(MAX)
	DECLARE @ReleasedValue INT
	DECLARE @PCRTDrawValue INT
	DECLARE @ItemTypePlateValue INT
	SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer')
	SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId')
	INSERT INTO @IdsTable
	SELECT [VALUE] FROM   
			[dbo].[fn_Split] (@Ids, @Seperator) 
	
	BEGIN TRAN
	   BEGIN TRY	     
		 
	      SELECT
	    	TOP 1 @ReleasedValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Line Status','ltpcr.lsta') WHERE UPPER([LNConstant]) = 'REL.MPLN'
	
	      SELECT
	    	TOP 1 @PCRTDrawValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('PCR Type','ltsfc.pcr.type') WHERE UPPER([LNConstant]) = 'DRAW'
	      
		  SELECT 
		    TOP 1 @Loc = [t_loca] FROM [dbo].[PCR506] WITH (NOLOCK) WHERE Id IN (SELECT CONVERT(INT, [Value]) FROM @IdsTable) 
		  
		  SELECT 
	        TOP 1 @ItemTypePlateValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PLATE'
		   --SELECT @Loc
		   SET @Sql = ' SELECT [t_pser] FROM ' + @LNLinkedServer + '.dbo.' + 'tltlnt000' + @LNCompanyId + ' AS A WITH (NOLOCK) WHERE [t_dimx] = ''' + @Loc + ''''
		   --PRINT(@Sql)
		   
		   INSERT INTO @ValueTable
		   EXEC (@Sql)
		   SELECT TOP 1 @Value = [Value] FROM @ValueTable
		   --TODO PCRN Computation
		   DELETE FROM @ValueTable
		   SET @Value = LEFT(@Value, 3)
		   --PRINT(@Value)
		   IF EXISTS (SELECT [t_pcrn] FROM [dbo].[PCR501] WHERE [t_pcrn] LIKE @Value + '%')
		   BEGIN
		     SELECT Top 1 @EarlierValue = [t_pcrn] FROM [dbo].[PCR501] WHERE [t_pcrn] LIKE @Value + '%' ORDER BY [t_pcrn] DESC
			 --PRINT(@EarlierValue)
			 SET @EarlierValue = RIGHT(@EarlierValue, 6)
			 --PRINT(@EarlierValue)
			 SELECT @MaxValue = CONVERT(INT, @EarlierValue)
			 PRINT(@MaxValue)
			 SET @MaxValue = @MaxValue + 1
			 SET @CharValue = CONVERT(NVARCHAR, @MaxValue)
			 SET @CharLength = LEN(@CharValue)
			 SET @PCRN = @Value
			 while(@StartLength < 6 - @CharLength)
			 BEGIN
			   SET @PCRN = CONCAT(@PCRN, '0')
			   SET @StartLength += 1
			 END
			 SET @PCRN = CONCAT(@PCRN, @MaxValue)
		   END
		   ELSE
		     BEGIN
			   SET @PCRN = CONCAT(@Value, '000001')
			 END
		   INSERT INTO [dbo].[PCR501]([t_pcrn], [t_cdat], [t_init], [t_loca])
		   SELECT @PCRN, GETUTCDATE(), @PSNo, @Loc		   
		   
		   SET @RowIndex = 1
		   SELECT @Total = COUNT(0) FROM @IdsTable
		   WHILE(@RowIndex <= @Total)
			 BEGIN
			     SET @Pono = @Pono + @PonoIncrement
				 SELECT @Id = CONVERT(INT, [Value]) FROM @IdsTable WHERE RowIndex = @RowIndex
				 SELECT @ChildItem = [t_sitm], @NumberOfPieces = [t_npcs] FROM [dbo].[PCR506] WITH (NOLOCK) WHERE [Id] = @Id
				 SELECT @TotalConsumed = SUM([t_npcs_m]) FROM [dbo].[PCR506] WITH (NOLOCK) WHERE [Id] = @Id
				 DELETE FROM @ThicknessTable				 				 
				 SET @Sql = ' SELECT [t_thic] FROM ' + @LNLinkedServer + '.dbo.' + 'tltibd903' + @LNCompanyId + ' WITH (NOLOCK) WHERE [t_item] = ''' + @ChildItem + ''''
				 --PRINT(@Sql)
				 INSERT INTO @ThicknessTable				 
				 EXEC(@Sql)
				 SELECT TOP 1 @Thickness = [Value] FROM @ThicknessTable 
				 INSERT INTO [dbo].[PCR503](
					t_pcrn, t_pono, t_revn, t_leng, t_widt, t_npcs, t_pcrt, 
					t_prtn, t_mitm, t_sitm, t_revi, t_cprj, t_cspa,
					t_thic, t_pcr_crdt, t_prdt, t_cwoc, t_prmk, t_pcr_rldt, t_lsta, t_area, t_ityp, t_init
				 )
				 SELECT 
				 @PCRN, @Pono, 0, P.[t_leng_m], P.[t_widt_m], P.[t_npcs_m], P.[t_pcrt], P.[t_sern], 
				 P.[t_mitm], P.[t_sitm], P.[t_revi], P.[t_cprj], P.[t_cspa], 
				 @Thickness, GETUTCDATE(), P.[t_prdt], P.[t_cwoc], P.[t_prmk], 
				 GETUTCDATE(), @ReleasedValue, (P.[t_leng_m] * P.[t_widt_m])/1000000, P.[t_ityp], @PSNo
				 FROM [dbo].[PCR506] AS P WITH (NOLOCK) 
				 WHERE P.Id = @Id
        		 
				 UPDATE [dbo].[PCR501] SET [t_rdat] = GETUTCDATE() WHERE [t_pcrn] = @PCRN
				 UPDATE [dbo].[PCR506] SET [t_pcrn] = @PCRN, [t_pono] = @Pono WHERE [Id] = @Id
				 SET @Diff = @NumberOfPieces - @TotalConsumed
				 IF(@Diff > 0)
				  BEGIN				   				   
				   SELECT 
				   @Project = P.[t_cprj], 
				   @Element = [t_cspa], 
				   @SerialNumber = [t_sern] 
				   FROM [dbo].[PCR506] AS P WITH (NOLOCK) 
				   WHERE P.[Id] = @Id
				   
				   SELECT TOP 1 @NextSeq = P.[t_seqn] 
				   FROM [dbo].[PCR506] AS P WITH (NOLOCK) 
				   WHERE 
				   P.[t_cprj] = @Project 
				   AND 
				   P.[t_cspa] = @Element 
				   AND 
				   P.[t_sern] = @SerialNumber
				   ORDER BY P.[t_seqn] DESC
				   
				   IF(@NextSeq IS NULL)
				     BEGIN
					  SET @NextSeq = 1
					 END
					ELSE
					 BEGIN
					  SET @NextSeq = @NextSeq + 1
					 END					
				    SET @Sql =
						'DECLARE @Proj NVARCHAR(100) = ' + '''' + @Project + '''
						 DECLARE @Elem NVARCHAR(100) = ' + '''' + @Element + '''
						 EXEC [IEMQS].[dbo].[SP_PCR_GET_FREQUENCY]  @Proj, @Elem, ''' + @LNLinkedServer + '''' + ',' + '''' + @LNCompanyId + ''''
					DELETE FROM @FrequencyTable
					INSERT INTO @FrequencyTable EXEC(@Sql)
					SELECT @Frequency = [Value] FROM @FrequencyTable	
					--PRINT(@Frequency)
					SET @Sql = 
					  'SELECT TOP 1 ''' + @Project + ''', ''' + @Element + ''''
					  SET @Sql = CONCAT(@Sql,', ' )
					  SET @Sql = CONCAT(@Sql, @SerialNumber)
					  SET @Sql = CONCAT(@Sql, ', ')
					  SET @Sql = CONCAT(@Sql, @NextSeq)
					  SET @Sql = CONCAT(@Sql, ', ')
					  SET @Sql = CONCAT(@Sql, ' GETUTCDATE(), ''' + @PSNo + ''', tbl911.[t_citm], tbl911.[t_crev], tbl911.[t_pitm], 
					  tbl911.[t_leng], tbl911.[t_widt], ' + 'CONVERT(NVARCHAR(100), tbl911.[t_noun] * ')
					  SET @Sql = CONCAT(@Sql, @Frequency)
					  SET @Sql = CONCAT(@Sql, '), tbl911.[t_leng], tbl911.[t_widt], '); 
					  SET @Sql = CONCAT(@Sql, @Diff)					  
					  SET @Sql = CONCAT(@Sql,  ', NULL, 0,')
					  SET @Sql = CONCAT(@Sql, @PCRTDrawValue)
					  SET @Sql = CONCAT(@Sql, ', NULL, NULL, NULL, ' + 
					  '''' + @Loc + '''' + ', ' + 
					  '''' + @PSNo + '''' + ', ')
					  SET @Sql = CONCAT(@Sql, @ItemTypePlateValue)
					  SET @Sql = CONCAT(@Sql, ' FROM ' + @LNLinkedServer + '.dbo.tltibd911' + @LNCompanyId + ' AS tbl911 WITH (NOLOCK) ')
					  SET @Sql = CONCAT(@Sql, 
					  ' WHERE tbl911.[t_cprj] = ''' + @Project + ''' AND tbl911.[t_cspa] = ''' +  @Element + ''' AND ')
					  SET @Sql = CONCAT(@Sql, 'CONVERT(NVARCHAR(100), tbl911.[t_sern]) = '''+ CONVERT(NVARCHAR(100), @SerialNumber) + '''')
					  PRINT(@Sql)
					  INSERT INTO [dbo].[PCR506](
					    [t_cprj], [t_cspa], [t_sern], [t_seqn], CreatedOn, CreatedBy, [t_sitm],
						[t_revi], [t_mitm], [t_leng], [t_widt], [t_npcs], [t_leng_m], [t_widt_m], [t_npcs_m], 
					    [t_pcrn], [t_pono],  [t_pcrt], [t_prdt], [t_cwoc], [t_prmk], [t_loca], [t_init], [t_ityp])
					  EXEC(@Sql)
				 END                               
           		 SET @RowIndex += 1
			END
		COMMIT TRAN
		SELECT 1
	END TRY 
	BEGIN CATCH 
	   IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRAN 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
					@ErrorMessage = ERROR_MESSAGE() ,  
					@ErrorSeverity = ERROR_SEVERITY(),  
					@ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	END CATCH 	
END 

