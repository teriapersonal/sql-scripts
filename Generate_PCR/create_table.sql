USE [Test4]
GO

/****** Object:  Table [dbo].[Table_5]    Script Date: 11/4/2020 9:47:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Table_5](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ViewCol1] [int] NOT NULL,
	[ViewCol2] [int] NOT NULL,
	[NormalCol1] [nvarchar](50) NULL,
	[NormalCol2] [nvarchar](50) NULL,
 CONSTRAINT [PK_Table_5] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


