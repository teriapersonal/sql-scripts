USE [IEMQS]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Oct 27th 2020
-- Description:	
-- =============================================

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GENERATE_PCR_ACTIONS_FETCH_DATA]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GENERATE_PCR_ACTIONS_FETCH_DATA]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO


Alter Procedure [dbo].[SP_PCR_GENERATE_PCR_ACTIONS_FETCH_DATA]
(
   @Project NVARCHAR(100),
   @PSNo NVARCHAR(100)  
)
AS

BEGIN
	SET NOCOUNT ON
	DECLARE @ElementsTable AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(100),[Description] NVARCHAR(500))
	DECLARE @ValidElementsTable AS TABLE(RowIndex INT IDENTITY(1,1), [Value] NVARCHAR(100))
    DECLARE @TempElementTable AS TABLE(Element NVARCHAR(100))
	DECLARE @Element NVARCHAR(100)
	DECLARE @Index INT = 1
	DECLARE @TotalElements INT = 10
	DECLARE @LNLinkedServer NVARCHAR(100)
	DECLARE @LNCompanyId NVARCHAR(10)
	DECLARE @Frequency INT
	DECLARE @FoundRowCount INT
	DECLARE @T_Dimx NVARCHAR(100) = NULL 
	DECLARE @RowIndexInSource INT, @TotalRowsInSource INT, @IsValid BIT 
	DECLARE @PCRTypeValue INT 
	DECLARE @Sum_Value FLOAT, @T_Seqn_Value INT 
	DECLARE @RowIndexInTempTable INT, @TotalRowsInTempTable INT 
	DECLARE @T_Cprj NVARCHAR(9), @T_Cspa NVARCHAR(8) , @T_Sern INT, @T_Seqn INT  	
	DECLARE @T_Citm NVARCHAR(47), @T_Leng FLOAT, @T_Widt FLOAT, @T_Npcs INT 
	DECLARE @TotalConsumed INT 
	DECLARE @Sql NVARCHAR(MAX)	
	DECLARE @InsertRecordCount INT = 0
	DECLARE @UpdateRecordCount INT = 0
	DECLARE @Loc NVARCHAR(100)
	DECLARE @ReleasedWorkAuthStatusValue INT
	DECLARE @ActualStatusValue INT
	DECLARE @V INT
	DECLARE @ItemTypePlateValue INT
	
 BEGIN TRAN
   BEGIN TRY 
	SELECT @LNLinkedServer = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer')
	SELECT @LNCompanyId = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId')
	SELECT @Loc = [t_loca] FROM COM003 WITH (NOLOCK) WHERE  [t_psno] = @PSNo
	SELECT
		TOP 1 @ReleasedWorkAuthStatusValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Work Authorization Status','tppdm.wast') WHERE UPPER([LNConstant]) = 'RELEASED'
	SELECT
		TOP 1 @ActualStatusValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Status Technical Calculation','tpptc.stat') WHERE UPPER([LNConstant]) = 'ACTUAL'
	SELECT
		TOP 1 @V = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Yes No Choice','tppdm.yeno') WHERE UPPER([LNCONSTANT]) = 'NO'
    SELECT 
	    TOP 1 @ItemTypePlateValue = [Value] FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS('Item Type','ltsfc.ityp') WHERE UPPER([LNConstant]) = 'PLATE'
	SELECT 
		@PCRTypeValue = tblEnum.[Value]  
	FROM 
		[dbo].[PCR000] AS tblEnum  
	WHERE  
		UPPER(tblEnum.LNTableName)  = 'LTSFC506' AND UPPER(tblEnum.LNDomainName) = 'LTSFC.PCR.TYPE'  
		AND UPPER(tblEnum.LNConstant) ='DRAW' 

    SET @Sql = 'SELECT DISTINCT A.[t_cspa], A.[t_desc] FROM ' + @LNLinkedServer + '.dbo.ttpptc100' + @LNCompanyId + ' AS A '
	  + ' JOIN ' + @LNLinkedServer + '.dbo.ttpptc120' + @LNCompanyId + ' AS B ON B.[t_cprj] = A.[t_cprj]   WHERE  ' +
	    ' A.[t_wast] = ' 
		SET @Sql = CONCAT(@Sql, @ReleasedWorkAuthStatusValue)
		SET @Sql = CONCAT (@Sql, '  AND A.[t_stat] = ')
		SET @Sql = CONCAT(@Sql, @ActualStatusValue)
		SET @Sql = CONCAT(@Sql, ' AND A.[t_cctr] <> ')
		SET @Sql = CONCAT(@Sql, @V)
		SET @Sql = CONCAT(@Sql, +' AND A.[t_cprj] = ''' + @Project + '''')
	 
	 INSERT INTO @ElementsTable([Value], [Description])
	 EXEC(@Sql)
	 --PRINT(@Sql)
     
	 SELECT @TotalElements = COUNT(0) FROM @ElementsTable
	
	 WHILE(@Index <= @TotalElements)
	  BEGIN
	    SELECT @Element = [Value] FROM @ElementsTable WHERE RowIndex = @Index
	    SET @Sql = 
		' SELECT [t_cspa] FROM ' + @LNLinkedServer + '.dbo.ttppdm600' + @LNCompanyId + ' WHERE [t_cprj] = ''' + @Project + ''' AND [t_cspa] = ''' + @Element + '''' 
	    
		DELETE FROM @TempElementTable
		INSERT INTO @TempElementTable
		EXEC(@Sql)
		SELECT @FoundRowCount = COUNT(0) FROM @TempElementTable
		IF (@FoundRowCount = 0)
		 BEGIN
		  INSERT INTO @ValidElementsTable([Value])
		  SELECT @Element
		 END
	   SET @Index += 1
	 END
     
	 SELECT @TotalElements = COUNT(0) FROM @ValidElementsTable
	 SET @Index = 1
	 --SELECT * FROM @ValidElementsTable 
	 WHILE(@Index <= @TotalElements)
	  BEGIN	   
	   DECLARE @FrequencyTable AS TABLE([Value] INT) 
	   DECLARE @DimxTable AS TABLE([Value] NVARCHAR(100)) 
	   DECLARE @SourceTable TABLE( 
		 [index] INT PRIMARY KEY IDENTITY(1,1) NOT NULL, [t_cprj] NVARCHAR(9) NOT NULL, [t_cspa] NVARCHAR(8) NOT NULL, 
		 [t_sern] INT NOT NULL, [t_citm] NVARCHAR(47) NOT NULL, [t_crev] NVARCHAR(6) NOT NULL, [t_pitm] NVARCHAR(47) NOT NULL, 
		 [t_leng] FLOAT NOT NULL, [t_widt] FLOAT NOT NULL, [t_noun] NVARCHAR(100) NULL 
	   )
	   DECLARE @IsValidTable AS TABLE([Value] INT)
	   DECLARE @TempTable TABLE( 
		 [index] INT IDENTITY(1,1) PRIMARY KEY, 
		 [t_npcs_m] FLOAT, [t_sern] NVARCHAR(100), 
		 [t_seqn] NVARCHAR(100), [sum] FLOAT 
	   )
	   SELECT @Element = [Value] FROM @ValidElementsTable WHERE RowIndex = @Index
	  
	   SET @Sql =
		'DECLARE @Proj NVARCHAR(100) = ' + '''' + @Project + '''
		 DECLARE @Elem NVARCHAR(100) = ' + '''' + @Element + '''
		 EXEC [IEMQS].[dbo].[SP_PCR_GET_FREQUENCY]  @Proj, @Elem, ''' + @LNLinkedServer + '''' + ',' + '''' + @LNCompanyId + ''''
       DELETE FROM @FrequencyTable
	   INSERT INTO @FrequencyTable EXEC(@Sql)
       SELECT @Frequency = [Value] FROM @FrequencyTable	
	   SET @Sql =   
		'SELECT Top 1 tbl000.[t_dimx] FROM ' + @LnLinkedServer + '.[dbo].[tltlnt000' + @LNCompanyId + '] AS tbl000 WITH (NOLOCK) WHERE 
		tbl000.[t_ptyp] <> '''''
       DELETE FROM @DimxTable
	   INSERT INTO @DimxTable EXEC(@Sql)
       SELECT @T_Dimx = [Value] FROM @DimxTable	
	   --SELECT @T_Dimx AS DIMX	   
	   	  
	   IF(@T_Dimx IS NULL) 
		 BEGIN 
			RAISERROR ('Parameter not maintain for ''Product Type ''', 11, 1) 
		 END
		 SET @Sql =
			   'DECLARE @Proj NVARCHAR(100) = ' + '''' + @Project + '''
				DECLARE @Elem NVARCHAR(100) = ' + '''' + @Element + '''			
				SELECT 
					tbl911.[t_cprj], tbl911.[t_cspa], tbl911.[t_sern], 
			  		tbl911.[t_citm], tbl911.[t_crev], tbl911.[t_pitm], 
					tbl911.[t_leng], tbl911.[t_widt], tbl911.[t_noun] 
			FROM ' + 
		 		@LNLinkedServer + '.[dbo].[tltibd911' + @LnCompanyId + '] AS tbl911 WITH (NOLOCK) 
			WHERE  
				tbl911.[t_cprj] = @Proj AND tbl911.[t_cspa] = @Elem '
		  DELETE FROM @SourceTable
		  INSERT INTO @SourceTable( 
				 [t_cprj], [t_cspa], [t_sern],[t_citm],[t_crev],[t_pitm], [t_leng], [t_widt],[t_noun] 
		   )
		  EXEC(@Sql)
			   
		  SELECT @TotalRowsInSource = COUNT(0) FROM @SourceTable 
		  SELECT @RowIndexInSource = 1 
		  WHILE @RowIndexInSource <= @TotalRowsInSource  
			    BEGIN
				 SELECT 
					@T_Citm = tblSource.[t_citm] 
				 FROM 
					@SourceTable AS tblSource WHERE tblSource.[index] = @RowIndexInSource 
				--SELECT @T_Citm AS T_CITM
				 DELETE FROM @IsValidTable
				 INSERT INTO 
					@IsValidTable 
				 EXEC [IEMQS].[dbo].[SP_PCR_IS_PLATE] @T_Citm, '', @T_Dimx, @LnLinkedServer, @LNCompanyId  
				
				 SELECT @IsValid = [Value] FROM @IsValidTable 
				 --SELECT @IsValid AS IsValid
				 IF(@IsValid = 1) 
					BEGIN 
						SELECT  
							@T_Cprj = tblSource.[t_cprj], 
							@T_Cspa = tblSource.[t_cspa], 
							@T_Sern = tblSource.[t_sern] 
						FROM   
							@SourceTable AS tblSource  
						WHERE  
							tblSource.[index] = @RowIndexInSource  
						IF NOT EXISTS(  
							SELECT 0 
							FROM 
								[dbo].[PCR506] AS tbl506  
							WHERE  
								tbl506.[t_cprj] = @Project AND tbl506.[t_cspa] = @Element AND tbl506.[t_sern] = @T_Sern   
								AND tbl506.[t_pcrn] IS NULL 
						) --Insert 
						 
							BEGIN	 	
								INSERT INTO [dbo].[PCR506] 
									( 
										[t_cprj], [t_cspa], [t_sern], [t_seqn], [t_sitm], [t_revi],  
										[t_mitm], [t_leng], [t_widt], [t_npcs], [t_leng_m], [t_widt_m],  
										[t_npcs_m], [t_loca], [t_init], [t_pcrn], [t_pono] , [t_ityp], CreatedBy, CreatedOn
									) 
								SELECT 
										tblSource.[t_cprj], tblSource.[t_cspa], tblSource.[t_sern], 1, tblSource.[t_citm], tblSource.[t_crev],  
										tblSource.[t_pitm], tblSource.[t_leng], tblSource.[t_widt], tblSource.[t_noun] * @Frequency, tblSource.[t_leng], tblSource.[t_widt],  
										tblSource.[t_noun] * @Frequency, @Loc, @PSNo, NULL, 0, @ItemTypePlateValue, @PSNo, GETUTCDATE() 
								FROM
										@SourceTable AS tblSource  
								WHERE  
										tblSource.[t_cprj] = @T_Cprj AND  tblSource.[t_cspa] = @T_Cspa 
										AND tblSource.[t_sern] = @T_Sern	
										SET @InsertRecordCount += 1 										
							END	
						ELSE --Update 
							BEGIN 
								SELECT 
	  								@T_Leng = tblSource.[t_leng], @T_Widt = tblSource.[t_widt], @T_Npcs = tblSource.[t_noun] * @Frequency 
								FROM  
									@SourceTable AS tblSource  
								WHERE  
									tblSource.[index] = @RowIndexInSource 
			                    DELETE FROM @TempTable
								INSERT INTO  
									@TempTable([t_npcs_m], [t_sern], [t_seqn], [sum]) 
								SELECT  
									tbl506.[t_npcs_m], tbl506.[t_sern], tbl506.[t_seqn], 0  
								FROM  
									[dbo].[PCR506] AS tbl506  
								WHERE  
									tbl506.[t_cprj] = @Project AND tbl506.[t_cspa] = @Element 
									AND tbl506.[t_sern] = @T_Sern  AND tbl506.[t_pcrn] IS NULL 
								
								SELECT @TotalRowsInTempTable = COUNT(0) FROM @TempTable 
								SET @RowIndexInTempTable = 1 
								WHILE(@RowIndexInTempTable <= @TotalRowsInTempTable)  
									BEGIN 
										SELECT 
											@T_Seqn_Value = T.[t_seqn] 
										FROM 
											@TempTable AS T 
										WHERE 
											T.[index] = @RowIndexInTempTable 
										SELECT 
											@Sum_Value =  SUM(T.[t_npcs_m]) 
										FROM 
											@TempTable AS T 
										WHERE 
											T.[t_seqn] <> @T_Seqn_Value 
										
										UPDATE @TempTable 
											SET [sum] = ISNULL(@Sum_Value, 0) 
										WHERE 
											[index] = @RowIndexInTempTable 
										
										SET @RowIndexInTempTable += 1 
									END
								UPDATE tbl506   
									SET 
									tbl506.[t_leng] = @T_Leng, tbl506.[t_widt] = @T_Widt, tbl506.[t_npcs] = @T_Npcs, 
									tbl506.[t_leng_m] = 
										CASE WHEN tbl506.[t_pcrt] = @PCRTypeValue 
											THEN 
												@T_Leng 
											ELSE 
												tbl506.[t_leng_m] 
											END, 
									tbl506.[t_widt_m] = 
										CASE WHEN tbl506.[t_pcrt] = @PCRTypeValue 
											THEN 
												@T_Widt 
											ELSE 
												tbl506.[t_widt_m] 
											END, 
									tbl506.[t_npcs_m] = 
										CASE WHEN T.[sum] < tbl506.[t_npcs] 
											THEN 
												tbl506.[t_npcs] - T.[sum] 
											ELSE tbl506.[t_npcs_m] 
										END 
									FROM 
										[dbo].[PCR506] AS tbl506			
									JOIN (SELECT * FROM @TempTable) AS T 
										ON T.[t_seqn] = tbl506.[t_seqn] 
									WHERE 
										tbl506.[t_cprj] = @Project AND tbl506.[t_cspa] = @Element 
										AND tbl506.[t_sern] = @T_Sern 
								SET @UpdateRecordCount += 1 										 
							END	-- Update Ends 	'
					END	--IsValid Ends 
					 	SET @RowIndexInSource += 1 
				END --While Ends			     
		  
	     SET @Index += 1
	  END
	  COMMIT TRAN						
	  SELECT (@InsertRecordCount + @UpdateRecordCount) AS EffectedRowsForElement
	END TRY 
	BEGIN CATCH 
		IF @@TRANCOUNT > 0 
			BEGIN		 
				ROLLBACK TRANSACTION 
				DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
				SELECT  
				  @ErrorMessage = ERROR_MESSAGE(),
				  @ErrorSeverity = ERROR_SEVERITY(),  
				  @ErrorState = ERROR_STATE() 
				PRINT @ErrorMessage 
				RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
			END
	 END CATCH 
END 

/*
USE [IEMQS]
GO
DECLARE @Result INT
DECLARE @Project NVARCHAR(100)
DECLARE @Element NVARCHAR(100)
DECLARE @T_Loc NVARCHAR(100)
DECLARE @T_Init NVARCHAR(100)
SET @Project ='0017008B'
SET @Element ='122963'
SET @T_init ='ER'


EXECUTE @Result = [dbo].[SP_PCR_GENERATE_PCR_ACTIONS_FETCH_DATA]
   @Project
  ,@Element
  ,@T_Init
  --SELECT @Result
GO



*/