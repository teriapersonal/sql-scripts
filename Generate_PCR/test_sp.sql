USE [Test4]
GO



GO

BEGIN

--select ViewCol1,ViewCol2, count(0) from Table_5 group by ViewCol1,ViewCol2
    DECLARE @Iterator INT = 1
	DECLARE @Iterator2 INT = 1
    DECLARE @Iterator3 INT = 1
	DECLARE @Total INT = 50
	DECLARE @Total2 INT = 50
	DECLARE @Total3 INT = 100
	DECLARE @ViewCol1 INT
	DECLARE @ViewCol2 INT
	WHILE(@Iterator <= @Total)
	BEGIN
		SET @ViewCol1 = (@Iterator * 9) + 2
		SET @Iterator2 = @Iterator + 50
		SET @Total2 = @Iterator2 + 100
		WHILE(@Iterator2 <= @Total2)
		 BEGIN
		   SET @ViewCol2 = (@Iterator2 * 6) - 8
		   SET @Iterator3 = 1
		   SET @Total3 = 100
		   WHILE(@Iterator3 < @Total3)
			   BEGIN
			   INSERT INTO [dbo].[Table_5]
				   ([ViewCol1]
				   ,[ViewCol2]
				   ,[NormalCol1]
				   ,[NormalCol2])
					 VALUES
				   (@ViewCol1
				   ,@ViewCol2
				   ,CONCAT('abc', (@Iterator * @Iterator2) + (700) + (@Iterator2 * @Iterator3)) 
				   ,CONCAT('deffc', (@Iterator * @Iterator2 * @Iterator3) ))
			   SET @Iterator3 += 1
			   END
				   
	          SET @Iterator2 += 1
		 END
		 SET @Iterator += 1
	END
END

select * from Table_5 where ViewCol1 = 11 and ViewCol2 =298 order by ViewCol1,ViewCol2