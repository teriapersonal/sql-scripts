
begin
declare @query nvarchar(max)
declare @rowindex int
declare @total int
set @rowindex =1
declare @tbl table(Id int identity(1,1), columnName nvarchar(100),data_type nvarchar(200), collation_name nvarchar(1000), is_nullable bit,is_identity bit, max_length int)
declare @columnName nvarchar(100)
declare @data_type nvarchar(200)
declare @collation_name nvarchar(1000)
declare @is_nullable bit
declare @is_identity bit
declare @max_length int
declare @tablename nvarchar(100) = 'tltsfc505175'
declare @equivalent nvarchar(100) ='PCR505'
declare @colquery nvarchar(max)
insert into @tbl(columnName,data_type, collation_name,is_nullable,is_identity,max_length)



select c.name, tt.name as DataType, c.collation_name,c.is_nullable,c.is_identity,c.max_length from [HZPDSQL2K12\IN02].[lnappqa3db].sys.tables t 
JOIN [HZPDSQL2K12\IN02].[lnappqa3db].sys.schemas s ON t.schema_id = s.schema_id
JOIN [HZPDSQL2K12\IN02].[lnappqa3db].sys.columns c ON t.object_id = c.object_id
Join [HZPDSQL2K12\IN02].[lnappqa3db].sys.types tt on tt.user_type_id = c.user_type_id
where t.name = @tablename

set @query ='create table ' + @equivalent + '(Id INT IDENTITY(1,1) PRIMARY KEY,'
select @total = count(0) from @tbl
while(@rowindex <= @total)
begin
select @columnName = t.columnName,
@data_type = t.data_type, 
@collation_name  = t.collation_name,
@is_nullable = t.is_nullable,
@is_identity = t.is_identity,
@max_length = t.max_length from @tbl t where t.Id = @rowindex

set @colquery = @columnName
if(@data_type ='nvarchar')
  begin
	set @colquery = concat(@colquery + ' ' + @data_type + '(',@max_length)
	set @colquery = concat(@colquery,')')	
  end
  else
  begin
    set @colquery = concat(@colquery + ' ', @data_type)
  end
  if(@data_type ='nvarchar' OR @data_type = 'varchar')
  begin
    set @colquery = concat(@colquery, ' COLLATE ' + @collation_name) 
  end
  if(@is_nullable = 0)
  begin
    set @colquery += ' ' + 'NULL'
  end
  else
  begin
    set @colquery += ' ' + ' NOT NULL'
  end
  
  set @query = @query + ' ' + @colquery
  if(@rowindex = @total)
  begin
   set @query += ' )'
  end
  else
  begin
   set @query += ' ,'
  end
set @rowindex += 1

end
print(@query)
print(@total)
end



