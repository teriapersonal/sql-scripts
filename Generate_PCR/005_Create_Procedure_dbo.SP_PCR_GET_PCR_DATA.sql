USE [IEMQS]
GO
IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_GET_PCR_DATA]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_GET_PCR_DATA]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [dbo].[SP_PCR_GET_PCR_DATA] @Project = '50553A'
ALTER PROCEDURE  [dbo].[SP_PCR_GET_PCR_DATA]
    @StartIndex INT = 1,
    @EndIndex INT = 10,
    @SortingFields VARCHAR(50) = '', 
	@Project NVARCHAR(100),
	@PCR504Id INT = 0	
AS
BEGIN
	DECLARE @FilterQuery NVARCHAR(MAX)  = ' P.[t_cprj] = ''' + @Project + ''
	DECLARE @DeclareQuery NVARCHAR(MAX) = ''	
	
	IF LEN(ISNULL(@SortingFields, '')) > 0
	BEGIN
		SET @SortingFields = SUBSTRING(LTRIM(RTRIM(@SortingFields)), 9, LEN(LTRIM(RTRIM(@SortingFields)))-8)
	END
	SET @SortingFields = ' ORDER BY P.[t_cprj], P.[t_cspa], P.[t_sern] '
	DECLARE @LNLinkedServer NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNLinkedServer');
	DECLARE @LNCompanyId NVARCHAR(MAX) = [dbo].[FN_COMMON_GET_CONFIG_VALUE]('LNCompanyId');	
	DECLARE @SQlQuery1 NVARCHAR(MAX) = ''
	DECLARE @SQlQuery2 NVARCHAR(MAX) = ''
	DECLARE @SQlQuery3 NVARCHAR(MAX) = ''
	DECLARE @Spacer NVARCHAR(100) 
    DECLARE @SegmentLength INT = 9
    DECLARE @i INT =0
    WHILE(@i < @SegmentLength)
    BEGIN
      SELECT @Spacer = CASE WHEN @i = 0 THEN ' ' ELSE   @Spacer + ' ' END 
      SET @i += 1
    END

	DECLARE @ConditionalJoin NVARCHAR(MAX) = ''
	 IF (@PCR504Id <> 0)
		BEGIN
		  SET @ConditionalJoin = @ConditionalJoin + ' INNER JOIN [dbo].[PCR503] AS P3 ON P.[t_pcrn] = P3.[t_pcrn] AND  P.[t_pono] = P3.[t_pono]'
		  SET @ConditionalJoin = @ConditionalJoin + ' INNER JOIN [dbo].[PCR504] AS P4 ON P4.[t_pcln] = P3.[t_pcln] AND P4.[t_revn] = P3.[t_revn] '
		  
		END
		ELSE
		BEGIN
		  SET @ConditionalJoin= ''
	END;

	DECLARE @ConditionalWhere NVARCHAR(MAX) = ''
	IF (@PCR504Id <> 0)
		BEGIN
		  SET @ConditionalWhere = @ConditionalWhere + ' AND P4.[Id] = '
		  SET @ConditionalWhere = CONCAT(@ConditionalWhere, @PCR504Id)
		END
		ELSE
		BEGIN
		  SET @ConditionalWhere= ''
	END;

	SET @DeclareQuery = '
	DECLARE @tblLocationAddress AS TABLE 
	(
		PCRN NVARCHAR(100), 
		LocationAddress NVARCHAR(100)
	)
	DECLARE @tblManufacturingItemAndFindNo TABLE
	(
		[t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_sern] INT,
		[t_pitm] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_dsca] NVARCHAR(200) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_fnno] INT
	)	
	DECLARE @tblChildItemAndGradeAndThickness TABLE
	(
		[t_cspa] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_sern] INT, 
		[t_item] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_dsca] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dscb] NVARCHAR(500) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_desc] NVARCHAR(1000) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_thic] INT,
		[t_mtrl] NVARCHAR(100) COLLATE Latin1_General_100_CS_AS_KS_WS
	)	
	DECLARE @tblWCLocation TABLE
	(
		[t_cadr] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS,
		[t_cwoc] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[t_dsca] NVARCHAR(MAX) COLLATE Latin1_General_100_CS_AS_KS_WS, 
		[WCDeliverName] NVARCHAR(MAX)
	)
	DECLARE @Spacer NVARCHAR(100) = ''' + @Spacer + '''
    DECLARE @SegmentLength INT = ' 
	SET @DeclareQuery = CONCAT(@DeclareQuery, @SegmentLength)
	SET @DeclareQuery = @DeclareQuery + ' ' +
	'INSERT INTO @tblManufacturingItemAndFindNo
	(
		[t_cspa], 
		[t_sern], 
		[t_pitm], 
		[t_dsca], 
		[t_fnno]
	)
	SELECT 
		B.[t_cspa],
		B.[t_sern], 
		A.[t_item], 
		A.[t_dsca], 
		tbl911.[t_fnno]
	FROM ' + @LNLinkedServer  + '.dbo.tltibd911' + @LNCompanyId + ' AS tbl911 
		INNER JOIN ' + @LNLinkedServer  + '.dbo.ttpptc120' + @LNCompanyId + ' AS B WITH (NOLOCK) 
			ON 	B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern]
		INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
	        ON A.[t_item] = tbl911.[t_pitm] 
	     WHERE B.[t_cprj] = ''' + @Project + '''
	
	INSERT INTO @tblChildItemAndGradeAndThickness
	(
		[t_cspa], 
		[t_sern], 
		[t_item], 
		[t_dsca], 
		[t_dscb], 
		[t_desc], 
		[t_thic],
		[t_mtrl]
	)
	SELECT 
	    DISTINCT
		B.[t_cspa],
		B.[t_sern], 
		A.[t_item], 
		A.[t_dsca], 
		A.[t_dscb], 
		'''', 
		tbl903.[t_thic],
		T.[t_mtrl]
	FROM ' + @LNLinkedServer   + '.dbo.tltibd911' + @LnCompanyId + ' AS tbl911 
	    INNER JOIN ' +	@LNLinkedServer + '.dbo.ttpptc120' + @LNCompanyId  + ' AS B WITH (NOLOCK) 
		  ON B.[t_cprj] = tbl911.[t_cprj] AND B.[t_cspa] = tbl911.[t_cspa] AND B.[t_sern] = tbl911.[t_sern] 
		INNER JOIN ' + @LNLinkedServer + '.dbo.ttcibd001' + @LNCompanyId + ' AS A WITH (NOLOCK) 
	       ON A.[t_item]= tbl911.[t_citm] 
	    INNER JOIN (SELECT distinct [t_mtrl] FROM   ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ') AS T ON T.[t_mtrl] = A.[t_dscb]
        INNER JOIN ' + @LNLinkedServer + '.dbo.tltibd903' + @LNCompanyId + ' AS tbl903 WITH (NOLOCK) 
		   ON tbl903.[t_item] = tbl911.[t_citm] 
	    WHERE B.[t_cprj] = ''' + @Project + '''
	
	UPDATE T
	SET T.[t_desc] = A.[t_desc]
	FROM @tblChildItemAndGradeAndThickness AS T
	JOIN ' + @LNLinkedServer + '.dbo.tltibd901' + @LNCompanyId + ' AS A
	ON A.[t_mtrl] COLLATE  Latin1_General_100_CS_AS_KS_WS = T.[t_mtrl]
	
	INSERT INTO @tblWCLocation([t_cadr], [t_cwoc], [t_dsca], [WCDeliverName])
		EXEC [dbo].[SP_PCR_Get_WorkCenter_With_Location] 1 '

	
	SET @SqlQuery1 = '
		
				  SELECT 
								P.[Id],
								P.[t_cprj] AS Project,
								P.[t_cspa] AS Element,
								P.[t_mitm] AS ManufacturingItem,
								ManufacturingItemTable.[t_dsca] AS ManufacturingItemDesc,
								P.[t_sern] AS PartNumber,								
								P.[t_seqn] AS [Sequence],
								ManufacturingItemTable.[t_fnno] AS FindNo,
								@Spacer AS [ChildItemSegment],
								CASE WHEN SUBSTRING(P.[t_sitm], 1, @SegmentLength ) = @Spacer THEN SUBSTRING(P.[t_sitm], @SegmentLength + 1, LEN(P.[t_sitm]) - @SegmentLength) ELSE P.[t_sitm] END AS [ChildItem], 
								ChildItemTable.[t_dsca] AS ChildItemDesc,
								P.[t_leng] AS [Length],
								P.[t_widt] AS Width,
								P.[t_leng_m] AS LengthModified,
								P.[t_widt_m] AS WidthModified,
								P.[t_npcs] AS NoOfPieces,
								P.[t_npcs_m] AS RequiredQuantity,
								ChildItemTable.[t_dscb] AS ItemMaterialGrade,
								ISNULL(P.[t_pcrn],'''') PCRNumber,								
								P.[t_pono] AS PCRLineNo,
								P.[t_pcrt] AS PCRType,
								P.[t_prdt] AS PCRRequirementDate,
								P.[t_cwoc] AS WCDeliver,
								P.[t_prmk] AS PlannerRemark, ' ;
	SET @SqlQuery2 = 
								'
								P.[t_prio] AS [Priority],
								P.[t_loca] AS [Location],
								P.[t_cloc] AS CuttingLocation,
								P.[t_sloc] AS SubLocation,
								P.[t_revi] AS ItemRevision,								
								ChildItemTable.[t_thic] AS Thickness,								
								PCRT.[PCRTypeName],																								
								ChildItemTable.[t_desc] AS ItemMaterialGradeName,								
								C2.[LocationName],
								WCLOCA.[WCDeliverName]								
								FROM [dbo].[PCR506] AS P
								LEFT JOIN @tblManufacturingItemAndFindNo AS ManufacturingItemTable ON ManufacturingItemTable.[t_cspa] = P.[t_cspa] ';
	SET @SQLQuery3 = 
								' AND ManufacturingItemTable.[t_sern] = P.[t_sern] AND ManufacturingItemTable.[t_pitm] = P.[t_mitm]
								LEFT JOIN @tblChildItemAndGradeAndThickness AS ChildItemTable ON ChildItemTable.[t_cspa] = P.[t_cspa] 
								AND ChildItemTable.[t_sern] = P.[t_sern] AND ChildItemTable.[t_item] = P.[t_sitm]
								LEFT JOIN (SELECT [t_dimx], [t_dimx] + ''-'' + [t_desc] AS LocationName FROM [dbo].[COM002] WITH (NOLOCK) WHERE [t_dtyp] = 1) 
								AS C2 ON P.[t_loca] = C2.[t_dimx] COLLATE Latin1_General_100_CS_AS_KS_WS
								LEFT JOIN @tblWCLocation AS WCLOCA ON P.[t_cwoc] = WCLOCA.[t_cwoc]
								LEFT JOIN (Select [Value], [Text] AS PCRTypeName FROM dbo.FN_GET_PCR_ENUM_DESCRIPTIONS(''PCR Type'',''ltsfc.pcr.type'')) AS PCRT 
								ON PCRT.[Value] = P.[t_pcrt] ' + @ConditionalJoin + ' 
  								WHERE ' + @FilterQuery  + '''' + @ConditionalWhere 
                        
						
	--SELECT CAST('<root><![CDATA[' + @DeclareQuery + @SQlQuery1 + @SQlQuery2  + @SQlQuery3 + @SortingFields + ']]></root>' AS XML)
    EXEC(@DeclareQuery+ @SqlQuery1 + @SqlQuery2 + @SqlQuery3)
END