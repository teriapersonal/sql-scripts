USE [IEMQS]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zapimon, Nishant Teria
-- Create date: Dec 12th, 2020
-- Description:	
-- =============================================

IF NOT EXISTS (
	SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PCR_Insert_Update_PCR_Data]') AND type in (N'P', N'PC'))
BEGIN 
	EXEC ('
	CREATE PROCEDURE [dbo].[SP_PCR_Insert_Update_PCR_Data]  
	AS  
	BEGIN  
	 SET NOCOUNT ON;
	END
	')
END

GO
ALTER PROCEDURE [dbo].[SP_PCR_Insert_Update_PCR_Data]
	@UserId NVARCHAR(20),
	@Mode INT,
	@Data XML,
	@IsDuplicate BIT  = 0
AS
BEGIN 
BEGIN TRAN
  BEGIN TRY
	 SET NOCOUNT ON;	
	 DECLARE @Id INT
	 DECLARE @TempTableName NVARCHAR(100) 
	 DECLARE @TBL AS TABLE
	 (
		 [Id] INT, 
		 [LengthModified] FLOAT, 
		 [WidthModified] FLOAT, 
		 [RequiredQuantity] FLOAT, 
		 [Location] NVARCHAR(100),
		 [CuttingLocation] NVARCHAR(100),
		 [SubLocation] NVARCHAR(100),
		 [WCDeliver] NVARCHAR(100), 
		 [PCRRequirementDate] DATETIME,
		 [PCRType] NVARCHAR(100),
		 [PlannerRemark] NVARCHAR(100),
		 [Priority] INT
	 )
	    
   IF(@IsDuplicate = 0)
     BEGIN
	   INSERT INTO @TBL
	   (
		[Id], 
		[LengthModified], 
		[WidthModified], 
		[RequiredQuantity], 
		[Location], 
		[CuttingLocation], 
		[SubLocation], 
		[WCDeliver], 
		[PCRRequirementDate],
		[PCRType],
		[PlannerRemark],
		[Priority]
	   )
	   SELECT 
		Array.Entity.query('Id').value('.','INT') AS [Id], 
		Array.Entity.query('LengthModified').value('.', 'FLOAT') AS [LengthModified], 
		Array.Entity.query('WidthModified').value('.', 'FLOAT') AS [WidthModified],  
		Array.Entity.query('RequiredQuantity').value('.', 'FLOAT') AS [RequiredQuantity],	
		Array.Entity.query('Location').value('.', 'NVARCHAR(100)') AS [Location],	
		Array.Entity.query('CuttingLocation').value('.', 'NVARCHAR(100)') AS [CuttingLocation],
		Array.Entity.query('SubLocation').value('.', 'NVARCHAR(100)') AS [SubLocation],
		Array.Entity.query('WCDeliver').value('.', 'NVARCHAR(100)') AS [WCDeliver],
		Array.Entity.query('PCRRequirementDate').value('.', 'DATETIME') AS [PCRRequirementDate],
		Array.Entity.query('PCRType').value('.', 'NVARCHAR(100)') AS [PCRType],
		Array.Entity.query('PlannerRemark').value('.', 'NVARCHAR(100)') AS [PlannerRemark],
		Array.Entity.query('Priority').value('.','INT') AS [Priority]
	  FROM @Data.nodes('/ArrayOfSP_PCR_GET_PCR_DATA_Result/SP_PCR_GET_PCR_DATA_Result') AS Array(Entity)
   END
   ELSE
     BEGIN
	  DECLARE @PartNumber INT, @Project NVARCHAR(100), @Element NVARCHAR(100)
	  DECLARE @MaxSeq INT
	   INSERT INTO @TBL
	   (
		[Id]
	   )
	   SELECT 
		  Array.Entity.query('Id').value('.','INT') AS [Id] 
	   FROM @Data.nodes('/ArrayOfSP_PCR_GET_PCR_DATA_Result/SP_PCR_GET_PCR_DATA_Result') AS Array(Entity)

		SELECT TOP 1 @Id = [Id] FROM @TBL
		SELECT TOP 1 @Project = P.[t_cprj] , @Element = P.[t_cspa] , @PartNumber = P.[t_sern] FROM [dbo].[PCR506] AS P WHERE P.[Id] = @Id
		SELECT TOP 1 @MaxSeq = P.[t_seqn]  FROM [dbo].[PCR506] AS P WHERE P.[t_cprj] = @Project AND P.[t_cspa] = @Element AND P.[t_sern] = @PartNumber
		ORDER BY P.[t_seqn] DESC
		SET @TempTableName = CONCAT('#TempPCR506_', @Id)
		DECLARE @Sql NVARCHAR(MAX) = '
			IF OBJECT_ID(N''tempdb..' + @TempTableName + ''') IS NOT NULL
				BEGIN
					DROP TABLE ' + @TempTableName +
			 '  END'
		
			--PRINT(@sql1)
			EXEC(@sql)
			SET @Sql =  CONCAT(' SELECT * INTO ' + @TempTableName + ' FROM [dbo].[PCR506] AS P WHERE P.[Id] = ',  @Id)
			SET @Sql += 
			' UPDATE ' + @TempTableName + ' 
			  SET 
			   [t_pcrn] = NULL, 
			   [t_seqn] = '
			   SET @Sql = CONCAT(@Sql, @MaxSeq + 1)
			   SET @Sql = CONCAT(@Sql, ',
			     [CreatedOn] = GETUTCDATE(), 
			     [t_rtby] = ''' + @UserId + ''', 
			     [t_rtdt] = NULL ' )
               SET @Sql += 
			    ' ALTER TABLE ' + @TempTableName + ' DROP COLUMN [Id];
		          INSERT INTO [dbo].[PCR506] 
			      SELECT * FROM ' + @TempTableName
	 END	

    IF(@Mode = 1 AND @IsDuplicate = 0) --UPDATE
	 BEGIN
	  UPDATE tbl506
		SET 
		  tbl506.[t_leng_m] = B.LengthModified,
		  tbl506.[t_widt_m] = B.WidthModified,
		  tbl506.[t_npcs_m] = B.RequiredQuantity,
		  tbl506.[t_loca] = B.[Location],
		  tbl506.[t_cloc] = B.CuttingLocation,
		  tbl506.[t_sloc] = B.SubLocation,
		  tbl506.[t_cwoc] = B.WCDeliver,
		  tbl506.[t_prio] = B.[Priority],
		  tbl506.[t_prdt] = B.PCRRequirementDate,
		  tbl506.[t_rtby] = @UserId,
		  tbl506.[t_rtdt] = GETUTCDATE(),
		  tbl506.[t_pcrt] = B.PCRType,
		  tbl506.[t_prmk] = B.PlannerRemark
		FROM [dbo].[PCR506] AS tbl506
		INNER JOIN @TBL AS B ON B.Id = tbl506.Id
		
	 END	
	  ELSE IF(@IsDuplicate = 1)
	   BEGIN
	     EXEC(@Sql)
	   END 
	   ELSE IF(@Mode = 0)
	    BEGIN
		  PRINT 1 --TODO
	    END

	  COMMIT
	SELECT 1
  END TRY
  BEGIN CATCH 
	IF @@TRANCOUNT > 0 
		BEGIN		 
			ROLLBACK TRANSACTION 
			DECLARE @ErrorMessage NVARCHAR(MAX), @ErrorSeverity INT, @ErrorState INT 
			SELECT  
				@ErrorMessage = ERROR_MESSAGE() ,
				@ErrorSeverity = ERROR_SEVERITY(),  
				@ErrorState = ERROR_STATE() 
			PRINT @ErrorMessage 
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
		END		
 END CATCH 	
END 


