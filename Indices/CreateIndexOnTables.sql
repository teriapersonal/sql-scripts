USE [IEMQS]  
DECLARE @IndexName NVARCHAR(100)
DECLARE @Query NVARCHAR(MAX)
DECLARE @SchemaName NVARCHAR(100) = '[dbo]';
DECLARE @Prefix NVARCHAR(100) = 'PCR'
DECLARE @TableName NVARCHAR(100)
DECLARE @IsUnique BIT
DECLARE @TableNames AS TABLE(RowIndex INT IDENTITY(1,1), TableId INT, TableName NVARCHAR(100))
DECLARE @IndexTables AS TABLE(RowIndex INT IDENTITY(1,1), IndexId INT, IndexName NVARCHAR(100),IsUnique BIT, TableId INT, ColumnNames NVARCHAR(MAX))
DECLARE @TableCounter INT  = 1
DECLARE @TableId INT = 0
DECLARE @TotalTables INT
DECLARE @IndexId INT = 0
DECLARE @IndexCounter INT = 1
DECLARE @ColumnNames NVARCHAR(MAX)
DECLARE @Q NVARCHAR(MAX) = ''
DECLARE @TotalIndices INT
SET NOCOUNT ON
INSERT INTO @TableNames(TableName, TableId)
VALUES
('506', 1),
('503', 2),
('501', 3),
('504', 4),
('505', 5)
/*,('500')*/


SELECT @TotalTables = COUNT(0) FROM @TableNames
WHILE(@TableCounter <= @TotalTables)
 BEGIN
   SELECT 
     @TableName = T.TableName,
	 @TableId = T.TableId
	FROM @TableNames AS T WHERE T.RowIndex = @TableCounter
   IF(@TableName = '506')
    BEGIN
	   SET @IndexName = 'IX_' + @Prefix + @TableName + '_'
		  INSERT INTO 
		  @IndexTables
		   (
		    IndexId, IndexName, IsUnique, TableId, ColumnNames
		   )
		  VALUES
		   (1, @IndexName + 'cprj', 0,  @TableId, '[t_cprj] ASC'),
		   (2, @IndexName + 'cprj_cspa', 0,  @TableId, '[t_cprj] ASC, [t_cspa] ASC'),
		   (3, @IndexName + 'cprj_cspa_sern_seq', 1,  @TableId, '[t_cprj] ASC, [t_cspa] ASC, [t_sern] ASC, [t_seqn] ASC'),
		   (4, @IndexName + 'cprj_cspa_seq_sern', 1,  @TableId, '[t_cprj] ASC, [t_cspa] ASC, [t_seqn] ASC, [t_sern] ASC')

    END
	IF(@TableName = '503')
    BEGIN
	   SET @IndexName = 'IX_' + @Prefix + @TableName + '_'
		  INSERT INTO 
		  @IndexTables
		   (
		    IndexId, IndexName, IsUnique, TableId, ColumnNames
		   )
		  VALUES
		   (1, @IndexName + 'pcrn_pono_revn', 1,  @TableId, '[t_pcrn] ASC, [t_pono] ASC, [t_revn] ASC'),
		   (2, @IndexName + 'pcrn_revn_cprj_cspa_mitm_sitm_prtn', 1,  @TableId, '[t_pcrn] ASC, [t_revn] ASC, [t_cprj] ASC, [t_cspa] ASC, [t_mitm] ASC, [t_sitm] ASC, [t_prtn] ASC'),
		   (3, @IndexName + 'pcln', 0,  @TableId, '[t_pcln] ASC')
		   

    END

	IF(@TableName = '501')
    BEGIN
	   SET @IndexName = 'IX_' + @Prefix + @TableName + '_'
		  INSERT INTO 
		  @IndexTables
		   (
		    IndexId, IndexName, IsUnique, TableId, ColumnNames
		   )
		  VALUES
		   (1, @IndexName + 'pcrn', 1,  @TableId, '[t_pcrn] ASC')
		   

    END

	IF(@TableName = '504')
    BEGIN
	   SET @IndexName = 'IX_' + @Prefix + @TableName + '_'
		  INSERT INTO 
		  @IndexTables
		   (
		    IndexId, IndexName, IsUnique, TableId, ColumnNames
		   )
		  VALUES
		   (1, @IndexName + 'pcln_revn', 1,  @TableId, '[t_pcln] ASC, [t_revn] ASC')
		   

    END

	IF(@TableName = '505')
    BEGIN
	   SET @IndexName = 'IX_' + @Prefix + @TableName + '_'
		  INSERT INTO 
		  @IndexTables
		   (
		    IndexId, IndexName, IsUnique, TableId, ColumnNames
		   )
		  VALUES
		   (1, @IndexName + 'pcln_revn_seqn', 1,  @TableId, '[t_pcln] ASC, [t_revn] ASC, [t_seqn] ASC')
		   

    END


	
   SET @TableCounter += 1
 END

DECLARE @TableQuery NVARCHAR(MAX)
SET @TableCounter = 1
SELECT @TotalTables = COUNT(0) FROM @TableNames

WHILE(@TableCounter <= @TotalTables)
 BEGIN
    SELECT 
	  @TableName = T.TableName,
	  @TableId = T.TableId 
	 FROM @TableNames AS T WHERE T.RowIndex = @TableCounter
        
    DECLARE @IndexTable AS TABLE(RowIndex INT IDENTITY(1,1), IndexId INT, IndexName NVARCHAR(100), IsUnique BIT, TableId INT, ColumnNames NVARCHAR(MAX))
    DELETE FROM @IndexTable
	INSERT INTO @IndexTable(IsUnique, TableId, IndexId, IndexName, ColumnNames)
	SELECT 
	 T.IsUnique,
	 T.TableId, 
	 T.IndexId,
	 T.IndexName, 
	 T.ColumnNames 
	FROM @IndexTables AS T WHERE T.TableId = @TableId
	
	SET @IndexCounter = 1
	SELECT @TotalIndices = COUNT(0) FROM @IndexTable 

	SET @TableQuery = ''
	WHILE(@IndexCounter <= @TotalIndices)
	 BEGIN
	   SELECT 
	     @IndexId = T.IndexId,
	     @IndexName = T.IndexName, 
		 @IsUnique = T.IsUnique,
		 @ColumnNames = T.ColumnNames 
		 FROM @IndexTable AS T WHERE T.IndexId = @IndexCounter

	   SET @TableQuery += 
	    ' '  + 
	   'IF NOT EXISTS (SELECT [name] FROM sys.indexes WHERE [name] = ''' + @IndexName + ''')   
	     BEGIN
	       CREATE ' + CASE WHEN @IsUnique = 1 THEN 'UNIQUE' ELSE '' END + '  NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @SchemaName + '.[' + @Prefix + @TableName + ']' +
           '(' + @ColumnNames + ')' +
				'WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, 
				 DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		 END '
	    
	   SET @IndexCounter += 1
	 END
	 PRINT(@TableQuery)
	 PRINT('ENDING FOR TABLE ')
   SET @TableCounter += 1
 END

